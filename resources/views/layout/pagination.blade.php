<div style="float:right">
    <form action="" method="get" class="forms-sample"> 
        {{csrf_field()}}
        <div class="form-group row">
        <div class="col-sm-9">
            <div class="input-group">
                <select name="search" id="search" class="form-control" required>
                    <option value="">Filtrar por idioma</option>
                    <option value="pt_br">Português</option>
                    <option value="en_us">Inglês</option>
                </select>
            </div>
        </div>
        <button type="submit" class="input-group-text bg-transparent">
            <i class="mdi mdi-magnify"></i>
        </button>
        </div>
    </form>
</div>