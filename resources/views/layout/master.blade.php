<!DOCTYPE html>
<html>
<head>
  <title>CMS - {{ config('app.name') }} </title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!-- CSRF Token -->
  <meta name="_token" content="{{ csrf_token() }}">
  
  <link rel="shortcut icon" href="{{ asset('/favicon.ico') }}">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

  <!-- plugin css -->
  {!! Html::style('assets/plugins/@mdi/font/css/materialdesignicons.min.css') !!}
  {!! Html::style('assets/plugins/perfect-scrollbar/perfect-scrollbar.css') !!}
  <!-- end plugin css -->

  @stack('plugin-styles')
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css" integrity="sha256-jKV9n9bkk/CTP8zbtEtnKaKf+ehRovOYeKoyfthwbC8=" crossorigin="anonymous" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css" crossorigin="anonymous" />
  
  <!-- common css -->
  {!! Html::style('css/app.css') !!}
  {!! Html::style('css/admin.css') !!}
  
  <!-- end common css -->
  @stack('style')

  
</head>



<body data-base-url="{{url('/')}}">

  <div class="container-scroller" id="app">
    @include('layout.header')
    <div class="container-fluid page-body-wrapper">
      @include('layout.sidebar')
      <div class="main-panel">
        <div class="content-wrapper">
          @yield('content')
        </div>
        @include('layout.footer')
      </div>
    </div>
  </div>




  <script type="application/javascript">        var host = "{{URL::to('/')}}";    </script>
  
  <!-- base js -->
  {!! Html::script('js/app.js') !!}
  

  {!! Html::script('assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js') !!}
  <!-- end base js -->

  <!-- plugin js -->
  @stack('plugin-scripts')
  <!-- end plugin js -->

  <!-- common js -->
  {!! Html::script('assets/js/off-canvas.js') !!}
  {!! Html::script('assets/js/hoverable-collapse.js') !!}
  {!! Html::script('assets/js/misc.js') !!}
  {!! Html::script('assets/js/settings.js') !!}
  {!! Html::script('assets/js/todolist.js') !!}
  <!-- end common js -->

  @stack('custom-scripts')
  {!! Html::script('js/admin.js') !!}
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  
  {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script> --}}
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha256-WqU1JavFxSAMcLP2WIOI+GB2zWmShMI82mTpLDcqFUg=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js" integrity="sha256-CgvH7sz3tHhkiVKh05kSUgG97YtzYNnWt6OXcmYzqHY=" crossorigin="anonymous"></script>
  
 
  
  
  
  {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> --}}
  <script src="{{ asset('node_modules/tinymce/tinymce.js') }}" type="application/javascript"></script>

  <script type="application/javascript">
    $('#modal').addClass('d-none');
    $('#image, .imagecropper').on('click',function() {
      $('#modal').removeClass('d-none');
      $('modal-backdrop').removeClass('d-none');
    });
    $('#crop').on('click',function() {
      $('#modal').addClass('d-none');
      $('.modal-backdrop').addClass('d-none');
    });
  </script>

  <script type="application/javascript">
      tinymce.init({
          selector:'textarea.description',
          editor_deselector : "mceNoEditor",
          width: 900,
          height: 300,
          setup: function (editor) {
                editor.on('init change', function () {
                    editor.save();
                });
            },
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste imagetools"
            ],
            menubar:'',
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | media",
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ],
            image_title: true,
            automatic_uploads: true,
            images_upload_url: '/uploads/content',
            images_upload_base_path: '/uploads/content',
            file_picker_types: 'image',
            images_upload_handler: function (blobInfo, success, failure) {
                var xhr, formData;
                xhr = new XMLHttpRequest();
                xhr.withCredentials = false;
                xhr.open('POST', '/admin/upload');
                var token = '{{ csrf_token() }}';
                xhr.setRequestHeader("X-CSRF-Token", token);
                xhr.onload = function() {
                    var json;
                    if (xhr.status != 200) {
                        failure('HTTP Error: ' + xhr.status);
                        return;
                    }
                    json = JSON.parse(xhr.responseText);

                    if (!json || typeof json.location != 'string') {
                        failure('Invalid JSON: ' + xhr.responseText);
                        return;
                    }
                    success(json.location);
                };
                formData = new FormData();
                formData.append('file', blobInfo.blob(), blobInfo.filename());
                xhr.send(formData);
            }
            
      });
  </script>
  <script type="application/javascript">
      $(function() {
        $( ".datepicker" ).datepicker();
      });
      $(function() {
        $( ".css-date" ).datepicker({
            dateFormat: "dd/mm/yy"
            
        });
      });
  </script>
  
  
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" ></script>

  
  <script type="application/javascript">
  $(function() {
    
    $("#sortable tbody").sortable({ 
        cursor: "move",
        update : function () { 
          var values = [];
          $("input[name='ids[]']").each(function() {
              values.push($(this).val());
          });
          var token = $(this).data("token");
          var model = $("input[name='model']").val();
          var parent_id = $("input[name='parent_id']").val();
          var language = $("input[name='language']").val();
          $.ajax({
            type: "PUT",
            url: host+'/admin/orders',
            headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
            data: {ids:values, model:model, parent_id:parent_id, language: language},
            dataType: "json",
            success: function( data ) {
                
            }
        });
        } 
    }); 
    
    $( "#sortable tbody" ).disableSelection({

    });
 
    $(".sortable tbody").sortable({ 
        cursor: "move",
        update : function () { 
          var values = [];
          $("input[name='ids_items[]']").each(function() {
              values.push($(this).val());
          });
          var token = $(this).data("token");
          var model = $("input[name='model_items']").val();
          var parent_id = $("input[name='parent_id_items']").val();
          var language = $("input[name='language_items']").val();
          $.ajax({
            type: "PUT",
            url: host+'/admin/orders',
            headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
            data: {ids:values, model:model, parent_id:parent_id, language:language},
            dataType: "json",
            success: function( data ) {
                
            }
          });
        } 
    }); 
      
    
    $( ".sortable tbody" ).disableSelection({

    });

    
  });
  </script>

<script type="application/javascript">

  var $modal = $('#modal');
  var image = document.getElementById('image');
  var nameImage = '';
  var input = '';
  var input_id = '';

  var cropper;
    
  $("body").on("change", ".imagecropper", function(e){
      var files = e.target.files;
      var done = function (url) {
        image.src = url;
        $modal.modal('show');
      };
      var reader;
      var file;
      var url;
      input = $(this).data('input');
      input_id = $(this).data('input_id');

      nameImage = $(this).val();

      
      if (files && files.length > 0) {
        file = files[0];
  
        if (URL) {
          done(URL.createObjectURL(file));
        } else if (FileReader) {
          reader = new FileReader();
          reader.onload = function (e) {
            done(reader.result);
          };
          reader.readAsDataURL(file);
        }
      }
  });
  
  $modal.on('shown.bs.modal', function () {
    
    cropper = new Cropper(image, {
      aspectRatio: 16/9,
      viewMode: 0, 
      preview: '.preview'
    });
    $(function() {
      var $image = $('#image'),
          height = $image.height() + 4;
      
      $('.preview').css({ 
        width: '100%', //width,  sets the starting size to the same as orig image  
        overflow: 'hidden',
        height:    height,
        maxWidth:  $image.width(),
        maxHeight: height
      });
        
      $image.cropper({
        preview: '.preview',
        ready: function (e) { 
          $(this).cropper('setData', { 
            height: 467,
            rotate: 0,
            scaleX: 1,
            scaleY: 1,
            width:  573,
            x:      469,
            y:      19
          });
        } 
      });
    });
  }).on('hidden.bs.modal', function () {
     cropper.destroy();
     cropper = null;
  });
  
  
  $("#crop").click(function(){
      canvas = cropper.getCroppedCanvas({
        width: 933,
        height: 525,
        });
  
      var tipo = $(this).data('media');
      var file = nameImage.split('\\');
      var nameFile = file.slice(-1);
      
      var model = 'testcropp';

      // var formData = new FormData();
        
      // formData.append("media", file);
      // formData.append("model", model);
      
      // formData.append("_token", $('meta[name="_token"]').attr('content'));
      
      canvas.toBlob(function(blob) {
          
          url = URL.createObjectURL(blob);
          var reader = new FileReader();
           reader.readAsDataURL(blob); 
           reader.onloadend = function() {
              var base64data = reader.result;	

              // formData.append("image", base64data);

              $.ajax({
                  type: "POST",
                  dataType: "json",
                  url: host+"/admin/image-cropper/upload",
                  // data: formData,
                  data: {'_token': $('meta[name="_token"]').attr('content'), 'image': base64data, 'nameimage' : nameFile, 'model': model},
                  success: function(data){
                      $modal.modal('hide');
                      console.log(data);
                      if (data.status) { 
                          $('#'+input_id).val(data.id)
                          if (data.file) { 
                              console.log(data.file);
                              $('.fl-donwload.'+ input_id).attr('src', host+'/'+data.file);
                              $('.fl-donwload.'+ input_id).removeClass('hide');
                          }    
                      } else {
                          $('.erro_'+ inputId).html('Ocorreu um erro ao fazer o upload da imagem.');
                      }
                  }
                });
           }
      });
  })

  


  
  
  </script>
</body>
</html>