<div style="float:right; width: 50%">
    <form action="" method="get" class="forms-sample"> 
        {{csrf_field()}}
        <div class="form-group row">
            <div class="col-sm-4">
                    <div class="input-group">
                        <select name="srctipo" id="srctipo" class="form-control" >
                            <option value="">Filtrar Tipo</option>
                            <option value="blog">Blog</option>
                            <option value="case">Cases</option>
                        </select>
                    </div>
            </div>
            <div class="col-sm-4">
                    <div class="input-group">
                        <select name="search" id="search" class="form-control">
                            <option value="">Filtrar por idioma</option>
                            <option value="pt_br">Português</option>
                            <option value="en_us">Inglês</option>
                        </select>
                    </div>
            </div>
            <div class="col-sm-4">
                <button type="submit" class="input-group-text bg-transparent">
                    <i class="mdi mdi-magnify"></i>
                </button>
            </div>
        </div>
    </form>
</div>