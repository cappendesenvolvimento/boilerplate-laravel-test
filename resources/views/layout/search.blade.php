


<div class="row clearfix">
    <div class="col-sm-6"></div>
    <div class="col-sm-6">
        <h4 class="card-title">Busca</h4>
        <form action="" method="get" class="forms-sample"> 
            {{csrf_field()}}
            <div class="form-group">
                <div class="input-group">
                    @php
        
                    $pos = (strpos(Request::url(), 'versao-modelo') > 0) ? true : false;
                    $pos_modelos = (strpos(Request::url(), 'modelos') > 0) ? true : false;
                    $pos_itens = (strpos(Request::url(), 'itens') > 0) ? true : false;
                    $pos_serviceBrand = (strpos(Request::url(), 'service-brand') > 0) ? true : false;
                    $pos_conceHours = (strpos(Request::url(), 'concessionaria-horarios') > 0) ? true : false;
                    $pos_conceshops = (strpos(Request::url(), 'concessionaria-lojas') > 0) ? true : false;
                    $pos_preShops = (strpos(Request::url(), 'preAgendamentoShop') > 0) ? true : false;
                    $pre_marca = (strpos(Request::url(), 'preAgendamentoModel') > 0) ? true : false;
                    
                    @endphp

                    @if ( !$pos_conceHours && !$pos_conceshops ) 
                    
                    <input type="text" name="searchtxt" id="searchtxt" class="validate form-control"
                        Placeholder="Nome" value="{{ app('request')->input('searchtxt') }}">

                    @endif    
                    @if ($pos)
                    <select name="searchModel" id="searchModel" class="form-control">
                        <option value="">Filtrar por Modelo</option>
                        @foreach($modelos as $m)
                            <option value="{{ $m->id }}" {{ (app('request')->input('searchModel') == $m->id) ? 'selected' : '' }}>{{ $m->name }}</option>
                        @endforeach
                    </select>
                    @endif
                    @if ($pos_modelos || $pos_serviceBrand || $pos_preShops)
                    <select name="searchBrand" id="searchBrand" class="form-control">
                        <option value="">Filtrar por Marca</option>
                        @foreach($marcas as $m)
                            <option value="{{ $m->id }}" {{ (app('request')->input('searchBrand') == $m->id) ? 'selected' : '' }}>{{ $m->name }}</option>
                        @endforeach
                    </select>
                    @endif
                    @if ($pos_itens)
                    <select name="searchTipo" id="searchTipo" class="form-control">
                        <option value="">Filtrar por Tipo</option>
                        @foreach($tipos as $m)
                            <option value="{{ $m->id }}" {{ (app('request')->input('searchTipo') == $m->id) ? 'selected' : '' }}>{{ $m->name }}</option>
                        @endforeach
                    </select>
                    @endif
                    @if ($pos_conceHours || $pos_conceshops)
                    <select name="searchLoja" id="searchLoja" class="form-control">
                        <option value="">Filtrar por Loja</option>
                        @foreach($shops as $m)
                            <option value="{{ $m->LojaID }}" {{ (app('request')->input('searchLoja') == $m->LojaID) ? 'selected' : '' }}>{{ $m->NomeFantasia }}</option>
                        @endforeach
                    </select>
                    @endif
                    @if ($pre_marca)
                    <select name="searchBrand" id="searchBrand" class="form-control">
                        <option value="">Filtrar por Marca</option>
                        @foreach($marcas as $m)
                            <option value="{{ $m->id }}" {{ (app('request')->input('searchBrand') == $m->id) ? 'selected' : '' }}>{{ $m->name }}</option>
                        @endforeach
                    </select>
                    @endif
                    <button type="submit" class="input-group-text bg-transparent">
                        <i class="mdi mdi-magnify"></i>
                    </button>    
                </div>
            </div>
        </form>
    </div>
</div>