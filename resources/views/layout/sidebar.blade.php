<nav class="sidebar sidebar-offcanvas dynamic-active-class-disabled" id="sidebar">
  <ul class="nav">
    <li class="nav-item nav-profile not-navigation-link">
      <div class="nav-link">
        <div class="user-wrapper">
          <div class="profile-image">
            <img src="{{ url('assets/images/faces-clipart/pic-1.png') }}" alt="profile image">
          </div>
          <div class="text-wrapper">
            <p class="profile-name">{{ Auth::user()->name }} </p>
            <div class="dropdown" data-display="static">
              <a href="#" class="nav-link d-flex user-switch-dropdown-toggler" id="UsersettingsDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                <small class="designation text-muted">{{ Auth::user()->rules[0]->name }}</small>
                <span class="status-indicator online"></span>
              </a>
              <div class="dropdown-menu" aria-labelledby="UsersettingsDropdown">
               
                <a class="dropdown-item mt-2" href="{{route('usuarios.index')}}"> Gerenciar Contas </a>
                <a class="dropdown-item" href="{{route('usuario.perfil.edit',  Auth::user()->id )}}"> Alterar Password</a>
               
                <a class="dropdown-item" href="{{ route('admin.logout') }}">Sair</a>
                            
              </div>
            </div>
          </div>
        </div>
        </button>
      </div>
    </li>

    @php

    $url = explode("/", url()->current());
    $page = @$url[4];

    $acesso = $home = $press= $settings = $produto = $agendamento = $institucional = $concessionaria = $outras = false;

    switch($page) {
        case 'usuarios' : 
        case 'rules' : $acesso = true; break;
        case 'banner' : 
        case 'home' : 
        case 'lancamentoHome' : $home = true; break;
        case 'userPress' : 
        case 'newsPressCategory' : 
        case 'ctaPress' : 
        case 'newsPress' : $press = true; break;
        case 'destinationEmail' : 
        case 'setting-pages' : 
        case 'sync' : 
        case 'links-rodape' : $settings = true; break;
        case 'colors' : 
        case 'itemTipo' : 
        case 'itens' : 
        case 'services' : 
        case 'motorizacao' : 
        case 'transmissao' : 
        case 'brand' : 
            case 'brand-banner' : 
            case 'posvenda-banner' : 
        case 'service-brand' : 
            case 'service-brand-banner' : 
        case 'modelos' : 
            case 'modelos-galerias' : 
            case 'modelos-galeria' : 
            case 'modelos-secao-destaque' : 
            case 'modelos-secao-diferencial' : 
            case 'versao-colors' : 
        case 'versao-modelo' : $produto = true; break;
        case 'preAgendamentoBrand' : 
        case 'preAgendamentoService' : 
        case 'preAgendamentoShop' : 
        case 'preAgendamentoModel' : 
        case 'preAgendamentoLead' : $agendamento = true; break;
        case 'concessionaria-lojas' : 
        case 'testdrive' : 
        case 'proposta' : 
        case 'atendimento' : 
        case 'concessionaria-horarios' : $concessionaria = true; break;
        case 'alertsite' : 
        case 'pagina' : 
        case 'newsCategory' : 
        case 'autor' : 
        case 'news' : $institucional = true; break;
        case 'canais-atendimento' : 
        case 'modelo-geral' : 
        case 'modelo-simples' : 
        case 'atendimentos' : 
        case 'pcd' : 
        case 'fabricas' : 
        case 'historia' : 
        case 'sobre' : 
        case 'ofertas' : 
        case 'seguranca-veicular' : 
        case 'import-export' : 
        case 'serv-mobilidade' : 
        case 'redes' : 
        case 'premium-service' : $outras = true; break;
        default : $template = 'home'; break;
    }


    @endphp


    <li class="nav-item {{ active_class(['admin/dashboard'], $page) }}">
      <a class="nav-link" href="{{ url('admin/dashboard') }}">
        <i class="menu-icon mdi mdi-television"></i>
        <span class="menu-title">Dashboard</span>
      </a>
    </li>

    <li class="nav-item ">
        <a data-toggle="collapse" href="#acessos-pages" aria-expanded="{{ ($acesso) ? 'true' : 'false'}}" aria-controls="acessos-pages" class="nav-link {{ ($acesso) ? '' : 'collapsed' }}">
            <i class="menu-icon mdi mdi-account-key"></i> 
            <span class="menu-title">Acessos</span> 
            <i class="menu-arrow"></i>
        </a> 
        <div id="acessos-pages" class="collapse {{ ($acesso) ? 'show in' : '' }}" >
            <ul  class="nav flex-column sub-menu">
                <li class="nav-item {{ active_class(['admin/usuarios'], $page) }}">
                    <a class="nav-link" href="{{route('usuarios.index')}}">
                        <i class="menu-icon mdi mdi-account"></i>
                        <span class="menu-title">Usuários</span>
                    </a>
                </li>
                @can('rule-view')
                    <li class="nav-item {{ active_class(['admin/rules'], $page) }}">
                        <a class="nav-link" href="{{route('rules.index')}}">
                            <i class="menu-icon mdi mdi-key"></i>
                            <span class="menu-title">Perfis</span>
                        </a>
                    </li>
                @endcan
            </ul>
        </div>
    </li>    

    {{-- HOME --}}
    @canany(['banner-view', 'home-view', 'lancamentoHome-view'])
        <li class="nav-item ">
            <a data-toggle="collapse" href="#home-pages" aria-expanded="{{ ($home) ? 'true' : 'false'}}" aria-controls="home-pages" class="nav-link {{ ($home) ? '' : 'collapsed' }}">
                <i class="menu-icon mdi mdi-home-automation"></i> 
                <span class="menu-title">Home</span> 
                <i class="menu-arrow"></i>
            </a> 
            
            <div id="home-pages" class="collapse  {{ ($home) ? 'show in' : '' }}" >
                <ul  class="nav flex-column sub-menu">
                    @can('banner-view')
                        <li class="nav-item {{ active_class(['admin/banner'], $page) }}">
                            <a class="nav-link" href="{{route('banner.index')}}">
                                <i class="menu-icon mdi mdi-image"></i>
                                <span class="menu-title">Banner</span>
                            </a>
                        </li>
                    @endcan
                    @can('home-view')
                    <li class="nav-item {{ active_class(['admin/home'], $page) }}">
                        <a class="nav-link" href="{{route('home.index')}}">
                            <i class="menu-icon mdi mdi-bank"></i>
                            <span class="menu-title">Home Intitucional</span>
                        </a>
                    </li>
                    @endcan
                    @can('lancamentoHome-view')
                    <li class="nav-item {{ active_class(['admin/lancamentoHome'], $page) }}">
                        <a class="nav-link" href="{{route('lancamento-home.index')}}">
                            <i class="menu-icon mdi mdi-new-box"></i>
                            <span class="menu-title">Lançamentos</span>
                        </a>
                    </li>
                    @endcan
                </ul>
            </div>
        </li> 
    @endcanany           

    {{-- PRESS --}}
    @canany(['userPress-view', 'newsPressCategory-view', 'ctaPress-view', 'newsPress-view'])
        <li class="nav-item ">
            <a data-toggle="collapse" href="#press-pages" aria-expanded="{{ ($press) ? 'true' : 'false'}}" aria-controls="press-pages" class="nav-link {{ ($press) ? '' : 'collapsed'}}">
                <i class="menu-icon mdi mdi-newspaper"></i> 
                <span class="menu-title">Imprensa</span> 
                <i class="menu-arrow"></i>
            </a> 
            <div id="press-pages" class="collapse {{ ($press) ? 'show in' : '' }}" >
                <ul  class="nav flex-column sub-menu">
                    @can('userPress-view')
                    <li class="nav-item {{ active_class(['admin/userPress'], $page) }}">
                        <a class="nav-link" href="{{route('userPress.index')}}">
                            <i class="menu-icon mdi mdi-account-card-details"></i>
                            <span class="menu-title">Usuário de Imprensa</span>
                        </a>
                    </li>
                    @endcan
                    @can('newsPressCategory-view')
                    <li class="nav-item {{ active_class(['admin/newsPressCategory'], $page) }}">
                        <a class="nav-link" href="{{route('newsPressCategory.index')}}">
                            <i class="menu-icon mdi mdi-tag-plus"></i>
                            <span class="menu-title">Categoria de Imprensa</span>
                        </a>
                    </li>
                    @endcan
                    @can('ctaPress-view')
                    <li class="nav-item {{ active_class(['admin/ctaPress'], $page) }}">
                        <a class="nav-link" href="{{route('cta-press.index')}}">
                            <i class="menu-icon mdi mdi-shuffle-variant"></i>
                            <span class="menu-title">Cta</span>
                        </a>
                    </li>
                    @endcan
                    @can('newsPress-view')
                    <li class="nav-item {{ active_class(['admin/newsPress'], $page) }}">
                        <a class="nav-link" href="{{route('newsPress.index')}}">
                            <i class="menu-icon mdi mdi-newspaper"></i>
                            <span class="menu-title">Notícia de Imprensa</span>
                        </a>
                    </li>
                    @endcan
                    
                </ul>
            </div>
        </li>    
    @endcanany         

    {{-- Settings --}}
    @canany(['destinationEmail-view', 'linksFooter-view', 'settingPages-edit', 'sync-view'])
        <li class="nav-item ">
            <a data-toggle="collapse" href="#settings-pages" aria-expanded="{{ ($settings) ? 'true' : 'false' }}" aria-controls="settings-pages" class="nav-link {{ ($settings) ? '' : 'collapsed'}}">
                <i class="menu-icon mdi mdi-settings"></i> 
                <span class="menu-title">Settings</span> 
                <i class="menu-arrow"></i>
            </a> 
            <div id="settings-pages" class="collapse {{ ($settings) ? 'show in' : '' }}" >
                <ul  class="nav flex-column sub-menu">
                    @can('destinationEmail-view')
                    <li class="nav-item {{ active_class(['admin/destinationEmail'], $page) }}">
                        <a class="nav-link" href="{{route('destinationEmail.index')}}">
                            <i class="menu-icon mdi mdi-email"></i>
                            <span class="menu-title">Email Settings</span>
                        </a>
                    </li>
                    @endcan
                    @can('linksFooter-view')
                    <li class="nav-item {{ active_class(['admin/links-rodape'], $page) }}">
                        <a class="nav-link" href="{{route('links-rodape.index')}}">
                            <i class="menu-icon mdi mdi-link"></i>
                            <span class="menu-title">Links Rodapé</span>
                        </a>
                    </li>
                    @endcan
                    @can('settingPages-edit')
                    <li class="nav-item {{ active_class(['admin/setting-pages'], $page) }}">
                        <a class="nav-link" href="{{route('setting-pages.edit', 1)}}">
                            <i class="menu-icon mdi mdi-settings"></i>
                            <span class="menu-title">Config. de Páginas</span>
                        </a>
                    </li>
                    @endcan
                    @can('sync-view')
                    <li class="nav-item {{ active_class(['admin/sync'], $page) }}">
                        <a class="nav-link" href="{{route('sync.index')}}">
                            <i class="menu-icon mdi mdi-sync"></i>
                            <span class="menu-title">Sincronização</span>
                        </a>
                    </li>
                    @endcan
                </ul>
            </div>
        </li>            
    @endcanany  

    {{-- Produtos/Serviços --}}
    @canany(['colors-view', 'itensTipo-view', 'itens-view', 'motor-view', 'transmissao-view', 'services-view', 'brand-view', 'servicesBrand-view', 'product-view', 'productVersion-view'])
        <li class="nav-item ">
            <a data-toggle="collapse" href="#produtos-pages" aria-expanded="{{ ($produto) ? 'true' : 'false'}}" aria-controls="produtos-pages" class="nav-link {{ ($produto) ? '' : 'collapsed'}}">
                <i class="menu-icon mdi mdi-car"></i> 
                <span class="menu-title">Produtos/Serviços</span> 
                <i class="menu-arrow"></i>
            </a> 
            <div id="produtos-pages" class="collapse {{ ($produto) ? 'show in' : ''}}" >
                <ul  class="nav flex-column sub-menu">
                    @can('colors-view')
                    <li class="nav-item {{ active_class(['admin/colors'], $page) }}">
                        <a class="nav-link" href="{{route('colors.index')}}">
                            <i class="menu-icon mdi mdi-format-color-fill"></i>
                            <span class="menu-title">Cores</span>
                        </a>
                    </li>
                    @endcan
                    @can('itensTipo-view')
                    <li class="nav-item {{ active_class(['admin/itemTipo'], $page) }}">
                        <a class="nav-link" href="{{route('itemTipo.index')}}">
                            <i class="menu-icon mdi mdi-format-list-bulleted-type"></i>
                            <span class="menu-title">Tipo de Itens</span>
                        </a>
                    </li>
                    @endcan
                    @can('itens-view')
                    <li class="nav-item {{ active_class(['admin/itens'], $page) }}">
                        <a class="nav-link" href="{{route('itens.index')}}">
                            <i class="menu-icon mdi mdi-car-battery"></i>
                            <span class="menu-title">Itens</span>
                        </a>
                    </li>
                    @endcan
                   {{-- 
                    <li class="nav-item {{ active_class(['admin/productCategory']) }}">
                        <a class="nav-link" href="{{route('productCategory.index')}}">
                            <i class="menu-icon mdi mdi-account"></i>
                            <span class="menu-title">Categoria de Modelos</span>
                        </a>
                    </li> --}}
                
                    
                    @can('motor-view')
                    <li class="nav-item {{ active_class(['admin/motorizacao'], $page) }}">
                        <a class="nav-link" href="{{route('motorizacao.index')}}">
                            <i class="menu-icon mdi mdi-settings-box"></i>
                            <span class="menu-title">Motorização</span>
                        </a>
                    </li>
                    @endcan
                    @can('transmissao-view')
                    <li class="nav-item {{ active_class(['admin/transmissao'], $page) }}">
                        <a class="nav-link" href="{{route('transmissao.index')}}">
                            <i class="menu-icon mdi mdi-settings-box"></i>
                            <span class="menu-title">Transmissão</span>
                        </a>
                    </li>
                    @endcan

                    @can('services-view')
                    <li class="nav-item {{ active_class(['admin/services'], $page) }}">
                        <a class="nav-link" href="{{route('services.index')}}">
                            <i class="menu-icon mdi mdi-oil"></i>
                            <span class="menu-title">Serviços</span>
                        </a>
                    </li>
                    @endcan

                    @can('brand-view')
                    <li class="nav-item {{ active_class(['admin/brand'], $page) }}">
                        <a class="nav-link" href="{{route('brand.index')}}">
                            <i class="menu-icon mdi mdi-octagram"></i>
                            <span class="menu-title">Marcas</span>
                        </a>
                    </li>
                    @endcan

                    @can('servicesBrand-view')
                    <li class="nav-item {{ active_class(['admin/service-brand'], $page) }}">
                        <a class="nav-link" href="{{route('service-brand.index')}}">
                            <i class="menu-icon mdi mdi-oil"></i>
                            <span class="menu-title">Serviços/ Marcas</span>
                        </a>
                    </li>
                    @endcan

                    @can('product-view')
                    <li class="nav-item {{ active_class(['admin/modelos'], $page) }}">
                        <a class="nav-link" href="{{route('modelos.index')}}">
                            <i class="menu-icon mdi mdi-car-side"></i>
                            <span class="menu-title">Modelos</span>
                        </a>
                    </li>
                    @endcan

                    @can('productVersion-view')
                    <li class="nav-item {{ active_class(['admin/versao-modelo'], $page) }}">
                        <a class="nav-link" href="{{route('versao-modelo.index')}}">
                            <i class="menu-icon mdi mdi-car-sports"></i>
                            <span class="menu-title">Versões dos Modelos</span>
                        </a>
                    </li>
                    @endcan
                
                </ul>
            </div>
        </li>     
    @endcanany         

    {{-- Concessionarias --}}
    @canany(['dealerHours-view', 'dealerShop-view', 'testDriveLead-view', 'propostaLead-view'])
        <li class="nav-item ">
            <a data-toggle="collapse" href="#concessionaria-pages" aria-expanded="{{ ($concessionaria) ? 'true' : 'false'}}" aria-controls="concessionaria-pages" class="nav-link {{ ($concessionaria) ? '' : 'collapsed'}}">
                <i class="menu-icon mdi mdi-store"></i> 
                <span class="menu-title">Concessionárias</span> 
                <i class="menu-arrow"></i>
            </a> 
            <div id="concessionaria-pages" class="collapse {{ ($concessionaria) ? 'show in' : '' }}" >
                <ul  class="nav flex-column sub-menu">
                    @can('dealerHours-view')
                    <li class="nav-item {{ active_class(['admin/concessionaria-horarios'], $page) }}">
                        <a class="nav-link" href="{{route('concessionaria-horarios.index')}}">
                            <i class="menu-icon mdi mdi-clock"></i>
                            <span class="menu-title">Horários</span>
                        </a>
                    </li>
                    @endcan

                    @can('dealerShop-view')
                    <li class="nav-item {{ active_class(['admin/concessionaria-lojas'], $page) }}">
                        <a class="nav-link" href="{{route('concessionaria-lojas.index')}}">
                            <i class="menu-icon mdi mdi-store"></i>
                            <span class="menu-title">Lojas</span>
                        </a>
                    </li>
                    @endcan

                    @can('testDriveLead-view')                    
                    <li class="nav-item {{ active_class(['admin/testdrive'], $page) }}">
                        <a class="nav-link" href="{{route('testdrive.index')}}">
                            <i class="menu-icon mdi mdi-cards-outline"></i>
                            <span class="menu-title">Test Drive Leads</span>
                        </a>
                    </li>
                    @endcan

                    @can('propostaLead-view')
                    <li class="nav-item {{ active_class(['admin/proposta'], $page) }}">
                        <a class="nav-link" href="{{route('proposta.index')}}">
                            <i class="menu-icon mdi mdi-cards-outline"></i>
                            <span class="menu-title">Proposta Leads</span>
                        </a>
                    </li>
                    @endcan

                    @can('atendimentoLead-view')
                    <li class="nav-item {{ active_class(['admin/atendimento'], $page) }}">
                        <a class="nav-link" href="{{route('atendimento.index')}}">
                            <i class="menu-icon mdi mdi-cards-outline"></i>
                            <span class="menu-title">Atendimento Leads</span>
                        </a>
                    </li>
                    @endcan

                </ul>
            </div>
        </li>            
    @endcanany  

    {{-- Pre Agendamento --}}
    @canany(['preAgendBrand-view', 'preAgendService-view', 'preAgendShop-view', 'preAgendModel-view', 'preAgendLead-view'])
        <li class="nav-item ">
            <a data-toggle="collapse" href="#preagendamento-pages" aria-expanded="{{ ($agendamento) ? 'true' : 'false'}}" aria-controls="preagendamento-pages" class="nav-link {{ ($agendamento) ? '' : 'collapsed'}}">
                <i class="menu-icon mdi mdi-calendar-check"></i> 
                <span class="menu-title">Pré Agendamento</span> 
                <i class="menu-arrow"></i>
            </a> 
            <div id="preagendamento-pages" class="collapse {{ ($agendamento) ? 'show in' : '' }}" >
                <ul  class="nav flex-column sub-menu">
                    @can('preAgendBrand-view')
                    <li class="nav-item {{ active_class(['admin/preAgendamentoBrand'], $page) }}">
                        <a class="nav-link" href="{{route('preAgendamentoBrand.index')}}">
                            <i class="menu-icon mdi mdi-octagram"></i>
                            <span class="menu-title">Marcas</span>
                        </a>
                    </li>
                    @endcan

                    @can('preAgendService-view')
                    <li class="nav-item {{ active_class(['admin/preAgendamentoService'], $page) }}">
                        <a class="nav-link" href="{{route('preAgendamentoService.index')}}">
                            <i class="menu-icon mdi mdi-oil"></i>
                            <span class="menu-title">Serviços</span>
                        </a>
                    </li>
                    @endcan

                    @can('preAgendShop-view')
                    <li class="nav-item {{ active_class(['admin/preAgendamentoShop'], $page) }}">
                        <a class="nav-link" href="{{route('preAgendamentoShop.index')}}">
                            <i class="menu-icon mdi mdi-store"></i>
                            <span class="menu-title">Lojas</span>
                        </a>
                    </li>
                    @endcan

                    @can('preAgendModel-view')
                    <li class="nav-item {{ active_class(['admin/preAgendamentoModel'], $page) }}">
                        <a class="nav-link" href="{{route('preAgendamentoModel.index')}}">
                            <i class="menu-icon mdi mdi-car-side"></i>
                            <span class="menu-title">Modelos</span>
                        </a>
                    </li>
                    @endcan

                    @can('preAgendLead-view')
                    <li class="nav-item {{ active_class(['admin/preAgendamentoLead'], $page) }}">
                        <a class="nav-link" href="{{route('preAgendamentoLead.index')}}">
                            <i class="menu-icon mdi mdi-playlist-check"></i>
                            <span class="menu-title">Leads</span>
                        </a>
                    </li>
                    @endcan

                </ul>
            </div>
        </li>  
    @endcanany            

    {{-- Institucional --}}
    @canany(['alertsite-view', 'paginas-view', 'autor-view', 'newsCategory-view', 'newsSite-view'])
        <li class="nav-item ">
            <a data-toggle="collapse" href="#institucional-pages" aria-expanded="{{ ($institucional) ? 'true' : 'false' }}" aria-controls="institucional-pages" class="nav-link {{ ($institucional) ? '' : 'collapsed' }}">
                <i class="menu-icon mdi mdi-bank"></i> 
                <span class="menu-title">Institucional</span> 
                <i class="menu-arrow"></i>
            </a> 
            <div id="institucional-pages" class="collapse {{ ($institucional) ? 'show in' : '' }}" >
                <ul  class="nav flex-column sub-menu">
                    @can('alertsite-view')
                    <li class="nav-item {{ active_class(['admin/alertsite'], $page) }}">
                        <a class="nav-link" href="{{route('alertsite.index')}}">
                            <i class="menu-icon mdi mdi-alert"></i>
                            <span class="menu-title">Alerts Site</span>
                        </a>
                    </li>
                    @endcan

                    @can('paginas-view')
                    <li class="nav-item {{ active_class(['admin/pagina'], $page) }}">
                        <a class="nav-link" href="{{route('pagina.index')}}">
                            <i class="menu-icon mdi mdi-page-layout-body"></i>
                            <span class="menu-title">Páginas</span>
                        </a>
                    </li>
                    @endcan

                    @can('autor-view')
                    <li class="nav-item {{ active_class(['admin/autor'], $page) }}">
                        <a class="nav-link" href="{{route('autor.index')}}">
                            <i class="menu-icon mdi mdi-account-box-outline"></i>
                            <span class="menu-title">Autor</span>
                        </a>
                    </li>
                    @endcan

                    @can('newsCategory-view')
                    <li class="nav-item {{ active_class(['admin/newsCategory'], $page) }}">
                        <a class="nav-link" href="{{route('newsCategory.index')}}">
                            <i class="menu-icon mdi  mdi-tag-plus"></i>
                            <span class="menu-title">Categoria de Notícias</span>
                        </a>
                    </li>
                    @endcan

                    @can('newsSite-view')
                    <li class="nav-item {{ active_class(['admin/news'], $page) }}">
                        <a class="nav-link" href="{{route('news.index')}}">
                            <i class="menu-icon mdi mdi-newspaper"></i>
                            <span class="menu-title">Notícias</span>
                        </a>
                    </li>
                    @endcan

                    
                    
                    
                </ul>
            </div>
        </li> 
    @endcanany    
    
    {{-- Outras Páginas --}}
    @canany(['canais-edit', 'pcd-edit', 'fabricas-view', 'historia-edit', 'sobre-edit', 'premium-view', 'segVeicular-edit', 'redes', 'serv-mobilidade', 'import-export', 'simples-view', 'generals-view', 'landing-view'])
        <li class="nav-item ">
            <a data-toggle="collapse" href="#outras-pages" aria-expanded="{{ ($outras) ? 'true' : 'false' }}" aria-controls="outras-pages" class="nav-link {{ ($outras) ? '' : 'collapsed' }}">
                <i class="menu-icon mdi mdi-bank"></i> 
                <span class="menu-title">Outras Páginas</span> 
                <i class="menu-arrow"></i>
            </a> 
            <div id="outras-pages" class="collapse {{ ($outras) ? 'show in' : '' }}" >
                <ul  class="nav flex-column sub-menu">
                    @can('canais-edit')
                    <li class="nav-item {{ active_class(['admin/canais'], $page) }}">
                        <a class="nav-link" href="{{route('canais.edit', 1)}}">
                            <i class="menu-icon mdimdi-cellphone-link"></i>
                            <span class="menu-title">Canais de Atendimento</span>
                        </a>
                    </li>
                    @endcan

                    @can('pcd-edit')
                    <li class="nav-item {{ active_class(['admin/pcd'], $page) }}">
                        <a class="nav-link" href="{{route('pcd.edit', 1)}}">
                            <i class="menu-icon mdi mdi-wheelchair-accessibility"></i>
                            <span class="menu-title">PCD</span>
                        </a>
                    </li>
                    @endcan
                    
                    @can('fabricas-view')
                    <li class="nav-item {{ active_class(['admin/fabricas'], $page) }}">
                        <a class="nav-link" href="{{route('fabricas.index')}}">
                            <i class="menu-icon mdi mdi-factory"></i>
                            <span class="menu-title">Fábricas</span>
                        </a>
                    </li>
                    @endcan
                    
                    @can('historia-edit')
                    <li class="nav-item {{ active_class(['admin/historia'], $page) }}">
                        <a class="nav-link" href="{{route('historia.edit', 1)}}">
                            <i class="menu-icon mdi mdi-history"></i>
                            <span class="menu-title">História</span>
                        </a>
                    </li>
                    @endcan
                    
                    @can('sobre-edit')
                    <li class="nav-item {{ active_class(['admin/sobre'], $page) }}">
                        <a class="nav-link" href="{{route('sobre.edit', 1)}}">
                            <i class="menu-icon mdi mdi-information"></i>
                            <span class="menu-title">Sobre</span>
                        </a>
                    </li>
                    @endcan
                    
                    @can('premium-view')
                    <li class="nav-item {{ active_class(['admin/premium-service'], $page) }}">
                        <a class="nav-link" href="{{route('premium-service.index')}}">
                            <i class="menu-icon mdi mdi-star"></i>
                            <span class="menu-title">Premium Service</span>
                        </a>
                    </li>
                    @endcan
                    
                    @can('segVeicular-edit')
                    <li class="nav-item {{ active_class(['admin/seguranca_veicular'], $page) }}">
                        <a class="nav-link" href="{{route('seguranca-veicular.edit', 1)}}">
                            <i class="menu-icon mdi mdi-car"></i>
                            <span class="menu-title">Segurança Veicular</span>
                        </a>
                    </li>
                    @endcan
                    
                    @can('redes-edit')
                    <li class="nav-item {{ active_class(['admin/redes'], $page) }}">
                        <a class="nav-link" href="{{route('redes.index')}}">
                            <i class="menu-icon mdi mdi-car"></i>
                            <span class="menu-title">Redes</span>
                        </a>
                    </li>
                    @endcan
                    
                    @can('serv-mobilidade-edit')
                    <li class="nav-item {{ active_class(['admin/serv-mobilidade'], $page) }}">
                        <a class="nav-link" href="{{route('serv-mobilidade.index')}}">
                            <i class="menu-icon mdi mdi-car"></i>
                            <span class="menu-title">Serviço e Mobilidade</span>
                        </a>
                    </li>
                    @endcan
                    
                    @can('import-export-edit')
                    <li class="nav-item {{ active_class(['admin/import-export'], $page) }}">
                        <a class="nav-link" href="{{route('import-export.index')}}">
                            <i class="menu-icon mdi mdi-car"></i>
                            <span class="menu-title">Importação e Exportação</span>
                        </a>
                    </li>
                    @endcan
                    
                    @can('simples-view')
                    <li class="nav-item {{ active_class(['admin/modelo-simples'], $page) }}">
                        <a class="nav-link" href="{{route('modelo-simples.index')}}">
                            <i class="menu-icon mdi mdi-page-layout-header"></i>
                            <span class="menu-title">Simples (página/Marca)</span>
                        </a>
                    </li>
                    @endcan
                    
                    @can('generals-view')
                    <li class="nav-item {{ active_class(['admin/modelo-geral'], $page) }}">
                        <a class="nav-link" href="{{route('modelo-geral.index')}}">
                            <i class="menu-icon mdi mdi-page-layout-body"></i>
                            <span class="menu-title">Geral (página)</span>
                        </a>
                    </li>
                    @endcan
                    
                    @can('landing-view')
                    <li class="nav-item {{ active_class(['admin/landing'], $page) }}">
                        <a class="nav-link" href="{{route('landing.index')}}">
                            <i class="menu-icon mdi mdi-google-pages"></i>
                            <span class="menu-title">Landing</span>
                        </a>
                    </li>
                    @endcan
                    
                </ul>
            </div>
        </li> 
    @endcanany               

    
  </ul>
</nav>