@extends('layout.master-mini')
@section('content')

    <div class="content-wrapper d-flex align-items-center justify-content-center auth theme-one"
        style="background-image: url({{ url('assets/images/auth/login_1.jpg') }}); background-size: cover;">
        <div class="row w-100">
            <div class="col-lg-4 mx-auto">
                <div class="auto-form-wrapper">
                    <form method="POST" action="{{ route('admin.password-update') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">
                        <input type="hidden" name="email" placeholder="Email" value="{{ $email }}">    

                        <div class="form-group">
                            <label class="label">Redefinir Senha</label>
                            <div class="input-group">
                                <input id="password" type="password" placeholder="Senha" class="form-control @error('password') is-invalid @enderror" name="password" required>
                                
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="mdi mdi-check-circle-outline"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <input id="password_confirmation" type="password" placeholder="Confirmar Senha" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" required >
                                
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="mdi mdi-check-circle-outline"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary submit-btn btn-block">Enviar</button>
                        </div>
                        <div class="form-group">
                          @if (session('status'))
                              <div class="row clearfix">
                                  <div class="col-sm-12">
                                      <div class="alert alert-success">
                                          {{ session('status') }}
                                      </div>
                                  </div>
                              </div>
                          @endif 
                          @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                          @endif
                        </div>
                        <div class="form-group d-flex justify-content-between">
                            <div class="form-check form-check-flat mt-0">
                                <label class="form-check-label">
                                    <!-- <input type="checkbox" class="form-check-input" checked> Keep me signed in </label> -->
                            </div>
                            
                        </div>

                    </form>
                </div>

                <p class="footer-text text-center">copyright © 2020 Cappen.com All rights reserved.</p>
            </div>
        </div>
    </div>

@endsection
