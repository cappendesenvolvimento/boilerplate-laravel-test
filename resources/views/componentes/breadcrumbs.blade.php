<div class="light-font">
  <nav aria-label="breadcrumb">
        @if(isset($caminhos))
            <ol class="breadcrumb primary-color">
            @foreach($caminhos as $caminho)
                @if($caminho['url'])
                    <li class="breadcrumb-item"><a href="{{ $caminho['url'] }}">{{$caminho['titulo']}}</a></li>
                @else
                    <span class="breadcrumb-item active"  aria-current="page"> {{$caminho['titulo']}}</span>    
                @endif
            @endforeach
            </ol>
        @else   
            <span></span>    
        @endif
    </nav>
</div>