<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Database Connections
	|--------------------------------------------------------------------------
	|
	| Here are each of the database connections setup for your application.
	| Of course, examples of configuring each database platform that is
	| supported by Laravel is shown below to make development simple.
	|
	|
	| All database work in Laravel is done through the PHP PDO facilities
	| so make sure you have the driver for your particular database of
	| choice installed on your machine before you begin development.
	|
	*/

	'connections' => array(

	    //local
//        'mysql' => array(
//            'driver'    => 'mysql',
//            'host'      => '127.0.0.1',
//            'database'  => 'z-caoa-institucional',
//            'username'  => 'homestead',
//            'password'  => 'secret',
//            'charset'   => 'utf8',
//            'collation' => 'utf8_unicode_ci',
//            'prefix'    => '',
//        ),

	    // prod
		'mysql' => array(
			'driver'    => 'mysql',
			'host'      => 'mysql8.webnow.com.br',
			'database'  => 'caoa_inst',
			'username'  => 'caoa',
			'password'  => 'YpR8J4p8',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		),

		'subaru-lp' => array(
			'driver' => 'mysql',
			'host' => 'subaru-mysql.csi9albk36io.us-east-1.rds.amazonaws.com',
			'database' => 'subaru-lp',
			'username' => 'admin',
			'password' => 'TfH8Adv6nMfRSfXH',
			'charset' => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix' => '',
		),

	),

);
