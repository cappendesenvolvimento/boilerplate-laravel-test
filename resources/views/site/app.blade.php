<!DOCTYPE html>

<?php $page = 'home'; ?>
<html class="no-js" lang="pt-BR" xmlns:og="http://ogp.me/ns#">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>@yield('meta_title', 'Caoa')</title>

    <meta name="author" content="Caoa">
    <meta name="title" content="@yield('meta_title', 'Caoa')">
    <meta name="description" content="@yield('meta_description', '')">

    <!-- CSRF Token -->
    <meta name="_token" content="{{ csrf_token() }}">

    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:title" content="@yield('meta_title', '')">
    <meta property="og:description" content="@yield('meta_description', '')">
    <meta property="og:image" content="@yield('meta_img', asset('assets/img/content/share.jpg'))">

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/img/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/img/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/img/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/img/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/img/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/imgapple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/imgapple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/imgapple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('assets/img/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/img/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/img/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/img/favicon-16x16.png') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('assets/img/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.typekit.net/gpd2hso.css">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
    <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@400&display=swap" rel="stylesheet">

    <script>
        document.documentElement.className = "js";
        var supportsCssVars = function() {
            var e, t = document.createElement("style");
            return t.innerHTML = "root: { --tmp-var: bold; }", document.head.appendChild(t), e = !!(window.CSS &&
                    window.CSS.supports && window.CSS.supports("font-weight", "var(--tmp-var)")), t.parentNode
                .removeChild(t), e
        };
        supportsCssVars() || alert("Please view this demo in a modern browser that supports CSS Variables.");

    </script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PJZHXZ5');</script>
    <!-- End Google Tag Manager -->
</head>

@php

$url = explode('/', url()->current());
$arrLocales = ['pt', 'en'];
$currentUrl = @$url[3];

if (in_array(@$url[3], $arrLocales)) {
    $currentUrl = @$url[4];
}

$page = @$currentUrl != '' ? $currentUrl : 'home';
$loading = $page == 'home' ? 'loading' : '';
$alertSite = isset($alertSite) ? 'alerta-comunicado' : '';
@endphp

<body class="{{ $loading }} {{ $alertSite }}">
<!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PJZHXZ5"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    
    @if ($page == 'home')
        @include ('site.partials.blocos.loader')
    @endif

    <header id="mainHeader">
        @if (parseLocale() == '/')
            <ul id="cta-box">
                <li class="nav-item ml-0 hidden-fixed"><a href="{{ isset($conversao) ? route('site.proposta') : '#' }}"
                        {{ isset($conversao) ? '' : (@$currentUrl == 'proposta' ? '' : 'data-toggle=modal data-target=#modalCadastroProposta') }}
                        class="proposta"><span class="line">{{ __('messages.links_floating_proposta') }}<span
                                class="emoji"><svg xmlns="http://www.w3.org/2000/svg" width="25" height="25"
                                    viewBox="0 0 25 25">
                                    <g fill="none" fill-rule="evenodd">
                                        <g fill="#FFF" fill-rule="nonzero">
                                            <path
                                                d="M205.217 289c1.314 0 2.394 1.055 2.434 2.36l.001.075v14.913c0 1.314-1.055 2.393-2.36 2.433l-.075.002H200.5l-1.004 3.59c-.03.123-.092.244-.153.336-.26.405-.714.673-1.204.698l-.074.002-.076-.002c-.405-.021-.802-.208-1.063-.517l-.048-.06-3.895-4.047h-7.548c-1.314 0-2.394-1.055-2.434-2.36l-.001-.075v-14.913c0-1.314 1.055-2.394 2.36-2.434l.075-.001h19.782zm0 2.435h-19.782v14.913h8.095c.311 0 .596.105.83.315l.053.05 3.226 3.348.791-2.83c.118-.499.574-.856 1.096-.882l.061-.001h5.63v-14.913zm-8.613 9.617v2.435h-8.582v-2.435h8.582zm5.722-3.348v2.435h-14.304v-2.435h14.304zm0-3.652v2.435h-14.304v-2.435h14.304z"
                                                transform="translate(-183.000000, -289.000000)" />
                                        </g>
                                    </g>
                                </svg></span></span></a></li>
                <li class="nav-item hidden-fixed"><a href="#"
                        {{ @$currentUrl == 'testdrive' ? '' : 'data-toggle=modal data-target=#modalCadastroTestDrive' }}
                        class="testdrive"><span class="line">{{ __('messages.links_floating_testdrive') }}<span
                                class="emoji"><svg xmlns="http://www.w3.org/2000/svg" width="22" height="25"
                                    viewBox="0 0 22 25">
                                    <g fill="none" fill-rule="evenodd">
                                        <g fill="#FFF" fill-rule="nonzero">
                                            <path
                                                d="M379.435 310.165h16.83c1.34 0 2.435-1.095 2.435-2.435v-17.013c0-1.339-1.096-2.434-2.435-2.434h-2.617v-1.066c0-.67-.548-1.217-1.218-1.217-.67 0-1.217.548-1.217 1.217v1.066h-6.726v-1.066c0-.67-.548-1.217-1.217-1.217-.67 0-1.218.548-1.218 1.217v1.066h-2.617c-1.34 0-2.435 1.095-2.435 2.434V307.7c0 1.37 1.096 2.465 2.435 2.465zm0-2.435v-10.287h16.83V307.7h-16.83v.03zm2.617-17.013v1.674c0 .67.548 1.218 1.218 1.218.67 0 1.217-.548 1.217-1.218v-1.674h6.756v1.674c0 .67.548 1.218 1.218 1.218.67 0 1.217-.548 1.217-1.218v-1.674h2.618v4.292h-16.861v-4.292h2.617z"
                                                transform="translate(-377.000000, -286.000000)" />
                                        </g>
                                    </g>
                                </svg></span></span></a></li>
                <li class="nav-item hidden-fixed"><a href="{{ route('dynamic', ['param1' => 'alerta-de-fraudes']) }}" class="alertafraudes"><span class="line">Alerta de Fraudes<span class="emoji">
                <svg xmlns="http://www.w3.org/2000/svg" width="26" height="22" viewBox="0 0 26 22">
                    <g fill="none" fill-rule="evenodd">
                        <g>
                            <g>
                                <g stroke="#FFF" stroke-width="2">
                                    <path d="M12.93 2.085L24.248 21H1.752L12.93 2.085z" transform="translate(-442.000000, -287.000000) translate(442.000000, 287.000000)"/>
                                </g>
                                <path fill="#FFF" d="M12 10H14V15H12zM12 16H14V18H12z" transform="translate(-442.000000, -287.000000) translate(442.000000, 287.000000)"/>
                            </g>
                        </g>
                    </g>
                </svg>
                </span></span></a></li>
            </ul>
        @endif
        <ul id="helper-nav" class="nav justify-content-between align-items-center">
            <div class="menu-toggle">
                <div id="nav-icon1">
                    <div class="flex-lines-nav">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <p class="txt-menu">Menu</p>
                </div>
            </div>
            <div class="d-flex icons-auto">
                @if (parseLocale() == '/')
                    <li class="nav-item ml-0 hidden-fixed"><a
                            href="{{ isset($conversao) ? route('site.proposta') : '#' }}"
                            {{ isset($conversao) ? '' : (@$currentUrl == 'proposta' ? '' : 'data-toggle=modal data-target=#modalCadastroProposta') }}
                            class="proposta d-none d-lg-block"><span class="emoji"><svg
                                    xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
                                    <g fill="none" fill-rule="evenodd">
                                        <g fill="#FFF" fill-rule="nonzero">
                                            <path
                                                d="M205.217 289c1.314 0 2.394 1.055 2.434 2.36l.001.075v14.913c0 1.314-1.055 2.393-2.36 2.433l-.075.002H200.5l-1.004 3.59c-.03.123-.092.244-.153.336-.26.405-.714.673-1.204.698l-.074.002-.076-.002c-.405-.021-.802-.208-1.063-.517l-.048-.06-3.895-4.047h-7.548c-1.314 0-2.394-1.055-2.434-2.36l-.001-.075v-14.913c0-1.314 1.055-2.394 2.36-2.434l.075-.001h19.782zm0 2.435h-19.782v14.913h8.095c.311 0 .596.105.83.315l.053.05 3.226 3.348.791-2.83c.118-.499.574-.856 1.096-.882l.061-.001h5.63v-14.913zm-8.613 9.617v2.435h-8.582v-2.435h8.582zm5.722-3.348v2.435h-14.304v-2.435h14.304zm0-3.652v2.435h-14.304v-2.435h14.304z"
                                                transform="translate(-183.000000, -289.000000)" />
                                        </g>
                                    </g>
                                </svg></span> {{ __('messages.links_floating_proposta') }}</a></li>
                    <li class="nav-item hidden-fixed"><a href="#"
                            {{ @$currentUrl == 'testdrive' ? '' : 'data-toggle=modal data-target=#modalCadastroTestDrive' }}
                            class="testdrive d-none d-lg-block"><span class="emoji"><svg
                                    xmlns="http://www.w3.org/2000/svg" width="22" height="25" viewBox="0 0 22 25">
                                    <g fill="none" fill-rule="evenodd">
                                        <g fill="#FFF" fill-rule="nonzero">
                                            <path
                                                d="M379.435 310.165h16.83c1.34 0 2.435-1.095 2.435-2.435v-17.013c0-1.339-1.096-2.434-2.435-2.434h-2.617v-1.066c0-.67-.548-1.217-1.218-1.217-.67 0-1.217.548-1.217 1.217v1.066h-6.726v-1.066c0-.67-.548-1.217-1.217-1.217-.67 0-1.218.548-1.218 1.217v1.066h-2.617c-1.34 0-2.435 1.095-2.435 2.434V307.7c0 1.37 1.096 2.465 2.435 2.465zm0-2.435v-10.287h16.83V307.7h-16.83v.03zm2.617-17.013v1.674c0 .67.548 1.218 1.218 1.218.67 0 1.217-.548 1.217-1.218v-1.674h6.756v1.674c0 .67.548 1.218 1.218 1.218.67 0 1.217-.548 1.217-1.218v-1.674h2.618v4.292h-16.861v-4.292h2.617z"
                                                transform="translate(-377.000000, -286.000000)" />
                                        </g>
                                    </g>
                                </svg></span> {{ __('messages.links_floating_testdrive') }}</a></li>
                    <li class="nav-item hidden-fixed"><a href="{{ route('dynamic', ['param1' => 'alerta-de-fraudes']) }}" class="alertafraudes d-none d-lg-block"><span class="emoji">
                <svg xmlns="http://www.w3.org/2000/svg" width="26" height="22" viewBox="0 0 26 22">
                    <g fill="none" fill-rule="evenodd">
                        <g>
                            <g>
                                <g stroke="#FFF" stroke-width="2">
                                    <path d="M12.93 2.085L24.248 21H1.752L12.93 2.085z" transform="translate(-442.000000, -287.000000) translate(442.000000, 287.000000)"/>
                                </g>
                                <path fill="#FFF" d="M12 10H14V15H12zM12 16H14V18H12z" transform="translate(-442.000000, -287.000000) translate(442.000000, 287.000000)"/>
                            </g>
                        </g>
                    </g>
                </svg>

                </span> Alerta de Fraudes</a></li>
                @endif
            </div>
            <li class="nav-item mx-auto logo">
                <a href="{{ url('/') }}">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="106"
                        height="30" viewBox="0 0 106 30">
                        <defs>
                            <filter id="g7m3odw47a">
                                <feColorMatrix in="SourceGraphic"
                                    values="0 0 0 0 1.000000 0 0 0 0 1.000000 0 0 0 0 1.000000 0 0 0 1.000000 0" />
                            </filter>
                            <path id="f4m9lnbelb" d="M0.008 0.04L19.743 0.04 19.743 29.867 0.008 29.867z" />
                        </defs>
                        <g fill="none" fill-rule="evenodd">
                            <g>
                                <g>
                                    <g filter="url(#g7m3odw47a)" transform="translate(-847 -40) translate(847 40)">
                                        <mask id="pqbprsucjc" fill="#fff">
                                            <use xlink:href="#f4m9lnbelb" />
                                        </mask>
                                        <path fill="#0C052E"
                                            d="M13.195 5.007c2.082 0 4.102.188 6.02.502L19.744.93C17.836.416 15.164.04 12.103.04 2.409.04 0 4.568 0 14.953c0 10.373 2.396 14.914 12.104 14.914 3.048 0 5.732-.364 7.639-.878l-.54-4.59c-1.881.313-3.95.5-6.008.5-7.049 0-7.15-4.176-7.15-9.946 0-5.744.126-9.946 7.15-9.946"
                                            mask="url(#pqbprsucjc)" />
                                    </g>
                                    <path fill="#FFF"
                                        d="M41.385 4.116C40.445 1.332 38.8.04 36.619.04c-2.245 0-3.826 1.217-4.804 4.076l-8.692 25.45h6.12L36.368 5.96l3.388 11.013H45.7L41.385 4.116M64.094 24.975c-5.544 0-5.67-4.064-5.67-10.022S58.55 4.92 64.095 4.92c5.532 0 5.657 4.076 5.657 10.034 0 5.958-.125 10.022-5.657 10.022zm0-24.935c-9.094 0-11.69 4.327-11.69 14.913 0 10.587 2.596 14.914 11.69 14.914 9.081 0 11.677-4.327 11.677-14.914C75.771 4.367 73.175.04 64.094.04zM91.72.04c-2.245 0-3.826 1.217-4.804 4.076l-8.692 25.45h6.12L91.47 5.96l3.388 11.013h5.944L96.486 4.116C95.546 1.332 93.902.04 91.72.04z"
                                        transform="translate(-847 -40) translate(847 40)" />
                                    <path fill="#66C18C"
                                        d="M41.693 23.269L43.63 29.566 49.927 29.566 47.813 23.269 41.693 23.269M96.794 23.269L98.731 29.566 105.028 29.566 102.915 23.269 96.794 23.269"
                                        transform="translate(-847 -40) translate(847 40)" />
                                </g>
                            </g>
                        </g>
                    </svg>
                </a>
            </li>
            <div class="d-flex">
                @if (parseLocale() == '/')
                    <li class="nav-item mr-0 txt-bottom"><a href="{{ route('site.concessionarias') }}"
                            class="localizador d-none d-lg-block">{{ __('messages.links_floating_concessionaria') }}</a>
                    </li>
                @endif
                <li class="nav-item dropdown lng hidden-fixed d-none d-lg-block">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="{{ url('/') }}" role="button"
                        aria-haspopup="true" aria-expanded="false">
                        @switch(parseLocale())
                            @case('pt')
                                PT
                            @break
                            @case('en')
                                EN
                            @break
                            @default
                                PT
                        @endswitch
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ url('/') }}">PT</a>
                        <a class="dropdown-item" href="{{ url('/en') }}">EN</a>
                    </div>
                </li>
                @if (parseLocale() == '/')
                <li class="nav-item mr-0"><a href="#" class="buscar">Buscar</a></li>
                @endif
            </div>
        </ul>

        {{-- Menu --}}
        @include('site.partials.shared.menu-nav')

    </header>
    <div class="c-busca-fixed">
        <span class="close-form">
            <img src="{{ asset('assets/img/layout/close.svg') }}" alt="">
        </span>
        <form action="{{ route('site.busca') }}" method="POST">
            {{ csrf_field() }}
            <div class="input-iconBtn">
                <input type="text" name="search_term" placeholder="Digite a sua Busca">
                <button type="submit" class="iconBtn">
                    <img src="{{ asset('assets/img/layout/search.svg') }}" alt="">
                </button>
            </div>
        </form>
    </div>
    <div class="form-message"><strong>Erro no Cadastro:</strong> Por favor, verifique se todos os campos foram
        preenchidos corretamente.</div>

    @yield('body')

   

    @php
        $paramProposta = ['contact_title' => 'Entre em contato com nosso atendimento especializado e solicite uma proposta', 'hide_product' => false];
        $paramTestDrive = ['contact_title' => 'AGENDE AGORA O SEU TEST DRIVE!', 'hide_product' => false];
        if ($pageBrand) {
            $paramProposta = ['contact_title' => 'Entre em contato com nosso atendimento especializado e solicite uma proposta', 'brand_id' => @$flag->BandeiraID, 'product_id' => @$catalog->LojaCatalogoID, 'hide_product' => @$hide_product];
            $paramTestDrive = ['brand_id' => @$flag->BandeiraID];
        }
        
    @endphp

    @include('site.partials.modal.cadastro-proposta' , $paramProposta)
    @include('site.partials.modal.cadastro-test-drive', $paramTestDrive)
    @include('site.partials.modal.proposta-sucesso')
    @include('site.partials.modal.test-drive-sucesso')

    {{-- Footer --}}
    @include('site.partials.shared.footer')

    <script>
        var SERVER_URL = "{{ URL::to('/') }}";
        var environment = '{{ App::environment() }}';

        function getQParam(param) {
            var query = decodeURI(window.location.search).substr(1).split('&');

            for (var p in query) {
                if (query[p].split('=')[0] == param) {
                    return query[p].split('=')[1];
                }
            }
        }

    </script>
    @yield('scripts_var')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBH4geVPNIF6tMJGemjO4tmD4rlYyNbgdM&libraries=places"></script>
    
    {{-- Apagar LInhas Abaixo --}}
        {{-- <script src="https://unpkg.com/@googlemaps/markerclustererplus/dist/index.min.js"></script>
        <script src="{{ asset('fulljs/app.bundle.js') }}"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="{{ asset('fulljs/site_forms.js') }}"></script>
        <script src="{{ asset('fulljs/jquery.geocomplete.min.js') }}"></script>
        <script src="{{ asset('fulljs/geolocator.js') }}"></script>
        <script src="{{ asset('fulljs/jquery.cookie.min.js') }}"></script>
        <script src="{{ asset('fulljs/bootstrap-datepicker.min.js') }}"></script>
        <script src="https://parsleyjs.org/dist/parsley.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js"
            integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous">
        </script>
        <script src="{{ asset('fulljs/proposta.js') }}"></script> --}}
    {{-- Apagar Até Aqui --}}
{{-- 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.js"></script>
    <script src="{{ asset('fulljs/testdrive.js') }}"></script> --}}

    {{-- Descomentar Linha de baixo --}}
        <script src="{{ asset('js/scripts.js') }}"></script>
    @yield('scripts')
    

    {{-- Descomentar bloco de baixo --}}
    {{-- <script type="text/javascript">
        (function(d){
            var b = d.body;
            var s = d.getElementsByTagName('script'), p = d.createElement('script');
            window.WidgetId = "FC_WIDGET";
            p.type = 'text/javascript';
            p.setAttribute('charset','utf-8');
            p.async = 1;
            p.id = "flychat";
            p.src = "https://caoavendas.sv4.flychats.com.br//widget/flychat_widget.js";
            b.appendChild(p);
        }(document));
    </script> --}}
</body>

</html>
