@extends('site.app')
@section('body')
<div class="l-404">
    <section class="c-banner-new">
        <picture class="c-banner-new__cover">
            <source media="(max-width: 768px)" srcset="{{ asset('assets/img/content/img-info-02.jpg') }}">
            <source media="(min-width: 769px)" srcset="{{ asset('assets/img/content/full-banner-01.jpg') }}">
            <img src="{{ asset('assets/img/content/full-banner-01.jpg') }}">
        </picture>
        <div class="c-banner-new__wrap container">
            <div class="c-banner-new__content">
                <h2 class="c-banner-new__title">404 - Esta página não está disponível</h2>
                <p>Pedimos desculpa, mas o endereço que inseriu já não se encontra disponível.</p>
                <a href="{{ route('principal') }}" class="c-link-line">Voltar para a página principal</a>
            </div>
        </div>
    </section>
</div>

@endsection
