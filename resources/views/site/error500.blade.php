@extends('site.app')
@section('body')
<div class="l-404">
    <section class="c-banner-new">
        <picture class="c-banner-new__cover">
            <source media="(max-width: 768px)" srcset="{{ asset('assets/img/content/img-info-02.jpg') }}">
            <source media="(min-width: 769px)" srcset="{{ asset('assets/img/content/full-banner-01.jpg') }}">
            <img src="{{ asset('assets/img/content/full-banner-01.jpg') }}">
        </picture>
        <div class="c-banner-new__wrap container">
            <div class="c-banner-new__content">
                <h2 class="c-banner-new__title">500 - Está temporariamente indisponível</h2>
                <p>Pedimos desculpa, ocorreu um erro inesperado.</p>
                <a href="#" class="c-link-line">Voltar para a página principal</a>
            </div>
        </div>
    </section>
</div>

@endsection
