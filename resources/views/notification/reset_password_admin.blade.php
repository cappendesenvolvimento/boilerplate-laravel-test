<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Caoa</title>
</head>
<body>
    <h2>Resetar a senha</h2>

    <div>
        Para redefinir sua senha, preencha este formulário: <a
                href="{{ URL::to('admin/password/reset/' . $token . '?email=' . $email) }}"
                target="_blank">{{ URL::to('admin/password/reset', array($token)) }}</a>.<br/>
        Este link irá expirar em {{ Config::get('auth.reminder.expire', 60) }} minutos.
    </div>
</body>
</html>