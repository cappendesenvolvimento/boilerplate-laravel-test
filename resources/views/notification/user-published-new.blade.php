<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8">
</head>
<body>
<b>Nova Notícia de {{$category}}</b>
<br>
<br>
<b>Título da notícia:</b> {{$titleNews}}
<br>
<a href="{{$link}}">Click aqui para ver a nova notícia</a>
<br>
<br>

Se você não deseja mais receber esse email <a href="{{$linkRemove}}">clique aqui </a>para se descadastrar.
</div>
</body>
</html>
