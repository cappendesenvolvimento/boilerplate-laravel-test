<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>MV</title>
</head>
<body>
    <table>
        <tr>
            <td>Nome:</td>
            <td>{{ $nome }}</td>
        </tr>
        <tr>
            <td>Email:</td>
            <td>{{ $email }}</td>
        </tr>
        <tr>
            <td>Telefone:</td>
            <td>{{ $telefone }}</td>
        </tr>
        <tr>
            <td>Empresa: </td>
            <td>{{ $empresa }}</td>
        </tr>
        <tr>
            <td>Cargo:</td>
            <td>{{ $cargo }}</td>
        </tr>
        
        <tr>
            <td>Página de Acesso:</td>
            <td>{{ $formpath }}</td>
        </tr>
        
    </table>
</body>
</html>