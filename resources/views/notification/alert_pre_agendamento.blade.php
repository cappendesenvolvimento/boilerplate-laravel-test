<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<p>Lead enviado a partir de caoa.com.br/pre-agendamento</p>

Lead<br>

    @if($name)
    Nome : {{$name}}<br>
    @endif
    @if($email)
    Email : {{$email}}<br>
    @endif
    @if($cpf_cnpj)
    CPF/CNPJ : {{$cpf_cnpj}}<br>
    @endif
    @if($phone)
    Fone : {{$phone}}<br>
    @endif
    @if($brand)
    Marca : {{$brand}}<br>
    @endif
    @if($model)
    Modelo : {{$model}}<br>
    @endif
    @if($date)
    Data : {{$date}}<br>
    @endif
    @if($time)
    Hora : {{$time}}<br>
    @endif
    @if($service)
    Service : {{$service}}<br>
    @endif
    @if($loja)
    Loja : {{$loja}}<br>
    @endif
    @if($placa_carro)
    Placa Carro : {{$placa_carro}}<br>
    @endif
    @if($carro_desejado)
    Próximo Carro : {{$carro_desejado}}<br>
    @endif

    @if($quero_conhecer)
    Locadora CAOA Rent a Car: Quero conhecer<br>
    @endif

</body>
</html>
