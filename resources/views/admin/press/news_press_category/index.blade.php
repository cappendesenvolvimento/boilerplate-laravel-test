@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
  

  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      
      <div class="card-body">

        @if (session('status'))
            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                </div>
            </div>
        @endif    
        @can('newsPressCategory-create')  
        @include('layout.search')
        <div>
          <a href="{{ route('newsPressCategory.create') }}" class="btn btn-primary btn-fw btn-add"><i class="mdi mdi-plus"></i>Adicionar</a>
        </div>
        @endcan
        <h4 class="card-title">Categorias de Imprensa</h4>
        <p class="card-description"> </p>
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th> Id </th>
                <th> Nome </th>
                <th> Criado dia </th>
                <th> Ações </th>
              </tr>
            </thead>
            <tbody>
            @forelse($objs as $obj)
              <tr>
                <td> {{ $obj->id }} </td>
                <td> {{ $obj->name }} </td>
                <td> {{ $obj->created_at }} </td>
                <td> 
                    <form action="{{route('newsPressCategory.destroy', $obj->id)}}" method="post">
                        @can('newsPressCategory-edit')  
                            <a title="Editar" class="btn btn-icons btn-inverse-warning" href="{{ route('newsPressCategory.edit', $obj->id) }}"><i class="mdi mdi-grease-pencil"></i></a>
                        @endcan  
                        @can('newsPressCategory-delete')  
                            {{method_field('DELETE')}}
                            {{csrf_field()}}
                            <button title="Deletar" class="btn btn-icons btn-inverse-danger"><i class="mdi mdi-delete"></i></button>   
                        @endcan
                    </form>     
                </td>
              </tr>
              @empty
            </tbody>
              <tr>
                <td colspan="6">Nenhum registro encontrado.</td>
              </tr>
            @endforelse
          </table>
          {{ $objs->appends(request()->input())->links() }}
        </div>
      </div>
    </div>
  </div>
  
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush