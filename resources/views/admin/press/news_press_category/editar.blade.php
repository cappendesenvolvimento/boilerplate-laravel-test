
@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
    <div class="col s12">
        @include('componentes.breadcrumbs')
    </div>
    
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
        
            <div class="card-body">
                @if (session('status'))
                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        </div>
                    </div>
                @endif

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif                
                
                <h4 class="card-title">Editar Categoria de Imprensa</h4>
                <p class="card-description"> </p>

                <div class="auto-form-wrapper">
                    <form action="{{ route('newsPressCategory.update', $obj->id)}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="row clearfix">  
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text"  name="name" id="name" class="form-control" placeholder="Nome"  value="{{ isset($obj->name) ?  $obj->name : old('name')}}" required>
                                        
                                    </div>
                                </div>
                            </div>  
                                
                        </div>
                        

                        <div class="row clearfix">  
                            <div class="col-sm-2">
                                <button class="btn btn-primary submit-btn btn-block">Atualizar</button>
                            </div>
                        </div>
                    </form>    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush