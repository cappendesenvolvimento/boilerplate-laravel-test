@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
  

  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      
      <div class="card-body">

        @if (session('status'))
            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                </div>
            </div>
        @endif    
             
        @can('userPress-create')  
        @include('layout.search') 
        <div>
          <a href="{{ route('userPress.create') }}" class="btn btn-primary btn-fw btn-add"><i class="mdi mdi-plus"></i>Adicionar</a>
        </div>
        @endcan
        <h4 class="card-title">Usuários de Imprensa</h4>
        <p class="card-description"> </p>
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th> Id </th>
                <th> Nome </th>
                <th> Email </th>
                <th> Criado dia </th>
                <th> Ativado </th>
                <th> Ações </th>
              </tr>
            </thead>
            <tbody>
            @forelse($usuarios as $usuario)
              <tr>
                <td> {{ $usuario->id }} </td>
                <td> {{ $usuario->name }} </td>
                <td> {{ $usuario->email }} </td>
                <td> {{ $usuario->created_at }} </td>
                <td> {{ $usuario->active_press }} </td>
                <td> 
                    <form action="{{route('userPress.destroy', $usuario->id)}}" method="post">
                        @can('userPress-edit')  
                            <a title="Editar" class="btn btn-icons btn-inverse-warning" href="{{ route('userPress.edit', $usuario->id) }}"><i class="mdi mdi-grease-pencil"></i></a>
                        @endcan  
                        @can('userPress-delete')  
                            {{method_field('DELETE')}}
                            {{csrf_field()}}
                            <button title="Deletar" class="btn btn-icons btn-inverse-danger"><i class="mdi mdi-delete"></i></button>   
                        @endcan
                    </form>     
                </td>
              </tr>
              @empty
            </tbody>
              <tr>
                <td colspan="6">Nenhum registro encontrado.</td>
              </tr>
            @endforelse
          </table>
          {{ $usuarios->appends(request()->input())->links() }}
        </div>
      </div>
    </div>
  </div>
  
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush