
@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
    <div class="col s12">
        @include('componentes.breadcrumbs')
    </div>

  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      
      <div class="card-body">
        
        <h4 class="card-title">Adicionar Usuários de Imprensa</h4>
        <p class="card-description"> </p>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif

        <div class="auto-form-wrapper">
        <form method="POST" action="{{ route('userPress.store') }}">
            {{ csrf_field() }}
            <div class="row clearfix">  
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text"  name="name" id="name" class="form-control" placeholder="Nome"  value="{{ old('name') }}" required>
                            
                        </div>
                    </div>
                </div>  
                <div class="col-sm-6">  
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" name="email" id="email" class="form-control" placeholder="Email"  value="{{ old('email') }}" required>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                <i class="mdi mdi-check-circle-outline"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
            <div class="row clearfix">  
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text"  name="date_birth" id="date_birth" class="form-control" placeholder="Data de Nasci."  value="{{ old('date_birth') }}" required>
                            
                        </div>
                    </div>
                </div>  
                <div class="col-sm-6">  
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" name="cpf" id="cpf" class="form-control" placeholder="CPF"  value="{{ old('cpf') }}" required>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                <i class="mdi mdi-check-circle-outline"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
            <div class="row clearfix">  
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text"  name="phone" id="phone" class="form-control" placeholder="Telefone"  value="{{ old('phone') }}" required>
                        </div>
                    </div>
                </div>  
            </div>
            
            <div class="row clearfix">  
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text"  name="company" id="company" class="form-control" placeholder="Empresa"  value="{{ old('company') }}" required>
                            
                        </div>
                    </div>
                </div>  
                <div class="col-sm-6">  
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" name="editor" id="editor" class="form-control" placeholder="Editor"  value="{{ old('editor') }}" required>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                <i class="mdi mdi-check-circle-outline"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>

            <div class="row clearfix">
                <div class="col-sm-6">
                    <div class="form-check form-check-flat">
                        <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" name="active_press" value="1" checked> Usuário ativo?  <i class="input-helper"></i></label>
                    </div>
                </div>  
                <div class="col-sm-6">
                    <div class="form-check form-check-flat">
                        <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" name="receive_news" value="1" checked> Receber notícia? <i class="input-helper"></i></label>
                    </div>
                </div>  
            </div>  
            
            
            <div class="row clearfix">  
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                <i class="mdi mdi-check-circle-outline"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="col-sm-6">  
                    <div class="form-group">
                        <div class="input-group">
                            <input type="password" name="password_confirmation" id="password_confirmation"  class="form-control" placeholder="Confirme o Password" required>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                <i class="mdi mdi-check-circle-outline"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
            <div class="row clearfix">  
                <div class="col-sm-2">
                    <button class="btn btn-primary submit-btn btn-block">Registrar</button>
                </div>
            </div>
          
        </form>
      </div>
        
    </div>
  </div>
  
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush