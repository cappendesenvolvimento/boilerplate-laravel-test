
@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')

@php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/" ;    
@endphp

<div class="row">
    <div class="col s12">
        @include('componentes.breadcrumbs')
    </div>
    
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
        
            <div class="card-body">
                @if (session('status'))
                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        </div>
                    </div>
                @endif

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif                
                
                <h4 class="card-title">Editar Cta</h4>
                <p class="card-description"> </p>

                <div class="auto-form-wrapper">

            
                    <form action="{{ route('cta-press.update', $obj->id)}}" method="post">
                        
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <input type="hidden" name="image_id" id="image_id" value="{{ isset($obj->image->id) ?  $obj->image->id : old('image_id')}}">

                        <div class="row clearfix">  
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text"  name="title" id="title" class="form-control" placeholder="Título"  value="{{ isset($obj->title) ?  $obj->title : old('title') }}" required>
                                    </div>
                                </div>
                            </div>  
                        </div>
            
                       <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <textarea name="text" placeholder="Texto" id="text" class="description" >{{ isset($obj->text) ?  $obj->text :  old('text')}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
            
                        <div class="row clearfix">  
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text"  name="link" id="link" class="form-control" placeholder="Link"  value="{{ isset($obj->link) ?  $obj->link :  old('link') }}">
                                    </div>
                                </div>
                            </div>  
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text"  name="link_text" id="link_text" class="form-control" placeholder="Texto do Link"  value="{{ isset($obj->link_text) ?  $obj->link_text :  old('link_text') }}">
                                    </div>
                                </div>
                            </div>  
                        </div>
            
                        <div class="row clearfix">  
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <select name="target" id="target" class="form-control" >
                                            <option value="">Selecione um Target</option>
                                            <option value="0" {{ ($obj->target == 0) ? 'selected' : '' }}>Abrir na mesma Aba</option>
                                            <option value="1" {{ ($obj->target == 1) ? 'selected' : '' }}>Abrir em nova Aba</option>
                                        </select>
                                    </div>
                                </div>
                            </div>  
                        </div>  
            
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <p>Imagem</p>
                                    <div class="input-group">
                                        <input type="file" name="image" id="image" data-model="colors" data-inputid="image_id" class="form-control image">
                                    </div>
                                    <br />
                                    <img src="{{ isset($obj->image->id) ? $actual_link.$obj->image->directory.$obj->image->url : '' }}" width="150" class="fl-donwload image_id {{ isset($obj->image->id) ? : 'hide'}} ">
                                    <p class="erro_image_id"></p>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">  
                            <div class="col-sm-2">
                                <button class="btn btn-primary submit-btn btn-block">Atualizar</button>
                            </div>
                        </div>
                    </form>    

                </div>
            
            </div>
        </div>
    </div>
</div>

@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush