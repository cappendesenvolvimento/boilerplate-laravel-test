
@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
    <div class="col s12">
        @include('componentes.breadcrumbs')
    </div>

  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      
      <div class="card-body">
        
        <h4 class="card-title">Adicionar Cta</h4>
        <p class="card-description"> </p>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif

        <div class="auto-form-wrapper">
        <form method="POST" action="{{ route('cta-press.store') }}">
            
            {{ csrf_field() }}

            <input type="hidden" name="image_id" id="image_id" value="{{ old('image_id')}}">

            <div class="row clearfix">  
                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text"  name="title" id="title" class="form-control" placeholder="Título"  value="{{ old('title') }}" required>
                        </div>
                    </div>
                </div>  
            </div>

           <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="input-group">
                            <textarea name="text" placeholder="Texto" id="text" class="description" >{{ old('text')}}</textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix">  
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text"  name="link" id="link" class="form-control" placeholder="Link"  value="{{ old('link') }}">
                        </div>
                    </div>
                </div>  
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text"  name="link_text" id="link_text" class="form-control" placeholder="Texto do Link"  value="{{ old('link_text') }}">
                        </div>
                    </div>
                </div>  
            </div>

            <div class="row clearfix">  
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="input-group">
                            <select name="target" id="target" class="form-control" >
                                <option value="">Selecione um Target</option>
                                <option value="0" {{ (old('target') == 0) ? 'selected' : '' }}>Abrir na mesma Aba</option>
                                <option value="1" {{ (old('target') == 1) ? 'selected' : '' }}>Abrir em nova Aba</option>
                            </select>
                        </div>
                    </div>
                </div>  
            </div>  

            <div class="row clearfix">
                <div class="col-sm-6">
                    <div class="form-group">
                        <p>Imagem</p>
                        <div class="input-group">
                            <input type="file" name="image" id="image" data-model="colors" data-inputid="image_id" class="form-control image">
                        </div>
                        <br />
                        <img src="" width="150" class="fl-donwload image_id hide">
                        <p class="erro_image_id"></p>
                    </div>
                </div>
            </div>

            <div class="row clearfix">  
                <div class="col-sm-2">
                    <button class="btn btn-primary submit-btn btn-block">Registrar</button>
                </div>
            </div>
          
        </form>
      </div>
        
    </div>
  </div>
  
</div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush