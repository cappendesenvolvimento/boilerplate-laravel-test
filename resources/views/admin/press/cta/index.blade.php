@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
  

  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      
      <div class="card-body">

        @if (session('status'))
            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                </div>
            </div>
        @endif    
             
        @can('ctaPress-create')  
        <div>
          <a href="{{ route('cta-press.create') }}" class="btn btn-primary btn-fw btn-add"><i class="mdi mdi-plus"></i>Adicionar</a>
        </div>
        @endcan
        <h4 class="card-title">Cta</h4>
        <p class="card-description"> </p>
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th> Id </th>
                <th> Título </th>
                <th> Criado dia </th>
                <th> Status</th>
                <th> Ações </th>
              </tr>
            </thead>
            <tbody>
            @forelse($objs as $obj)
              @php
                  $ativo = ($obj->status == 1) ? 'btn-inverse-success' : 'btn-inverse-info';
                  $icon = ($obj->status == 1) ? 'mdi mdi-eye' : 'mdi mdi-eye-off';
                @endphp
              <tr>
                <td> {{ $obj->id }} </td>
                <td> {{ (strlen($obj->title) > 50) ? substr($obj->title,0,50).'...' : $obj->title }} </td>
                <td> {{ $obj->created_at }} </td>
                <td> 
                  @can('newsPress-edit')
                    <a title="Status"  id="linkstatus{{$obj->id}}" class="btn btn-icons {{$ativo}} alteraStatus" data-id="{{$obj->id}}" data-secao="ctaPress" href="#"><i id="iconStatus" class="{{$icon}}"></i></a>
                  @else
                      <a title="Status"  class="btn btn-icons {{$ativo}}" href="#"><i id="iconStatus" class="{{$icon}}"></i></a>
                  @endcan 
                </td>
                <td> 
                    <form action="{{route('cta-press.destroy', $obj->id)}}" method="post">
                        @can('ctaPress-edit')  
                            <a title="Editar" class="btn btn-icons btn-inverse-warning" href="{{ route('cta-press.edit', $obj->id) }}"><i class="mdi mdi-grease-pencil"></i></a>
                        @endcan  
                        @can('ctaPress-delete')  
                            {{method_field('DELETE')}}
                            {{csrf_field()}}
                            <button title="Deletar" class="btn btn-icons btn-inverse-danger"><i class="mdi mdi-delete"></i></button>   
                        @endcan
                    </form>     
                </td>
              </tr>
              @empty
            </tbody>
              <tr>
                <td colspan="6">Nenhum registro encontrado.</td>
              </tr>
            @endforelse
          </table>
          {{ $objs->links() }}
        </div>
      </div>
    </div>
  </div>
  
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush