
@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
    <div class="col s12">
        @include('componentes.breadcrumbs')
    </div>

  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      
      <div class="card-body">
        
        <h4 class="card-title">Adicionar Notícias de Imprensa</h4>
        <p class="card-description"> </p>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif

        <div class="auto-form-wrapper">

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#menu1">Geral</a></li>
            <li class=""><a>Arquivos</a></li>
        </ul>
        <form method="POST" action="{{ route('newsPress.store') }}">
            
            {{ csrf_field() }}

            <input type="hidden" name="featured_img_id" id="featured_img_id" value="{{ old('featured_img_id')}}">
            <input type="hidden" name="featured_img_mobile_id" id="featured_img_mobile_id" value="{{ old('featured_img_mobile_id')}}">

            <div class="row clearfix">  
                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text"  name="main_title" id="main_title" class="form-control" placeholder="Título" required  value="{{ old('main_title') }}">
                            
                        </div>
                    </div>
                </div>  
            </div>

            <div class="row clearfix">
                <div class="col-sm-6">
                    <div class="form-group">

                        <p>Imagem Banner</p>
                        
                        <div class="input-group">
                            
                            <input type="file" name="image"  data-input="featured_img" data-input_id="featured_img_id" class="form-control imagecropper">

                            <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="modalLabel">Crop Image</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <div class="img-container">
                                          <div class="row">
                                              <div class="col-md-8">
                                                  <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                                              </div>
                                              <div class="col-md-4">
                                                  <div class="preview"></div>
                                              </div>
                                          </div>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                      <button type="button" class="btn btn-primary" id="crop" data-media="media-press">Crop</button>
                                    </div>
                                  </div>
                                </div>
                              </div>

                            <!-- <input type="file" name="image" id="image" data-model="news-press" data-input_id="featured_img_id" class="form-control imagepress imagecropper"> -->

                        </div>
                        <br />
                        <img src="{{ isset($obj->featured_img->id) ? $actual_link.$obj->featured_img->directory.$obj->featured_img->url : '' }}" width="150" class="fl-donwload featured_img_id {{ isset($obj->featured_img->id) ? : 'hide'}} ">
                        <p class="erro_featured_img_id"></p>
                    </div>
                </div>
            </div>

            {{-- <div class="row clearfix">
                <div class="col-sm-6">
                    <div class="form-group">
                        <p>Imagem Mobile</p>
                        <div class="input-group">
                            <input type="file" name="image_mobile" id="image_mobile" data-model="news-press" data-input_id="featured_img_mobile_id" class="form-control imagecropper">
                        </div>
                        <br />
                        <img src="" width="150" class="fl-donwload featured_img_mobile_id hide">
                        <p class="erro_featured_img_mobile_id"></p>
                    </div>
                </div>
            </div> --}}

            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="form-group">
                        <p>Resumo</p>
                        <div class="input-group">
                            <input name="summary" id="summary" maxlength="140" class="form-control" value="{{ old('summary')}}">
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="form-group">
                        <p>Conteúdo</p>
                        <div class="input-group">
                            <textarea name="content" id="content" class="description" >{{ old('content')}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">  
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text"  name="date_published" id="date_published" class="form-control" placeholder="Data de Publicação"  value="{{ old('date_published') }}" >
                        </div>
                    </div>
                </div>  
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="input-group">
                            <select name="news_press_category_id" id="news_press_category_id" class="validate form-control" required >
                                <option value="">Selecione uma categoria</option>
                                @if(@$categorias)
                                    @foreach($categorias as $categoria)
                                        <option value="{{ $categoria->id }}">
                                            {{$categoria->name}}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>      
            </div>

            <div class="row clearfix">  
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="input-group">
                            <select name="autor_id" id="autor_id" class="validate form-control" >
                                <option value="">Selecione um Autor</option>
                                @if(@$autores)
                                    @foreach($autores as $autor)
                                        <option value="{{ $autor->id }}">
                                            {{$autor->name}}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>      
            </div>

            <div class="row clearfix">
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="input-field">
                            <select name="active" id="active" class="form-control c-tipo__blog" >
                                <option value="">Selecione um tipo</option>
                                <option value="0">Rascunho</option>
                                <option value="1">Publicado</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-check form-check-flat">
                        <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" name="show_date" value="1" checked> Exibir Data  <i class="input-helper"></i></label>
                    </div>
                </div>  
                <div class="col-sm-3">
                    <div class="form-check form-check-flat">
                        <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" name="destaque" value="1" > Notícia aberta  <i class="input-helper"></i></label>
                    </div>
                </div>  
            </div>  


            <h5>Banner </h5>
            <div class="row clearfix">  
                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text"  name="banner_text" id="banner_text" class="form-control" placeholder="Texto Banner"  value="{{ old('banner_text') }}" >
                        </div>
                    </div>
                </div>  
            </div>  

            <div class="row clearfix">  
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text"  name="banner_button" id="banner_button" class="form-control" placeholder="Botão Banner"  value="{{ old('banner_button') }}" >
                        </div>
                    </div>
                </div>  
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text"  name="banner_link" id="banner_link" class="form-control" placeholder="Link Banner"  value="{{ old('banner_link') }}" >
                        </div>
                    </div>
                </div>  
            </div>  
               
            <div class="row clearfix">  
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="input-group">
                            <select name="banner_target" id="banner_target" class="form-control" >
                                <option value="">Selecione um Target</option>
                                <option value="0" {{ (old('banner_target') == 0) ? 'selected' : '' }}>Abrir na mesma Aba</option>
                                <option value="1" {{ (old('banner_target') == 1) ? 'selected' : '' }}>Abrir em nova Aba</option>
                            </select>
                        </div>
                    </div>
                </div>  
            </div>  

            <h5>Notícias Relacionadas</h5>
            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="input-group">
                            <select name="relateds[]" id="relateds" class="form-multiselect" multiple>
                                @if ($news)
                                    @foreach($news as $new)
                                        <option value="{{$new->id}}">{{ $new->main_title }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">  
                <div class="col-sm-2">
                    <button class="btn btn-primary submit-btn btn-block">Registrar</button>
                </div>
            </div>
          
        </form>
      </div>
        
    </div>
  </div>
  
</div>

</div>

@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush