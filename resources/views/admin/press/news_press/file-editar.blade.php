
@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')

@php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/" ;    
$aba = app('request')->input('aba');
@endphp

<div class="row">
    <div class="col s12">
        @include('componentes.breadcrumbs')
    </div>
    
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
        
            <div class="card-body">
                @if (session('status'))
                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        </div>
                    </div>
                @endif

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif                
                
                <h4 class="card-title">Editar Arquivo</h4>
                <p class="card-description"> </p>

                <div class="auto-form-wrapper">

                    <form action="{{ route('newsPress.files.update', $obj->id)}}" method="post" 
                        enctype="multipart/form-data" id="BannersBrand">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}

                        <input type="hidden" name="media_id" id="media_id" value="{{ isset($obj->media_id) ?  $obj->media_id : old('media_id')}}">
                        
                        
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-field">
                                        <select name="type" id="modal_type" class="form-control c-tipo__blog" >
                                            <option value="">Selecione um tipo</option>
                                            <option value="i" {{ ($obj->type == 'i') ? 'selected' : '' }}>Imagem</option>
                                            <option value="d" {{ ($obj->type == 'd') ? 'selected' : '' }}>Doc</option>
                                            <option value="v" {{ ($obj->type == 'v') ? 'selected' : '' }}>Video</option>
                                            <option value="z" {{ ($obj->type == 'v') ? 'selected' : '' }}>Zip</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="title" id="modal_title"
                                            class="validate form-control" value="{{ isset($obj->title) ?  $obj->title : old('title')}}"
                                            Placeholder="Título">
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="link" id="modal_link"
                                            class="validate form-control" value="{{ isset($obj->link) ?  $obj->link : old('link')}}"
                                            Placeholder="Link">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <p>Descrição</p>
                                    <div class="input-group">
                                        <textarea name="description" id="modal_description" class="description">{{ isset($obj->description) ?  $obj->description : old('description')}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <p>Arquivo para Download</p>
                                    <div class="input-group">
                                        <input type="file" name="media" id="media" data-model="pressfiles" data-inputid="modal_media_id" class="form-control imagepress">
                                    </div>
                                    <br />
                                    <p><a href="{{ isset($obj->media->id) ? $actual_link. $obj->media->directory.$obj->media->url : '' }}" download class="fl-donwload {{ isset($obj->media->id) ? '' : 'hide'}}"> Download </a></p>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">  
                            <div class="col-sm-2">
                                <button class="btn btn-primary submit-btn btn-block">Atualizar</button>
                            </div>
                        </div>
                        
                    </form>
                            
                </div>
            
            </div>
        </div>
    </div>
</div>

@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush