@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
  

  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      
      <div class="card-body">

        @if (session('status'))
            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                </div>
            </div>
        @endif    
             
        @can('newsPress-create')  
        @include('layout.search')
        <div>
          <a href="{{ route('newsPress.create') }}" class="btn btn-primary btn-fw btn-add"><i class="mdi mdi-plus"></i>Adicionar</a>
        </div>
        @endcan
        <h4 class="card-title">Notícias de Imprensa</h4>
        <p class="card-description"> </p>
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th> Id </th>
                <th> Título </th>
                <th> Categoria </th>
                <th> Criado dia </th>
                <th> status </th>
                <th> Ações </th>
              </tr>
            </thead>
            <tbody>
            @forelse($objs as $obj)
              @php
                  $ativo = ($obj->active == 1) ? 'btn-inverse-success' : 'btn-inverse-info';
                  $icon = ($obj->active == 1) ? 'mdi mdi-eye' : 'mdi mdi-eye-off';
                @endphp
              <tr>
                <td> {{ $obj->id }} </td>
                <td> {{ (strlen($obj->main_title) > 50) ? substr($obj->main_title,0,50).'...' : $obj->main_title }} </td>
                <td> {{ @$obj->category->name }} </td>
                <td> {{ $obj->created_at }} </td>
                <td> 
                  @can('newsPress-edit')
                    <a title="Status"  id="linkstatus{{$obj->id}}" class="btn btn-icons {{$ativo}} alteraStatus" data-id="{{$obj->id}}" data-secao="newsPress" href="#"><i id="iconStatus" class="{{$icon}}"></i></a>
                  @else
                      <a title="Status"  class="btn btn-icons {{$ativo}}" href="#"><i id="iconStatus" class="{{$icon}}"></i></a>
                  @endcan 
                </td>
                <td> 
                    <form action="{{route('newsPress.destroy', $obj->id)}}" method="post">
                        @can('newsPress-edit')  
                            <a title="Editar" class="btn btn-icons btn-inverse-warning" href="{{ route('newsPress.edit', $obj->id) }}"><i class="mdi mdi-grease-pencil"></i></a>
                        @endcan  
                        @can('newsPress-delete')  
                            {{method_field('DELETE')}}
                            {{csrf_field()}}
                            <button title="Deletar" class="btn btn-icons btn-inverse-danger"><i class="mdi mdi-delete"></i></button>   
                        @endcan
                    </form>     
                </td>
              </tr>
              @empty
            </tbody>
              <tr>
                <td colspan="6">Nenhum registro encontrado.</td>
              </tr>
            @endforelse
          </table>
          {{ $objs->appends(request()->input())->links() }}
        </div>
      </div>
    </div>
  </div>
  
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush