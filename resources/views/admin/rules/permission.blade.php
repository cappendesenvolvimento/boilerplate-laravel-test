
@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
    <div class="col s12">
        @include('componentes.breadcrumbs')
    </div>    
    
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            @can('permission-create')
            
                <div class="card-body">
                
                
                    <h4 class="card-title">Adicionar Perfil/Regra</h4>
                    <p class="card-description"> </p>
                    <h4 class="card-title">Lista de Permissões para: <span class="text-primary">{{$rules->name}}</span></h4>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                    @endif

                    <div class="auto-form-wrapper">
                        <form action="{{route('rules.permission.store', $rules->id)}}" method="post">
                            {{ csrf_field() }}

                            {{-- <div class="row clearfix">  
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <select name="permission_id" id="permission_id" class="form-control">
                                            @php
                                                $module = ''; $first = 1;   
                                            @endphp 
                                            @if ($permissions)
                                                @foreach($permissions as $valor)
                                                    @php
                                                        if ($module != $valor->module) :
                                                        
                                                            $module = $valor->module;
                                                    
                                                            if($first != 1) :
                                                                echo '</optgroup>';
                                                            endif;
                                                            $first = 0;
                                                    @endphp                                                    
                                                        <optgroup label="{{$valor->module}}">
                                                    @php
                                                        endif;
                                                    @endphp
                                                    <option value="{{$valor->id}}">{{$valor->label}}</option>
                                                    
                                                @endforeach
                                                </optgroup>
                                            @endif    
                                            </select>
                                        </div>
                                    </div>
                                </div>  
                            </div> --}}

                           
                                @if ($permissions)
                                    @php
                                        $grupo = $module = ''; $first = 1;   $modulofirst = 1;   
                                    @endphp 
                                    @foreach($permissions as $valor)
                                        
                                        @php
                                            

                                            if ($module != $valor->module) :
                                            
                                                $module = $valor->module;
                                        
                                                if($first != 1) :
                                                    echo '</div>';
                                                endif;
                                                $first = 0;
                                                if ($valor->grupo != $grupo) {
                                                    $grupo =  $valor->grupo;
                                                    if($modulofirst != 1) :
                                                        echo '</div>';
                                                    endif;
                                                    $modulofirst = 0;
                                                    echo '<h3 class="title_section">'.$valor->grupo.'</h3><div class="boxPermissao">';
                                            } 
                                        @endphp                                                    
                                         <h5 class="title_section">{{$valor->module}} </h5>
                                       
                                        <div class="row clearfix">
                                        @php
                                            endif;
                                        @endphp     
                                        <div class="col-sm-3">
                                            <div class="form-check form-check-flat">
                                                <label class="form-check-label">
                                                  <input type="checkbox" class="form-check-input"name="permission_id[]"  value="{{$valor->id}}" {{ (in_array($valor->id, $permRule)) ? 'checked': '' }}>{{$valor->label}} 
                                                </label>
                                            </div> 
                                        </div> 
                                    @endforeach
                                    </div>
                                
                                @endif
                            </div> 
                            <div class="row clearfix">  
                                <div class="col-sm-2">
                                    <button class="btn btn-primary submit-btn btn-block">Adicionar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @endcan    
            
        </div>
    </div>
  
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush