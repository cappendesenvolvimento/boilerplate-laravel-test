@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
  

  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      
      <div class="card-body">

        @if (session('status'))
            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                </div>
            </div>
        @endif  
               
        @can('rules-create')
        <div>
          <a href="{{ route('rules.create') }}" class="btn btn-primary btn-fw btn-add"><i class="mdi mdi-plus"></i>Adicionar</a>
        </div>
        @endcan
        <h4 class="card-title">Perfil </h4>
        <p class="card-description"> Listagem de perfis/regras de acesso </p>
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th> Id </th>
                <th> Nome </th>
                <th> Descrição </th>
                <th> Ações </th>
              </tr>
            </thead>
            <tbody>
            @forelse($rules as $rule)
              <tr>
                <td> {{ $rule->id }} </td>
                <td> {{ $rule->name }} </td>
                <td> {{ $rule->description }} </td>
                <td> 
                    <form action="{{route('rules.destroy', $rule->id)}}" method="post">
                        @can('rule-edit')
                            <a title="Editar" class="btn btn-icons btn-inverse-warning" href="{{route('rules.edit', $rule->id)}}"><i class="mdi mdi-grease-pencil"></i></a>
                            @can('permission-view')
                                <a title="Permissões" class="btn btn-icons btn-inverse-primary" href="{{route('rules.permission', $rule->id)}}" ><i class="mdi mdi-lock"></i></a>
                            @endcan
                        @endcan
                        @can('rule-delete')
                            {{method_field('DELETE')}}
                            {{csrf_field()}}
                            <button title="Deletar" class="btn btn-icons btn-inverse-danger"><i class="mdi mdi-delete"></i></button>   
                        @endcan
                    </form>     
                </td>
              </tr>
              @empty
            </tbody>
              <tr>
                <td colspan="6">Nenhum registro encontrado.</td>
              </tr>
            @endforelse
          </table>
          {{ $rules->links() }}
        </div>
      </div>
    </div>
  </div>
  
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush

        