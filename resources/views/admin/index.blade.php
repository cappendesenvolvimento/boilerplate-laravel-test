@extends('layout.master')

@push('plugin-styles')
  <!-- {!! Html::style('/assets/plugins/plugin.css') !!} -->
@endpush

@section('content')
<div class="row">
  <div class="col-12 grid-margin">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Logs</h4>
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th> # </th>
                <th> Usuário </th>
                <th> Ação </th>
                <th> Descrição </th>
              </tr>
            </thead>
            <tbody>
              @if($logs)
                @foreach ($logs as $log)
              <tr>
                <td class="font-weight-medium"> {{$log->id}} </td>
                <td> {{$log->users->name}}  </td>
                <td> {{$log->acao}}</td>
                <td> {{$log->descricao}} </td>
              </tr>
              @endforeach
              @else 
              <tr>
                <td colspan="6"></td>
              </tr>
              @endif
            </tbody>
          </table>
          
        </div>
        <br /><br />
        {{ $logs->appends(request()->input())->links() }}
      </div>
    </div>
  </div>
</div>

@endsection
{{-- 
@push('plugin-scripts')
  {!! Html::script('/assets/plugins/chartjs/chart.min.js') !!}
  {!! Html::script('/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') !!}
@endpush

@push('custom-scripts')
  {!! Html::script('/assets/js/dashboard.js') !!}
@endpush --}}