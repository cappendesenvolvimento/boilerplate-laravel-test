@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')

@php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/" ;    
@endphp

<div class="row">
    <div class="col s12">
        @include('componentes.breadcrumbs')
    </div>

    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">

            <div class="card-body">

                <h4 class="card-title">Editar banner</h4>
                <p class="card-description"> </p>

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif

                <div class="auto-form-wrapper">
                    <form action="{{ route('banner.update', $obj->id)}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <input type="hidden" name="file_id" id="file_id" value="{{ isset($obj->file_id) ?  $obj->file_id : old('file_id')}}">
                        <input type="hidden" name="file_mobile_id" id="file_mobile_id" value="{{ isset($obj->file_mobile_id) ?  $obj->file_mobile_id : old('file_mobile_id')}}">

                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="name" id="name" class="validate form-control"
                                            value="{{ isset($obj->name) ?  $obj->name : old('name')}}"
                                            Placeholder="Nome do banner" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-field">
                                        <select name="language" id="language" class="form-control">
                                            <option value="pt_br" {{ ($obj->language == 'pt_br') ?  'selected' : ''}}>Português</option>
                                            <option value="en_us" {{ ($obj->language == 'en_us') ?  'selected' : ''}}>Inglês</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <textarea name="text" id="text" placeholder="Texto do Banner"  class="form-control">{{ isset($obj->text) ?  $obj->text : old('text')}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-check form-check-flat">
                                <label class="form-check-label">
                                  <input type="checkbox" class="form-check-input" name="exibir_titulo" value="1" {{ ($obj->exibir_titulo == 1) ?  'checked' : ''}}>Exibir Texto 
                                </label>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="txtbotao" id="txtbotao" class="validate form-control"
                                            value="{{ isset($obj->txtbotao) ?  $obj->txtbotao : old('txtbotao')}}"
                                            Placeholder="Texto Link" >
                                    </div>
                                    <p class="obs">Obs: Se o texto do link não for informado, o link não será exibido</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="link" id="link" class="validate form-control"
                                            value="{{ isset($obj->link) ?  $obj->link : old('link')}}"
                                            Placeholder="Link" >
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <select name="target" id="target" class="form-control" >
                                            <option value="">Selecione um Target</option>
                                            <option value="0" {{ ($obj->target == 0) ?  'selected' : ''}}>Abrir na mesma Aba</option>
                                            <option value="1" {{ ($obj->target == 1) ?  'selected' : ''}}>Abrir em nova Aba</option>
                                            <option value="2" {{ ($obj->target == 2) ?  'selected' : ''}}>Abrir Modal de Contato</option>
                                        </select>
                                        
                                    </div>
                                </div>
                            </div>  
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <select name="posicao" id="posicao" class="form-control" >
                                            <option value="">Selecione uma Posição</option>
                                            <option value="t" {{ ($obj->posicao == "t") ? 'selected' : ''}}>Topo</option>
                                            <option value="c" {{ ($obj->posicao == "c") ? 'selected' : ''}}>Centro</option>
                                            <option value="i" {{ ($obj->posicao == "i") ? 'selected' : ''}}>Rodapé</option>
                                        </select>
                                        
                                    </div>
                                </div>
                            </div>      
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <select name="status" id="status" class="form-control" >
                                            <option value="">Selecione um Status</option>
                                            <option value="1" {{ ($obj->status == 1) ? 'selected' : ''}}>Ativo</option>
                                            <option value="0" {{ ($obj->status == 0) ? 'selected' : ''}}>Inativo</option>
                                        </select>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        

                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <p>Arquivo Banner (Imagem/Video)</p>
                                    <div class="input-group">
                                        <input type="file" name="file" id="file" data-model="banner" data-inputid="file_id" class="form-control media">
                                    </div>
                                    <br />
                                    @php    
                                        $image = $video = '';
                                        
                                        if ($obj->file) :
                                            if (isset($obj->file->id) && $obj->file->type != 'video/mp4')  : 
                                                $image = $actual_link.$obj->file->directory.$obj->file->url;
                                            else : 
                                                $video = $actual_link.$obj->file->directory.$obj->file->url;
                                            endif;
                                        endif;
                                    @endphp
                                    <img src="{{ ($image != '') ? $image : '' }}" width="150" class="fl-donwload file_id {{ ($image != '') ? '' : 'hide'}} ">
                                    <video width="320" height="240" controls class="vd-donwload file_id {{ ($video != '') ? '' : 'hide'}}">
                                        <source src="{{ ($video != '')  ? $video : '' }}" type="video/mp4">
                                        Your browser does not support the video tag.
                                    </video>
                                    <p class="erro_file_id"></p>
                                </div>
                            </div>
                        </div>
            
            
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <p>Arquivo Mobile (Imagem)</p>
                                    <div class="input-group">
                                        <input type="file" name="image_mobile" id="image_mobile" data-model="banner" data-inputid="file_mobile_id" class="form-control media">
                                    </div>
                                    <br />
                                    @php    
                                        $image = $video = '';
                                        
                                        if ($obj->file_mobile) :
                                            if (isset($obj->file_mobile->id) && $obj->file_mobile->type != 'video/mp4')  : 
                                                $image = $actual_link.$obj->file_mobile->directory.$obj->file_mobile->url;
                                            else : 
                                                $video = $actual_link.$obj->file_mobile->directory.$obj->file_mobile->url;
                                            endif;
                                        endif;
                                    @endphp
                                    <img src="{{ ($image != '') ? $image : '' }}" width="150" class="fl-donwload file_mobile_id {{ ($image != '') ? '' : 'hide'}} ">
                                    <video width="320" height="240" controls class="vd-donwload file_mobile_id {{ ($video != '') ? '' : 'hide'}}">
                                        <source src="{{ ($video != '') ? $video : '' }}" type="video/mp4">
                                        Your browser does not support the video tag.
                                    </video>
                                    <p class="erro_file_mobile_id"></p>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">  
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text"  name="date_start" id="date_start" class="form-control css-date" placeholder="Data Inicial"  value="{{ isset($obj->date_start) ?  dataBr($obj->date_start) : old('date_start')}}" >
                                    </div>
                                </div>
                            </div>  
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text"  name="date_end" id="date_end" class="form-control css-date" placeholder="Data Final"  value="{{ isset($obj->date_end) ?  dataBr($obj->date_end) : old('date_end')}}" >
                                    </div>
                                </div>
                            </div>  
                        </div>  

                        

                        <div class="row clearfix">
                            <div class="col-sm-2">
                                <button class="btn btn-primary submit-btn btn-block">Atualizar</button>
                            </div>
                        </div>

                    </form>
                </div>

            </div>
        </div>

    </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush