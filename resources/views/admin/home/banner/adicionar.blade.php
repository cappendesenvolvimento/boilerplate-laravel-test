@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
    <div class="col s12">
        @include('componentes.breadcrumbs')
    </div>

    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">

            <div class="card-body">

                <h4 class="card-title">Adicionar banner</h4>
                <p class="card-description"> </p>

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif

                <div class="auto-form-wrapper">
                    <form action="{{ route('banner.store')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <input type="hidden" name="file_id" id="file_id" value="{{ old('file_id')}}">
                        <input type="hidden" name="file_mobile_id" id="file_mobile_id" value="{{ old('file_mobile_id')}}">

                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="name" id="name" class="validate form-control"
                                            value="{{ old('name')}}"
                                            Placeholder="Nome do banner" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-field">
                                        <select name="language" id="language" class="form-control">
                                            <option value="pt_br">Português</option>
                                            <option value="en_us">Inglês</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <textarea name="text" id="text" placeholder="Texto do Banner" class="form-control">{{ old('text')}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-check form-check-flat">
                                <label class="form-check-label">
                                  <input type="checkbox" class="form-check-input" name="exibir_titulo" value="1" checked>Exibir Texto 
                                </label>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="txtbotao" id="txtbotao" class="validate form-control"
                                            value="{{ old('txtbotao')}}"
                                            Placeholder="Texto Link" >
                                    </div>
                                    <p class="obs">Obs: Se o texto do link não for informado, o link não será exibido</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="link" id="link" class="validate form-control"
                                            value="{{ old('link')}}"
                                            Placeholder="Link" >
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <select name="target" id="target" class="form-control" >
                                            <option value="">Selecione um Target</option>
                                            <option value="0" {{ (old('target') == 0) ? 'selected' : ''}}>Abrir na mesma Aba</option>
                                            <option value="1" {{ (old('target') == 1) ? 'selected' : ''}}>Abrir em nova Aba</option>
                                        </select>
                                        
                                    </div>
                                </div>
                            </div>  
                             <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <select name="posicao" id="posicao" class="form-control" >
                                            <option value="">Selecione uma Posição</option>
                                            <option value="t" {{ (old('posicao') == "t") ? 'selected' : ''}}>Topo</option>
                                            <option value="c" {{ (old('posicao') == "c") ? 'selected' : ''}}>Centro</option>
                                            <option value="i" {{ (old('posicao') == "i") ? 'selected' : ''}}>Rodapé</option>
                                        </select>
                                        
                                    </div>
                                </div>
                            </div>      
                        </div>


                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <select name="status" id="status" class="form-control" >
                                            <option value="">Selecione um Status</option>
                                            <option value="1" {{ (old('status') == 1) ? 'selected' : ''}}>Ativo</option>
                                            <option value="0" {{ (old('status') == 0) ? 'selected' : ''}}>Inativo</option>
                                        </select>
                                        
                                    </div>
                                </div>
                            </div>    
                        </div>    


                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <p>Arquivo Banner (Imagem/Video)</p>
                                    <div class="input-group">
                                        <input type="file" name="file" id="file" data-model="banner" data-inputid="file_id" class="form-control image">
                                    </div>
                                    <br />
                                    <img src="" width="150" class="fl-donwload file_id hide ">
                                    <p class="erro_file_id"></p>
                                </div>
                            </div>
                        </div>
            
            
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <p>Arquivo Mobile (Imagem)</p>
                                    <div class="input-group">
                                        <input type="file" name="image_mobile" id="image_mobile" data-model="banner" data-inputid="file_mobile_id" class="form-control image">
                                    </div>
                                    <br />
                                    <img src="" width="150" class="fl-donwload file_mobile_id hide">
                                    <p class="erro_file_mobile_id"></p>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">  
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text"  name="date_start" id="date_start" class="form-control css-date" placeholder="Data Inicial"  value="{{ old('date_start')}}" >
                                    </div>
                                </div>
                            </div>  
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text"  name="date_end" id="date_end" class="form-control css-date" placeholder="Data Final"  value="{{ old('date_end')}}" >
                                    </div>
                                </div>
                            </div>  
                        </div>  

                        
                        <div class="row clearfix">
                            <div class="col-sm-2">
                                <button class="btn btn-primary submit-btn btn-block">Registrar</button>
                            </div>
                        </div>

                    </form>
                </div>

            </div>
        </div>

    </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush