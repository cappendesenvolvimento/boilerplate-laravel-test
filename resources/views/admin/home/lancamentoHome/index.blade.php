@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')

    @php
    $aba = app('request')->input('aba');
    @endphp

    <div class="row">

        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">

                <div class="card-body">

                    @if (session('status'))
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            </div>
                        </div>
                    @endif

                    <div id="pesquisar">
                        @can('lancamentoHome-create')
                            <div style="float:left">
                                <a href="{{ route('lancamento-home.create') }}" class="btn btn-primary btn-fw btn-add"><i
                                        class="mdi mdi-plus"></i>Adicionar</a>
                            </div>
                        @endcan


                    </div>

                    <div style="clear: both;">
                        <h4 class="card-title">Lançamentos </h4>
                        <p class="card-description"> Listagem de lançamentos </p>
                    </div>

                    <ul class="nav nav-tabs">
                        <li class="{{ $aba != 'pt_br' && $aba ? '' : 'active' }}"><a
                                href="{{ route('lancamento-home.index') }}?aba=pt_br">Português</a></li>
                        <li class="{{ $aba == 'en_us' ? 'active' : '' }}"><a
                                href="{{ route('lancamento-home.index') }}?aba=en_us">Inglês</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="menu1"
                            class="tab-pane {{ $aba != 'pt_br' && $aba ? 'fade aba' : 'show in active' }} ">
                            <div class="table-responsive">
                                <table class="table table-striped" id="sortable">
                                    <thead>
                                        <tr>
                                            <th> Id </th>
                                            <th> Título </th>
                                            <th> Idioma </th>
                                            <th> Ações </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($objsPt as $obj)
                                            <tr>
                                                <td>
                                                    <input type="hidden" name="ids[]" value="{{ $obj->id }}">
                                                    {{ $obj->id }}
                                                    <input type="hidden" name="model" value="homeLancamento">
                                                    <input type="hidden" name="parent_id" value="">
                                                    <input type="hidden" name="language" value="pt_br">
                                                </td>
                                                <td> {{ $obj->title }} </td>
                                                <td> {{ $obj->language }} </td>
                                                <td>
                                                    <form action="{{ route('lancamento-home.destroy', $obj->id) }}"
                                                        method="post">
                                                        @php
                                                            $ativo = $obj->status == 1 ? 'btn-inverse-success' : 'btn-inverse-info';
                                                            $icon = $obj->status == 1 ? 'mdi mdi-eye' : 'mdi mdi-eye-off';
                                                        @endphp
                                                        @can('lancamentoHome-edit')
                                                            <a title="Status" id="linkstatus{{ $obj->id }}"
                                                                class="btn btn-icons {{ $ativo }} alteraStatus"
                                                                data-id="{{ $obj->id }}" data-secao="lancamento-home"
                                                                href="#"><i id="iconStatus"
                                                                    class="{{ $icon }}"></i></a>
                                                        @else
                                                            <a title="Status" class="btn btn-icons {{ $ativo }}"
                                                                href="#"><i id="iconStatus"
                                                                    class="{{ $icon }}"></i></a>
                                                        @endcan
                                                        @can('lancamentoHome-edit')
                                                            <a title="Editar" class="btn btn-icons btn-inverse-warning"
                                                                href="{{ route('lancamento-home.edit', $obj->id) }}"><i
                                                                    class="mdi mdi-grease-pencil"></i></a>
                                                        @endcan

                                                        @can('lancamentoHome-delete')
                                                            {{ method_field('DELETE') }}
                                                            {{ csrf_field() }}
                                                            <button title="Deletar" class="btn btn-icons btn-inverse-danger"><i
                                                                    class="mdi mdi-delete"></i></button>
                                                        @endcan
                                                    </form>
                                                </td>
                                            </tr>
                                        @empty
                                    </tbody>
                                    <tr>
                                        <td colspan="6">Nenhum registro encontrado.</td>
                                    </tr>
                                    @endforelse
                                </table>
                                @php
                                    $params = ['aba' => 'pt_br'];
                                @endphp
                                {{ $objsPt->appends($params)->links() }}
                            </div>
                        </div>

                        <div id="menu2" class="tab-pane {{ $aba == 'en_us' ? 'show in active' : 'fade aba' }} ">
                            <div class="table-responsive">
                                <table class="table table-striped sortable">
                                    <thead>
                                        <tr>
                                            <th> Id </th>
                                            <th> Título </th>
                                            <th> Idioma </th>
                                            <th> Ações </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($objsEn as $obj)
                                            <tr>
                                                <td>
                                                    <input type="hidden" name="ids_items[]" value="{{ $obj->id }}">
                                                    {{ $obj->id }}
                                                    <input type="hidden" name="model_items" value="homeLancamento">
                                                    <input type="hidden" name="parent_id_items" value="">
                                                    <input type="hidden" name="language_items" value="en_us">
                                                </td>
                                                <td> {{ $obj->title }} </td>
                                                <td> {{ $obj->language }} </td>
                                                <td>
                                                    <form action="{{ route('lancamento-home.destroy', $obj->id) }}"
                                                        method="post">
                                                        @php
                                                            $ativo = $obj->status == 1 ? 'btn-inverse-success' : 'btn-inverse-info';
                                                            $icon = $obj->status == 1 ? 'mdi mdi-eye' : 'mdi mdi-eye-off';
                                                        @endphp
                                                        @can('lancamentoHome-edit')
                                                            <a title="Status" id="linkstatus{{ $obj->id }}"
                                                                class="btn btn-icons {{ $ativo }} alteraStatus"
                                                                data-id="{{ $obj->id }}" data-secao="lancamento-home"
                                                                href="#"><i id="iconStatus"
                                                                    class="{{ $icon }}"></i></a>
                                                        @else
                                                            <a title="Status" class="btn btn-icons {{ $ativo }}"
                                                                href="#"><i id="iconStatus"
                                                                    class="{{ $icon }}"></i></a>
                                                        @endcan
                                                        @can('lancamentoHome-edit')
                                                            <a title="Editar" class="btn btn-icons btn-inverse-warning"
                                                                href="{{ route('lancamento-home.edit', $obj->id) }}"><i
                                                                    class="mdi mdi-grease-pencil"></i></a>
                                                        @endcan

                                                        @can('lancamentoHome-delete')
                                                            {{ method_field('DELETE') }}
                                                            {{ csrf_field() }}
                                                            <button title="Deletar" class="btn btn-icons btn-inverse-danger"><i
                                                                    class="mdi mdi-delete"></i></button>
                                                        @endcan
                                                    </form>
                                                </td>
                                            </tr>
                                        @empty
                                    </tbody>
                                    <tr>
                                        <td colspan="6">Nenhum registro encontrado.</td>
                                    </tr>
                                    @endforelse
                                </table>
                                @php
                                    $params = ['aba' => 'en_us'];
                                @endphp
                                {{ $objsEn->appends($params)->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush
