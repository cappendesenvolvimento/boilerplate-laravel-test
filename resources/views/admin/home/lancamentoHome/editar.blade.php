@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')

@php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/" ;    
@endphp

<div class="row">
    <div class="col s12">
        @include('componentes.breadcrumbs')
    </div>

    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">

            <div class="card-body">

                <h4 class="card-title">Editar Lançamento</h4>
                <p class="card-description"> </p>

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif

                <div class="auto-form-wrapper">
                    <form action="{{ route('lancamento-home.update', $obj->id)}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <input type="hidden" name="image_id" id="image_id" value="{{ isset($obj->image_id) ?  $obj->image_id : old('image_id')}}">
                        
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-field">
                                        <select name="language" id="language" class="form-control" required>
                                            <option value="">Selecione um Idioma</option>
                                            <option value="pt_br" {{ ($obj->language == 'pt_br') ? 'selected' : ''}}>Português</option>
                                            <option value="en_us" {{ ($obj->language == 'en_us') ? 'selected' : ''}}>Inglês</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-field">
                                        <select name="status" id="status" class="form-control">
                                            <option value="" >Seleciona um Status</option>
                                            <option value="1" {{ ($obj->status == '1') ? 'selected' : ''}}>Ativo</option>
                                            <option value="0" {{ ($obj->status == '0') ? 'selected' : ''}}>Inativo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="title" id="title" class="validate form-control"
                                            value="{{ isset($obj->title) ?  $obj->title : old('title')}}"
                                            Placeholder="Título" required>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="subtitle" id="subtitle" class="validate form-control"
                                            value="{{ isset($obj->subtitle) ?  $obj->subtitle : old('subtitle')}}"
                                            Placeholder="Subtítulo" required>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="link_text" id="link_text" class="validate form-control"
                                            value="{{ isset($obj->link_text) ?  $obj->link_text : old('link_text')}}"
                                            Placeholder="Texto Link" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="link" id="link" class="validate form-control"
                                            value="{{ isset($obj->link) ?  $obj->link : old('link')}}"
                                            Placeholder="Link" >
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <select name="link_target" id="link_target" class="form-control" >
                                            <option value="">Selecione um Target</option>
                                            <option value="0" {{ ($obj->link_target == '0') ? 'selected' : ''}}>Abrir na mesma Aba</option>
                                            <option value="1" {{ ($obj->link_target == '1') ? 'selected' : ''}}>Abrir em nova Aba</option>
                                        </select>
                                        
                                    </div>
                                </div>
                            </div>    
                        </div>
                        
            
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <p>Imagem</p>
                                    <div class="input-group">
                                        <input type="file" name="image" id="image" data-model="lancamento" data-inputid="image_id" class="form-control media">
                                    </div>
                                    <br />
                                    <img src="{{ (@$obj->image->id != '') ? $actual_link.$obj->image->directory.$obj->image->url : '' }}" width="150" class="fl-donwload image_id {{ (@$obj->image->id != '') ? '' : 'hide'}} ">
                                    <p class="erro_image_id"></p>
                                </div>
                            </div>
                        </div>
                        

                        <div class="row clearfix">
                            <div class="col-sm-2">
                                <button class="btn btn-primary submit-btn btn-block">Atualizar</button>
                            </div>
                        </div>

                    </form>
                </div>

            </div>
        </div>

    </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush