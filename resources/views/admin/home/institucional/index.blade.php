@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
  

  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      
      <div class="card-body">

        @if (session('status'))
            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                </div>
            </div>
        @endif

        <div id="pesquisar">
          @can('home-create')
            <div style="float:left">
              <a href="{{ route('home.create') }}" class="btn btn-primary btn-fw btn-add"><i class="mdi mdi-plus"></i>Adicionar</a>
            </div>
            @endcan
            
            @include('layout.pagination')
        </div>

        <div style="clear: both;">
            <h4 class="card-title">Homes </h4>
            <p class="card-description"> Listagem de Homes </p>
        </div>
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th> Id </th>
                <th> Idioma </th>
                <th> Ações </th>
              </tr>
            </thead>
            <tbody>
            @forelse($objs as $obj)
              <tr>
                <td> {{ $obj->id }} </td>
                <td> {{ getLegendLanguage($obj->language) }} </td>
                <td> 
                  @can('home-edit')
                      <a title="Editar" class="btn btn-icons btn-inverse-warning" href="{{route('home.edit', $obj->id)}}"><i class="mdi mdi-grease-pencil"></i></a>
                  @endcan
                </td>
              </tr>
              @empty
            </tbody>
              <tr>
                <td colspan="6">Nenhum registro encontrado.</td>
              </tr>
            @endforelse
          </table>
          {{ $objs->appends(request()->input())->links() }}
        </div>
      </div>
    </div>
  </div>
  
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush

        