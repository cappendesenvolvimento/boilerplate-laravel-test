<form action="{{ route('home.update', ["id"=> $obj->id, "action" => "half"])}}" method="post"
    enctype="multipart/form-data" id="homeHalf">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

<input type="hidden" name="action"  value="split">    
<input type="hidden" name="img_half_one_id" id="img_half_one_id" value="{{ isset($obj->img_half_one_id) ?  $obj->img_half_one_id : old('img_half_one_id')}}">
<input type="hidden" name="img_half_two_id" id="img_half_two_id" value="{{ isset($obj->img_half_two_id) ?  $obj->img_half_two_id : old('img_half_two_id')}}">
{{-- CHAMADA SPLIT LEFT --}}
<h5 class="title_section">Chamada Half Um</h5>    
<div class="row clearfix">
    <div class="col-sm-6">
        <div class="form-group">
            <p>Imagem </p>
            <div class="input-group">
                <input type="file" name="img_half_one" id="img_half_one" data-model="home" data-inputid="img_half_one_id" class="form-control image">
            </div>
            <br />

            <img src="{{ isset($obj->img_half_one->id) ? $actual_link.$obj->img_half_one->directory.$obj->img_half_one->url : '' }}" width="150" class="fl-donwload img_half_one_id {{ isset($obj->img_half_one->id) ? '' : 'hide'}} ">
            @if ($obj->img_half_one_id)
                <a title="Remover Imagem" class="btn btn-icons btn-inverse-danger bt-excluir-img" data-token="{{ csrf_token() }}" data-id="{{ $obj->id }}" data-pagina="homes" data-campo="img_half_one_id"><i class="mdi mdi-delete"></i></a>    
            @endif
            <p class="erro_img_half_one_id"></p>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-sm-12">
        <div class="form-group">
            <div class="input-group">
                <textarea name="ttl_half_one" id="ttl_half_one" placeholder="Título Chamada" class="form-control">{{ isset($obj->ttl_half_one) ?  $obj->ttl_half_one : old('ttl_half_one')}}</textarea>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-sm-12">
        <div class="form-group">
            <div class="input-group">
                <textarea name="tx_half_one" id="tx_half_one" placeholder="Texto Chamada" class="form-control">{{ isset($obj->tx_half_one) ?  $obj->tx_half_one : old('tx_half_one')}}</textarea>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-sm-6">
        <div class="form-group">
            <div class="input-group">
                <input type="text" name="lk_text_half_one" id="lk_text_half_one" class="validate form-control"
                    value="{{ isset($obj->lk_text_half_one) ?  $obj->lk_text_half_one : old('lk_text_half_one')}}"
                    Placeholder="Texto Link" >
            </div>
            <p class="obs">Obs: Se o texto do link não for informado, o link não será exibido</p>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <div class="input-group">
                <input type="text" name="lk_half_one" id="lk_half_one" class="validate form-control"
                    value="{{ isset($obj->lk_half_one) ?  $obj->lk_half_one : old('lk_half_one')}}"
                    Placeholder="Link" >
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-sm-6">
        <div class="form-group">
            <div class="input-group">
                <select name="lk_target_half_one" id="lk_target_half_one" class="form-control" >
                    <option value="">Selecione um Target</option>
                    <option value="0" {{ ($obj->lk_target_half_one == 0) ?  'selected' : ''}}>Abrir na mesma Aba</option>
                    <option value="1" {{ ($obj->lk_target_half_one == 1) ?  'selected' : ''}}>Abrir em nova Aba</option>
                </select>
            </div>
        </div>
    </div>    
</div>
<br />

{{-- CHAMADA SPLIT DIREITA --}}
<h5 class="title_section">Chamada Half Dois</h5>    
<div class="row clearfix">
    <div class="col-sm-6">
        <div class="form-group">
            <p>Imagem </p>
            <div class="input-group">
                <input type="file" name="img_half_two" id="img_half_two" data-model="home" data-inputid="img_half_two_id" class="form-control image">
            </div>
            <br />

            <img src="{{ isset($obj->img_half_two->id) ? $actual_link.$obj->img_half_two->directory.$obj->img_half_two->url : '' }}" width="150" class="fl-donwload img_half_two_id {{ isset($obj->img_half_two->id) ? '' : 'hide'}} ">
            @if ($obj->img_half_two_id)
                <a title="Remover Imagem" class="btn btn-icons btn-inverse-danger bt-excluir-img" data-token="{{ csrf_token() }}" data-id="{{ $obj->id }}" data-pagina="homes" data-campo="img_half_two_id"><i class="mdi mdi-delete"></i></a>    
            @endif
            <p class="erro_img_half_two_id"></p>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-sm-12">
        <div class="form-group">
            <div class="input-group">
                <textarea name="ttl_half_two" id="ttl_half_two" placeholder="Título Chamada" class="form-control">{{ isset($obj->ttl_half_two) ?  $obj->ttl_half_two : old('ttl_half_two')}}</textarea>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-sm-12">
        <div class="form-group">
            <div class="input-group">
                <textarea name="tx_half_two" id="tx_half_two" placeholder="Texto Chamada" class="form-control">{{ isset($obj->tx_half_two) ?  $obj->tx_half_two : old('tx_half_two')}}</textarea>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-sm-6">
        <div class="form-group">
            <div class="input-group">
                <input type="text" name="lk_text_half_two" id="lk_text_half_two" class="validate form-control"
                    value="{{ isset($obj->lk_text_half_two) ?  $obj->lk_text_half_two : old('lk_text_half_two')}}"
                    Placeholder="Texto Link" >
            </div>
            <p class="obs">Obs: Se o texto do link não for informado, o link não será exibido</p>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <div class="input-group">
                <input type="text" name="lk_half_two" id="lk_half_two" class="validate form-control"
                    value="{{ isset($obj->lk_half_two) ?  $obj->lk_half_two : old('lk_half_two')}}"
                    Placeholder="Link" >
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-sm-6">
        <div class="form-group">
            <div class="input-group">
                <select name="lk_target_half_two" id="lk_target_half_two" class="form-control" >
                    <option value="">Selecione um Target</option>
                    <option value="0" {{ ($obj->lk_target_half_two == 0) ?  'selected' : ''}}>Abrir na mesma Aba</option>
                    <option value="1" {{ ($obj->lk_target_half_two == 1) ?  'selected' : ''}}>Abrir em nova Aba</option>
                </select>
            </div>
        </div>
    </div>    
</div>

<div class="row clearfix">
    <div class="col-sm-2">
        <button class="btn btn-primary submit-btn btn-block">Atualizar</button>
    </div>
</div>

</form>