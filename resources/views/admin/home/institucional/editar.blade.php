@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')

@php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/" ;    
$aba = app('request')->input('aba');
@endphp

<div class="row">
    <div class="col s12">
        @include('componentes.breadcrumbs')
    </div>

    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">

            <div class="card-body">
                @if (session('status'))
                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        </div>
                    </div>
                @endif

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif  


                <h4 class="card-title">Editar home</h4>
                <p class="card-description"> </p>

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif

                <div class="auto-form-wrapper">

                    <ul class="nav nav-tabs">
                        <li class="{{ ($aba != 'institucional' && ($aba)) ?  '' : 'active' }}"><a data-toggle="tab" href="#menu1">Institucional</a></li>
                        <li class="{{ ($aba == 'chamadas') ?  'active' : '' }}"><a data-toggle="tab" href="#menu2">Chamadas Full</a></li>
                        <li class="{{ ($aba == 'split') ?  'active' : '' }}"><a data-toggle="tab" href="#menu3">Chamadas Split</a></li>
                        <li class="{{ ($aba == 'half') ?  'active' : '' }}"><a data-toggle="tab" href="#menu5">Chamadas Half</a></li>
                        <li class="{{ ($aba == 'ctas') ?  'active' : '' }}"><a data-toggle="tab" href="#menu4">Ctas</a></li>
                    </ul>

                    <div class="tab-content">
                        {{-- INTITUCIONAL --}}
                        <div id="menu1" class="tab-pane {{ (($aba != 'institucional')  && ($aba)) ?  'fade aba' : 'show in active' }} ">
                            <form action="{{ route('home.update', ["id"=> $obj->id, "action" => "institucional"])}}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}

                                <input type="hidden" name="action"  value="institucional">
                                <input type="hidden" name="image_one_id" id="image_one_id" value="{{ isset($obj->image_one_id) ?  $obj->image_one_id : old('image_one_id')}}">
                                <input type="hidden" name="image_two_id" id="image_two_id" value="{{ isset($obj->image_two_id) ?  $obj->image_two_id : old('image_two_id')}}">
                                <input type="hidden" name="image_three_id" id="image_three_id" value="{{ isset($obj->image_three_id) ?  $obj->image_three_id : old('image_three_id')}}"> 
                                <input type="hidden" name="image_video_id" id="image_video_id" value="{{ isset($obj->image_video_id) ?  $obj->image_video_id : old('image_video_id')}}"> 
                                
                                <h5 class="title_section">Institucional</h5>
                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="chamada" id="chamada" class="validate form-control"
                                                    value="{{ isset($obj->chamada) ?  $obj->chamada : old('chamada')}}"
                                                    Placeholder="Chamada institucional" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <p>Texto Institucional</p>
                                            <div class="input-group">
                                                <textarea name="text" id="text" class="description">{{ isset($obj->text) ?  $obj->text : old('text')}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="nfografico" style="padding: 15px; background: #e1e1e1; margin-bottom: 10px">
                                    <div class="wrapper" >
                                        <h6 class="title_section">Links</h6>
                                        <p class="obs">Obs: Se o texto do link não for informado, o link não será exibido</p>
                                        @if (count(@$itens) > 0)
                                        @foreach($itens as $key => $item)
                                        <div class="input-box">
                                            <div class="row clearfix">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <div class="input-group ">
                                                            <input type="text" name="in_item_value[]" class="form-control" placeholder="Link" value="{{ $item['link'] }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <div class="input-group ">
                                                            <input type="text" name="in_item_text[]" class="form-control" placeholder="Texto Link" value="{{ $item['text'] }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <select name="in_item_target[]"  class="form-control" >
                                                                <option value="">Target</option>
                                                                <option value="0" {{ ($item['target'] == 0) ? 'selected' : '' }}>Abrir na mesma Aba</option>
                                                                <option value="1" {{ ($item['target'] == 1) ? 'selected' : '' }}>Abrir em nova Aba</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    @if ($key > 0) 
                                                    <button class="remove-lnk btn btn-icons btn-rounded btn-outline-primary" ><i class="mdi mdi-delete"></i></button>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>  
                                        @endforeach
                                        @else
                                        
                                        <div class="input-box">
                                            <div class="row clearfix">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <div class="input-group ">
                                                            <input type="text" name="in_item_value[]" placeholder="Link" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <div class="input-group ">
                                                            <input type="text" name="in_item_text[]" placeholder="Texto Link"  class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <select name="in_item_target[]"  class="form-control" >
                                                                <option value="">Target</option>
                                                                <option value="0" >Abrir na mesma Aba</option>
                                                                <option value="1" >Abrir em nova Aba</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                    
                                        @endif
                                        
                                    </div>   
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <p><br /></p>
                                            <div class="input-group">
                                                <button class="btn btn-icons btn-rounded btn-outline-primary add-btn"><i class="mdi mdi-plus"></i></button>
                                                
                                            </div>
                                        </div>
                                    </div>

                                </div>   
                                    


                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="link_text" id="link_text" class="validate form-control"
                                                    value="{{ isset($obj->link_text) ?  $obj->link_text : old('link_text')}}"
                                                    Placeholder="Texto Link" >
                                            </div>
                                            <p class="obs">Obs: Se o texto do link não for informado, o link não será exibido</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="link" id="link" class="validate form-control"
                                                    value="{{ isset($obj->link) ?  $obj->link : old('link')}}"
                                                    Placeholder="Link" >
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <select name="link_target" id="link_target" class="form-control" >
                                                    <option value="">Selecione um Target</option>
                                                    <option value="0" {{ ($obj->link_target == 0) ?  'selected' : ''}}>Abrir na mesma Aba</option>
                                                    <option value="1" {{ ($obj->link_target == 1) ?  'selected' : ''}}>Abrir em nova Aba</option>
                                                </select>
                                                
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                                
                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <p>Imagem (opcional)</p>
                                            <div class="input-group">
                                                <input type="file" name="image_one" id="image_one" data-model="home" data-inputid="image_one_id" class="form-control image">
                                            </div>
                                            <br />
                                            <img src="{{ isset($obj->image_one->id) ? $actual_link.$obj->image_one->directory.$obj->image_one->url : '' }}" width="150" class="fl-donwload image_one_id {{ isset($obj->image_one->id) ? '' : 'hide'}} ">
                                            @if ($obj->image_one_id)
                                                <a title="Remover Imagem" class="btn btn-icons btn-inverse-danger bt-excluir-img" data-token="{{ csrf_token() }}" data-id="{{ $obj->id }}" data-pagina="homes" data-campo="image_one_id"><i class="mdi mdi-delete"></i></a>    
                                            @endif
                                            <p class="erro_image_one_id"></p>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <p>Imagem (opcional)</p>
                                            <div class="input-group">
                                                <input type="file" name="image_two" id="image_two" data-model="home" data-inputid="image_two_id" class="form-control image">
                                            </div>
                                            <br />

                                            <img src="{{ isset($obj->image_two->id) ? $actual_link.$obj->image_two->directory.$obj->image_two->url : '' }}" width="150" class="fl-donwload image_two_id {{ isset($obj->image_two->id) ? '' : 'hide'}} ">
                                            @if ($obj->image_two_id)
                                                <a title="Remover Imagem" class="btn btn-icons btn-inverse-danger bt-excluir-img" data-token="{{ csrf_token() }}" data-id="{{ $obj->id }}" data-pagina="homes" data-campo="image_two_id"><i class="mdi mdi-delete"></i></a>    
                                            @endif
                                            <p class="erro_image_two_id"></p>
                                        </div>
                                    </div>
                                </div>

                            
                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <p>Imagem (opcional)</p>
                                            <div class="input-group">
                                                <input type="file" name="image_three" id="image_three" data-model="home" data-inputid="image_three_id" class="form-control image">
                                            </div>
                                            <br />

                                            <img src="{{ isset($obj->image_three->id) ? $actual_link.$obj->image_three->directory.$obj->image_three->url : '' }}" width="150" class="fl-donwload image_three_id {{ isset($obj->image_three->id) ? '' : 'hide'}} ">
                                            @if ($obj->image_three_id)
                                                <a title="Remover Imagem" class="btn btn-icons btn-inverse-danger bt-excluir-img" data-token="{{ csrf_token() }}" data-id="{{ $obj->id }}" data-pagina="homes" data-campo="image_three_id"><i class="mdi mdi-delete"></i></a>    
                                            @endif
                                            <p class="erro_image_three_id"></p>
                                        </div>
                                    </div>
                                </div>

                                <h5 class="title_section">Vídeo</h5>
                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="video_embed" id="video_embed" class="validate form-control"
                                                    value="{{ isset($obj->video_embed) ?  $obj->video_embed : old('video_embed')}}"
                                                    Placeholder="Vídeo Embed" >
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="video_title" id="video_title" class="validate form-control"
                                                    value="{{ isset($obj->video_title) ?  $obj->video_title : old('video_title')}}"
                                                    Placeholder="Vídeo Título" >
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <p>Imagem (opcional)</p>
                                            <div class="input-group">
                                                <input type="file" name="image_video" id="image_video" data-model="home" data-inputid="image_video_id" class="form-control image">
                                            </div>
                                            <br />
                                            
                                            <img src="{{ isset($obj->image_video->id) ? $actual_link.$obj->image_video->directory.$obj->image_video->url : '' }}" width="150" class="fl-donwload image_video_id {{ isset($obj->image_video->id) ? '' : 'hide'}} ">
                                            @if ($obj->image_video_id)
                                                <a title="Remover Imagem" class="btn btn-icons btn-inverse-danger bt-excluir-img" data-token="{{ csrf_token() }}" data-id="{{ $obj->id }}" data-pagina="homes" data-campo="image_video_id"><i class="mdi mdi-delete"></i></a>    
                                            @endif
                                        
                                            <p class="erro_image_video_id"></p>
                                        </div>
                                    </div>
                                </div>

                                <h5 class="title_section">Metas Tag</h5>
                                <div class="row clearfix">  
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text"  name="title_html" id="title_html" class="form-control" placeholder="Título Meta"  value="{{ isset($obj->title_html) ?  $obj->title_html :  old('title_html') }}" >
                                            </div>
                                        </div>
                                    </div>  
                                </div>
                                <div class="row clearfix">  
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text"  name="description_html" id="description_html" class="form-control" placeholder="Descrição Meta"  value="{{ isset($obj->description_html) ?  $obj->description_html :  old('description_html') }}" >
                                            </div>
                                        </div>
                                    </div>  
                                </div>


                                

                                <div class="row clearfix">
                                    <div class="col-sm-2">
                                        <button class="btn btn-primary submit-btn btn-block">Atualizar</button>
                                    </div>
                                </div>

                            </form>
                        </div> 
                        
                        {{-- CHAMADA CHAMADAS --}}
                        <div id="menu2" class="tab-pane {{ ($aba == 'chamadas') ?  'show in active' : 'fade aba' }} ">
                            <form action="{{ route('home.update', ["id"=> $obj->id, "action" => "chamadas"])}}" method="post"
                                    enctype="multipart/form-data" id="homeChamadas">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}

                                <input type="hidden" name="action"  value="chamadas">    
                                <input type="hidden" name="img_full_one_id" id="img_full_one_id" value="{{ isset($obj->img_full_one_id) ?  $obj->img_full_one_id : old('img_full_one_id')}}">
                                <input type="hidden" name="img_full_one_mob_id" id="img_full_one_mob_id" value="{{ isset($obj->img_full_one_mob_id) ?  $obj->img_full_one_mob_id : old('img_full_one_mob_id')}}">
                                <input type="hidden" name="img_full_two_id" id="img_full_two_id" value="{{ isset($obj->img_full_two_id) ?  $obj->img_full_two_id : old('img_full_two_id')}}">
                                
                                <h5 class="title_section">Chamada Full 1</h5>    
                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <p>Imagem Full</p>
                                            <div class="input-group">
                                                <input type="file" name="img_full_one" id="img_full_one" data-model="home" data-inputid="img_full_one_id" class="form-control image">
                                            </div>
                                            <br />

                                            <img src="{{ isset($obj->img_full_one->id) ? $actual_link.$obj->img_full_one->directory.$obj->img_full_one->url : '' }}" width="150" class="fl-donwload img_full_one_id {{ isset($obj->img_full_one->id) ? '' : 'hide'}} ">
                                            @if ($obj->img_full_one_id)
                                                <a title="Remover Imagem" class="btn btn-icons btn-inverse-danger bt-excluir-img" data-token="{{ csrf_token() }}" data-id="{{ $obj->id }}" data-pagina="homes" data-campo="img_full_one_id"><i class="mdi mdi-delete"></i></a>    
                                            @endif
                                            <p class="erro_img_full_one_id"></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <p>Imagem Full mobile</p>
                                            <div class="input-group">
                                                <input type="file" name="img_full_one_mob" id="img_full_one_mob" data-model="home" data-inputid="img_full_one_mob_id" class="form-control image">
                                            </div>
                                            <br />

                                            <img src="{{ isset($obj->img_full_one_mob->id) ? $actual_link.$obj->img_full_one_mob->directory.$obj->img_full_one_mob->url : '' }}" width="150" class="fl-donwload img_full_one_mob_id {{ isset($obj->img_full_one_mob->id) ? '' : 'hide'}} ">
                                            @if ($obj->img_full_one_mob_id)
                                                <a title="Remover Imagem" class="btn btn-icons btn-inverse-danger bt-excluir-img" data-token="{{ csrf_token() }}" data-id="{{ $obj->id }}" data-pagina="homes" data-campo="img_full_one_mob_id"><i class="mdi mdi-delete"></i></a>    
                                            @endif
                                            <p class="erro_img_full_one_mob_id"></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        
                                        <div class="form-group">
                                            <div class="input-group">
                                                <textarea name="tx_full_one" id="tx_full_one" placeholder="Texto Chamada Full" class="form-control" >{{ isset($obj->tx_full_one) ?  $obj->tx_full_one : old('tx_full_one')}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <select name="tx_postition_full_one" id="tx_postition_full_one" class="form-control" >
                                                    <option value="">Posição do Texto</option>
                                                    <option value="sl" {{ ($obj->tx_postition_full_one == 'sl') ?  'selected' : ''}}>Superior Esquerda</option>
                                                    <option value="sr" {{ ($obj->tx_postition_full_one == 'sr') ?  'selected' : ''}}>Superior Direita</option>
                                                    <option value="sc" {{ ($obj->tx_postition_full_one == 'sc') ?  'selected' : ''}}>Superior Centro</option>
                                                    <option value="cl" {{ ($obj->tx_postition_full_one == 'cl') ?  'selected' : ''}}>Centro Esquerda</option>
                                                    <option value="cr" {{ ($obj->tx_postition_full_one == 'cr') ?  'selected' : ''}}>Centro Direita</option>
                                                    <option value="cc" {{ ($obj->tx_postition_full_one == 'cc') ?  'selected' : ''}}>Centro</option>
                                                    <option value="il" {{ ($obj->tx_postition_full_one == 'il') ?  'selected' : ''}}>Inferior Esquerda</option>
                                                    <option value="ir" {{ ($obj->tx_postition_full_one == 'ir') ?  'selected' : ''}}>Inferior Direita</option>
                                                    <option value="ic" {{ ($obj->tx_postition_full_one == 'ic') ?  'selected' : ''}}>Inferior Centro</option>
                                                </select>
                                                
                                            </div>
                                        </div>
                                    </div>  
                                </div>  
                                
                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="lk_text_full_one" id="lk_text_full_one" class="validate form-control"
                                                    value="{{ isset($obj->lk_text_full_one) ?  $obj->lk_text_full_one : old('lk_text_full_one')}}"
                                                    Placeholder="Texto Link" >
                                            </div>
                                            <p class="obs">Obs: Se o texto do link não for informado, o link não será exibido</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="lk_full_one" id="lk_full_one" class="validate form-control"
                                                    value="{{ isset($obj->lk_full_one) ?  $obj->lk_full_one : old('lk_full_one')}}"
                                                    Placeholder="Link" >
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <select name="lk_target_full_one" id="lk_target_full_one" class="form-control" >
                                                    <option value="">Selecione um Target</option>
                                                    <option value="0" {{ ($obj->lk_target_full_one == 0) ?  'selected' : ''}}>Abrir na mesma Aba</option>
                                                    <option value="1" {{ ($obj->lk_target_full_one == 1) ?  'selected' : ''}}>Abrir em nova Aba</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                                <br />
                                {{-- <h5 class="title_section">Chamada Full 2</h5>    
                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <p>Imagem Full</p>
                                            <div class="input-group">
                                                <input type="file" name="img_full_two" id="img_full_two" data-model="home" data-inputid="img_full_two_id" class="form-control image">
                                            </div>
                                            <br />
                                            <img src="{{ isset($obj->img_full_two->id) ? $actual_link.$obj->img_full_two->directory.$obj->img_full_two->url : '' }}" width="150" class="fl-donwload img_full_two_id {{ isset($obj->img_full_two->id) ? '' : 'hide'}} ">
                                            <p class="erro_img_full_two_id"></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <textarea name="tx_full_two" id="tx_full_two" placeholder="Texto Chamda Full" class="form-control">{{ isset($obj->tx_full_two) ?  $obj->tx_full_two : old('tx_full_two')}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <select name="tx_postition_full_two" id="tx_postition_full_two" class="form-control" >
                                                    <option value="">Posição do Texto</option>
                                                    <option value="l" {{ ($obj->tx_postition_full_two == 'l') ?  'selected' : ''}}>Esquerda</option>
                                                    <option value="c" {{ ($obj->tx_postition_full_two == 'c') ?  'selected' : ''}}>Centro</option>
                                                    <option value="i" {{ ($obj->tx_postition_full_two == 'i') ?  'selected' : ''}}>Direita</option>
                                                </select>
                                                
                                            </div>
                                        </div>
                                    </div>  
                                </div>  
                                
                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="lk_text_full_two" id="lk_text_full_two" class="validate form-control"
                                                    value="{{ isset($obj->lk_text_full_two) ?  $obj->lk_text_full_two : old('lk_text_full_two')}}"
                                                    Placeholder="Texto Link" >
                                            </div>
                                            <p class="obs">Obs: Se o texto do link não for informado, o link não será exibido</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="lk_full_two" id="lk_full_two" class="validate form-control"
                                                    value="{{ isset($obj->lk_full_two) ?  $obj->lk_full_two : old('lk_full_two')}}"
                                                    Placeholder="Link" >
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <select name="lk_target_full_two" id="lk_target_full_two" class="form-control" >
                                                    <option value="">Selecione um Target</option>
                                                    <option value="0" {{ ($obj->lk_target_full_two == 0) ?  'selected' : ''}}>Abrir na mesma Aba</option>
                                                    <option value="1" {{ ($obj->lk_target_full_two == 1) ?  'selected' : ''}}>Abrir em nova Aba</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>    
                                </div> --}}


                                <div class="row clearfix">
                                    <div class="col-sm-2">
                                        <button class="btn btn-primary submit-btn btn-block">Atualizar</button>
                                    </div>
                                </div>

                            </form>
                        </div>    

                        {{-- CHAMADA SPLIT --}}
                        <div id="menu3" class="tab-pane {{ ($aba == 'split') ?  'show in active' : 'fade aba' }} ">
                            <form action="{{ route('home.update', ["id"=> $obj->id, "action" => "split"])}}" method="post"
                                    enctype="multipart/form-data" id="homeSplit">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}

                                <input type="hidden" name="action"  value="split">    
                                <input type="hidden" name="img_split_left_id" id="img_split_left_id" value="{{ isset($obj->img_split_left_id) ?  $obj->img_split_left_id : old('img_split_left_id')}}">
                                <input type="hidden" name="img_split_right_id" id="img_split_right_id" value="{{ isset($obj->img_split_right_id) ?  $obj->img_split_right_id : old('img_split_right_id')}}">
                                <input type="hidden" name="img_split_full_id" id="img_split_full_id" value="{{ isset($obj->img_split_full_id) ?  $obj->img_split_full_id : old('img_split_full_id')}}">
                                {{-- CHAMADA SPLIT LEFT --}}
                                <h5 class="title_section">Chamada Split Esquerda</h5>    
                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <p>Imagem </p>
                                            <div class="input-group">
                                                <input type="file" name="img_split_left" id="img_split_left" data-model="home" data-inputid="img_split_left_id" class="form-control image">
                                            </div>
                                            <br />

                                            <img src="{{ isset($obj->img_split_left->id) ? $actual_link.$obj->img_split_left->directory.$obj->img_split_left->url : '' }}" width="150" class="fl-donwload img_split_left_id {{ isset($obj->img_split_left->id) ? '' : 'hide'}} ">
                                            @if ($obj->img_split_left_id)
                                                <a title="Remover Imagem" class="btn btn-icons btn-inverse-danger bt-excluir-img" data-token="{{ csrf_token() }}" data-id="{{ $obj->id }}" data-pagina="homes" data-campo="img_split_left_id"><i class="mdi mdi-delete"></i></a>    
                                            @endif
                                            <p class="erro_img_split_left_id"></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <textarea name="tx_split_left" id="tx_split_left" placeholder="Texto Chamada" class="form-control">{{ isset($obj->tx_split_left) ?  $obj->tx_split_left : old('tx_split_left')}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <select name="tx_postition_split_left" id="tx_postition_split_left" class="form-control" >
                                                    <option value="">Posição do Texto</option>
                                                    <option value="s" {{ ($obj->tx_postition_split_left == 's') ?  'selected' : ''}}>Superior</option>
                                                    <option value="c" {{ ($obj->tx_postition_split_left == 'c') ?  'selected' : ''}}>Centro</option>
                                                    <option value="i" {{ ($obj->tx_postition_split_left == 'i') ?  'selected' : ''}}>Inferior</option>
                                                </select>
                                                
                                            </div>
                                        </div>
                                    </div>  
                                </div>  
                                
                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="lk_text_split_left" id="lk_text_split_left" class="validate form-control"
                                                    value="{{ isset($obj->lk_text_split_left) ?  $obj->lk_text_split_left : old('lk_text_split_left')}}"
                                                    Placeholder="Texto Link" >
                                            </div>
                                            <p class="obs">Obs: Se o texto do link não for informado, o link não será exibido</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="lk_split_left" id="lk_split_left" class="validate form-control"
                                                    value="{{ isset($obj->lk_split_left) ?  $obj->lk_split_left : old('lk_split_left')}}"
                                                    Placeholder="Link" >
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <select name="lk_target_split_left" id="lk_target_split_left" class="form-control" >
                                                    <option value="">Selecione um Target</option>
                                                    <option value="0" {{ ($obj->lk_target_split_left == 0) ?  'selected' : ''}}>Abrir na mesma Aba</option>
                                                    <option value="1" {{ ($obj->lk_target_split_left == 1) ?  'selected' : ''}}>Abrir em nova Aba</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                                <br />
                                {{-- CHAMADA SPLIT DIREITA --}}
                                <h5 class="title_section">Chamada Split Direita</h5>    
                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <p>Imagem </p>
                                            <div class="input-group">
                                                <input type="file" name="img_split_right" id="img_split_right" data-model="home" data-inputid="img_split_right_id" class="form-control image">
                                            </div>
                                            <br />

                                            <img src="{{ isset($obj->img_split_right->id) ? $actual_link.$obj->img_split_right->directory.$obj->img_split_right->url : '' }}" width="150" class="fl-donwload img_split_right_id {{ isset($obj->img_split_right->id) ? '' : 'hide'}} ">
                                            @if ($obj->img_split_right_id)
                                                <a title="Remover Imagem" class="btn btn-icons btn-inverse-danger bt-excluir-img" data-token="{{ csrf_token() }}" data-id="{{ $obj->id }}" data-pagina="homes" data-campo="img_split_right_id"><i class="mdi mdi-delete"></i></a>    
                                            @endif
                                            <p class="erro_img_split_right_id"></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <textarea name="tx_split_right" id="tx_split_right" placeholder="Texto Chamada" class="form-control">{{ isset($obj->tx_split_right) ?  $obj->tx_split_right : old('tx_split_right')}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <select name="tx_postition_split_right" id="tx_postition_split_right" class="form-control" >
                                                    <option value="">Posição do Texto</option>
                                                    <option value="s" {{ ($obj->tx_postition_split_right == 's') ?  'selected' : ''}}>Superior</option>
                                                    <option value="c" {{ ($obj->tx_postition_split_right == 'c') ?  'selected' : ''}}>Centro</option>
                                                    <option value="i" {{ ($obj->tx_postition_split_right == 'i') ?  'selected' : ''}}>Inferior</option>
                                                </select>
                                                
                                            </div>
                                        </div>
                                    </div>  
                                </div>  
                                
                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="lk_text_split_right" id="lk_text_split_right" class="validate form-control"
                                                    value="{{ isset($obj->lk_text_split_right) ?  $obj->lk_text_split_right : old('lk_text_split_right')}}"
                                                    Placeholder="Texto Link" >
                                            </div>
                                            <p class="obs">Obs: Se o texto do link não for informado, o link não será exibido</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="lk_split_right" id="lk_split_right" class="validate form-control"
                                                    value="{{ isset($obj->lk_split_right) ?  $obj->lk_split_right : old('lk_split_right')}}"
                                                    Placeholder="Link" >
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <select name="lk_target_split_right" id="lk_target_split_right" class="form-control" >
                                                    <option value="">Selecione um Target</option>
                                                    <option value="0" {{ ($obj->lk_target_split_right == 0) ?  'selected' : ''}}>Abrir na mesma Aba</option>
                                                    <option value="1" {{ ($obj->lk_target_split_right == 1) ?  'selected' : ''}}>Abrir em nova Aba</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                                {{-- CHAMADA SPLIT BOTTOM --}}
                                <h5 class="title_section">Chamada Full</h5>    
                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <p>Imagem Full</p>
                                            <div class="input-group">
                                                <input type="file" name="img_split_full" id="img_split_full" data-model="home" data-inputid="img_split_full_id" class="form-control image">
                                            </div>
                                            <br />

                                            <img src="{{ isset($obj->img_split_full->id) ? $actual_link.$obj->img_split_full->directory.$obj->img_split_full->url : '' }}" width="150" class="fl-donwload img_split_full_id {{ isset($obj->img_split_full->id) ? '' : 'hide'}} ">
                                            @if ($obj->img_split_full_id)
                                                <a title="Remover Imagem" class="btn btn-icons btn-inverse-danger bt-excluir-img" data-token="{{ csrf_token() }}" data-id="{{ $obj->id }}" data-pagina="homes" data-campo="img_split_full_id"><i class="mdi mdi-delete"></i></a>    
                                            @endif
                                            <p class="erro_img_split_full_id"></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <textarea name="tx_split_full" id="tx_split_full" placeholder="Texto Chamada Full" class="form-control">{{ isset($obj->tx_split_full) ?  $obj->tx_split_full : old('tx_split_full')}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <select name="tx_postition_split_full" id="tx_postition_split_full" class="form-control" >
                                                    <option value="">Posição do Texto</option>
                                                    <option value="sl" {{ ($obj->tx_postition_split_full == 'sl') ?  'selected' : ''}}>Superior Esquerda</option>
                                                    <option value="sr" {{ ($obj->tx_postition_split_full == 'sr') ?  'selected' : ''}}>Superior Direita</option>
                                                    <option value="sc" {{ ($obj->tx_postition_split_full == 'sc') ?  'selected' : ''}}>Superior Centro</option>
                                                    <option value="cl" {{ ($obj->tx_postition_split_full == 'cl') ?  'selected' : ''}}>Centro Esquerda</option>
                                                    <option value="cr" {{ ($obj->tx_postition_split_full == 'cr') ?  'selected' : ''}}>Centro Direita</option>
                                                    <option value="cc" {{ ($obj->tx_postition_split_full == 'cc') ?  'selected' : ''}}>Centro</option>
                                                    <option value="il" {{ ($obj->tx_postition_split_full == 'il') ?  'selected' : ''}}>Inferior Esquerda</option>
                                                    <option value="ir" {{ ($obj->tx_postition_split_full == 'ir') ?  'selected' : ''}}>Inferior Direita</option>
                                                    <option value="ic" {{ ($obj->tx_postition_split_full == 'ic') ?  'selected' : ''}}>Inferior Centro</option>
                                                </select>
                                                
                                            </div>
                                        </div>
                                    </div>  
                                </div>  
                                
                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="lk_text_split_full" id="lk_text_split_full" class="validate form-control"
                                                    value="{{ isset($obj->lk_text_split_full) ?  $obj->lk_text_split_full : old('lk_text_split_full')}}"
                                                    Placeholder="Texto Link" >
                                            </div>
                                            <p class="obs">Obs: Se o texto do link não for informado, o link não será exibido</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="lk_split_full" id="lk_split_full" class="validate form-control"
                                                    value="{{ isset($obj->lk_split_full) ?  $obj->lk_split_full : old('lk_split_full')}}"
                                                    Placeholder="Link" >
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <select name="lk_target_split_full" id="lk_target_split_full" class="form-control" >
                                                    <option value="">Selecione um Target</option>
                                                    <option value="0" {{ ($obj->lk_target_split_full == 0) ?  'selected' : ''}}>Abrir na mesma Aba</option>
                                                    <option value="1" {{ ($obj->lk_target_split_full == 1) ?  'selected' : ''}}>Abrir em nova Aba</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>    
                                </div>


                                <div class="row clearfix">
                                    <div class="col-sm-2">
                                        <button class="btn btn-primary submit-btn btn-block">Atualizar</button>
                                    </div>
                                </div>

                            </form>
                        </div>     
                        
                        <div id="menu5" class="tab-pane {{ ($aba == 'half') ?  'show in active' : 'fade aba' }} ">
                            @include('admin.home.institucional.form-half')
                        </div>
                        
                        {{-- CHAMADA CTAS --}}
                        <div id="menu4" class="tab-pane {{ ($aba == 'ctas') ?  'show in active' : 'fade aba' }} ">
                            <form action="{{ route('home.update', ["id"=> $obj->id, "action" => "ctas"])}}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}

                                <input type="hidden" name="action"  value="ctas">
                                
                                <h5 class="title_section">CTA Lançamento</h5>
                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="cta_lancamento_text" id="cta_lancamento_text" class="validate form-control"
                                                    value="{{ isset($obj->cta_lancamento_text) ?  $obj->cta_lancamento_text : old('cta_lancamento_text')}}"
                                                    Placeholder="Texto Link" >
                                            </div>
                                            <p class="obs">Obs: Se o texto do link não for informado, o link não será exibido</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="cta_lancamento_link" id="cta_lancamento_link" class="validate form-control"
                                                    value="{{ isset($obj->cta_lancamento_link) ?  $obj->cta_lancamento_link : old('cta_lancamento_link')}}"
                                                    Placeholder="Link" >
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <select name="cta_lancamento_target" id="cta_lancamento_target" class="form-control" >
                                                    <option value="">Selecione um Target</option>
                                                    <option value="0" {{ ($obj->cta_lancamento_target == 0) ?  'selected' : ''}}>Abrir na mesma Aba</option>
                                                    <option value="1" {{ ($obj->cta_lancamento_target == 1) ?  'selected' : ''}}>Abrir em nova Aba</option>
                                                </select>
                                                
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                                
                                <h5 class="title_section">CTA Notícias</h5>
                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="cta_news_text" id="cta_news_text" class="validate form-control"
                                                    value="{{ isset($obj->cta_news_text) ?  $obj->cta_news_text : old('cta_news_text')}}"
                                                    Placeholder="Texto Link" >
                                            </div>
                                            <p class="obs">Obs: Se o texto do link não for informado, o link não será exibido</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" name="cta_news_link" id="cta_news_link" class="validate form-control"
                                                    value="{{ isset($obj->cta_news_link) ?  $obj->cta_news_link : old('cta_news_link')}}"
                                                    Placeholder="Link" >
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <select name="cta_news_target" id="cta_news_target" class="form-control" >
                                                    <option value="">Selecione um Target</option>
                                                    <option value="0" {{ ($obj->cta_news_target == 0) ?  'selected' : ''}}>Abrir na mesma Aba</option>
                                                    <option value="1" {{ ($obj->cta_news_target == 1) ?  'selected' : ''}}>Abrir em nova Aba</option>
                                                </select>
                                                
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                                

                                <div class="row clearfix">
                                    <div class="col-sm-2">
                                        <button class="btn btn-primary submit-btn btn-block">Atualizar</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush