@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
  

  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      
      <div class="card-body">

        @if (session('status'))
            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                </div>
            </div>
        @endif 

        <div style="clear: both;">
            <h4 class="card-title">About</h4>
        </div>
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th> Título </th>
                <th> Idioma </th>
                <th> Ações </th>
              </tr>
            </thead>
            <tbody>
            @forelse($objs as $obj)
              <tr>
                <td> 
                  {{strip_tags($obj->titulo)}} 
                  
                </td>
                <td> 
                    @if ($obj->language_id == 'pt')  <a title="Editar" href="{{url('admin/about/'.$obj->id.'/edit')}}"><img class="bandeira" src="{{url('assets/images/brand_icons/pt.jpg')}}"></a> @endif
                    @if ($obj->language_id == 'es')  <a title="Editar" href="{{url('admin/about/'.$obj->id.'/edit')}}"><img class="bandeira" src="{{url('assets/images/brand_icons/es.jpg')}}"></a> @endif
                    @if ($obj->language_id == 'en')  <a title="Editar" href="{{url('admin/about/'.$obj->id.'/edit')}}"><img class="bandeira" src="{{url('assets/images/brand_icons/en.jpg')}}"></a> @endif
                </td>
                <td> 
                    @if ($obj->language_id == 'pt')  <a title="Editar" class="btn btn-icons btn-inverse-warning" href="{{url('admin/about/'.$obj->id.'/edit')}}"><i class="mdi mdi-grease-pencil"></i></a>@endif
                    @if ($obj->language_id == 'es')  <a title="Editar" class="btn btn-icons btn-inverse-warning" href="{{url('admin/about/'.$obj->id.'/edit')}}"><i class="mdi mdi-grease-pencil"></i></a> @endif
                    @if ($obj->language_id == 'en')  <a title="Editar" class="btn btn-icons btn-inverse-warning" href="{{url('admin/about/'.$obj->id.'/edit')}}"><i class="mdi mdi-grease-pencil"></i></a> @endif
                </td>
              </tr>
              @empty
            </tbody>
              <tr>
                <td colspan="6">Nenhum registro encontrado.</td>
              </tr>
            @endforelse
          </table>
          {{ $objs->appends(request()->input())->links() }}
        </div>
      </div>
    </div>
  </div>
  
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush

        