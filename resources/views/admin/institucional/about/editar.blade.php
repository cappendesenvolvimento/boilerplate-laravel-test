@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
    <div class="col s12">
        @include('componentes._breadcrumbs')
    </div>

    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">

            <div class="card-body">

                @if (session('status'))
                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        </div>
                    </div>
                @endif

                <h4 class="card-title">Editar About</h4>
                <p class="card-description"> </p>

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif

                <div class="auto-form-wrapper">

                    
                    <form action="{{ route('about.update', $obj->id)}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <input type="hidden" name="old_image" value="{{ isset($obj->image) ? $obj->image : '' }}" >
                        <input type="hidden" name="old_banner" value="{{ isset($obj->banner) ? $obj->banner : '' }}" >
                        <input type="hidden" name="old_foto1" value="{{ isset($obj->foto1) ? $obj->foto1 : '' }}" >
                        <input type="hidden" name="old_foto2" value="{{ isset($obj->foto2) ? $obj->foto2 : '' }}" >
                        <input type="hidden" name="old_imagectacontato" value="{{ isset($obj->imagectacontato) ? $obj->imagectacontato : '' }}" >
                        <input type="hidden" name="old_imagectasol" value="{{ isset($obj->imagectasolucao) ? $obj->imagectasolucao : '' }}" >
                        
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="titulo" id="titulo" class="validate form-control"
                                            Placeholder="Titulo About" value="{{ isset($obj->titulo) ?  $obj->titulo : old('titulo')}}" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-field">
                                        <select name="language_id" id="language_id" class="form-control" required>
                                            <option value="">Selecione um Idioma</option>
                                            <option value="pt" {{ ($obj->language_id == 'pt') ? 'selected' : '' }}>Português</option>
                                            <option value="es" {{ ($obj->language_id == 'es') ? 'selected' : '' }}>Espanhol</option>
                                            <option value="en" {{ ($obj->language_id == 'en') ? 'selected' : '' }}>Inglês</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <p>Banner</p>
                                    <div class="input-group">
                                        <input type="file" name="banner" id="banner" class="form-control">
                                    </div>
                                    <p>Tamanho da imagem: 1920 x 1066px</p>
                                </div>
                            </div>
                        </div>

                        @if( $obj->banner != '' )
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group content-img-banner">
                                    <img src="{{ isset($obj->banner) ?   url('storage/about/'.$obj->banner) : '' }}"  width="200px"  />
                                    <a title="Remover Imagem" class="btn btn-icons btn-inverse-danger bt-excluir-img" data-token="{{ csrf_token() }}" data-id="{{ $obj->id }}" data-pagina="about" data-campo="banner"><i class="mdi mdi-delete"></i></a>    
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif

                        <h5 class="title_section">Timeline</h5>

                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="titulo_timeline" id="titulo_timeline" class="validate form-control"
                                            Placeholder="Títilo Timeline" value="{{ isset($obj->titulo_timeline) ?  $obj->titulo_timeline : old('titulo_timeline')}}" >
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h5 class="title_section">História</h5>
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="titulo_hist" id="titulo_hist" class="validate form-control"
                                            Placeholder="Títilo História" value="{{ isset($obj->titulo_hist) ?  $obj->titulo_hist : old('titulo_hist')}}" >
                                    </div>
                                </div>
                            </div>
                        </div>

                        
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <p>Descrição</p>
                                    <div class="input-group">
                                        <textarea name="descricao_hist" id="descricao_hist" class="description">{{ isset($obj->descricao_hist) ?  $obj->descricao_hist : old('descricao_hist')}}</textarea>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <p>Missão</p>
                                    <div class="input-group">
                                        <textarea name="missao" id="missao" class="description">{{ isset($obj->missao) ?  $obj->missao : old('missao')}}</textarea>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <p>Visão</p>
                                    <div class="input-group">
                                        <textarea name="visao" id="visao" class="description">{{ isset($obj->visao) ?  $obj->visao : old('visao')}}</textarea>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <p>Valores</p>
                                    <div class="input-group">
                                        <textarea name="valores" id="valores" class="description">{{ isset($obj->valores) ?  $obj->valores : old('valores')}}</textarea>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <p>Imagem História</p>
                                    <div class="input-group">
                                        <input type="file" name="image" id="image" class="form-control">
                                    </div>
                                    <p>Tamanho da imagem: 1920 x 1066px</p>
                                </div>
                            </div>
                        </div>

                        @if( $obj->image != '' )
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group content-img-image">
                                    <img src="{{ isset($obj->image) ?   url('storage/about/'.$obj->image) : '' }}"  width="200px"  />
                                    <a title="Remover Imagem" class="btn btn-icons btn-inverse-danger bt-excluir-img" data-token="{{ csrf_token() }}" data-id="{{ $obj->id }}" data-pagina="about" data-campo="image"><i class="mdi mdi-delete"></i></a>    
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif

                        <h5 class="title_section">Liderança</h5>
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="titulo_lider" id="titulo_lider" class="validate form-control"
                                            Placeholder="Título Liderança" value="{{ isset($obj->titulo_lider) ?  $obj->titulo_lider : old('titulo_lider')}}" >
                                    </div>
                                </div>
                            </div>
                        </div>

                        
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <p>Descrição Liderança</p>
                                    <div class="input-group">
                                        <textarea name="descricao_lider" id="descricao_lider" class="description">{{ isset($obj->descricao_lider) ?  $obj->descricao_lider : old('descricao_lider')}}</textarea>

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <p>Foto Lider</p>
                                    <div class="input-group">
                                        <input type="file" name="foto1" id="foto1" class="form-control">
                                    </div>
                                    <p>Tamanho da imagem: 1920 x 1066px</p>
                                </div>
                            </div>
                        </div>

                        @if( $obj->foto1 != '' )
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group content-img-foto1">
                                    <img src="{{ isset($obj->foto1) ?   url('storage/about/'.$obj->foto1) : '' }}"  width="200px"  />
                                    <a title="Remover Imagem" class="btn btn-icons btn-inverse-danger bt-excluir-img" data-token="{{ csrf_token() }}" data-id="{{ $obj->id }}" data-pagina="about" data-campo="foto1"><i class="mdi mdi-delete"></i></a>    
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif     
                        
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="foto1_nome" id="foto1_nome" class="validate form-control"
                                            Placeholder="Nome Lider" value="{{ isset($obj->foto1_nome) ?  $obj->foto1_nome : old('foto1_nome')}}" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="foto1_titulo" id="foto1_titulo" class="validate form-control"
                                            Placeholder="Titularidade Lider" value="{{ isset($obj->foto1_titulo) ?  $obj->foto1_titulo : old('foto1_titulo')}}" >
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <p>Foto Lider</p>
                                    <div class="input-group">
                                        <input type="file" name="foto2" id="foto2" class="form-control">
                                    </div>
                                    <p>Tamanho da imagem: 1920 x 1066px</p>
                                </div>
                            </div>
                        </div>

                        @if( $obj->foto2 != '' )
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group content-img-foto2">
                                    <img src="{{ isset($obj->foto2) ?   url('storage/about/'.$obj->foto2) : '' }}"  width="200px"  />
                                    <a title="Remover Imagem" class="btn btn-icons btn-inverse-danger bt-excluir-img" data-token="{{ csrf_token() }}" data-id="{{ $obj->id }}" data-pagina="about" data-campo="foto2"><i class="mdi mdi-delete"></i></a>    
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif      
                        
                         
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="foto2_nome" id="foto2_nome" class="validate form-control"
                                            Placeholder="Nome Lider" value="{{ isset($obj->foto2_nome) ?  $obj->foto2_nome : old('foto2_nome')}}" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="foto2_titulo" id="foto2_titulo" class="validate form-control"
                                            Placeholder="Titularidade Lider" value="{{ isset($obj->foto2_titulo) ?  $obj->foto2_titulo : old('foto2_titulo')}}" >
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h5 class="title_section">CTA Soluções</h5>
                        
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="tituloctasol" id="tituloctasol" class="validate form-control"
                                            Placeholder="Título Cta Solução" value="{{ isset($obj->tituloctasol) ?  $obj->tituloctasol : old('tituloctasol')}}" >
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="btnctasol" id="btnctasol" class="validate form-control"
                                            Placeholder="Botão CTA Solução" value="{{ isset($obj->btnctasol) ?  $obj->btnctasol : old('btnctasol')}}" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="linkctasol" id="linkctasol" class="validate form-control"
                                            Placeholder="Link CTA Solução" value="{{ isset($obj->linkctasol) ?  $obj->linkctasol : old('linkctasol')}}" >
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <p>Imagem Background</p>
                                    <div class="input-group">
                                        <input type="file" name="imagectasol" id="imagectasol" class="form-control">
                                    </div>
                                    <p>Tamanho da imagem: 1920 x 640px</p>
                                </div>
                            </div>
                        </div>
                        @if( $obj->imagectasol != '' )
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group content-img-imagectasol">
                                    <img src="{{ isset($obj->imagectasol) ?   url('storage/about/'.$obj->imagectasol) : '' }}"  width="200px"  />
                                    <a title="Remover Imagem" class="btn btn-icons btn-inverse-danger bt-excluir-img" data-token="{{ csrf_token() }}" data-id="{{ $obj->id }}" data-pagina="about" data-campo="imagectasol"><i class="mdi mdi-delete"></i></a>    
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif

                        <h5 class="title_section">CTA Contato</h5>
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="cta_contato" id="cta_contato" class="validate form-control"
                                            Placeholder="Título Cta Contato" value="{{ isset($obj->cta_contato) ?  $obj->cta_contato : old('cta_contato')}}" >
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="btnctacontato" id="btnctacontato" class="validate form-control"
                                            Placeholder="Botão CTA Solução" value="{{ isset($obj->btnctacontato) ?  $obj->btnctacontato : old('btnctacontato')}}" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="linkctacontato" id="linkctacontato" class="validate form-control"
                                            Placeholder="Link CTA Solução" value="{{ isset($obj->linkctacontato) ?  $obj->linkctacontato : old('linkctacontato')}}" >
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <p>Imagem Background</p>
                                    <div class="input-group">
                                        <input type="file" name="imagectacontato" id="imagectacontato" class="form-control">
                                    </div>
                                    <p>Tamanho da imagem: 1920 x 640px</p>
                                </div>
                            </div>
                        </div>
                        @if( $obj->imagectacontato != '' )
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group content-img-imagectacontato">
                                    <img src="{{ isset($obj->imagectacontato) ?   url('storage/about/'.$obj->imagectacontato) : '' }}"  width="200px"  />
                                    <a title="Remover Imagem" class="btn btn-icons btn-inverse-danger bt-excluir-img" data-token="{{ csrf_token() }}" data-id="{{ $obj->id }}" data-pagina="about" data-campo="imagectacontato"><i class="mdi mdi-delete"></i></a>    
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        

                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <button class="btn btn-primary submit-btn btn-block">Atualizar</button>
                            </div>
                        </div>
                    
                    </form>  
                   
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush