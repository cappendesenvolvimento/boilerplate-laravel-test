@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')

@php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/" ;    
@endphp

<div class="row">
    <div class="col s12">
        @include('componentes.breadcrumbs')
    </div>

    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">

            <div class="card-body">

                @if (session('status'))
                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        </div>
                    </div>
                @endif

                <h4 class="card-title">Editar Páginas</h4>
                <p class="card-description"> </p>

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif

                <div class="auto-form-wrapper">
                    
                
                    <form action="{{ route('pagina.update', $obj->id)}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <input type="hidden" name="featured_img_id" id="featured_img_id" value="{{ isset($obj->featured_img_id) ?  $obj->featured_img_id : old('featured_img_id')}}">
                        <input type="hidden" name="featured_img_mobile_id" id="featured_img_mobile_id" value="{{ isset($obj->featured_img_mobile_id) ?  $obj->featured_img_mobile_id : old('featured_img_mobile_id')}}">

                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="title" id="title" class="validate form-control"
                                            value="{{ isset($obj->title) ?  $obj->title : old('title')}}"
                                            Placeholder="Título da Página" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        

                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <p>Imagem Banner</p>
                                    <div class="input-group">
                                        <input type="file" name="image" id="image" data-model="pagina" data-inputid="featured_img_id" class="form-control image">
                                    </div>
                                    <br />
                                    <p><img src="{{ isset($obj->featured_img->id) ? $actual_link.$obj->featured_img->directory.$obj->featured_img->url : '' }}" width="150" class="fl-donwload featured_img_id {{ isset($obj->featured_img->id) ? : 'hide'}} "></p>
                                </div>
                            </div>
                        </div>
            

                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <p>Imagem Mobile</p>
                                    <div class="input-group">
                                        <input type="file" name="image_mobile" id="image_mobile" data-model="pagina" data-inputid="featured_img_mobile_id" class="form-control image">
                                    </div>
                                    <br />
                                    <p><img src="{{ isset($obj->featured_img_mobile->id) ? $actual_link. $obj->featured_img_mobile->directory.$obj->featured_img_mobile->url : '' }}" width="150" class="fl-donwload featured_img_mobile_id {{ isset($obj->featured_img_mobile->id) ? : 'hide'}}"></p>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="title_banner" id="title_banner" class="validate form-control"
                                            value="{{ isset($obj->title_banner) ?  $obj->title_banner : old('title_banner')}}"
                                            Placeholder="Título Banner" >
                                    </div>
                                </div>
                            </div>
                        </div>

                        

                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <p>Descrição</p>
                                    <div class="input-group">
                                        <textarea name="description" id="description" class="description">{{ isset($obj->description) ?  $obj->description : old('description')}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <p>Descrição EN</p>
                                    <div class="input-group">
                                        <textarea name="description_en" id="description_en" class="description">{{ isset($obj->description_en) ?  $obj->description_en : old('description_en')}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @php
                        $datePubArray = explode(" ", $obj->date_published);
                        $datePub = date('Y-m-d', strtotime($datePubArray[0]));
                        @endphp

                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="date" name="date_published" id="date_published" class="validate form-control "
                                            value="{{ ($obj->date_published) ?  $datePub : old('date_published')}}"
                                            Placeholder="Data de Publicação" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-field">
                                        <select name="status" id="status" class="form-control c-tipo__blog" required>
                                            <option value="">Status</option>
                                            <option value="1"  {{ $obj->status == '1' ?  'selected' : ''}}>Ativo</option>
                                            <option value="0"  {{ $obj->status == '0' ?  'selected' : ''}}>Inativo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="hour_published" id="hour_published" class="validate form-control"
                                            value="{{ ($obj->date_published) ?  substr(end($datePubArray),0,5) : old('hour_published')}}"
                                            Placeholder="Hora de Publicação" >
                                    </div>
                                </div>
                            </div> --}}
                        </div>   
                                
                        <h5 class="title_section">CTA para Contato</h5>
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="cta_text" id="cta_text" class="validate form-control"
                                            value="{{ isset($obj->cta_text) ?  $obj->cta_text : old('cta_text')}}"
                                            Placeholder="Cta Texto" >
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="cta_button" id="cta_button" class="validate form-control"
                                            value="{{ isset($obj->cta_button) ?  $obj->cta_button : old('cta_button')}}"
                                            Placeholder="Texto Botão" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="cta_btcolor" id="cta_bt{{ isset($obj->cta_btcolor) ?  $obj->cta_btcolor : old('cta_btcolor')}}color" class="validate form-control"
                                            value=""
                                            Placeholder="Cor do botão" >
                                    </div>
                                </div>
                            </div>
                        </div>   
                        
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="cta_link" id="cta_link" class="validate form-control"
                                            value="{{ isset($obj->cta_link) ?  $obj->cta_link : old('cta_link')}}"
                                            Placeholder="Link Cta" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <select name="target" id="target" class="form-control" >
                                            <option value="">Selecione um Target</option>
                                            <option value="0" {{ $obj->target == '0' ?  'selected' : ''}}>Abrir na mesma Aba</option>
                                            <option value="1" {{ $obj->target == '1' ?  'selected' : ''}}>Abrir em nova Aba</option>
                                        </select>
                                    </div>
                                </div>
                            </div>   
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-check form-check-flat">
                                    <label class="form-check-label">
                                      <input type="checkbox" class="form-check-input" name="bt_proposta" value="1" {{ $obj->bt_proposta == '1' ?  'checked' : ''}} >Exibir botão "Solicitar Proposta" 
                                    </label>
                                </div>
                            </div>  
                        </div>

                        <h5 class="title_section">Metas Tag</h5>
                        <div class="row clearfix">  
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text"  name="title_html" id="title_html" class="form-control" placeholder="Título Meta"  value="{{ isset($obj->title_html) ?  $obj->title_html :  old('title_html') }}" >
                                    </div>
                                </div>
                            </div>  
                        </div>
                        <div class="row clearfix">  
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text"  name="description_html" id="description_html" class="form-control" placeholder="Descrição Meta"  value="{{ isset($obj->description_html) ?  $obj->description_html :  old('description_html') }}" >
                                    </div>
                                </div>
                            </div>  
                        </div>


                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <button class="btn btn-primary submit-btn btn-block">Atualizar</button>
                            </div>
                        </div>
                    
                    </form>    
                        
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush