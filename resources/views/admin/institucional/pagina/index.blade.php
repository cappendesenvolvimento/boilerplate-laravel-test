@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
  

  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      
      <div class="card-body">


        @if (session('status'))
            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                </div>
            </div>
        @endif  
             
        @can('paginas-create')
        <div>
          <a href="{{ route('pagina.create') }}" class="btn btn-primary btn-fw btn-add"><i class="mdi mdi-plus"></i>Adicionar</a>
        </div>
        @endcan
        <h4 class="card-title">Páginas</h4>
        <p class="card-description"> Listagem de páginas </p>
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th> Id </th>
                <th> Título </th>
                <th> slug </th>
                <th> Ações </th>
              </tr>
            </thead>
            <tbody>
            @forelse($objs as $obj)
              <tr>
                <td> {{ $obj->id }} </td>
                <td> {{$obj->title}}</td>
                <td> {{$obj->slug}}</td>
               
                <td> 
                    <form action="{{route('pagina.destroy', $obj->id)}}" method="post">
                        @can('paginas-edit')
                            <a title="Editar" class="btn btn-icons btn-inverse-warning" href="{{route('pagina.edit', $obj->id)}}"><i class="mdi mdi-grease-pencil"></i></a>
                        @endcan
                        @can('paginas-delete')
                            {{method_field('DELETE')}}
                            {{csrf_field()}}
                            <button title="Deletar" class="btn btn-icons btn-inverse-danger"><i class="mdi mdi-delete"></i></button>   
                        @endcan
                        
                    </form>     
                </td>
              </tr>
              @empty
            </tbody>
              <tr>
                <td colspan="6">Nenhum registro encontrado.</td>
              </tr>
            @endforelse
          </table>
          {{ $objs->appends(request()->input())->links() }}
        </div>
      </div>
    </div>
  </div>
  
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush

        