@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
    <div class="col s12">
        @include('componentes._breadcrumbs')
    </div>

    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">

            <div class="card-body">

                <h4 class="card-title">Adicionar Depoimento</h4>
                <p class="card-description"> </p>

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif

                <div class="auto-form-wrapper">
                    <form action="{{ route('timeline.store')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <select name="language_id" id="language_id" class="validate form-control" require>
                                            <option value="">Selecione um Idioma</option>
                                            <option value="pt">Português</option>
                                            <option value="es">Espanhol</option>
                                            <option value="en">Inglês</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="ano" id="ano" class="validate form-control"
                                            value="{{ old('ano')}}"
                                            Placeholder="Ano" required>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="file" name="image" id="image" class="form-control">
                                    </div>
                                    <p>Tamanho da imagem: 1920 x 1066px</p>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">    
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="titulo" id="titulo" class="validate form-control"
                                            value="{{ old('titulo')}}"
                                            Placeholder="Título" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix aba">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="card-title">Descrição</label>
                                    <div class="input-group">
                                        <textarea name="description" id="description" class="description">{{ old('description') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                            

                        <div class="row clearfix">
                            <div class="col-sm-2">
                                <button class="btn btn-primary submit-btn btn-block">Registrar</button>
                            </div>
                        </div>

                    </form>
                </div>

            </div>
        </div>

    </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush