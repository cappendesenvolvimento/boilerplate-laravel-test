
@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')

@php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/" ;    
@endphp

<div class="row">
    <div class="col s12">
        @include('componentes.breadcrumbs')
    </div>
    
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
        
            <div class="card-body">
                @if (session('status'))
                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        </div>
                    </div>
                @endif

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif                
                
                <h4 class="card-title">Editar Notícias</h4>
                <p class="card-description"> </p>

                <div class="auto-form-wrapper">

            
                    <form action="{{ route('news.update', $obj->id)}}" method="post">
                        
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <input type="hidden" name="featured_img_id" id="featured_img_id" value="{{ isset($obj->featured_img_id) ?  $obj->featured_img_id : old('featured_img_id')}}">
                        <input type="hidden" name="featured_img_mobile_id" id="featured_img_mobile_id" value="{{ isset($obj->featured_img_mobile_id) ?  $obj->featured_img_mobile_id : old('featured_img_mobile_id')}}">

                        <div class="row clearfix">  
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text"  name="main_title" id="main_title" class="form-control" placeholder="Título"  value="{{ isset($obj->main_title) ?  $obj->main_title : old('main_title')}}" required>
                                    </div>
                                </div>
                            </div>  
                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <p>Imagem Banner</p>
                                    <div class="input-group">
                                        <input type="file" name="image" id="image" data-model="news" data-inputid="featured_img_id" class="form-control image">
                                    </div>
                                    <br />
                                    <img src="{{ isset($obj->featured_img->id) ? $actual_link.$obj->featured_img->directory.$obj->featured_img->url : '' }}" width="150" class="fl-donwload featured_img_id {{ isset($obj->featured_img->id) ? : 'hide'}} ">
                                    <p class="erro_featured_img_id"></p>
                                </div>
                            </div>
                        </div>
            

                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <p>Imagem Mobile</p>
                                    <div class="input-group">
                                        <input type="file" name="image_mobile" id="image_mobile" data-model="news" data-inputid="featured_img_mobile_id" class="form-control image">
                                    </div>
                                    <br />
                                    <img src="{{ isset($obj->featured_img_mobile->id) ? $actual_link. $obj->featured_img_mobile->directory.$obj->featured_img_mobile->url : '' }}" width="150" class="fl-donwload featured_img_mobile_id {{ isset($obj->featured_img_mobile->id) ? : 'hide'}}">
                                    <p class="erro_featured_img_mobile_id"></p>
                                </div>
                            </div>
                        </div>
            
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <p>Resumo</p>
                                    <div class="input-group">
                                        <textarea name="summary" id="summary" class="description">{{ isset($obj->summary) ?  $obj->summary : old('summary')}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <p>Conteúdo</p>
                                    <div class="input-group">
                                        <textarea name="content" id="content" class="description">{{ isset($obj->content) ?  $obj->content : old('content')}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">  
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text"  name="date_published" id="date_published" class="form-control" placeholder="Data de Publicação"  value="{{ isset($obj->date_published) ?  $obj->date_published : old('date_published')}}" >
                                    </div>
                                </div>
                            </div>  
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <select name="news_category_id" id="news_category_id" class="validate form-control" required>
                                            <option value="">Selecione uma categoria</option>
                                            @if(@$categorias)
                                                @foreach($categorias as $categoria)
                                                    <option value="{{ $categoria->id }}" {{ (@$obj->category->id == $categoria->id) ? 'selected' : '' }}>
                                                        {{$categoria->name}}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>      
                        </div>
                        
                        <div class="row clearfix">  
                           
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <select name="autor_id" id="autor_id" class="validate form-control" required>
                                            <option value="">Selecione um autor</option>
                                            @if(@$autores)
                                                @foreach($autores as $autor)
                                                    <option value="{{ $autor->id }}" {{ (@$obj->autor->id == $autor->id) ? 'selected' : '' }}>
                                                        {{$autor->name}}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>      
                        </div>
            
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-field">
                                        <select name="active" id="active" class="form-control c-tipo__blog" >
                                            <option value="">Selecione um tipo</option>
                                            <option value="0" {{ ($obj->active == 0) ?  'selected' : ''}}>Rascunho</option>
                                            <option value="1" {{ ($obj->active == 1) ?  'selected' : ''}}>Publicado</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-check form-check-flat">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="show_date" value="1" {{ ($obj->show_date == 1) ?  'checked' : ''}}> Exibir Data  <i class="input-helper"></i></label>
                                </div>
                            </div>  
                            <div class="col-sm-3">
                                <div class="form-check form-check-flat">
                                    <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="destaque" value="1" {{ ($obj->destaque == 1) ?  'checked' : ''}}> Destaque  <i class="input-helper"></i></label>
                                </div>
                            </div>  
                        </div>  
 
                        
                        <h5>Banner </h5>
                        <div class="row clearfix">  
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text"  name="banner_text" id="banner_text" class="form-control" placeholder="Texto Banner"  value="{{ isset($obj->banner_text) ?  $obj->banner_text : old('banner_text')}}" >
                                    </div>
                                </div>
                            </div>  
                        </div>  
            
                        <div class="row clearfix">  
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text"  name="banner_button" id="banner_button" class="form-control" placeholder="Botão Banner"  value="{{ isset($obj->banner_button) ?  $obj->banner_button : old('banner_button')}}" >
                                    </div>
                                </div>
                            </div>  
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text"  name="banner_link" id="banner_link" class="form-control" placeholder="Link Banner"  value="{{ isset($obj->banner_link) ?  $obj->banner_link : old('banner_link')}}" >
                                    </div>
                                </div>
                            </div>  
                        </div>  
                        
                        <div class="row clearfix">  
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <select name="banner_target" id="banner_target" class="form-control" >
                                            <option value="">Selecione um Target</option>
                                            <option value="0" {{ ($obj->banner_target == 0) ? 'selected' : '' }}>Abrir na mesma Aba</option>
                                            <option value="1" {{ ($obj->banner_target == 1) ? 'selected' : '' }}>Abrir em nova Aba</option>
                                        </select>
                                    </div>
                                </div>
                            </div>  
                        </div>
                        <div class="row clearfix">  
                            <div class="col-sm-2">
                                <button class="btn btn-primary submit-btn btn-block">Atualizar</button>
                            </div>
                        </div>
                    </form>    

                    <div>
                        <h4 class="card-title">Adicionar Posts Relacionados</h4>
                        <form action="{{ route('news.relacionados.store', $obj->id)}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <select name="relateds[]" id="relateds" class="form-multiselect" multiple>
                                                @if ($news)
                                                    @foreach($news as $new)
                                                        <option value="{{$new->id}}">{{ $new->main_title }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <p>Segure "ctrl" para selecionar mais de uma news.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <button class="btn btn-primary submit-btn btn-block">Adicionar</button>
                                </div>
                            </div>
                        </form>    
        
                        
                        <div class="table-responsive" id="listItensFeaturesSolucao">
                            @if ($relacionados) 
                            <br />
                            <p>News Relacionadas</p>
                            <table class="table table-striped" id="sortable">
                                <thead>
                                    <tr>
                                        <th> Id </th>
                                        <th> Título </th>
                                        <th> Ações </th>
                                    </tr>
                                </thead>
                                <tbody>
                                @forelse($relacionados as $rel)
                                <tr>
                                    <td>{{ $rel['post']->id }}</td>
                                    <td>{{$rel['post']->main_title}}</td>
                                    <td style="display:flex">
                                        <form action="{{route('news.relacionados.destroy', $rel['rel_id'])}}" method="post" class="excluir_item box-sizing">
                                            {{ method_field('DELETE')}}
                                            {{csrf_field()}}
                                            <button title="Deletar" class="btn btn-icons" href="#"><i class="mdi mdi-delete"></i></button>
                                        </form> 
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="6">Nenhum registro encontrado.</td>
                                </tr>
                                @endforelse
                                </tbody>
                            </table>
                            @endif
                        </div>
                                
                    </div>
            

                </div>
            
            </div>
        </div>
    </div>
</div>

@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush