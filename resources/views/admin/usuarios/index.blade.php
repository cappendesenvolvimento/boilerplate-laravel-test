@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
  

  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      
      <div class="card-body">

        @if (session('status'))
            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                </div>
            </div>
        @endif    
             
        @can('usuario-create')  
        <div>
          <a href="{{ route('usuarios.create') }}" class="btn btn-primary btn-fw btn-add"><i class="mdi mdi-plus"></i>Adicionar</a>
        </div>
        @endcan
        <h4 class="card-title">Usuários</h4>
        <p class="card-description"> </p>
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th> Id </th>
                <th> Nome </th>
                <th> Email </th>
                <th> Perfil </th>
                <th> Ações </th>
              </tr>
            </thead>
            <tbody>
            @forelse($usuarios as $usuario)
              <tr>
                <td> {{ $usuario->id }} </td>
                <td> {{ $usuario->name }} </td>
                <td>
                  {{ $usuario->email }}
                </td>
                <td> {{ @$usuario->rules[0]->name }} </td>
                <td> 
                    <form action="{{route('usuarios.destroy', $usuario->id)}}" method="post">
                        @can('usuario-edit')  
                            <a title="Editar" class="btn btn-icons btn-inverse-warning" href="{{ route('usuarios.edit', $usuario->id) }}"><i class="mdi mdi-grease-pencil"></i></a>
                        @endcan  
                        @can('usuario-delete')  
                            {{method_field('DELETE')}}
                            {{csrf_field()}}
                            <button title="Deletar" class="btn btn-icons btn-inverse-danger"><i class="mdi mdi-delete"></i></button>   
                        @endcan
                    </form>     
                </td>
              </tr>
              @empty
            </tbody>
              <tr>
                <td colspan="6">Nenhum registro encontrado.</td>
              </tr>
            @endforelse
          </table>
          {{ $usuarios->appends(request()->input())->links() }}
        </div>
      </div>
    </div>
  </div>
  
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush