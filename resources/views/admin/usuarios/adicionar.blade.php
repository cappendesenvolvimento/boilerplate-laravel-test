
@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
    <div class="col s12">
        @include('componentes.breadcrumbs')
    </div>

  <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
      
      <div class="card-body">
        
        <h4 class="card-title">Adicionar Usuários</h4>
        <p class="card-description"> </p>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif

        <div class="auto-form-wrapper">
        <form method="POST" action="{{ route('usuarios.store') }}">
            {{ csrf_field() }}
            <div class="row clearfix">  
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text"  name="name" id="name" class="form-control" placeholder="Nome"  value="{{ old('name') }}" required>
                            
                        </div>
                    </div>
                </div>  
                <div class="col-sm-6">  
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" name="email" id="email" class="form-control" placeholder="Email"  value="{{ old('email') }}" required>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                <i class="mdi mdi-check-circle-outline"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
            <div class="row clearfix">  
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="input-group">
                            <select name="rule_id" id="rule_id" class="form-control" required>
                                <option value="">Selecione um Perfil</option>
                                @if($rules)
                                    @foreach($rules as $rule)
                                        <option value="{{ $rule->id }}">{{ $rule->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>  
                 
            </div>
            <div class="row clearfix">  
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                <i class="mdi mdi-check-circle-outline"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="col-sm-6">  
                    <div class="form-group">
                        <div class="input-group">
                            <input type="password" name="password_confirmation" id="password_confirmation"  class="form-control" placeholder="Confirme o Password" required>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                <i class="mdi mdi-check-circle-outline"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
            <div class="row clearfix">  
                <div class="col-sm-2">
                    <button class="btn btn-primary submit-btn btn-block">Registrar</button>
                </div>
            </div>
          
        </form>
      </div>
        
    </div>
  </div>
  
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush