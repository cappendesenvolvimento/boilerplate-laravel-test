@extends('layouts.app')

@section('content')
    <div class="container">
        <h2 class="center">Lista de Papéis para {{$usuarios->name}}</h2>

        @include('admin.breadcrumb')

        <div class="row">
            <form action="{{route('usuarios.rule.store', $usuarios->id)}}" method="post">
                {{ csrf_field() }}
                <div class="input-field">
                    <select name="rule_id" id="rule_id" style="display:block">
                        @foreach($rules as $valor)
                            <option value="{{$valor->id}}">{{$valor->name}}</option>
                        @endforeach
                    </select>
                </div>
                <button class="btn blue">Adicionar</button>
            </form>
        </div>

    
        <div class="row">
            <table>
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nome</th>
                        <th>Ação</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($usuarios->rules as $rule)
                    <tr>
                        <td>{{$rule->name}}</td>
                        <td>{{$rule->description}}</td>
                        <td>
                            <form action="{{route('usuarios.rule.destroy', [$usuarios->id, $rule->id])}}" method="post">
                                {{ method_field('DELETE')}}
                                {{csrf_field()}}
                                <button title="Deletar" class="btn red" href="#"><i class="material-icons">delete</i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection    

