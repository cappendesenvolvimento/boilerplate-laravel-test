
@extends('layout.master')

@push('plugin-styles')
@endpush

@section('content')
<div class="row">
    <div class="col s12">
        @include('componentes.breadcrumbs')
    </div>
    
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
        
            <div class="card-body">
                @if (session('status'))
                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        </div>
                    </div>
                @endif

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif                
                
                <h4 class="card-title">Editar Usuário</h4>
                <p class="card-description"> </p>

                <div class="auto-form-wrapper">
                    <form action="{{ route('usuarios.update', $usuario->id)}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="row clearfix">  
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="name" id="name" class="form-control" placeholder="Nome" value="{{ $usuario->name }}">
                                        
                                    </div>
                                </div>
                            </div>  
                            <div class="col-sm-6">  
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" name="email" id="email" class="form-control" placeholder="Email" value="{{ $usuario->email }}">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                            <i class="mdi mdi-check-circle-outline"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>      
                        </div>
                        <div class="row clearfix">  
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <select name="rule_id" id="rule_id" class="form-control" required>
                                            @if (!isset($usuario->rules[0]->id))
                                            <option value="">Selecione um Perfil</option>
                                                @if($rules)
                                                    @foreach($rules as $rule)
                                                        <option value="{{ $rule->id }}" >{{ $rule->name }}</option>
                                                    @endforeach
                                                @endif    
                                            @else
                                                @if($rules)
                                                    @foreach($rules as $rule)
                                                        <option value="{{ $rule->id }}" {{$rule->id == @$usuario->rules[0]->id ? 'selected' : ''}}>{{ $rule->name }}</option>
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>  
                            
                        </div>
                        
                        <div class="row clearfix">  
                            <div class="col-sm-2">
                                <button class="btn btn-primary submit-btn btn-block">Atualizar</button>
                            </div>
                        </div>
                    
                    </form>
                </div>
            </div>
            <div class="card-body">
                
                <h4 class="card-title">Atualizar Senha</h4>
                <p class="card-description"> </p>

                <div class="auto-form-wrapper">
                    
                    <form action="{{ route('usuario.password.update', $usuario->id)}}" method="POST">
                        @method('PUT')
                        @csrf
                        <div class="row clearfix">  
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="password" class="form-control" placeholder="Password" name="password" value="">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                            <i class="mdi mdi-check-circle-outline"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                            <div class="col-sm-6">  
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="password" class="form-control" placeholder="Confirme o Password" name="password_confirmation" value="">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                            <i class="mdi mdi-check-circle-outline"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>      
                        </div>
                        <div class="row clearfix">  
                            <div class="col-sm-2">
                                <button class="btn btn-primary submit-btn btn-block">Atualizar</button>
                            </div>
                        </div>
                    </form>    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('plugin-scripts')
@endpush

@push('custom-scripts')
@endpush