<?php

return [

    'menu-caoa_caoa' => 'Caoa',
    'menu-caoa_sobre' => 'Sobre',
    'menu-caoa_historia' => 'História',
    'menu-caoa_redes' => 'Redes',
    'menu-caoa_importacao' => 'Importação e Exportação',
    'menu-caoa_servico' => 'Serviços e Mobilidade',
    'menu-fabricas' => 'Fábricas',
    'menu-carros' => 'Nossos Carros',
    'menu-ofertas' => 'Ofertas',
    'menu-posvenda' => 'Pós Venda',
    'menu-posvenda_premium' => 'Hyundai Premium Services',
    'menu-posvenda_mecanica' => 'Mecânica para Elas',
    'menu-locadora' => 'Locadora',
    'menu-seguros' => 'Corretora de Seguros',
    'menu-consorsio' => 'Consórcios',
    'menu-concessionarias' => 'Concessionárias',
    'menu-contato' => 'Contato',
    'menu-contato_atendimento' => 'Canais de Atendimento',
    'menu-contato_trabalhe' => 'Trabalhe Conosco',
    'menu-contato_imprensa' => 'Imprensa',
    'menu-contato_contato' => 'Contato',
    'menu-seminovos' => 'Seminovos',


    'footer-privacidade' => 'Política de Privacidade',
    'footer-cookies' => 'Política de Cookies',
    'footer-etiqueta' => 'Etiqueta de segurança veicular',
    'footer-fraudes' => 'Alerta de Fraudes',
    
    'links_floating_testdrive' => 'Agende um Test Drive',
    'links_floating_proposta' => 'Solicite uma Proposta', 
    'links_floating_concessionaria' => 'Ache uma Concessionária', 
    'links_floating_testdrive_ja' => 'FAÇA JÁ UM TEST DRIVE', 
    'links_floating_ofertas' => 'Ofertas', 

    'link-clique_aqui' => 'Clique aqui',
    'link-saiba_mais' => 'Saiba Mais',
    'link-confira' => 'Confira',
    'link-voltar' => 'Voltar',
    'link-exterior' => 'VEJA O EXTERIOR',
    'link-interior' => 'VEJA O INTERIOR',

    'buttom_mais_informacoes' => 'Mais Informações',
    'buttom_carregar_mais' => 'CARREGAR MAIS NOTÍCIAS',
    

    'text_price' => 'A partir de',
    'text_legal_site' => 'As informações do site estão sujeitas a modificações sem prévio aviso inclusive referentes as empresas e veículos da representada. Preserve a vida.
    Use o cinto de segurança. As imagens dos veículos são meramente ilustrativas.',
    'text_condicoes_comer' => 'Condições comerciais, ',
    'text_condicoes_comer_plus' => 'e saiba mais.',    
    'text_bilhao' => 'Bilhão',
    'text_cifrao' => 'R$',
    'text_home_explore' => 'EXPLORE TAMBÉM:',
    'text_home_video' => 'Assista',
    'text_configure' => 'Configure o seu',
    'text_escolha_cor' => 'Consulte as versões e cores ',
    'text_selecione_cor' => 'Cores disponíveis: ',
    'text_imagem_ilus' => 'Imagem meramente Ilustrativa',
    'text_imagens_ilus' => 'imagens meramente Ilustrativa',
    'text_ficha' => 'FICHA TÉCNICA',
    'text_selct_itens' => 'SELECIONE A VERSÃO ACIMA E CONFIRA OS PRINCIPAIS ITENS',

    'title_lancamento' => 'Lançamentos',
    'title_ultimas_noticias' => 'Últimas Notícias',
    'title_caoa_em_numeros' => 'A CAOA EM NÚMEROS',
    'title-carros' => 'Nossos Carros',
    'title-modelos' => 'Nossos Modelos',
    'title_detalhes_versao' => 'DETALHES DAS VERSÕES',
    'title_timeline' => 'Linha do Tempo',
    'title_proposta' => 'ENTRE EM CONTATO E GARANTA O SEU!',
    'subtitle_proposta' => 'Preencha os campos abaixo para solicitar uma proposta',   
    'title_testdrive' => 'AGENDE AGORA O SEU TEST DRIVE!',
    'subtitle_testdrive' => 'Preencha os campos abaixo para agendar o seu Test Drive.',     
    'subtitle_serv_mobilidade' => 'Servços e Mobilidade',     
    


];
