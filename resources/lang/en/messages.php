<?php

return [

    'menu-caoa_caoa' => 'Caoa',
    'menu-caoa_sobre' => 'About',
    'menu-caoa_historia' => 'History',
    'menu-caoa_redes' => 'Dealers',
    'menu-caoa_importacao' => 'Import and Export',
    'menu-caoa_servico' => 'Services and Mobility',
    'menu-fabricas' => 'Factories',
    'menu-carros' => 'Our Cars',
    'menu-ofertas' => 'Offers',
    'menu-posvenda' => 'Pós Venda',
    'menu-posvenda_premium' => 'Hyundai Premium Services',
    'menu-posvenda_mecanica' => 'Mecânica para Elas',
    'menu-locadora' => 'Rental',
    'menu-seguros' => 'Caoa Insurance Broker',
    'menu-consorsio' => 'Consórcios',
    'menu-concessionarias' => 'Concessionárias',
    'menu-contato' => 'Contact',
    'menu-contato_atendimento' => 'Service channels',
    'menu-contato_trabalhe' => 'Work with us',
    'menu-contato_imprensa' => 'Press',
    'menu-contato_contato' => 'Contact',
    'menu-seminovos' => 'Seminovos',

    'footer-privacidade' => 'Privacy Policy',
    'footer-cookies' => 'Cookies Policy',
    'footer-etiqueta' => 'Vehicle security tag',


    'links_floating_testdrive' => 'Schedule a Testdrive',
    'links_floating_proposta' => 'Request a proposal', 
    'links_floating_concessionaria' => 'Find a Dealership', 
    'links_floating_testdrive_ja' => 'FAÇA JÁ UM TEST-DRIVE', 
    'links_floating_ofertas' => 'Offers', 

    'link-clique_aqui' => 'Clique aqui',
    'link-saiba_mais' => 'Saiba Mais',
    'link-confira' => 'Check out',
    'link-voltar' => 'Back',
    'link-exterior' => 'SEE ABROAD',
    'link-interior' => 'SEE INSIDE',

    'buttom_mais_informacoes' => 'more information',
    'buttom_carregar_mais' => 'LOAD MORE',

    'text_price' => 'From',
    'text_legal_site' => 'The information on the site is subject to change without prior notice, including referring to the companies and vehicles of the represented company. Preserve life. Use the seat belt. Vehicle images are for illustrative purposes only.',
    'text_condicoes_comer' => 'Condições comerciais, ',
    'text_condicoes_comer_plus' => 'e saiba mais.',
    'text_bilhao' => 'Billion',
    'text_cifrao' => 'R$',
    'text_home_explore' => 'ALSO EXPLORE:',
    'text_home_video' => 'Watch',
    'text_configure' => 'CONFIGURE YOURS',
    'text_escolha_cor' => 'Choose the versions and colors of the',
    'text_selecione_cor' => 'SELECT YOUR COLOR:',
    'text_imagem_ilus' => 'Image for illustrative purposes only',
    'text_ficha' => 'FICHA TÉCNICA',
    'text_selct_itens' => 'SELECT THE VERSION ABOVE AND CHECK THE MAIN ITEMS',
    

    'title_lancamento' => 'Releases',
    'title_ultimas_noticias' => 'Latest news',
    'title_caoa_em_numeros' => 'CAOA IN NUMBERS',
    'title-carros' => 'Our Cars',
    'title-modelos' => 'Our Models',
    'title_detalhes_versao' => 'VERSION DETAILS',
    'title_timeline' => 'Timeline',
    'title_proposta' => 'CONTACT US AND GUARANTEE YOURS!',
    'subtitle_proposta' => 'Fill in the fields below to request a proposal',
    'title_testdrive' => 'AGENDE AGORA O SEU TEST-DRIVE!',
    'subtitle_testdrive' => 'Preencha os campos abaixo para agendar o seu test-drive.',
    'subtitle_serv_mobilidade' => 'Services and Mobility',



    

];
 