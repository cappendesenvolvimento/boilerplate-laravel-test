<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        // error_reporting(0);
        
        // if ($exception->getMessage() != 'The given data was invalid.') { 
        //     if (!$this->isHttpException($exception)) {
        //         return response()->view('site.error' . '500', [], 500);
        //     }
        // }
            
        if ($this->isHttpException($exception)) {
            if ($exception->getStatusCode() == 404) {
                return response()->view('site.error' . '404', [], 404);
            }
            if ($exception->getStatusCode() == 500) {
                return response()->view('site.error' . '500', [], 500);
            }
        }
        
        return parent::render($request, $exception);
        
        // if($this->isHttpException($exception)) {
            
        //     switch ($exception->getStatusCode()) {
        //         // not found
        //         case 404:
        //             return response()->view('site.error' . '404', [], 404);
        //             //return redirect()->route('principal');
        //             break;
        //         // internal error
        //         case 500:
        //             return response()->view('site.error' . '500', [], 500);
        //             break;

        //         default:
        //             return $this->renderHttpException($exception);
        //             break;
        //     }
        // } else {
        //     return parent::render($request, $exception);
        // }
        
    }

    
}
