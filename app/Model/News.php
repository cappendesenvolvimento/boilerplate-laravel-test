<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use SoftDeletes;

    protected $table = 'news';

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'id';

    public static $rules = [
        'title' => 'required'
    ];

    public static $messages = [
        'title.required' => 'Campo Título obrigatório'
    ];

    // public function validate($data)
    // {
    //     dd($data);
    //     return \Validator::make( $data, $this::$rules, $this::$messages);
    // }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug', 'title', 'sumary','content', 'date_published', 'show_date', 
        'active', 'show_image_featured', 'featured_img_id', 'news_category_id',
        'featured_img_mobile_id', 'banner_text', 'banner_button', 'banner_link', 'banner_target'
    ];

    public function category()
    {
      return $this->belongsTo('App\Model\NewsCategory', 'news_category_id');
    }

    public function autor()
    {
      return $this->belongsTo('App\Model\Autor', 'autor_id');
    }

    public function relateds()
    {
        return $this->belongsToMany('App\Model\NewsRelated' );
    }


    public function featured_img()
    {
      return $this->belongsTo('App\Model\Media', 'featured_img_id');
    }

    public function featured_img_mobile()
    {
      return $this->belongsTo('App\Model\Media', 'featured_img_mobile_id');
    }    

    public function store($data)
    {

        $newSlug = ($data['slug'] != '') ? $data['slug'] : '/'.$data['main_title'];
        $slug = $this->existeSlug(Str::slug($newSlug, "-"), 0);

        $object = new News();
        $object->main_title = $data['main_title'];
        $object->title_page = $data['main_title'];
        $object->slug = '/'.$slug;
        $object->summary = $data['summary'];
        $object->content = $data['content'];
        $object->show_date = $data['show_date'];
        $object->active = isset($data['active']) ? $data['active'] : 0;
        $object->destaque = isset($data['destaque']) ? $data['destaque'] : 0;
        $object->show_image_featured = isset($data['show_image_featured']) ? $data['show_image_featured'] : 0;
        $object->news_category_id = $data['news_category_id'];
        $object->autor_id = $data['autor_id'];
        $object->featured_img_id = $data['featured_img_id'];
        $object->featured_img_mobile_id = $data['featured_img_mobile_id'];
        $object->banner_text = $data['banner_text'];
        $object->banner_button = $data['banner_button'];
        $object->banner_link = $data['banner_link'];
        $object->banner_target = $data['banner_target'];
        $object->date_published = (strlen($data['date_published']) == 10) ? \Admin::formatDate('d/m/Y', 'Y-m-d', $data['date_published']) : null;
        
        return $object;
    }


    public function put($id, $data)
    {
        $newSlug = ($data['slug'] != '') ? $data['slug'] : '/'.$data['main_title'];
        $slug = $this->existeSlug(Str::slug($newSlug, "-"), $id);

        $object = News::find($id);
        $object->main_title = $data['main_title'];
        $object->title_page = $data['main_title'];
        $object->slug = '/'.$slug;
        $object->summary = $data['summary'];
        $object->content = $data['content'];
        $object->show_date = $data['show_date'];
        $object->active = isset($data['active']) ? $data['active'] : 0;
        $object->destaque = isset($data['destaque']) ? $data['destaque'] : 0;
        $object->show_image_featured = isset($data['show_image_featured']) ? $data['show_image_featured'] : 0;
        $object->news_category_id = $data['news_category_id'];
        $object->autor_id = $data['autor_id'];
        $object->featured_img_id = $data['featured_img_id'];
        $object->featured_img_mobile_id = $data['featured_img_mobile_id'];
        $object->banner_text = $data['banner_text'];
        $object->banner_button = $data['banner_button'];
        $object->banner_link = $data['banner_link'];
        $object->banner_target = $data['banner_target'];
        $object->date_published = (strlen($data['date_published']) == 10) ? \Admin::formatDate('d/m/Y', 'Y-m-d', $data['date_published']) : null;

        return $object;
    }

    public static function existeSlug($slug, $id)
    {

        if ($id != 0) {
            $existe = News::where('slug' , '=', $slug)
                ->where('id', '<>', $id)->get();
        } else {
            $existe = News::where('slug', '=', $slug)->get();
        }

        if (count($existe) > 0) {
            $slug = $slug . (count($existe));
        }

        return $slug;
    }


    

}