<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Home extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //Institucional
        'chamada', 'text', 'language',  'link',  'link_text', 'link_target', 'links_itens', 'image_one_id', 'image_two_id', 'image_three_id',
        'image_video_id', 'video_embed', 'video_title', 'title_html', 'description_html',
        //Chamadas
        'img_full_one_id', 'tx_full_one', 'tx_postition_full_one',  'lk_text_full_one', 'lk_full_one', 'lk_target_full_one', 
        'img_full_two_id', 'tx_full_two', 'tx_postition_full_two',  'lk_text_full_two', 'lk_full_two', 'lk_target_full_two', 
        // Split
        'img_split_left_id', 'tx_split_left', 'tx_postition_split_left', 'lk_text_split_left', 'lk_split_left', 'lk_target_split_left',
        'img_split_right_id', 'tx_split_right', 'tx_postition_split_right', 'lk_text_split_right', 'lk_split_right', 'lk_target_split_right',
        'img_split_full_id', 'tx_split_full', 'tx_postition_split_full', 'lk_text_split_full', 'lk_split_full', 'lk_target_split_full',
         //CTAS
         'cta_lancamento_text', 'cta_lancamento_link', 'cta_lancamento_target', 
         'cta_news_text', 'cta_news_link', 'cta_news_target'
    ];


    public function image_one()
    {
      return $this->belongsTo('App\Model\Media', 'image_one_id');
    }

    public function image_two()
    {
      return $this->belongsTo('App\Model\Media', 'image_two_id');
    }

    public function image_three()
    {
      return $this->belongsTo('App\Model\Media', 'image_three_id');
    }

    public function image_video()
    {
      return $this->belongsTo('App\Model\Media', 'image_video_id');
    }

    public function img_full_one()
    {
      return $this->belongsTo('App\Model\Media', 'img_full_one_id');
    }

    public function img_full_one_mob()
    {
      return $this->belongsTo('App\Model\Media', 'img_full_one_mob_id');
    }

    public function img_full_two()
    {
      return $this->belongsTo('App\Model\Media', 'img_full_two_id');
    }

    public function img_split_left()
    {
      return $this->belongsTo('App\Model\Media', 'img_split_left_id');
    }

    public function img_split_right()
    {
      return $this->belongsTo('App\Model\Media', 'img_split_right_id');
    }

    public function img_split_full()
    {
      return $this->belongsTo('App\Model\Media', 'img_split_full_id');
    }

    public function img_half_one()
    {
      return $this->belongsTo('App\Model\Media', 'img_half_one_id');
    }

    public function img_half_two()
    {
      return $this->belongsTo('App\Model\Media', 'img_half_two_id');
    }


    public static function putInstitucional($id, $data)
    {

        $obj = Home::find($id);
        
        $obj->chamada = $data['chamada'];
        $obj->text = $data['text'];
        $obj->link = $data['link'];
        $obj->link_text = $data['link_text'];
        $obj->link_target = $data['link_target'];
        $obj->image_one_id = $data['image_one_id'];
        $obj->image_two_id = $data['image_two_id'];
        $obj->image_three_id = $data['image_three_id'];

        for ($x=0; $x < count($data['in_item_value']); $x++) {
            $links[] = array("link" => $data['in_item_value'][$x], "text" =>$data['in_item_text'][$x] , "target" =>$data['in_item_target'][$x] );
        }

        $obj->links_itens = serialize($links);

        $obj->image_video_id = $data['image_video_id'];
        $obj->video_embed = $data['video_embed'];
        $obj->video_title = $data['video_title'];

        $obj->title_html   = $data['title_html'];
        $obj->description_html  = $data['description_html']; 
        
        return $obj;
    }

    public static function putChamadas($id, $data)
    {

        $obj = Home::find($id);
        
        $obj->img_full_one_id = $data['img_full_one_id'];
        $obj->img_full_one_mob_id = $data['img_full_one_mob_id'];
        $obj->tx_full_one = $data['tx_full_one'];
        $obj->tx_postition_full_one = $data['tx_postition_full_one'];
        $obj->lk_text_full_one = $data['lk_text_full_one'];
        $obj->lk_full_one = $data['lk_full_one'];
        $obj->lk_target_full_one = $data['lk_target_full_one'];
        
        $obj->img_full_two_id = $data['img_full_two_id'];
        $obj->tx_full_two = $data['tx_full_two'];
        $obj->tx_postition_full_two = $data['tx_postition_full_two'];
        $obj->lk_text_full_two = $data['lk_text_full_two'];
        $obj->lk_full_two = $data['lk_full_two'];
        $obj->lk_target_full_two = $data['lk_target_full_two'];
        
        return $obj;
    }

    public static function putSplit($id, $data)
    {
        
        $obj = Home::find($id);
        
        $obj->img_split_left_id = $data['img_split_left_id'];
        $obj->tx_split_left = $data['tx_split_left'];
        $obj->tx_postition_split_left = $data['tx_postition_split_left'];
        $obj->lk_text_split_left = $data['lk_text_split_left'];
        $obj->lk_split_left = $data['lk_split_left'];
        $obj->lk_target_split_left = $data['lk_target_split_left'];
        
        $obj->img_split_right_id = $data['img_split_right_id'];
        $obj->tx_split_right = $data['tx_split_right'];
        $obj->tx_postition_split_right = $data['tx_postition_split_right'];
        $obj->lk_text_split_right = $data['lk_text_split_right'];
        $obj->lk_split_right = $data['lk_split_right'];
        $obj->lk_target_split_right = $data['lk_target_split_right'];
        
        $obj->img_split_full_id = $data['img_split_full_id'];
        $obj->tx_split_full = $data['tx_split_full'];
        $obj->tx_postition_split_full = $data['tx_postition_split_full'];
        $obj->lk_text_split_full = $data['lk_text_split_full'];
        $obj->lk_split_full = $data['lk_split_full'];
        $obj->lk_target_split_full = $data['lk_target_split_full'];
                
        return $obj;
    }

    
    public static function putHalf($id, $data)
    {
        
        $obj = Home::find($id);
        
        $obj->img_half_one_id = $data['img_half_one_id'];
        $obj->ttl_half_one = $data['ttl_half_one'];
        $obj->tx_half_one = $data['tx_half_one'];
        $obj->lk_text_half_one = $data['lk_text_half_one'];
        $obj->lk_half_one = $data['lk_half_one'];
        $obj->lk_target_half_one = $data['lk_target_half_one'];
        
        $obj->img_half_two_id = $data['img_half_two_id'];
        $obj->ttl_half_two = $data['ttl_half_two'];
        $obj->tx_half_two = $data['tx_half_two'];
        $obj->lk_text_half_two = $data['lk_text_half_two'];
        $obj->lk_half_two = $data['lk_half_two'];
        $obj->lk_target_half_two = $data['lk_target_half_two'];
                
        return $obj;
    }

    public static function putCtas($id, $data)
    {
        
        $obj = Home::find($id);
        
        $obj->cta_lancamento_text = $data['cta_lancamento_text'];
        $obj->cta_lancamento_link = $data['cta_lancamento_link'];
        $obj->cta_lancamento_target = $data['cta_lancamento_target'];
        
        $obj->cta_news_text = $data['cta_news_text'];
        $obj->cta_news_link = $data['cta_news_link'];
        $obj->cta_news_target = $data['cta_news_target'];
        
                
        return $obj;
    }
}



