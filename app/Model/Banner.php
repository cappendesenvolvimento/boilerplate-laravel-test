<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'text', 'posicao', 'language', 'file_id', 'file_mobile_id', 'status', 'target', 'link',  'txtbotao', 'exibir_titulo'
    ];


        
    public function file()
    {
      return $this->belongsTo('App\Model\Media', 'file_id');
    }    
    
    public function file_mobile()
    {
      return $this->belongsTo('App\Model\Media', 'file_mobile_id');
    }  

    public function store($data){

        $obj = new Banner();
        
        $obj->name = $data['name'];
        $obj->text = $data['text'];
        $obj->posicao = $data['posicao'];
        $obj->language = $data['language'];
        $obj->file_id =  $data['file_id'];
        $obj->file_mobile_id =  $data['file_mobile_id'];
        $obj->status = ($data['status'] != 0) ? $data['status'] : 0;
        $obj->target = ($data['target'] != 0) ? $data['target'] : 0;
        $obj->link = $data['link'];
        $obj->txtbotao = $data['txtbotao'];
        $obj->exibir_titulo = ($data['exibir_titulo'] == 1) ?  $data['exibir_titulo'] : 0 ;
        $obj->date_start = (strlen($data['date_start']) == 10) ? formatDatPress('d/m/Y', 'Y-m-d', $data['date_start']) : null;
        $obj->date_end = (strlen($data['date_end']) == 10) ? formatDatPress('d/m/Y', 'Y-m-d', $data['date_end']) : null;
        
        return $obj;
    }

    public function put($id, $data){

        $obj = Banner::find($id);
        
        $obj->name = $data['name'];
        $obj->text = $data['text'];
        $obj->posicao = $data['posicao'];
        $obj->language = $data['language'];
        $obj->file_id =  $data['file_id'];
        $obj->file_mobile_id =  $data['file_mobile_id'];
        $obj->status = ($data['status'] != 0) ? $data['status'] : 0;
        $obj->target = ($data['target'] != 0) ? $data['target'] : 0;
        $obj->link = $data['link'];
        $obj->txtbotao = $data['txtbotao'];
        $obj->exibir_titulo = ($data['exibir_titulo'] == 1) ?  $data['exibir_titulo'] : 0 ;
        $obj->date_start = (strlen($data['date_start']) == 10) ? formatDatPress('d/m/Y', 'Y-m-d', $data['date_start']) : null;
        $obj->date_end = (strlen($data['date_end']) == 10) ? formatDatPress('d/m/Y', 'Y-m-d', $data['date_end']) : null;
        
        return $obj;
    }
}

