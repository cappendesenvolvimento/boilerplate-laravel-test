<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewsCategory extends Model
{
    use SoftDeletes;

    protected $table = 'news_categories';

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'image_id'
    ];

    
    public function image()
    {

        return $this->belongsTo('App\Model\Media', 'image_id');

    }

    
   
}