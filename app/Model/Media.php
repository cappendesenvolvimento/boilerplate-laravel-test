<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;

class Media  extends Model
{
    protected $table = 'medias';
    
    protected $fillable = [];

    public function uploadFile($file, $model)
    {
        
        // set destination and save path ( relative and absolute )
        $destinationPath = 'uploads/'.$model.'/'.date('Y').'/'.date('m').'/'.date('d').'/';
        $savePath        = public_path($destinationPath);
        
        // create directory if isn't created
        if(!File::isDirectory($savePath)){
            File::makeDirectory($savePath, 0777, true);
        }

        $newName = '_'.strtotime(date('Y-m-d h:i:s'));

        // set the filename and type
        $originalName = $file->getClientOriginalName();
        $filename     = Str::slug(substr($originalName, 0, -3)).$newName.'.'.$file->getClientOriginalExtension();
        
        //if ($file->getClientOriginalExtension() != 'mp4') { 
            $type         = $file->getMimeType();
        //}    
        
        // try save file
        if(!$file->move($savePath,$filename)) {
            return false;
        }

        $img = new Media();

        $img->name      = $originalName;
        $img->url       = $filename;
        $img->directory = $destinationPath;
        $img->type      = $type;
        $img->is_tmp    = true;

        // try save in db
        if(!$img->save()) {
            return false;
        }

        return $img;
    }


    public function upload($file, $model, $width = false, $height = false, $method = "fit")
    {
        // set destination and save path ( relative and absolute )
        $destinationPath = 'uploads/'.$model.'/'.date('Y').'/'.date('m').'/'.date('d').'/';
        $savePath        = public_path($destinationPath);
        
        $mobilePrefix    = 'mobile_';

        // create directory if isn't created
        if(!File::isDirectory($savePath)){
            File::makeDirectory($savePath, 0775, true);
        }
        
        // set the filename and type
        $originalName = $file->getClientOriginalName();
        
        $newName = '_'.strtotime(date('Y-m-d h:i:s'));
            // $filename     = \Str::slug(time().'-'.substr($originalName, 0, -3)).'.'.$file->getClientOriginalExtension();

        $filename     = Str::slug(substr($originalName, 0, -3)).$newName.'.'.$file->getClientOriginalExtension();
        $type         = $file->getMimeType();
        
        // transform in a manipulable object
        //$file = Image::make($file->getRealPath());
        
        
        // manipulate if necessary
        if($width && $height){
            switch ($method) {
                case 'fit':
                $file->{$method}($width, $height);
                break;
            }
        }
        
        // try save file
        if(!$file->move($savePath,$filename)) {
            return false;
        }

        // //generate half-size image for mobile
        // $file->fit( round($width/2), round($height/2));

        // try save mobile file
        // if(!$file->save($savePath.$mobilePrefix.$filename)) {
        //     return false;
        // }

        $img = new Media();

        $img->name       = $originalName;
        $img->url        = $filename;
        $img->url_mobile = $mobilePrefix.$filename;
        $img->directory  = $destinationPath;
        $img->type       = $type;
        $img->is_tmp     = true;

        // try save in db
        if(!$img->save()) {
            return false;
        }

        return $img;
    }



}