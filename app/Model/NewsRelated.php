<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NewsRelated extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'news_related';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'news_id', 'related_id'
    ];

    public function news()
    {
        return $this->belongsToMany('App\Model\NewsRelated' );
    }

    public static function populateNewsRelated($relateds, $news){
        
        $success = true;
        
        foreach($relateds as $rel) {
            
            $objRel = NewsRelated::where('related_id', '=', $rel)
                                  ->where('news_id', '=', $news->id)
                                  ->first();
                                  
            if (!$objRel) {
                $obj = new NewsRelated();
                $obj->news_id = $news->id;
                $obj->related_id = $rel;
                if(!$obj->save()){
                    $success = false;
                }
            }
            

        }
        
        return $success;

    }


}