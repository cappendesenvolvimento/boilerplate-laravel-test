<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pagina extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'featured_img_id', 'featured_img_mobile_id', 'title_banner', 'slug', 'description', 'status', 'date_published',
        'cta_link', 'cta_text', 'cta_btcolor', 'cta_target',  'cta_button', 'bt_proposta', 'title_html', 'description_html'
    ];

    public function featured_img()
    {
      return $this->belongsTo('App\Model\Media', 'featured_img_id');
    }

    public function featured_img_mobile()
    {
      return $this->belongsTo('App\Model\Media', 'featured_img_mobile_id');
    }

    public function store($data){
        
        $newSlug = ($data['slug'] != '') ? $data['slug'] : $data['title'];
        $slug = Pagina::existeSlug(Str::slug($newSlug, "-"), 0 );
        
        $obj = new Pagina();

        $obj->title = $data['title'];
        
        $obj->featured_img_id = $data['featured_img_id'];
        $obj->featured_img_mobile_id = $data['featured_img_mobile_id'];
        $obj->title_banner = $data['title_banner'];
        $obj->slug = $slug;
        $obj->description = $data['description'];
        $obj->description_en = $data['description_en'];
        $obj->status = ($data['status'] != 0) ? $data['status'] : 0;
        $obj->cta_link = $data['cta_link'];
        $obj->cta_text = $data['cta_text'];        
        $obj->cta_btcolor = $data['cta_btcolor'];        
        $obj->cta_button = $data['cta_button'];        
        $obj->cta_target = ($data['cta_target'] != 0) ? $data['cta_target'] : 0;
        $obj->bt_proposta = ($data['bt_proposta'] != 0) ? $data['bt_proposta'] : 0;
        //$hora = ($data['hour_published'] != '') ? $data['hour_published'].":00" : '00:00:00';
        $obj->date_published = date('Y-m-d', strtotime($data['date_published']));  

        $obj->title_html   = $data['title_html'];
        $obj->description_html  = $data['description_html']; 

        return $obj;

    }

    public function put($id, $data){
        
        $obj = Pagina::find($id);
        
        $newSlug = ($data['slug'] != '') ? $data['slug'] : $data['title'];
        $slug = Pagina::existeSlug(Str::slug($newSlug, "-"), $id);
        
        $obj->title = $data['title'];
        
        $obj->featured_img_id = $data['featured_img_id'];
        $obj->featured_img_mobile_id = $data['featured_img_mobile_id'];
        $obj->title_banner = $data['title_banner'];
        $obj->slug = $slug;
        $obj->description = $data['description'];
        $obj->description_en = $data['description_en'];
        $obj->status = ($data['status'] != 0) ? $data['status'] : 0;
        $obj->cta_link = $data['cta_link'];
        $obj->cta_text = $data['cta_text'];        
        $obj->cta_btcolor = $data['cta_btcolor'];  
        $obj->cta_button = $data['cta_button'];               
        $obj->cta_target = ($data['cta_target'] != 0) ? $data['cta_target'] : 0;
        $obj->bt_proposta = ($data['bt_proposta'] != 0) ? $data['bt_proposta'] : 0;
        //$hora = ($data['hour_published'] != '') ? $data['hour_published'].":00" : '00:00:00';
        $obj->date_published = date('Y-m-d', strtotime($data['date_published']));         

        $obj->title_html   = $data['title_html'];
        $obj->description_html  = $data['description_html']; 

        return $obj;

    }

    public static function existeSlug($slug, $id)
    {
        if ($id != 0) {
            $existe = Pagina::where('slug' , '=', $slug)
                ->where('id', '<>', $id)->get();
        } else {
            $existe = Pagina::where('slug', '=', $slug)->get();
        }

        if (count($existe) > 0) {
            $slug = $slug . (count($existe));
        }

        return $slug;
    }


    public static function existePagina($slug)
    {

        $existe = Pagina::where('slug', '=', $slug)->get();

        if (count($existe) > 0) {
            $slug = $slug;
        }

        return $slug;
    }  
    
    
}
