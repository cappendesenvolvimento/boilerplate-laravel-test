<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Rule extends Model
{

    use SoftDeletes;

    protected $table = 'rules';
    protected $fillable = ['name', 'description'];

    protected $dates = ['deleted_at'];
    
    //Muitos usuarios tem muitos papeis
    public function users()
    {
      return $this->belongsToMany('App\User');
    }

    //Muitos usuarios tem muitos papeis
    public function permissions()
    {
      return $this->belongsToMany('App\Permission');
    }

    public function adicionarPermission($permission){
      if (is_string($permission)) {
        $permission = Permission::where('name', '=', $permission)->firstOrFail();
      }

      if ($this->existePermission($permission)){
        return;
      }

      return $this->permissions()->attach($permission); //attach - adiciona um relacionamento

    } 

    public function existePermission($permission){
        if (is_string($permission)){
            $permission = Permission::where('name', '=', $permission)->firstOrFail();
        }

        return (boolean) $this->permissions()->find($permission->id);
    }

    public function removePermission($permission = null){
        if (is_string($permission)){
            $permission = Permission::where('name', '=', $permission)->firstOrFail();
        }

        return $this->permissions()->detach($permission); //detach - remove um relacionamento
    }
}
