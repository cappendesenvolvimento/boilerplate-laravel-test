<?php

namespace App\Alerts;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use \App\Mail\MailNotification;
use App\Model\DestinationEmail;
use App\User;

class AlertEmail {

     // ADMIN - Alert email new user ( news press ) (Cadastro de novo usuario)
     public function newUserNewsPress( $newObj )
     {
         
         $data = array('user' => $newObj);
         $emailListTo = $this->getEmailToList(DestinationEmail::$TYPE_NEWS);
         
         $view = 'notification.new_user-news_press';
         $subject = '[Site Caoa] Novo Usuário Imprensa Cadastrado';
         $tipo = 'new_user-news_press';
         $this->sendAlertImprensa($view, $emailListTo, $data, $subject, $tipo);
     }

     // USER -  Alert email new user ( news press ) (Confirmação de cadastro)
    public function registerUserNewsPress( $newObj )
    {
        $data = array('user' => $newObj);
        $emailListTo = $newObj->email;
        $view = 'notification.register_user-news_press';
        $subject = '[Site Caoa] Cadastro';
        $tipo = 'register_user-news_press';
        $this->sendAlertImprensa($view, $emailListTo, $data, $subject, $tipo);
    }

    // ADMIN - Alert email active user ( news press )
    public function activeUserNewsPress( $newObj )
    {
        
        $data = array('user' => $newObj);
        $emailListTo = $this->getEmailToList(DestinationEmail::$TYPE_NEWS);
        $view = 'notification.active_user-news_press';
        $subject = '[Site Caoa] Usuário Imprensa ativado';
        $tipo = 'active_user-news_press';
       
        $this->sendAlert($view, $emailListTo, $data, $subject, $tipo);
    }

    // USER - Alert email active user ( news press )
    public function activedUserNewsPress( $newObj )
    {
        $data = array('user' => $newObj);
        $emailListTo = $newObj->email;
        
        $view = 'notification.actived_user-news_press';
        $subject = '[Site Caoa] Ativação';
        $tipo = 'actived_user-news_press';
        $this->sim($view, $emailListTo, $data, $subject, $tipo);
    }

    // ADMIN - Alert email new published ( news press )
    public function publishedNewsPress( $newObj )
    {
        $attributesNews = $newObj->getAttributes();
        $data = array(  'category'=>$newObj->category->name,
            'titleNews'=>$attributesNews['main_title'],
            'link'=> \URL::to('imprensa').$attributesNews['slug'] );
        $emailListTo = $this->getEmailToList(DestinationEmail::$TYPE_NEWS);
        $view = 'notification.published-new';
        $subject = '[Site Caoa] Nova Notícia Cadastrada';
        $tipo = 'published-new';
        $this->sendAlert($view, $emailListTo, $data, $subject, $tipo);
    }
    
    // USERS - Alert email new published ( news press )
    public function publishedNewsPressUsers( $newObj )
    {
        $attributesNews = $newObj->getAttributes();
        $data = array(  'category'=>$newObj->category->name,
            'titleNews'=>$attributesNews['main_title'],
            'link'=> \URL::to('imprensa').$attributesNews['slug'],
         );
        $emailListTo = $this->getEmailUsersToList();
        $view = 'notification.user-published-new';
        $subject = '[Site Caoa] Nova Notícia Cadastrada';
        $tipo = 'user-published-new';
        $this->sendAlertUsers($view, $emailListTo, $data, $subject, $tipo);
    }
    
 
     private function getEmailToList( $type, $brandId = null ){
         $alertDestinations = DestinationEmail::getDestinationByType($type);
         $emailTo = array();
 
         if( $type == DestinationEmail::$TYPE_NEWS ){
             foreach( $alertDestinations  as $destination ){
                 array_push($emailTo, $destination->email);
             }
         }
 
         if( $type == DestinationEmail::$TYPE_PRICE_PRODUCTS ||  $type == DestinationEmail::$TYPE_DEALES){
             if($brandId != null ){
                 foreach( $alertDestinations  as $destination ){
                     if(in_array($brandId, $destination->getListMyBrands())){
                         array_push($emailTo, $destination->email);
                     }
                 }
             }
         }
         return $emailTo;
     }

     private function getEmailUsersToList(){
         $alertDestinations = User::getUsersPress();
         $emailTo = array();
 
        foreach( $alertDestinations  as $destination ){
            array_push($emailTo, $destination->email);
        }
 
        return $emailTo;
     }

    private function sendAlertImprensa( $viewEmail, $emailsTo, $data , $subject, $tipo){
        $environment = App::environment();
       
        //if(count($emailsTo) > 0 && $environment!='local'){
        if($emailsTo){
        
            if (is_array($emailsTo)) {
                
                $to = $emailsTo[0];
                array_shift($emailsTo);
                
                Mail::to($to) 
                      ->cc($emailsTo)   
                      ->bcc('anderson.cavalcante@caoa.com.br')
                      ->bcc('joana.bezerra@caoa.com.br')
                      ->bcc('mariana.santos@caoa.com.br')
                      ->send( new MailNotification($viewEmail, $subject, $tipo, $data));
            } else {

                $to = $emailsTo;
                Mail::to($to) 
                    ->send( new MailNotification($viewEmail, $subject, $tipo, $data));

            }
            
        }
    }

    private function sendAlert( $viewEmail, $emailsTo, $data , $subject, $tipo){
        $environment = App::environment();
        

        //if(count($emailsTo) > 0 && $environment!='local'){
        if(count($emailsTo) > 0){

            // dd($emailsTo);
            if (is_array($emailsTo)) {
                
                $to = $emailsTo[0];
                array_shift($emailsTo);

                Mail::to($to)
                      ->cc($emailsTo)  
                       ->bcc('anderson.cavalcante@caoa.com.br')
                       ->bcc('joana.bezerra@caoa.com.br')
                       ->bcc('mariana.santos@caoa.com.br')
                      ->send( new MailNotification($viewEmail, $subject, $tipo, $data));
                      
            } else {
                
                $to = $emailsTo;
                Mail::to($to) 
                    ->send( new MailNotification($viewEmail, $subject, $tipo, $data));
            }


        }
    }
    private function sendAlertUsers( $viewEmail, $emailsTo, $data , $subject, $tipo){
        $environment = App::environment();
        
        //if(count($emailsTo) > 0 && $environment!='local'){
        if(count($emailsTo) > 0){
            
            if (is_array($emailsTo)) {
                
                foreach($emailsTo as $key => $to) { 
                   
                    Mail::to($to) 
                        ->send( new MailNotification($viewEmail, $subject, $tipo, $data, $to));
                }
                      
            }

        }
    }

    private function sim( $viewEmail, $emailsTo, $data , $subject, $tipo){
        $environment = App::environment();

        //if(count($emailsTo) > 0 && $environment!='local'){
        if($emailsTo){
            $to = $emailsTo;
            Mail::to($to) 
                ->send( new MailNotification($viewEmail, $subject, $tipo, $data));

        }
    }



    public function alertPreAgendamento( $newObj ){

        $data = $newObj->toArray();
        $data['model'] = $newObj->models->name;
        $data['brand'] = $newObj->brands->name;
        $data['loja'] = $newObj->shops->name;
        $data['placa_carro'] = $newObj->placa_carro;
        $data['carro_desejado'] = $newObj->carro_desejado;
        
        // $emailListTo = 'gabriela@cappen.com';
        $emailListTo = $newObj->shops->email;
        $view = 'notification.alert_pre_agendamento';
        $tipo = 'alert_pre_agendamento';
        $subject = "[Site Caoa] Pre-Agendamento {$newObj->models->name} {$newObj->date} {$newObj->time} - {$newObj->shops->name}";
        
        return $this->sendAlert($view, $emailListTo, $data, $subject, $tipo);
    }

    public function alertPreAgendamento_Locadora( $newObj ){

        $data = $newObj->toArray();
        $data['model'] = $newObj->models->name;
        $data['brand'] = $newObj->brands->name;
        $data['loja'] = $newObj->shops->name;
        $data['placa_carro'] = $newObj->placa_carro;
        $data['carro_desejado'] = $newObj->carro_desejado;
        $data['quero_conhecer'] = $newObj->quero_conhecer;
        
        // $emailListTo = 'gabriela@cappen.com';
        $emailListTo = $newObj->shops->email;
        $view = 'notification.alert_pre_agendamento';
        $tipo = 'alert_pre_agendamento';
        $subject = "[Site Caoa] Pre-Agendamento {$newObj->models->name} {$newObj->date} {$newObj->time} - {$newObj->shops->name}";

        return $this->sendAlert_Locadora($view, $emailListTo, $data, $subject, $tipo);
    }

    private function sendAlert_Locadora( $viewEmail, $emailsTo, $data , $subject, $tipo){
        $environment = App::environment();

        //if(count($emailsTo) > 0 && $environment!='local'){
        if ($emailsTo) {
            if($data['quero_conhecer'] != ""){
                $email_bcc = ['fernanda.bruno@caoa.com.br', 'andreza.paz@caoa.com.br', 'fabiana.borges@caoa.com.br', 'thais.azevedo@caoa.com.br', 'mariana.santos@caoa.com.br', 'claudia.assuncao@caoa.com.br', 'rodolfo.silveira@caoa.com.br' ];
            }else{
                $email_bcc = ['fernanda.bruno@caoa.com.br', 'andreza.paz@caoa.com.br', 'fabiana.borges@caoa.com.br', 'thais.azevedo@caoa.com.br', 'mariana.santos@caoa.com.br'];
            }

            Mail::to($emailsTo) 
                ->bcc($email_bcc)
                ->send( new MailNotification($viewEmail, $subject, $tipo, $data));

        }
    }

}