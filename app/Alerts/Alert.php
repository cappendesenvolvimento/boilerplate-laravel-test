<?php
namespace App\Alerts;

use Illuminate\Support\Facades\Facade;

class Alert extends Facade {
    /**
     * Get the binding IoC container
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'alert';
    }
} 