<?php

namespace App\Console\Commands;

use App\Model\ZCatalog;
use App\Model\ZFlag;
use App\Model\ZShop;
use App\MrSales\MrSalesService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MrSales extends Command {
    /**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mrsales:update';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Mr Sales update informations.';

    /**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

    public function handle()
    {

        $clearCache = boolval(true);
        if ($clearCache) {
            MrSalesService::cleanCache();
            $this->info("Clean Cache");
        }

        DB::beginTransaction();


        try {

            // $this->updateFlags();
            $this->updateShops();
            $this->updateCatalog();

            DB::commit();

        } catch(\Exception $e) {

            DB::rollback();

            $this->comment('A atualização das informações de Mr Sales falhou.');
            $this->line(json_encode([
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            ]));
        }

        $this->info("The End");

    }


    private function updateFlags()
    {
        $httpResult = MrSalesService::getFlags();
        
        if ($httpResult->isSuccess()) {
            $items = $httpResult->getData()['Data'];

            $progress = $this->output->createProgressBar(count($items));
            
            $this->info("Update flags...");

            ZFlag::query()->delete();
            foreach ($items as $item) {
                $model = ZFlag::findOrNew($item['BandeiraID']);
                $model->fill($item);
                if ($model->save()) {
                    $progress->advance();
                } else {
                    $this->comment("Dont Save -> ".$item['BandeiraID'] . ' - '.$item['Nome']);
                }
            }

            $progress->finish();
        } else {
            $this->error("\nFlags -> ".$httpResult->getMessage());
        }
    }

    private function updateShops()
    {
        $httpResult = MrSalesService::getShops();
        
        if ($httpResult->isSuccess()) {

            $items = $httpResult->getData()['Data'];

            $progress = $this->output->createProgressBar(count($items));
            
            $this->info("Update shops...");

            ZShop::query()->delete();
            foreach ($items as $item) {
                $model = ZShop::findOrNew($item['LojaID']);

                $item['LojaEnderecoID'] = $item['Enderecos']['LojaEnderecoID'];
                $item['CEP'] = $item['Enderecos']['CEP'];
                $item['Logradouro'] = $item['Enderecos']['Logradouro'];
                $item['Numero'] = $item['Enderecos']['Numero'];
                $item['Complemento'] = $item['Enderecos']['Complemento'];
                $item['Bairro'] = $item['Enderecos']['Bairro'];
                $item['Estado'] = $item['Enderecos']['Estado'];
                $item['Cidade'] = $item['Enderecos']['Cidade'];
                $item['Latitude'] = $item['Enderecos']['Latitude'];
                $item['Longitude'] = $item['Enderecos']['Longitude'];
                $item['EnderecoCobranca'] = $item['Enderecos']['EnderecoCobranca'];

                $item['Enderecos'] = json_encode($item['Enderecos']);
                $item['Contatos'] = json_encode($item['Contatos']);
                $model->fill($item);
                if ($model->save()) {
                    $progress->advance();
                } else {
                    $this->comment("Dont Save -> ".$item['LojaID'].' - '.$item['NomeFantasia']);
                }
            }

            $progress->finish();
        } else {
            $this->error("\nShops -> ".$httpResult->getMessage());
        }
    }


    private function updateCatalog()
    {
        $zShops = ZShop::query()->get();
        $progress = $this->output->createProgressBar($zShops->count());
        
        $this->info("Update catalogs...");
        ZCatalog::query()->delete();

        foreach ($zShops as $zShop) {

            $httpResult = MrSalesService::getCatalog($zShop->LojaID);

            if ($httpResult->isSuccess()) {
                
                $items = $httpResult->getData()['Data'];
                
                foreach ($items as $item) {
                    $model = ZCatalog::findOrNew($item['LojaCatalogoID']);
                    $item['LojaID'] = $zShop->LojaID;
                    $item['TestDriveSegSexta'] = boolval($item['TestDriveSegSexta']);
                    $item['TestDriveSabado'] = boolval($item['TestDriveSabado']);
                    $item['TestDriveDomingo'] = boolval($item['TestDriveDomingo']);
                    $model->fill($item);
                    if (!$model->save()) {
                        $this->comment("Dont Save -> ".$item['LojaCatalogoID'].' - '.$item['Modelo'].' - '.$item['Versao']);
                    }
                }
            } else {
                $this->error("\nCatalogs -> ".$httpResult->getMessage());
                break;
            }
            $progress->advance();
        }
        $progress->finish();
    }



}