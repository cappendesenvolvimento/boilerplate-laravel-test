<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class NewsPressCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newspress:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envia Notificações de email para usuários que desejam receber novas news cadastradas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
       // DB::table('emails')->get(); // pega os e-mails
        // siga o código de sua preferencia
        // executando as funções de envio de e-mail
        $this->info('Example Cron comando rodando com êxito');
        return 0;
    }
}
