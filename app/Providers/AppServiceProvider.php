<?php

namespace App\Providers;

use App\Model\Brand;
use App\Model\City;
use App\Model\Fabricas;
use App\Model\LinksFooter;
use App\Model\ModeloMenu;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->bind('path.public', function() {
        //     return realpath(base_path().'/../public_html');
        // });

      
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
        $lang = 'pt_br';
        if(parseLocale() != '/') {
            $lang = parseLocale();
            switch ($lang) {
                case 'pt': $lang = 'pt_br'; break;
                case 'en': $lang = 'en_us'; break;
                default:  $lang = 'pt_br'; break;
            }
    
        }

        $linksFooter = LinksFooter::first();
       
        $objBrands = Brand::where('status', 1)->where('id', '<>', 12)->orderby('sequence', "ASC")->get(); 
        $models = null;
        foreach($objBrands as $brand) {
            $models[] = ModeloMenu::getModelos($brand);
        }

        $fabricas = Fabricas::whereNULL('deleted_at')->where('language_id', $lang)->get();

        $url = explode("/", url()->current());
        $arrLocales = array('pt', 'en', 'es');
        $currentLocale = '';
        if (in_array(@$url[3], $arrLocales)) {
            $currentLocale = @$url[3].'/';
        }

        $states = City::groupBy('uf')->orderBy('uf', 'ASC')->select('cities.uf')->get();

                
        if (isset($_GET['utm_source'])) {
            $UTM_CAMPAIGN = $_GET['utm_campaign'];           //opcional ex.conheca-novo-carro
            $UTM_SOURCE = $_GET['utm_source'];         //opcional   ex. UOL, Globo, Google Display, Facebook ads
            $UTM_CONTENT = $_GET['utm_content'];          //opcional
            $UTM_MEDIUM = $_GET['utm_medium'];           //opcional
            $UTM_TERM = $_GET['utm_term'];

        } elseif (isset($_COOKIE['__utmz'])) {
            $__UTMZ = $_COOKIE['__utmz'];
            $array_utmz_explode = explode("|", $__UTMZ);

            for ($x = 0; $x < count($array_utmz_explode); $x++) {
                $array_utmz[$x] = explode("=", $array_utmz_explode[$x]);
            }

            $UTM_CAMPAIGN = (isset($array_utmz[2][1]) ? $array_utmz[2][1] : NULL);           //opcional ex.conheca-novo-carro
            $UTM_SOURCE = (isset($array_utmz[0][1]) ? $array_utmz[0][1] : NULL);         //opcional   ex. UOL, Globo, Google Display, Facebook ads
            $UTM_CONTENT = (isset($array_utmz[3][1]) ? $array_utmz[3][1] : NULL);          //opcional
            $UTM_MEDIUM = (isset($array_utmz[1][1]) ? $array_utmz[1][1] : NULL);           //opcional
            $UTM_TERM = (isset($array_utmz[4][1]) ? $array_utmz[4][1] : NULL);
        } else {
            $UTM_CAMPAIGN = NULL;           //opcional ex.conheca-novo-carro
            $UTM_SOURCE = NULL;         //opcional   ex. UOL, Globo, Google Display, Facebook ads
            $UTM_CONTENT = NULL;          //opcional
            $UTM_MEDIUM = NULL;           //opcional
            $UTM_TERM = NULL;
        }

        view()->share('menu_brand', $objBrands);
        view()->share('lang', $lang);
        view()->share('menu_models', $models);
        view()->share('linksFooter', $linksFooter);
        view()->share('fabricas', $fabricas);
        view()->share('currentLocale', $currentLocale);
        view()->share('pageBrand', false);
        view()->share('states', $states);
        view()->share('UTM_CAMPAIGN', $UTM_CAMPAIGN);
        view()->share('UTM_SOURCE', $UTM_SOURCE);
        view()->share('UTM_CONTENT', $UTM_CONTENT);
        view()->share('UTM_MEDIUM', $UTM_MEDIUM);
        view()->share('UTM_TERM', $UTM_TERM);
    }
}
