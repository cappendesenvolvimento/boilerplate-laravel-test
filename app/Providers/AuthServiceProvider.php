<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

use \App\Chamado;
use \App\Permission;
use \App\Rule;
use Illuminate\Support\Facades\Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        '\App\Chamado' => '\App\Policies\ChamadoPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Gate::define('ver-chamado', function($user, Chamado $chamado){
        //     return $user->id == $chamado->user_id;
        // });

        foreach ($this->listPermissions() as $permission) {
           
           Gate::define($permission->name, function($user) use ($permission){
                return $user->eAdmin() || $user->hasPermission($permission->rules); //este metodo ira verificar se o usuario logado tem alguma regra que possui essa permissao 
            });
        }
    }

    public function listPermissions()
    {
        return $permissions = Permission::with('rules')->get(); //traz todas as permissões com as suas regras
    }
}
