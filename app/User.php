<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'name', 'email', 'date_birth', 'company', 'phone', 'cpf', 'editor', 'active_press', 'receive_news', 'hash_press', 'role', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static $rules = [
        'name' => 'required',
        'email' => 'required|email|unique:users'
    ];

    public static $messages = [
        'name.required' => 'Campo nome obrigatório',
        'email.required' => 'Campo email e obrigatório',
        'email.email' => 'Email com formato invalido',
        'email.unique' => 'Email já cadastrado'
    ];

    public function validate($data)
    {
        return Validator::make($data, $this::$rules, $this::$messages);
    }


    static $TYPE_ADMIN = 1; //ANTIGO 9
    static $TYPE_NEWS = 2;
    static $TYPE_PRICE_PRODUCTS = 3;
    static $TYPE_DEALES = 4;
    static $TYPE_PRESS = 6;
    static $TYPE_NEWS_PRESS = 7;
    static $TYPE_LEADS = 10;


    public function store($data)
    {
        $object = new User();
        $object->name = $data['name'];
        $object->email = $data['email'];
        $object->cpf = isset($data['cpf']) ? $data['cpf'] : null;
        $object->phone = isset($data['phone']) ? $data['phone']: null;
        $object->company = isset($data['company']) ? $data['company']: null;
        $object->editor = isset($data['editor']) ? $data['editor']: null;
        $object->date_birth = isset($data['date_birth']) ? $data['date_birth']: null;
        $object->active_press = isset($data['active_press']) ? $data['active_press'] : false;
        $object->receive_news = isset($data['receive_news']) ? $data['receive_news'] : false;
        $object->hash_press = isset($data['active_press']) ? Hash::make($data['email']) : null;
        $object->role = isset($data['role']) ? $data['role'] : 6;
        $object->password = Hash::make($data['password']);
        
        return $object;
    }


    public function put($id, $data)
    {
        $object = User::find($id);
        $object->name = $data['name'];
        $object->email = $data['email'];
        $object->cpf = isset($data['cpf']) ? $data['cpf'] : null;
        $object->phone = isset($data['phone']) ? $data['phone']: null;
        $object->company = isset($data['company']) ? $data['company']: null;
        $object->editor = isset($data['editor']) ? $data['editor']: null;
        $object->date_birth = isset($data['date_birth']) ? $data['date_birth']: null;
        if (!isset($data['active_press']))
            $object->hash_press = isset($object->hash_press) ? null : Hash::make($object->email);
        else
            $object->hash_press = null;

        $object->active_press = isset($data['active_press']) ? $data['active_press'] : false;
        $object->receive_news = isset($data['receive_news']) ? $data['receive_news'] : false;
        $object->role = isset($data['role']) ? $data['role'] : 6;
        if (isset($data['password']) && strlen($data['password']) > 0) {
            $object->password = Hash::make($data['password']);
        }
        
        return $object;
    }

        
    public function storeAdmin($data)
    {
 
        $object = new User();
        $object->name = $data['name'];
        $object->email = $data['email'];
        $object->cpf = null;
        $object->phone = null;
        $object->company = null;
        $object->editor = null;
        $object->date_birth = null;
        $object->active_press = false;
        $object->receive_news = false;
        $object->hash_press = null;
        $object->role = $data['rule_id'];
        $object->password = Hash::make($data['password']);
        
        return $object;
    }

   
        
    public function putAdmin($id, $data)
    {
 
        $object = User::find($id);
        $object->name = $data['name'];
        $object->email = $data['email'];
        $object->role =  $data['rule_id'];
        
        return $object;
    }

   

    //Relacionamento.
    public function logs()
    {
        //     $this->hasOne(relacao, chave estrangeira, primary key);
        return $this->hasOne('App\Log', 'user_id', 'id');
    }


   
    public function eAdmin()
    {
      //return $this->id == 1 ? true : false;
      return $this->existeRule('Admin');
      
    }


    //Muitos usuarios tem muitos papeis
    public function rules()
    {
      return $this->belongsToMany('App\Rule');
    }

    public function adicionarRule($rule)
    {
      
        if (is_string($rule)) {
          $rule = Rule::where('name', '=', $rule)->firstOrFail();
        }
        
        $this->rules()->detach();

        return $this->rules()->attach($rule); //attach - adiciona um relacionamento

    }

    public function existeRule($rule){
      
        if (is_string($rule)){
            $rule = Rule::where('name', '=', $rule)->firstOrFail();
        }
        
        return (boolean) $this->rules()->find($rule->id);
    }

    public function removeRule($rule){
        if (is_string($rule)){
            $rule = Rule::where('name', '=', $rule)->firstOrFail();
        }

        return $this->rules()->detach($rule); //detach - remove um relacionamento
    }

    public function hasPermission($rules) 
    {

        $userRules = $this->rules;
        
        return $rules->intersect($userRules)->count(); //o metodo intersect traz a intersecção entre duas listas (ela compara as listas e verifica se pelo menos um item igual).
    }

    

    public function isProfileAdmin()
    {   
        if ($this->role == self::$TYPE_ADMIN) {
            return TRUE;
        }
        return FALSE;
    }

    public function isProfileNews()
    {
        if ($this->role == self::$TYPE_NEWS)
            return TRUE;
        return FALSE;
    }


    public function isProfilePress()
    {
        if ($this->role == self::$TYPE_PRESS)
            return TRUE;
        return FALSE;
    }

    public function isProfileAdminPress()
    {
        if ($this->role == self::$TYPE_NEWS_PRESS)
            return TRUE;
        return FALSE;
    }

    public static function getUsersPress(){
        return User::where('active_press','=', 1)->where('role','=', 6)->where('receive_news','=', 1)->get();
    }

}
