<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailNotification extends Mailable
{
    use Queueable, SerializesModels;
    protected $dados;
    protected $tipo;
    public $view;
    public $subject;
    public $para;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($viewEmail = '', $subject = '', $tipo = '', $dados = null, $para = null)
    {
        $this->dados = $dados;
        $this->tipo = $tipo;
        $this->view = $viewEmail;
        $this->subject = $subject;
        $this->para = $para;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->tipo == 'new_user-news_press') {

            return $this->view($this->view)
                ->from('preagendamento@caoa.com.br', 'Site Caoa')
                ->subject($this->subject);

        } 
        
        if ($this->tipo == 'register_user-news_press') {

            return $this->view($this->view)
                ->from('preagendamento@caoa.com.br', 'Site Caoa')
                ->subject($this->subject);

        } 
        
        if ($this->tipo == 'active_user-news_press') {
            
            return $this->view($this->view)->with([
                    "user" => $this->dados['user']
                ])
                ->from('preagendamento@caoa.com.br', 'Site Caoa')
                ->subject($this->subject);

        } 
        
        if ($this->tipo == 'actived_user-news_press') {
            
            return $this->view($this->view)
                ->from('preagendamento@caoa.com.br', 'Site Caoa')
                ->subject($this->subject);

        } 

        if ($this->tipo == 'published-new') {
            
            return $this->view($this->view)->with([
                    'category'=>$this->dados['category'],
                    'titleNews'=>$this->dados['titleNews'],
                    'link'=>$this->dados['link']
                ])
                ->from('preagendamento@caoa.com.br', 'Site Caoa')
                ->subject($this->subject);

        } 
        
        if ($this->tipo == 'user-published-new') {
            return $obj = $this->view($this->view)->with([
                    'category'=>$this->dados['category'],
                    'titleNews'=>$this->dados['titleNews'],
                    'link'=>$this->dados['link'],
                    'linkRemove'=> \URL::to('news-press/descadastrado/') .'/' .$this->para
                ])
                ->from('preagendamento@caoa.com.br', 'Site Caoa')
                ->subject($this->subject);

        } 
        
        if ($this->tipo == 'alert_pre_agendamento') {
            
            return $obj = $this->view($this->view)->with($this->dados)
                ->from('preagendamento@caoa.com.br', 'Site Caoa')
                ->subject($this->subject);

        } 
        
        if ($this->tipo == 'send_reset_password') {
            
            return $this->view($this->view)->with([
                    "email" => $this->dados->email,
                    "token" => $this->dados->token,
                    "tipo" => "reset_password"
                ])
                ->from('preagendamento@caoa.com.br', 'Site Caoa')
                ->subject($this->subject);
        }
    }
}
