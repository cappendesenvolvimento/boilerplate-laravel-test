<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Log extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'acao', 'descricao',
    ];

    
    public function users()
    {
        //     $this->belongsTo(relação, chave estrangeira local, primary key da relação);
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function insertLog($descricao, $idRegistro)
    {
        //DELETE, INSERT, UPDATE
        $userLog = User::find(auth()->user()->id);
        $userLog->logs()->create(['acao' => 'Inserir', 'descricao' => 'Inseriu '.$descricao.' '.$idRegistro]);
        return ;
    }

    public function updateLog($descricao, $idRegistro)
    {
        //DELETE, INSERT, UPDATE
        $userLog = User::find(auth()->user()->id);
        $userLog->logs()->create(['acao' => 'Atualizar', 'descricao' => 'Atualizou '.$descricao.' '.$idRegistro]);
        return ;
    }

    public function deleteLog($descricao, $idRegistro)
    {
        //DELETE, INSERT, UPDATE
        $userLog = User::find(auth()->user()->id);
        $userLog->logs()->create(['acao' => 'Deletar', 'descricao' => 'Removeu '.$descricao.' '.$idRegistro]);
        return ;
    }


}
