<?php

namespace App\Jobs;

use App\Mail\MailNotification;
use App\Model\NewsPress;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class SendNewsPressEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $timeout = 180000;

    private $newsPress;
    private $users;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(NewsPress $newsPress, $users)
    {
        $this->newsPress = $newsPress;
        $this->users = $users;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        /*$data = array(  'category'  =>$this->newsPress->category->name,
                        'titleNews' => $this->newsPress->main_title,
                        'link'=> \URL::to('imprensa'). $this->newsPress->slug,
         );
        $view = 'notification.user-published-new';
        $subject = '[Site Caoa] Nova Notícia Cadastrada';
        $tipo = 'user-published-new';
        
        if(count($this->users) > 0){
            $cont = 0;
            foreach($this->users as $key => $user) { 
                
                if( $cont === 1) {

                    sleep(10);
                    //$this->release(5);
                    $cont = 0;
                }
                $cont++;
                    
                // Mail::to($user->email) 
                //     ->send( new MailNotification($view, $subject, $tipo, $data, $user->email));
                DB::insert('INSERT INTO test_job (name, created_at, updated_at) VALUES ( ?, NOW(), NOW())', array($user->email));
            }
            dd($cont);
        }
        */
    }

}
