<?php

namespace App\MrSales;

use App\Model\ZCatalog;
use App\Model\ZFlag;
use App\Model\ZShop;
Use App;
use GuzzleHttp\Client;
use Psr\Http\Message;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MrSalesService
{
    /**
     * @param array $cURLOpts
     * @return HttpResult
     */



    static private function httpClient($method, $url, $params)
    {

        $config = self::getConfig();
        $token = self::getToken();

        $headers = [
            'Authorization' => $token,
            'Content-Type' => 'application/json',
            'User-Agent' => 'CAOA cURL Request',
        ];

        $client = new Client([
            'headers' => $headers
        ]);
        
       
        $cURLError = null;
        try {
            if ($method == 'GET') {
                
                $response = $client->get($url, [
                    'headers' => $headers
                ]);
            } else if ($method == 'POST') {
                // $url = str_replace("https://api.mrsales.com.br/", "http://api-homolog.mrsales.com.br/", $url);
                
                $response = $client->post($url, [
                    'headers' => $headers,
                    'body'=> $params
                ]);
            }
        } catch (ClientException $e) {
            $response = $e->getResponse();
            $cURLError = $response->getBody()->getContents();
        }
        
        $data = $response->getBody();
        $contentIsJson = strpos($response->getHeader('Content-type')[0], 'application/json') !== false;
        

        $httpResult = new HttpResult();
        
        $httpResult->setCode($response->getStatusCode());
        $httpResult->setData($contentIsJson ? json_decode($data, true) : $data);
        
        if ($httpResult->isSuccess()) {
            $message = 'success';
            if (array_key_exists('Message', $httpResult->getData())) {
                $message = $httpResult->getData()['Message'];
            }
        }

        // dd($message);
        $httpResult->setMessage($httpResult->isSuccess() ? $message : $cURLError);
        // dd($httpResult);
        if (array_key_exists('Success', $httpResult->getData())) {
            $httpResult->setCode($httpResult->getData()['Success'] ? 200 : 500);
        }
        // dd($httpResult);
        return $httpResult;
        // $cURL = curl_init();
        // $cURLOpts[CURLOPT_RETURNTRANSFER] = 1;
        // $cURLOpts[CURLOPT_USERAGENT] = 'CAOA cURL Request';
        // curl_setopt_array($cURL, $cURLOpts);
        // $data = curl_exec($cURL);

        // $cURLGetInfo = curl_getinfo($cURL);
        // $contentIsJson = strpos($cURLGetInfo['content_type'], 'application/json') !== false;

        // $httpResult = new HttpResult();
        // $httpResult->setCode($cURLGetInfo['http_code']);
        // $httpResult->setData($contentIsJson ? json_decode($data, true) : $data);
        // if ($httpResult->isSuccess()) {
        //     $message = 'Ok';
        //     if (array_key_exists('Message', $httpResult->getData())) {
        //         $message = $httpResult->getData()['Message'];
        //     }
        // }
        // $httpResult->setMessage($httpResult->isSuccess() ? $message : curl_error($cURL));
        // if (array_key_exists('Success', $httpResult->getData())) {
        //     $httpResult->setCode($httpResult->getData()['Success'] ? 200 : 500);
        // }

        // curl_close($cURL);

        // return $httpResult;
    }

    static private function jsonGET($url, array $headers = array())
    {
        // $headers = array_merge(array( 'Content-Type: application/json',), $headers);
        $httpResult = self::httpClient('GET', $url, $headers);
        return $httpResult;
    }

    static private function jsonPOST($url, array $postParams = array())
    {
        
        $httpResult = self::httpClient('POST', $url, json_encode($postParams));
        return $httpResult;
    }

    static private function getConfig()
    {
        
        
        // $config = app('config')->get('mrsales');
        $config = Config::get('mrsales');

        $env = $config['env'];
        //if ($_SERVER['HTTP_HOST'] === 'caoa.test') $env = 'homolog';
        $url = $config['url'][App::environment()];
        $cacheKeysKey = 'cache_keys';
        foreach ($config[$cacheKeysKey] as $k => $v) {
            $config[$cacheKeysKey][$k] .= '-' . ucfirst($env);
        }
        $apiKey = 'api';
        foreach ($config[$apiKey] as $k => $v) {
            $config[$apiKey][$k] = $url.$v;
        }
        
        return $config;
    }

    static function cleanCache() {
        $config = self::getConfig();
        foreach ($config['cache_keys'] as $cacheKey) {
            Cache::forget($cacheKey);
        }
    }

    /**
     * Autenticação (atualizado em 07/10)
     *
     * Para acessar qualquer serviço da API é necessário passar o Token fixo no header.
     * (Caso não possua um token válido, solicitar em suporte@mrsales.com.br)
     *
     * https://github.com/MrSales/parceiros-api#autentica%C3%A7%C3%A3o-atualizado-em-0710
     *
     * @return bool|string
     */
    static function getToken()
    {
        $logKey = array_reverse(explode('\\', __METHOD__))[0];
        $config = self::getConfig();

        $cacheResult = $config['authorization_token'];
        Log::info($logKey.'.Success' . ' - ' . $cacheResult);
        return $cacheResult;
    }

    /**
     * Bandeiras (novo - atualizado em 11/10/2018)
     *
     * Listar de bandeiras com base nas lojas associadas no token
     *
     * https://github.com/MrSales/parceiros-api#bandeiras-novo---atualizado-em-11102018
     *
     * @return HttpResult
     */
    static function getFlags()
    {
        $logKey = array_reverse(explode('\\', __METHOD__))[0];
        $config = self::getConfig();
        $token = self::getToken();
//        if (empty($token)) return false;
        $cacheKey = $config['cache_keys']['flags'];
        $httpResult = Cache::get($cacheKey);
        if (empty($httpResult)) {
            $cacheMinutes = $config['cache_minutes'];
            $url = $config['api']['flags'];
            $httpResult = self::jsonGET($url);
            if ($httpResult->isSuccess()) {
                Cache::put($cacheKey, $httpResult, $cacheMinutes);
                //ZFlag::importAll($httpResult->getData()['Data']);
                Log::info($logKey.'.Success' . sprintf(' - %s', $httpResult));
            } else {
                Log::error($logKey.'.Error' . sprintf(' - %s', $httpResult));
            }
        }
        return $httpResult;
    }

    /**
     * Lojas
     *
     * Lista de lojas que o usuário tem permissão para cadastrar lead.
     *
     * https://github.com/MrSales/parceiros-api#lojas
     *
     * @param array $getParams [BandeiraID=1, marca=X&modelo=Y, observacao=Caminhões]
     * @return HttpResult
     */
    static function getShops(array $getParams = array())
    {
        $logKey = array_reverse(explode('\\', __METHOD__))[0];
        $config = self::getConfig();
        $token = self::getToken();
//        if (empty($token)) return false;
        $cacheKey = $config['cache_keys']['shops'];
        $httpResult = Cache::get($cacheKey);
        if (empty($httpResult)) {
            $cacheMinutes = $config['cache_minutes'];
            $urlParams = '';
            if (!empty($getParams)) {
                $urlParams = http_build_query($getParams);
            }
            $url = $config['api']['shops'] . (!empty($urlParams) ? '?'.$urlParams : '');
            $httpResult = self::jsonGET($url);
            if ($httpResult->isSuccess()) {
                Cache::put($cacheKey, $httpResult, $cacheMinutes);
                //ZShop::importAll($httpResult->getData()['Data']);
                Log::info($logKey.'.Success' . sprintf(' - %s', $httpResult));
            } else {
                Log::error($logKey.'.Error' . sprintf(' - %s', $httpResult));
            }
        }
        return $httpResult;
    }

    /**
     * Catálogo da Loja
     *
     * Listar o catálogo de veiculos da loja
     *
     * https://github.com/MrSales/parceiros-api#cat%C3%A1logo-da-loja
     *
     * @param int $shopId
     * @return HttpResult
     */
    static function getCatalog($shopId)
    {
        $logKey = array_reverse(explode('\\', __METHOD__))[0];
        $config = self::getConfig();
        
        $token = self::getToken();
//        if (empty($token)) return false;
        $cacheKey = $config['cache_keys']['catalog'];
        $httpResult = Cache::get($cacheKey);
        if (empty($cacheResult)) {
            $cacheMinutes = $config['cache_minutes'];
            $url = sprintf($config['api']['catalog'], $shopId);
            $httpResult = self::jsonGET($url);
            if ($httpResult->isSuccess()) {
                Cache::put($cacheKey, $httpResult, $cacheMinutes);
                
                Log::info($logKey.'.Success' . sprintf(' - %s', $httpResult));
            } else {
                Log::error($logKey.'.Error' . sprintf(' - %s', $httpResult));
            }
        }
        return $httpResult;
    }

    /**
     * Catálogo - Horários disponiveis para agendamento (novo - atualizado em 07/10/2018)
     *
     * Listar os horários disponíveis para agendamento de um veículo
     *
     * https://github.com/MrSales/parceiros-api#cat%C3%A1logo---hor%C3%A1rios-disponiveis-para-agendamento-novo---atualizado-em-07102018
     *
     * @param array $data
     * @return HttpResult
     */
    static function postCatalogSchedules(array $data = array())
    {
        /*
        $data = array(
            'LojaID' => 1,
            'LojaCatalogoID' => 1,
            'Dia' => '2018-10-08',
        );
        */
        $logKey = array_reverse(explode('\\', __METHOD__))[0];
        $config = self::getConfig();
        $token = self::getToken();
//        if (empty($token)) return false;
        $cacheKey = $config['cache_keys']['catalog_schedules'];
        $httpResult = Cache::get($cacheKey);
        if (empty($cacheResult)) {
            $cacheMinutes = $config['cache_minutes'];
            $url = $config['api']['catalog_schedules'];
            $httpResult = self::jsonPOST($url, $data);
            if ($httpResult->isSuccess()) {
                Cache::put($cacheKey, $httpResult, $cacheMinutes);
                Log::info($logKey.'.Success' . sprintf(' - %s', $httpResult));
            } else {
                Log::error($logKey.'.Error' . sprintf(' - %s', $httpResult));
            }
        }
        return $httpResult;
    }

    /**
     * Cadastrar Lead
     *
     * https://github.com/MrSales/parceiros-api#cadastrar-lead
     *
     * @param array $data
     * @return HttpResult
     */
    static function postStoreLead(array $data = array())
    {
    
        $logKey = array_reverse(explode('\\', __METHOD__))[0];
        $config = self::getConfig();
        $token = self::getToken();
        $url = $config['api']['store_lead'];
        $httpResult = self::jsonPOST($url, $data);
        if ($httpResult->isSuccess()) {
            Log::info($logKey.'.Success' . sprintf(' - %s', $httpResult), $data);
        } else {
            Log::error($logKey.'.Error' . sprintf(' - %s', $httpResult), $data);
        }
        return $httpResult;
    }

    /**
     * Cadastrar Lead Test Drive
     *
     * https://github.com/MrSales/parceiros-api#cadastrar-lead-test-drive
     *
     * @param array $data
     * @return HttpResult
     */
    static function postStoreLeadTestDrive(array $data = array())
    {

        $logKey = array_reverse(explode('\\', __METHOD__))[0];
        $config = self::getConfig();
        $token = self::getToken();
        $url = $config['api']['store_lead_test_drive'];
 
        $httpResult = self::jsonPOST($url, $data);
        if ($httpResult->isSuccess()) {
            Log::info($logKey.'.Success' . sprintf(' - %s', $httpResult), $data);
        } else {
            Log::error($logKey.'.Error' . sprintf(' - %s', $httpResult), $data);
        }
        return $httpResult;
    }
}