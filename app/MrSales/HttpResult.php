<?php

namespace App\MrSales;

class HttpResult
{
    private $code;
    private $message;
    private $data;

    public function __toString()
    {
        return json_encode(array(
            'code' => $this->getCode(),
            'message' => $this->getMessage(),
            'data' => $this->getData(),
        ));
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function dataIsEmpty()
    {
        return empty($this->data);
    }

    public function isSuccess()
    {
        return $this->code == 200;
    }
}