<?php

namespace App\Http\Middleware;

use App\Model\LinksFooter;
use Closure;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Auth;

class PressUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        
        if (!Auth::guest()) {

            if (!(Auth::user()->isProfilePress() || Auth::user()->isProfileNews() || Auth::user()->isProfileAdmin() || Auth::user()->isProfileAdminPress())) {
               
                if (request()->ajax()) {
                    return  response('Unauthorized.', 401);
                }
               
                if (Auth::check() && Auth::user()->role != '6') {
                    Auth::logout();
                    return redirect()->route('site.imprensa');
                }
            }
        } else {
            //return  redirect()->route('site.imprensa');
            // $linksFooter = LinksFooter::first();
            // return view('site.imprensa.imprensa', compact('linksFooter'));
        }


        // if (!Auth::check()) {
        //     //login do site
        //     return redirect()->route('site.imprensa');
        // }

        // if (Auth::check() && Auth::user()->role != '6') {
        //     return redirect()->route('principal');
        // }

        // return $next($request);
        return $this->nocache( $next($request) );
    }

    protected function nocache($response)
    {
        $response->headers->set('Cache-Control','nocache, no-store, max-age=0, must-revalidate');
        $response->headers->set('Expires','Fri, 01 Jan 1990 00:00:00 GMT');
        $response->headers->set('Pragma','no-cache');

        return $response;
    }
}
