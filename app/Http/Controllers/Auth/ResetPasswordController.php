<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\MailNotification;
use App\Providers\RouteServiceProvider;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo = '/admin';

    public function postRemind(Request $request)
    {
        $email = $request['email'];
        $user = User::where('email', $email)->first();
        
        //Check if the user exists
        if (!$user) {
            return redirect()->back()->withErrors(['Usuário não cadastrado ou inválido.']);
        }

        //Create Password Reset Token
        DB::table('password_reminders')->insert([
            'email' => $request->email,
            'token' => str_random(60),
            'created_at' => Carbon::now()
        ]);
        //Get the token just created above
        $tokenData = DB::table('password_reminders')
                    ->where('email', $request->email)->first();

        try{            
            Mail::to($request->email)->send( new MailNotification('notification.reset_password_admin', 'Caoa - Redefinir Senha', 'send_reset_password', $tokenData));
        } catch(\Exception $e){
            return redirect()->back()->withErrors(['Ocorreu um erro ao enviar uma redefinição de senha para o seu email. Por favor, tente novamente.']);
            
        }
        return redirect()->back()->with(['status' => 'Um link de redefinição foi enviado para seu endereço de e-mail.']);
       
    }

    public function getReset(Request $request, $token = null)
    {
        if (is_null($token)) App::abort(404);
        $email = $request['email'];

        return view('auth.confirm', compact('token', 'email'));
    }


    public function postReset(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|same:password_confirmation',
            'password_confirmation' => 'required|same:password',
            'token' => 'required' ]);
    
        if ($validator->fails()) {
            return redirect()->back()->withErrors(['Preencha os dados corretamente.']);
        }
        
        $password = $request->password;
       
        $tokenData = DB::table('password_reminders')
                    ->where('token', $request['token'])->first();

        $created_at = $tokenData->created_at;
        
        $expired = false;
        $expired = Carbon::parse($created_at)->addSeconds(config('auth.passwords.users.expire')*60)->isPast();
        
        //Redirect the user back to the password reset request form if the token is invalid
        if ((!$tokenData) || ($expired)){
            return redirect()->back()->withErrors(['Token expirado ou inválido.']);
        }
        
        $user = User::where('email', $tokenData->email)->first();
        // Redirect the user back if the email is invalid
        if (!$user) {
            return redirect()->back()->withErrors(['Usuário não cadastrado ou inválido.']);
        }
        //Hash and update the new password
        $user->password = Hash::make($password);
        if ($user->update()) {

            DB::table('password_reminders')->where('email', $user->email)
            ->delete();
            return redirect()->route('admin.login')->with('status', 'Senha alterada com sucesso!');
        } else {
            return redirect()->back()->withErrors(['Usuário não cadastrado ou inválido.']);
        }
       
    }
}
