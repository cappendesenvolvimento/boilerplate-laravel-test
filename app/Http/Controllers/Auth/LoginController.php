<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class LoginController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function logout(Request $request)
    {
        $redirectTo = '/admin/login';
        
        Auth::logout();
        Session::flush();

        $request->session()->flush();
        $request->session()->regenerate(true);
        $request->session()->invalidate();
      
        $request->session()->regenerateToken();
        
        header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache"); // HTTP 1.0.
        header("Expires: 0"); // Proxies.
        session()->flash('alert-success', 'Success logged out');
        return redirect()->to( $redirectTo );
        //redirect(URL::previous());
    }

    
//     public function redirectTo()
//     {
//         if (Auth::user()->role == 6) {
//             $this->redirectTo = '/imprensa-list';
//         } else {
//             $this->redirectTo = '/admin/dashboard';
//         }
//         return $this->redirectTo;
//         //return $next($request);
//     }
}
