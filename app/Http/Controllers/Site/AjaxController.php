<?php namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Model\City;
use App\Model\Dealer;
use App\Model\Brand;
use App\Model\Product;
use App\Model\Recall;
use App\Model\ZCatalog;
use App\Model\ZFlag;
use Caoa\Utils\Utils;

class AjaxController extends Controller{

    public function allstates()
    {
        $states = City::groupBy('uf')->orderBy('uf', 'ASC')->select('cities.uf')->get();

        $statesJson = array();

        foreach($states as $state){

            $s = array("id" => $state->uf, "name" => $state->uf);

            $statesJson[] = $s;

        }

        return response()->json($statesJson);
    }

    public function allcitiesbystate($state)
    {
        $cities = City::where('uf', '=', strtoupper($state))->orderBy('name', 'ASC')->select('cities.*')->get();

        $citiesJson = array();

        foreach($cities as $city){

            $c = array("id" => $city->id, "name" => $city->name);

            $citiesJson[] = $c;

        }

        return response()->json($citiesJson);
    }

    public function formatStrToSlug($str){
        $newString = Str::slug($str);

        $string = array('string' => "/".$newString);

        return response()->json($string);
    }

    public function allProducts(){

        $products = ZCatalog::query()->get();
        $json = array();
        $separator = ' | ';

        foreach($products as $p){
            $marca = ZFlag::query()->where('Nome', $p->Marca)->first();
            $json[] = array(
                'id' => intval($p->LojaCatalogoID),
                'status' => 'active',
                'id_hyponobox' => intval($p->LojaCatalogoID),
                'name' => $p->Modelo.$separator.$p->Versao,
                'brand_id' => $marca ? intval($marca->BandeiraID) : null,
                'brand_id_hyponobox' => $marca ? intval($marca->BandeiraID) : null,
                'price' => 0,
                'car_model' => $p->Modelo,
                'shop_id' => $p->LojaID,
                'is_truck' => boolval(preg_match('/caminh(o|õ)es/i', $p->shop->Observacao)),
            );
        }

        return response()->json($json);
    }

    // public function service(){

    //     $model = new Dealer();

    //     $dealers = $model->searchDealerByStateBrandCategory();

    //     $dealersJson = $model->generateArrayForJson($dealers);

    //     return response()->json($dealersJson);
    // }

    // public function allBrands(){

    //     $brands = ZFlag::query()->get();

    //     $json = array();

    //     $filterPattern = '/(HMB)|(Oficina)/';

    //     if (strpos($_SERVER['HTTP_REFERER'], Utils::getHostCAOA()) !== false) {
    //         $filterPattern = '/(Oficina)/';
    //     }

    //     foreach($brands as $b){
    //         if (preg_match($filterPattern, $b->Nome)) continue;
    //         $json[] = array(
    //             'id' => $b->BandeiraID,
    //             'id_hyponobox' => $b->BandeiraID,
    //             'name' => $b->Nome,
    //             'category' => ''
    //         );
    //     }

    //     return response()->json($json);
    // }

    // public function allProducts(){

    //     $products = ZCatalog::query()->get();
    //     $json = array();
    //     $separator = ' | ';

    //     foreach($products as $p){
    //         $marca = ZFlag::query()->where('Nome', $p->Marca)->first();
    //         $json[] = array(
    //             'id' => intval($p->LojaCatalogoID),
    //             'status' => 'active',
    //             'id_hyponobox' => intval($p->LojaCatalogoID),
    //             'name' => $p->Modelo.$separator.$p->Versao,
    //             'brand_id' => $marca ? intval($marca->BandeiraID) : null,
    //             'brand_id_hyponobox' => $marca ? intval($marca->BandeiraID) : null,
    //             'price' => 0,
    //             'car_model' => $p->Modelo,
    //             'shop_id' => $p->LojaID,
    //             'is_truck' => boolval(preg_match('/caminh(o|õ)es/i', $p->shop->Observacao)),
    //         );
    //     }

    //     return response()->json($json);
    // }

    // private function formatCurrency($value) {
    //     $value = number_format($value, 2, ',', '.');
    //     return $value;
    // }

    // public function allProductsByBrand($hyponobox_id){
    //     $brands = Brand::where('hyponobox_id', '=', $hyponobox_id)->get();

    //     $brand= null;
    //     foreach($brands as $b){
    //         $brand = $b;
    //         break;
    //     }

    //     $products = Product::where('brand_id', '=', $brand->id)->get();

    //     $json = array();

    //     foreach($products as $p){
    //         $json[] = array('id' => $p->id, 'id_hyponobox' => $p->hyponobox_id, 'name' => $p->name);
    //     }

    //     return response()->json($json);
    // }

    // public function allProductsByBrandId($brand){
    //     $products = Product::where('brand_id', '=', $brand)->get();

    //     $json = array();

    //     foreach($products as $p){
    //         $json[] = array('id' => $p->id, 'id_hyponobox' => $p->hyponobox_id, 'name' => $p->name);
    //     }

    //     return response()->json($json);
    // }

    // public function checkProcessImportChassi($recall_id){
    //     $recall = Recall::find($recall_id);

    //     $json = array('sit' => $recall->process);

    //     return response()->json($json);
    // }
}
