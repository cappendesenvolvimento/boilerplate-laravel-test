<?php

namespace App\Http\Controllers\Site;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use Illuminate\Support\Facades\Mail;
use \App\Mail\MailNotification;
use App\Model\AlertSite;
use App\Model\Banner;
use App\Model\Brand;
use App\Model\Home;
use App\Model\HomeLancamento;
use App\Model\LinksFooter;
use App\Model\Media;
use App\Model\ModeloMenu;
use App\Model\News;
use App\Model\NewsCategory;
use App\Model\NewsPress;
use App\Model\NewsPressCategory;
use App\Model\NewsPressFile;
use App\Model\Product;
use App\Model\ProductVersion;
use stdClass;
use Symfony\Component\Console\Helper\Helper;

class HomeController extends Controller
{

    private $lang = 'pt';
    private $links_footer = null;
    
    private $limit = 8;
    private $skip;

    private $menu_brand = '';
    private $menu_models = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        if(parseLocale() == '/') {
            $this->lang = 'pt_br';
        } else {
            $this->lang = parseLocale();
            switch ($this->lang) {
                case 'pt': $this->lang = 'pt_br'; break;
                case 'en': $this->lang = 'en_us'; break;
                default:  $this->lang = 'pt_br'; break;
            }
    
        }

        //$this->links_footer = LinksFooter::first();


        $this->menu_brands = Brand::where('status', 1)->where('id', '<>', 12)->orderby('sequence', "ASC")->get();    
        $this->models = null;
        foreach($this->menu_brands as $brand) {
            $models[] = ModeloMenu::getModelos($brand);
        }

        $this->menu_models = $models;
        
    }

    

    public function index($page = '')
    {
        // switch ($this->lang) {
        //     case 'pt': $lang = 'pt_br'; break;
        //     case 'en': $lang = 'en_us'; break;
        //     default:  $lang = 'pt_br'; break;
        // }

        $dataAtual = date('Y-m-d');
            
        $banners = Banner::with('file')
                          ->where("language", "=", $this->lang)
                          ->where("status", "=", 1)
                          ->where(function ($q) use($dataAtual) {
                                $q->whereNull('date_start')->orWhere('date_start', '<=', $dataAtual);
                            })
                          ->where(function ($q) use($dataAtual) {
                                $q->whereNull('date_end')->orWhere('date_end', '>=', $dataAtual);
                            })
                          ->orderby('sequence', 'ASC')
                          ->get();
        
        
        $home = Home::with('image_one', 'image_two', 'image_three', 'image_video', 'img_full_one', 'img_full_two', 'img_split_right', 'img_split_left', 'img_split_full')
                      ->where("language", "=", $this->lang)
                      ->first();
        
        $lancamentos = HomeLancamento::with('image')->where("language", "=", $this->lang)->where("status", "=", 1)->orderby("order", 'ASC')->get();
        
        $news = NewsPress::with('featured_img')
                     ->where('active', '=', 1)
                     ->where('destaque', '=', 1)
                     ->where(function($query){
                        return $query
                        ->orWhereNull('date_published')
                        ->orWhere('date_published', '<=', date('Y-m-d'));
                     })
                     ->orderby('created_at', 'DESC')
                     ->limit(3)
                     ->get();


        $objBrands = Brand::where('status', 1)->where('id', '<>', 12)->orderby('sequence', "ASC")->get();      
        $models = null;
        foreach($objBrands as $brand) {
            $models[] = ModeloMenu::getModelos($brand);
        }

        $comunicados = AlertSite::where('status', 1)->where('date_published', '<=', NOW())->orderby('created_at', 'DESC')->first();
        $alertSite = false;
        if ($comunicados)
            $alertSite = true;
        
        $linksFooter = $this->links_footer;    
        
        return view('site.home.home', compact('banners', 'home', 'lancamentos', 'news', 'objBrands', 'models', 'comunicados', 'alertSite'));
    }

    public function news(Request $request)
    {

        $pagina = 'blog';
        $skip = ($request['page']-1) * $this->limit;

        $categorie = $request['categorie'];
        $search = $request['search'];
        $dtinicio = (@$request['dtinicio'] != '') ? dataIng($request['dtinicio']) : '1980-01-01';
        $dtPublicacao = (@$request['dtinicio'] != '') ? dataIng($request['dtinicio']) : '';
        $dtfinal = (@$request['dtfinal'] != '') ? dataIng($request['dtfinal']) : '2050-12-31';
        
        
        $categorias = NewsPressCategory::all();

        $allnews = NewsPress::with('featured_img')
                    ->where('active', '=', 1)
                    ->where('destaque', '=', 1)
                    ->where(function($query) use($dtPublicacao, $dtinicio){
                        if ($dtPublicacao != '') {
                            return $query
                                    ->orWhereNull('date_published')
                                    ->orwhere('date_published', '>=', $dtPublicacao)
                                    ->where('date_published', '<=', date('Y-m-d'));
                        } else {
                            return $query
                                    ->orWhereNull('date_published')
                                    ->orwhere('date_published', '>=', $dtinicio)
                                    ->where('date_published', '<=', date('Y-m-d'));
                        }  
                    })
                    ->where(function($query) use($dtinicio, $dtfinal){
                        return $query->where('created_at', '>=', $dtinicio)
                               ->where('created_at', '<=', $dtfinal);
                    })
                    ->where(function($query)  use ($search){
                        if ($search != '') :
                            return $query->where('main_title', 'LIKE', '%'.$search.'%')->orWhere('summary', 'LIKE', '%'.$search.'%')->orWhere('content', 'LIKE', '%'.$search.'%');
                        endif;
                    })->where(function($query)  use ($categorie){
                        if ($categorie != '')  :
                            return $query->where('news_category_id', '=', $categorie);
                        endif;
                    })
                    ->orderby('created_at', 'DESC')
                    ->get();

        $news = NewsPress::with('featured_img')
                    ->where('active', '=', 1)
                    ->where('destaque', '=', 1)
                    ->where(function($query) use($dtPublicacao, $dtinicio){
                        if ($dtPublicacao != '') {
                            return $query
                                    ->orWhereNull('date_published')
                                    ->orwhere('date_published', '>=', $dtPublicacao)
                                    ->where('date_published', '<=', date('Y-m-d'));
                        } else {
                            return $query
                                    ->orWhereNull('date_published')
                                    ->orwhere('date_published', '>=', $dtinicio)
                                    ->where('date_published', '<=', date('Y-m-d'));
                        }  
                    })
                    ->where(function($query) use($dtinicio, $dtfinal){
                        return $query->where('created_at', '>=', $dtinicio)
                               ->where('created_at', '<=', $dtfinal);
                    })
                    ->where(function($query)  use ($search){
                        if ($search != '') :
                            return $query->where('main_title', 'LIKE', '%'.$search.'%')->orWhere('summary', 'LIKE', '%'.$search.'%')->orWhere('content', 'LIKE', '%'.$search.'%');
                        endif;
                    })->where(function($query)  use ($categorie){
                        if ($categorie != '')  :
                            return $query->where('news_category_id', '=', $categorie);
                        endif;
                    })
                    ->orderby('created_at', 'DESC')
                    ->skip($skip)
                    ->limit($this->limit)
                    ->get();

        $totalRegistros = count($allnews);
        $totalPages = ceil(($totalRegistros) / $this->limit);

        if ($request['action'] == 'ajax') { 
            $html = '';
            if ($news) :
                if (count($news) > 0) :
                    foreach($news as $obj) :
                        
                        $image = isset($obj->featured_img) ? url($obj->featured_img->directory.$obj->featured_img->url) : '';
                        $showdate = ($obj->show_date == 1) ? dataBr($obj->created_at) : "";
                        $html .= '<div class="c-card-noticia c-h-img-zoom">
                            <a href="' . route('site.noticia', substr($obj->slug,1)).'">';
                                if($image != '') :
                                $html .= '<figure>
                                                <img src="'. $image .'" alt="">
                                            </figure>';
                                endif;
                                $html .= '<div class="text-noticia">
                                    <span class="data">'.  $showdate .'</span>
                                    <h2>'. $obj->main_title .'</h2>
                                    <p>'.$obj->summary .'</p>
                                </div>
                            </a>
                        </div>';
                        
                    endforeach;
                endif;
            endif;
    
            $data = array("success" => true, "html" => $html, "totalPages" => $totalPages);
            return response()->json($data);
        } 
        
        return view('site.noticias.noticias', compact('news', 'categorias', 'totalPages', 'pagina'));
    }



    public function newInterna(Request $request)
    {


        $news = NewsPress::with('featured_img', 'autor.image')->where("slug", "=", '/'.$request['slug'])->where('destaque', '=', 1)->where('active', '=', 1)->first();
        $newsFiles = NewsPressFile::with('media')->where('news_press_id', $news->id)->orderBy('sequence', 'ASC')->get();
        if (!$news) {
            return response()->view('site.error' . '404', [], 404);
        }
        
        return view('site.noticias.noticia', compact('news', 'newsFiles'));
    }


    public function ofertas(Request $request)
    {

        return view('site.ofertas.ofertas', compact());
    }

    


    public function versaoByMotor(Request $request)
    {
        
        $motor_id = $request['motor_id'];
        $modelo_id = $request['modelo_id'];

        $versionsMotorBase = ProductVersion::with('version_color')->where('product_id', $modelo_id)->where('active', 1)->where('motor_id', $motor_id )->orderby('sequence', 'ASC')->get();

        $prod = Product::find($modelo_id);

        // $versions = ['' => 'Selecione uma versão'];
        // foreach ($versionsMotorBase as $v) {
        //     $versions[$v->id] = $v->name . ' ' . getTransmissao($v->transmissao);
        // }

        $html = '<div class="tab-content">';
                // if ($versionsMotorBase[0]->version_color) : 
                //     foreach($versionsMotorBase[0]->version_color as $key => $cor) : 
                        $image_modelo = ($prod->image) ?  url($prod->image->directory.$prod->image->url) : '' ; 
                        // $active = ($key == 0) ? 'active' : '';
                        // $html .= '<div class="tab-pane fade show '.$active.' " id="versao_color-'. $cor->id .'" role="tabpanel" aria-labelledby="versao_color-'. $cor->id .'-tab">
                        $html .= '<div class="tab-pane fade show active">
                            <img src="'. $image_modelo  .'">
                        </div>';
                //     endforeach;
                // endif;
        $html .= '</div>
            <p class="text-center">Imagem meramente Ilustrativa</p>
            <ul class="nav nav-cores d-flex justify-content-md-center" role="tablist">
                <li class="nav-item label">
                    Selecione a sua cor:
                </li>';
                if (@$versionsMotorBase[0]->version_color) : 
                    foreach($versionsMotorBase[0]->version_color as $key => $cor) : 
                        $image_color = ($cor->color->image) ?  url($cor->color->image->directory.$cor->color->image->url) : '' ;
                        $active = ($key == 0) ? 'active' : '';
                        $ariaselected = ($key == 0) ? 'true' : 'false';
                        $html .= '<li class="nav-item">
                            <a class="nav-link '. $active .' s-color " id="versao_color-'. $cor->id .'-tab" data-toggle="tab" href="#versao_color-'. $cor->id .'" role="tab" aria-controls="versao_color-'. $cor->id .'" aria-selected="'.$ariaselected.'" data-condicoes="'. $cor->texto_legal_color .'" data-preco="R$ '. number_format($cor->price_color, 2, ',', '.') .'">
                                <img src="'. $image_color  .'">
                            </a>
                        </li>';
                    endforeach;
                endif;    
        $html .= '</ul>
            <p class="condicoes small">'. strip_tags(html_entity_decode( $versionsMotorBase[0]->version_color[0]->texto_legal_color)).'</p>';


        $dados = array("status" => 1, "html" => $html, "price" => number_format($versionsMotorBase[0]->version_color[0]->price_color, 2, ',', '.'), "versions" => $versionsMotorBase);
       
        return response()->json($dados,200);
    }

    public function versaoBySelect(Request $request)
    {
        
        $version_id = $request['version_id'];

        $versionsMotorBase = ProductVersion::with('version_color')->where('id', $version_id)->where('active', 1)->get();
        
        $prod = Product::find($versionsMotorBase[0]->product_id);
        
        // $versions = ['' => 'Selecione uma versão'];
        // foreach ($versionsMotorBase as $v) {
        //     $versions[$v->id] = $v->name . ' ' . getTransmissao($v->transmissao);
        // }

        $html = '<div class="tab-content">';
                // if ($versionsMotorBase[0]->version_color) : 
                //     foreach($versionsMotorBase[0]->version_color as $key => $cor) : 
                        $image_modelo = ($prod->image) ?  url($prod->image->directory.$prod->image->url) : '' ; 
                        // $active = ($key == 0) ? 'active' : '';
                        // $html .= '<div class="tab-pane fade show " id="versao_color-'. $cor->id .'" role="tabpanel" aria-labelledby="versao_color-'. $cor->id .'-tab">
                        $html .= '<div class="tab-pane fade show active" >
                            <img src="'. $image_modelo  .'">
                        </div>';
                //     endforeach;
                // endif;
        $html .= '</div>
            <p class="text-center">Imagem meramente Ilustrativa</p>
            <ul class="nav nav-cores d-flex justify-content-md-center" role="tablist">
                <li class="nav-item label">
                    Selecione a sua cor:
                </li>';
                if (@$versionsMotorBase[0]->version_color) : 
                    foreach($versionsMotorBase[0]->version_color as $key => $cor) : 
                        $image_color = ($cor->color->image) ?  url($cor->color->image->directory.$cor->color->image->url) : '' ;
                        $active = ($key == 0) ? 'active' : '';
                        $ariaselected = ($key == 0) ? 'true' : 'false';
                        $html .= '<li class="nav-item">
                            <a class="nav-link '. $active .' s-color " id="versao_color-'. $cor->id .'-tab" data-toggle="tab" href="#versao_color-'. $cor->id .'" role="tab" aria-controls="versao_color-'. $cor->id .'" aria-selected="'.$ariaselected.'" data-condicoes="'. $cor->texto_legal_color .'" data-preco="R$ '. number_format($cor->price_color, 2, ',', '.') .'">
                                <span class="tooltip">'. $cor->color->name .'</span>
                                <img src="'. $image_color  .'">
                            </a>
                        </li>';
                    endforeach;
                endif;    
        $html .= '</ul>
            <p class="condicoes small">'. strip_tags(html_entity_decode( $versionsMotorBase[0]->version_color[0]->texto_legal_color)).'</p>';


        $dados = array("status" => 1, "html" => $html, "price" => number_format($versionsMotorBase[0]->version_color[0]->price_color, 2, ',', '.'), "versions" => $versionsMotorBase);
       
        return response()->json($dados,200);
    }

    public function modalFichaTecnica(Request $request)
    {

        $motor_id = $request['motor_id'];
        $modelo_id = $request['modelo_id'];

        $versionsMotorBase = ProductVersion::with('version_color')->where('product_id', $modelo_id)->where('active', 1)->where('motor_id', $motor_id )->orderby('sequence', 'ASC')->get();

        $allItensVersionsBase = ProductVersion::getAllVersionsBase($modelo_id, $motor_id);

        $html = '
                <div class="table">
                    <div class="row row-header">
                        <div class="col"></div>
                ';                        
                        if (@$versionsMotorBase) : 
                            foreach($versionsMotorBase as $versionMotor) : 
                                $html .= '<div class="col">
                                    <div class="table-header text-center">
                                        '. $versionMotor->name .'
                                    </div>
                                </div>';
                            endforeach;
                        endif  ;
                       
                        $html .= '</div>
                        <div class="row row-line row-spacer">
                            <div class="col">
                                <div class="table-header">
                                    Transmissão
                                </div>
                            </div>';
                            if (@$versionsMotorBase) : 
                                foreach($versionsMotorBase as $versionMotor) : 
                                    $html .= '<div class="col">
                                        <div class="text-center">
                                            '. getTransmissao($versionMotor->transmissao->name) .'
                                        </div>
                                    </div>';
                                endforeach;
                            endif  ;
                            $html .= '</div>';
                        
                            if($allItensVersionsBase) : 
                                $tipoItem = $allItensVersionsBase[0]->tipo_id; 
                                $html .= '<div class="row row-line row-spacer">
                                <div class="col">
                                    <div class="table-header">
                                        '. $allItensVersionsBase[0]->tipo_name .'
                                    </div>
                                </div>
                            </div>';
                            endif;
                            foreach($allItensVersionsBase as $key => $itemBase) : 
                                if($itemBase->tipo_id != $tipoItem) : 
                                    $html .= '<div class="row row-line row-spacer">
                                        <div class="col">
                                            <div class="table-header">
                                                '. $itemBase->tipo_name .'
                                            </div>
                                        </div>
                                    </div>';
                                    $tipoItem = $itemBase->tipo_id;
                                endif;
                            $html .= ' <div class="row row-line">
                                    <div class="col">
                                        '. $itemBase->name .'
                                    </div>';
                                    foreach($versionsMotorBase as $versionMotor) : 
                                        $checked = '';
                                        foreach($versionMotor->itens as $item) 
                                            if ($itemBase->id == $item->id) : 
                                                $checked = 'checked'; 
                                            endif;
                                    
                                            $html .= ' <div class="col">
                                                    <div class="text-center">
                                                        <span class="'. $checked .'"></span>
                                                    </div>
                                                </div>';
                                        endforeach;
                                
                        $html .= '  </div>';
                                    endforeach;
                            
                    $html .= ' </div>';
        
                $dados = array("status" => 1, "html" => $html);
            
                return response()->json($dados,200);
    }

    public function resultadobusca(Request $request){
        
        $result = null;
        $termo_busca = @$request['search_term'];

        if ($termo_busca != '') {
            for($x=0; $x < 17; $x++){
                $paramBind[] = '%'.$termo_busca.'%';
            }
            
            $result = DB::select('SELECT a.title_html, description_html, a.tipo, param1, param2
                                    FROM (
                                        SELECT products.title_html, products.description_html, "products" as tipo, brands.slug as param1, products.slug as param2 
                                          FROM products inner Join brands ON products.brand_id = brands.id
                                            WHERE products.status = 1
                                            AND (products.title_html LIKE ? or products.description_html LIKE ?)
                                            AND products.deleted_at is NUll
                                        UNION  
                                        SELECT title_html, description_html, "brands" as tipo, slug as param1, "" as param2
                                            FROM brands
                                            WHERE status = 1
                                            AND (title_html LIKE ? or description_html LIKE ?)
                                            AND brands.deleted_at is NUll
                                        UNION
                                        SELECT title_html, description_html, "modelo_geral" as tipo, slug  as param1, "" as param2
                                            FROM modelo_geral
                                            WHERE status = 1
                                            AND (title_html LIKE ? or description_html LIKE ?) 
                                            AND deleted_at is null
                                            UNION
                                        SELECT title_html, description_html, "sobre" as tipo, "/sobre" as param1, "" as param2
                                            FROM sobre
                                            WHERE (title_html LIKE ? or description_html LIKE ?)
                                            AND deleted_at is null     
                                        UNION
                                        SELECT title_html, description_html, "historia" as tipo, "/historia" as param1, "" as param2
                                            FROM historia
                                            WHERE (title_html LIKE ? or description_html LIKE ?)       
                                            AND deleted_at is null
                                        UNION
                                        SELECT title_html, description_html, "pcd" as tipo, "/pcd" as param1, "" as param2
                                            FROM pcds
                                            WHERE (title_html LIKE ? or description_html LIKE ?)    
                                            AND deleted_at is null
                                        UNION
                                        SELECT title_html, description_html, "canais_atendimento" as tipo, "/canais_atendimento" as param1, "" as param2
                                            FROM canais_atendimento
                                            WHERE (title_html LIKE ? or description_html LIKE ?)   
                                            AND deleted_at is null
                                        UNION
                                            SELECT modelo_simples.title_html, modelo_simples.description_html, "modelo_simples" as tipo, modelo_simples.slug as param1, brands.slug as param2 
                                            FROM modelo_simples inner Join brands ON modelo_simples.brand_id = brands.id
                                            WHERE (modelo_simples.title_html LIKE ? or modelo_simples.description_html LIKE ?) 
                                                AND brands.status = 1      
                                                AND modelo_simples.deleted_at is null
                                        ) a', $paramBind);
            
        }

        return view('site.resultado-busca.resultado-busca', compact('result', 'termo_busca'));

    }
    
}
