<?php

namespace App\Http\Controllers\Site;
use App\Http\Controllers\Controller;
use App\Mail\MailNotification;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class RemindersController extends Controller
{

    public function __construct()
    {
        // parent::__construct();
    }

    /**
     * Display the password reminder view.
     *
     * @return Response
     */
    public function getRemind()
    {
        return View::make('password.remind');
    }

    /**
     * Handle a POST request to remind a user of their password.
     *
     * @return Response
     */
    public function postRemind(Request $request)
    {
        $email = $request['email'];
        $user = User::where('email', $email)->first();
        
        //Check if the user exists
        if (!$user) {
            return response()->json(['status' => '0', 'message' => 'Usuário não cadastrado ou inválido.'], 200);
        }

        //Create Password Reset Token
        DB::table('password_reminders')->insert([
            'email' => $request->email,
            'token' => str_random(60),
            'created_at' => Carbon::now()
        ]);
        //Get the token just created above
        $tokenData = DB::table('password_reminders')
                    ->where('email', $request->email)->first();

        try{            
            Mail::to($request->email)->send( new MailNotification('notification.reset_password', 'Caoa - Redefinir Senha', 'send_reset_password', $tokenData));
        } catch(\Exception $e){
           return  response()->json(['status' => '0', 'message' => 'Ocorreu um erro ao enviar uma redefinição de senha para o seu email. Por favor, tente novamente.'], 200);
        }
        return response()->json(['status' => '1', 'message' => 'Um link de redefinição foi enviado para seu endereço de e-mail.'], 200);
       
    }

    /**
     * Display the password reset view for the given token.
     *
     * @param  string $token
     * @return Response
     */
    public function getReset(Request $request, $token = null)
    {
        if (is_null($token)) App::abort(404);
        $email = $request['email'];

        return View::make('site.password.reset', compact('token', 'email'));
    }

    /**
     * Handle a POST request to reset a user's password .
     *
     * @return Response
     */
    public function postReset(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|confirmed',
            'token' => 'required' ]);
    
        if ($validator->fails()) {
            return response()->json(['status' => '0', 'message' => 'Preencha os dados corretamente.'], 500);
        }
        
        $password = $request->password;
       
        $tokenData = DB::table('password_reminders')
                    ->where('token', $request['token'])->first();

        $created_at = $tokenData->created_at;

        $expired = false;
        $expired = Carbon::parse($created_at)->addSeconds(config('auth.passwords.users.expire')*60)->isPast();

        //Redirect the user back to the password reset request form if the token is invalid
        if ((!$tokenData) || ($expired)){
            return response()->json(['status' => '0', 'message' => 'Token expirado ou inválido.'], 200);
        }
        
        $user = User::where('email', $tokenData->email)->first();
        // Redirect the user back if the email is invalid
        if (!$user) {
            return response()->json(['status' => '0', 'message' => 'Usuário não cadastrado ou inválido.'], 200);
        }
        //Hash and update the new password
        $user->password = Hash::make($password);
        if ($user->update()) {

            DB::table('password_reminders')->where('email', $user->email)
            ->delete();

            return response()->json(['status' => '1', 'message' => 'Senha redefinida com sucesso!'], 200);
        } else {
            return response()->json(['status' => '0', 'message' => 'Usuário não cadastrado ou inválido.'], 200);
        }
       
    }

}
