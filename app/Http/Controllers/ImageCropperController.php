<?php

namespace App\Http\Controllers;

use App\Model\MediaPress;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class ImageCropperController extends Controller
{

    public function index()
    {
        return view('admin.cropper.cropper');
    }

    public function upload(Request $request)
    {

        $image = $request->image;
        
        // set destination and save path ( relative and absolute )
        $destinationPath = 'uploads/'.$request->model.'/'.date('Y').'/'.date('m').'/'.date('d').'/';
        $folderPath        = public_path($destinationPath);
        
         // create directory if isn't created
        if(!File::isDirectory($folderPath)){
            File::makeDirectory($folderPath, 0775, true);
        }
        if(!File::isDirectory($folderPath)){
            
            return response()->json([
                "status" => false,
                "msg" => "Ocorreu um erro criar o diretório"
            ]);
            
        } else {
            
            $mobilePrefix    = 'mobile_';

            // set the filename and type
            $originalName = $request->nameimage;
            $allName = explode(".",$originalName[0]);   
            $extension = end($allName);
        
            
            $newName = '_'.strtotime(date('Y-m-d h:i:s'));
                // $filename     = \Str::slug(time().'-'.substr($originalName, 0, -3)).'.'.$file->getClientOriginalExtension();
    
            $filename     = Str::slug(substr($allName[0], 0, -3)).$newName.'.'.$extension;
            $type         = 'image/'.$extension;
            
            
            $img = new MediaPress();
    
            $img->name       = $allName[0].'.'.$extension; //$originalName
            $img->url        = $filename;
            $img->url_mobile = $mobilePrefix.$filename;
            $img->directory  = $destinationPath;
            $img->type       = $type;
            $img->is_tmp     = true;
            


            /*--*/
                // $folderPath = public_path('upload/cropper/');

                $image_parts = explode(";base64,", $request->image);
                
                $image_type_aux = explode("image/", $image_parts[0]);
                $image_type = $image_type_aux[1];
                $image_base64 = base64_decode($image_parts[1]);
                $file = $folderPath . $filename;

                if (!file_put_contents($file, $image_base64)) {
                    return response()->json([
                        "status" => false,
                        "msg" => "Ocorreu um erro ao fazer o upload da imagem."
                    ]);
                }

                // return response()->json(['success'=>'success']);
            /*--*/

            if(!$img->save()) {
                return response()->json([
                    "status" => false,
                    "msg" => "Ocorreu um erro ao fazer o upload da imagem."
                ]);
            }

            return response()->json([
                "status" => true,
                "id" => $img->id,
                "file" => $img->directory . $img->url
            ]);
        }
        

        

        
    }
}