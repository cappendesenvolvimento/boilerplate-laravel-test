<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Model\Pagina;
use App\Log;

class PaginaController extends Controller
{


    protected $model;

    public function __construct(Pagina $model)
    {
        $this->model = $model;
    }


    public function index()
    {
        if (Gate::denies('paginas-view')){
            abort(403, "Não autorizado");
        }

        $objs = Pagina::orderBy('id', 'DESC')->paginate(30);
        //dd($objs);
        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => '', 'titulo' => 'Páginas'],
        ];
        return view('admin.institucional.pagina.index', compact('objs', 'caminhos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('paginas-create')){
            abort(403, "Não autorizado");
        }        

        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => route('pagina.index'), 'titulo' => 'Páginas'],
            ['url' => '', 'titulo' => 'Adicionar'],
        ];
        return view('admin.institucional.pagina.adicionar', compact('caminhos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('paginas-create')){
            abort(403, "Não autorizado");
        }        

        $request->validate([
            'title' => 'required|max:255',
        ],
        [
            'title.required' => 'Título precisa ser informado',
        ]);

        $obj = $this->model->store($request);

        if (!$obj->save()) {
            return redirect()->back()->withErrors(['Houve um erro ao processar seu pedido, tente novamente.']);
        }

        $logs = new Log();
        $logs->insertLog('Página', $obj->id);         
        
        return redirect()->route('pagina.index')->with('status', 'Registro Salvo!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('paginas-edit')){
            abort(403, "Não autorizado");
        }                

        $obj = Pagina::with('featured_img', 'featured_img_mobile')->find($id);
        //dd($obj);
        
        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => route('pagina.index'), 'titulo' => 'Paginas'],
            ['url' => '', 'titulo' => 'Editar'],
        ];
        return view('admin.institucional.pagina.editar', compact('obj','caminhos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies('paginas-edit')){
            abort(403, "Não autorizado");
        }        

        $request->validate([
            'title' => 'required|max:255',
        ],
        [
            'title.required' => 'Título precisa ser informado',
        ]);

        $obj = $this->model->put($id, $request);

        
       
        $obj->save();
        
        $logs = new Log();
        $logs->updateLog('Página', $id);         

        return redirect()->route('pagina.edit', $id )->with('status', 'Registro Atualizado!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('paginas-delete')){
            abort(403, "Não autorizado");
        }                
        
        Pagina::find($id)->delete();
        
        $logs = new Log();
        $logs->deleteLog('Página', $id); 

        return redirect()->route('pagina.index')->with('status', 'Registro Excluído!');
    }

}
