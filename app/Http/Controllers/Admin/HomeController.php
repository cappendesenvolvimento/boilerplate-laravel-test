<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Model\Home;
use App\Log;

class HomeController extends Controller
{

    
    protected $model;

    public function __construct(Home $model)
    {
        $this->model = $model;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Gate::denies('home-view')){
            abort(403, "Não autorizado");
        } 

        if (isset($request['search'])) {
            $objs = Home::where('language', '=', $request['search'] )->paginate(30);    
        } else {
            $objs = Home::paginate(30);
        }

        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => '', 'titulo' => 'Home'],
        ];
        return view('admin.home.institucional.index', compact('objs', 'caminhos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('home-create')){
            abort(403, "Não autorizado");
        } 

        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => route('home.index'), 'titulo' => 'Home'],
            ['url' => '', 'titulo' => 'Adicionar'],
        ];
        return view('admin.home.institucional.adicionar', compact('caminhos'));
    }

  
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('home-edit')){
            abort(403, "Não autorizado");
        } 

        $obj = Home::with('image_one', 'image_two', 'image_three', 'image_video', 'img_full_one', 'img_full_two', 'img_split_left', 'img_split_right', 'img_split_full')->find($id);
        $itens = (@$obj->links_itens) ? unserialize(@$obj->links_itens) : array();

        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => route('home.index'), 'titulo' => 'Home'],
            ['url' => '', 'titulo' => 'Editar'],
        ];
        return view('admin.home.institucional.editar', compact('obj','caminhos', 'itens'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $action)
    {
        if (Gate::denies('home-edit')){
            abort(403, "Não autorizado");
        } 

        
        if ($action == 'institucional') : 
           $obj = self::updateInstitucional($request, $id);
        elseif ($action == 'chamadas') : 
            $obj = self::updateChamadas($request, $id);   
        elseif ($action == 'split') : 
            $obj = self::updateSplit($request, $id);   
        elseif ($action == 'half') : 
            $obj = self::updateHalf($request, $id);   
        elseif ($action == 'ctas') : 
            $obj = self::updateCtas($request, $id);   
        endif;

        if (!$obj->save()) {
            return redirect()->back()->withErrors(['Houve um erro ao processar seu pedido, tente novamente.']);
        }
       
        $logs = new Log();
        $logs->updateLog('Home', $id);

        return redirect()->route('home.edit', [$obj, 'aba' => $action])->with('status', 'Registro Atualizado!');

    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function updateInstitucional(Request $request, $id)
    {
        if (Gate::denies('home-edit')){
            abort(403, "Não autorizado");
        } 

        $request->validate([
            'chamada' => 'required|max:255',
        ]);

        return Home::putInstitucional($id, $request);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function updateChamadas(Request $request, $id)
    {
        if (Gate::denies('home-edit')){
            abort(403, "Não autorizado");
        } 

        return Home::putChamadas($id, $request);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function updateSplit(Request $request, $id)
    {
        if (Gate::denies('home-edit')){
            abort(403, "Não autorizado");
        } 

        return Home::putSplit($id, $request);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function updateHalf(Request $request, $id)
    {
        
        if (Gate::denies('home-edit')){
            abort(403, "Não autorizado");
        } 

        return Home::putHalf($id, $request);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function updateCtas(Request $request, $id)
    {
        if (Gate::denies('home-edit')){
            abort(403, "Não autorizado");
        } 

        return Home::putCtas($id, $request);

    }

  
}
