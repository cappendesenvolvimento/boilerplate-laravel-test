<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Gate;
use App\Model\About;
use App\Log;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Gate::denies('about-view')){
            abort(403, "Não autorizado");
        }
        
        if (isset($request['search'])) {
            $objs = About::where('language_id', '=', $request['search'] )->orderBy('id', 'DESC')->paginate(30);    
        } else {
            $objs = About::orderBy('id', 'DESC')->paginate(30);
        }
        
        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => '', 'titulo' => 'About'],
        ];
        return view('admin.about.index', compact('objs', 'caminhos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('about-create')){
            abort(403, "Não autorizado");
        }

        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => route('about.index'), 'titulo' => 'About'],
            ['url' => '', 'titulo' => 'Adicionar'],
        ];
        return view('admin.about.adicionar', compact('caminhos', 'clientes', 'faqs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('about-edit')){
            abort(403, "Não autorizado");
        }        

        $obj = About::find($id);
        //dd($obj);
        
        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => route('about.index'), 'titulo' => 'About'],
            ['url' => '', 'titulo' => 'Editar'],
        ];
        return view('admin.about.editar', compact('obj','caminhos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies('about-edit')){
            abort(403, "Não autorizado");
        }

        $request->validate([
            'titulo' => 'required|max:255',
        ]);

        $obj = About::find($id);
        

        $nameFile = '';

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->image->extension();
            $nameFile = "{$name}.{$extension}";
            $upload = $request->image->storeAs(config('app.folder').'about', $nameFile);
            if ( !$upload )
                return redirect()
                            ->back()
                            ->with('error', 'Falha ao fazer upload')
                            ->withInput();
        } else {
            $nameFile = $request['old_image'];
        }


        if ($request->hasFile('banner') && $request->file('banner')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->banner->extension();
            $nameFileBanner = "{$name}.{$extension}";
            $upload = $request->banner->storeAs(config('app.folder').'about', $nameFileBanner);
            if ( !$upload )
                return redirect()
                            ->back()
                            ->with('error', 'Falha ao fazer upload')
                            ->withInput();
        } else {
            $nameFileBanner = $request['old_banner'];
        } 

        if ($request->hasFile('foto1') && $request->file('foto1')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->foto1->extension();
            $nameFileFoto1 = "{$name}.{$extension}";
            $upload = $request->foto1->storeAs(config('app.folder').'about', $nameFileFoto1);
            if ( !$upload )
                return redirect()
                            ->back()
                            ->with('error', 'Falha ao fazer upload')
                            ->withInput();
                            
        } else {
            $nameFileFoto1 = $request['old_foto1'];
        }  

        if ($request->hasFile('foto2') && $request->file('foto2')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->foto2->extension();
            $nameFileFoto2 = "{$name}.{$extension}";
            $upload = $request->foto2->storeAs(config('app.folder').'about', $nameFileFoto2);
            if ( !$upload )
                return redirect()
                            ->back()
                            ->with('error', 'Falha ao fazer upload')
                            ->withInput();
                            
        } else {
            $nameFileFoto2 = $request['old_foto2'];
        }  

        if ($request->hasFile('imagectacontato') && $request->file('imagectacontato')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->imagectacontato->extension();
            $nameFileCtaContato = "{$name}.{$extension}";
            $upload = $request->imagectacontato->storeAs(config('app.folder').'about', $nameFileCtaContato);
            if ( !$upload )
                return redirect()
                            ->back()
                            ->with('error', 'Falha ao fazer upload')
                            ->withInput();
                            
        } else {
            $nameFileCtaContato = $request['old_imagectacontato'];
        }  

        if ($request->hasFile('imagectasol') && $request->file('imagectasol')->isValid()) {
            $name = uniqid(date('HisYmd'));
            $extension = $request->imagectasol->extension();
            $nameFileCtaSol = "{$name}.{$extension}";
            $upload = $request->imagectasol->storeAs(config('app.folder').'about', $nameFileCtaSol);
            if ( !$upload )
                return redirect()
                            ->back()
                            ->with('error', 'Falha ao fazer upload')
                            ->withInput();
        } else {
            $nameFileCtaSol = $request['old_imagectasol'];
        }  

        
        $obj->titulo = $request['titulo'];
        $obj->language_id = $request['language_id'];
        $obj->banner = $nameFileBanner;
        $obj->titulo_timeline = $request['titulo_timeline'];
        $obj->titulo_hist = $request['titulo_hist'];
        $obj->descricao_hist = $request['descricao_hist'];
        $obj->missao = $request['missao'];
        $obj->visao = $request['visao'];
        $obj->valores = $request['valores'];
        $obj->image = $nameFile;
        $obj->titulo_lider = $request['titulo_lider'];
        $obj->descricao_lider = $request['descricao_lider'];
        $obj->foto1 = $nameFileFoto1;
        $obj->foto1_nome = $request['foto1_nome'];
        $obj->foto1_titulo = $request['foto1_titulo'];
        $obj->foto2 = $nameFileFoto2;
        $obj->foto2_nome = $request['foto2_nome'];
        $obj->foto2_titulo = $request['foto2_titulo'];
        $obj->tituloctasol = $request['tituloctasol'];
        $obj->btnctasol = $request['btnctasol'];
        $obj->linkctasol = $request['linkctasol'];
        $obj->imagectasolucao = $nameFileCtaSol;
        $obj->cta_contato = $request['cta_contato'];
        $obj->btnctacontato = $request['btnctacontato'];
        $obj->linkctacontato = $request['linkctacontato'];
        $obj->imagectacontato = $nameFileCtaContato;

        $obj->save();

        $logs = new Log();
        $logs->updateLog('About', $id);        
        
        return redirect()->route('about.edit', $obj->id)->with('status', 'Registro Atualizado!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('about-delete')){
            abort(403, "Não autorizado");
        } 

        About::find($id)->delete();

        $logs = new Log();
        $logs->deleteLog('About', $id); 

        return redirect()->route('about.index')->with('status', 'Registro Excluído!');
    }

}
