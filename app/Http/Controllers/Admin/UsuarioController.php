<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\Rule;
use \App\User;
use \App\Log;
use Illuminate\Support\Facades\Hash;

class UsuarioController extends Controller
{

    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => '', 'titulo' => 'Usuários'],
        ];

        $usuarios = User::with('rules')->where('role', '<>', 6)->orderBy('id', 'DESC')->paginate(30);
       
        return view('admin.usuarios.index', compact('usuarios', 'caminhos'));

    }

    public function rule($id)
    {

        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => route('usuarios.index'), 'titulo' => 'Usuários'],
            ['url' => '', 'titulo' => 'Papel'],
        ];

        $usuarios = User::find($id);
        $rules = Rule::all();
        
        return view('admin.usuarios.rule', compact('usuarios', 'rules', 'caminhos'));

    }

    //Adicionar relacionamento de rule com usuario    
    public function ruleStore(Request $request, $id)
    {
        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => route('usuarios.index'), 'titulo' => 'Usuários'],
            ['url' => '', 'titulo' => 'Papel'],
        ];

        $usuario = User::find($id);
        $dados = $request->all();
        $rule = Rule::find($dados['rule_id']);
        $usuario->adicionarRule($rule);

        return redirect()->back()->with('Permissão', 'Permissão Salva!');

    }

    //Remover relacionamento de Rule com usuario
    public function ruleDestroy($id, $rule_id)
    {
        $usuario = User::find($id);
        $rule = Rule::find($rule_id);
        $usuario->removeRule($rule);

        return redirect()->back()->with('Permissão', 'Permissão Excluída!');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => route('usuarios.index'), 'titulo' => 'Usuários'],
            ['url' => '', 'titulo' => 'Adicionar'],
        ];

        $rules = Rule::where('acesso', 'ADM')->get();

        return view('admin.usuarios.adicionar', compact('rules','caminhos'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|unique:users',
            'rule_id' => 'required',
            'password' => 'required|confirmed|min:6'
        ]);

        
        $obj = $this->model->storeAdmin($request);

        if (!$obj->save()) {
            return redirect()->back()->withErrors(['Houve um erro ao processar seu pedido, tente novamente.']);
        }


        $rule = Rule::find($request['rule_id']);
        $obj->adicionarRule($rule);

        $logs = new Log();
        $logs->insertLog('Usuário', $obj->id);

        return redirect()->route('usuarios.index')->with('status', 'Registro Salvo!');

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        if (User::find($id)->nome == 'Admin') {
            return redirect()->route('rules.index');
        }
        
        $rules = Rule::where('acesso', 'ADM')->get();
        $usuario = User::with('rules')->find($id);
        
        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => route('usuarios.index'), 'titulo' => 'Rules'],
            ['url' => '', 'titulo' => 'Editar'],
        ];
        return view('admin.usuarios.editar', compact('usuario','rules', 'caminhos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required',
            'rule_id' => 'required',
        ]);

        $existe = User::where('email', $request['email'])->where('id', '<>', $id)->first();
        if ($existe) {
            return redirect()->back()->withErrors(['The email has already been taken.']);
        }

        if (auth()->user()->id != $id) { 
            //Verificar se o perfil do usuario é diferente de ADMIN
            if (User::with('rules')->find($id)->nome == 'Admin') {
                return redirect()->route('usuarios.index');
            }
        }

        $obj = $this->model->putAdmin($id, $request);

        if (!$obj->save()) {
            return redirect()->back()->withErrors(['Houve um erro ao processar seu pedido, tente novamente.']);
        }


        $rule = Rule::find($request['rule_id']);
        $obj->adicionarRule($rule);

        $logs = new Log();
        $logs->updateLog('Usuário', $id);
       
        
        return redirect()->route('usuarios.index')->with('status', 'Registro atualizado!');
    }

    public function changePassword(Request $request, $id)
    {
        
        $this->validate(request(), [
            'password' => 'required|min:6|confirmed'
        ]);

        $obj = User::find($id);
        $obj->password = Hash::make($request['password']);
        $obj->save();

        return back()->with('status', 'Senha atualizada!');
    }

    public function editPerfil($id)
    {
       
        $user = auth()->user();
        $usuario = User::find($user->id);
        
        
        return view('admin.usuarios.perfil', compact('usuario')); 
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Verificar se o perfil do usuario é diferente de ADMIN
        if (User::find($id)->nome == 'Admin') {
            return redirect()->route('rules.index');
        }

        User::find($id)->delete();
        
        $logs = new Log();
        $logs->deleteLog('Usuário', $id);

        return redirect()->route('usuarios.index')->with('status', 'Registro Excluído!');
    }

    
}
