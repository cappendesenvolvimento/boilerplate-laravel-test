<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Media;
use Illuminate\Http\Request;
use Psy\Exception\FatalErrorException;

class MediasController extends Controller
{

    protected $model;

    public function __construct(Media $model)
    {
        $this->model = $model;
    }

    public function uploadFile(Request $request)
    {
        try {
            
            $file = $this->model->uploadFile($request->file('media'), $request['model']);

            return response()->json([
                "status" => true,
                "id" => $file->id,
                "type" => $file->type,
                "file" => $file->directory . $file->url
            ]);

        } catch (\Exception $e) {

            return response()->json([
                "status" => false,
                "msg" => $e->getMessage()
            ]);
        }
    }



    public function upload(Request $request)
    {
        try {

            $img = null;
            $error = false;
            $segment = (@$request['segment']) ? $request['segment'] : 'no-segment';

            if ($segment == "news-press") {
                $resolution = getimagesize($request->file('media'));
                if ($resolution) { 
                    $img = $this->model->upload(
                        $request->file('media'), $request['model'],
                        (isset($request['width'])) ? $resolution[0] : false,
                        (isset($request['height'])) ? $resolution[1] : false);
                } else {
                    $error = true;
                }
            } else {
               
                $resolution = getimagesize($request->file('media'));
                if ($resolution) { 
                    $img = $this->model->upload(
                        $request->file('media'), $request['model'],
                        (isset($request['width'])) ? $resolution[0] : false,
                        (isset($request['height'])) ? $resolution[1] : false);
                } else {
                    $error = true;
                }
            }

            if (!$error) { 
                return response()->json([
                    "status" => true,
                    "id" => $img->id,
                    "file" => $img->directory . $img->url
                ]);
            } else {
                return response()->json([
                    "status" => false,
                    "msg" => "Ocorreu um erro ao fazer o upload da imagem."
                ]);
            }

        } catch (\Throwable $e) {

            return response()->json([
                "status" => false,
                "msg" => $e->getTraceAsString()
            ]);

        }
    }

    
    // public function uploadMultipleSizes()
    // {
    //     try {

    //         $img = $this->model->uploadMultipleSizes(
    //             \Input::file('media'), \Input::get('model'),
    //             \Input::has('width') ? \Input::get('width') : false,
    //             \Input::has('height') ? \Input::get('height') : false,
    //             \Input::has('width_mobile') ? \Input::get('width_mobile') : false,
    //             \Input::has('height_mobile') ? \Input::get('height_mobile') : false);

    //         return \Response::json([
    //             "status" => true,
    //             "id" => $img['original'],
    //             "id_mobile" => $img['mobile'],
    //             "file" => $img['directory'] . $img['url']
    //         ]);

    //     } catch (\Exception $e) {

    //         return \Response::json([
    //             "status" => false,
    //             "msg" => $e->getMessage()
    //         ]);
    //     }
    // }

    // public function crop()
    // {
    //     try {

    //         $img = $this->model->crop(
    //             \Input::get('image'), \Input::get('model'),
    //             \Input::get('x'), \Input::get('y'),
    //             \Input::get('w'), \Input::get('h'),
    //             \Input::get('fw'), \Input::get('fh'),
    //             \Input::get('suffix'));

    //         return \Response::json([
    //             "status" => true,
    //             "id" => $img->id,
    //             "file" => $img->directory . $img->url
    //         ]);

    //     } catch (\Exception $e) {

    //         return \Response::json([
    //             "status" => false,
    //             "msg" => $e->getMessage()
    //         ]);
    //     }
    // }

    public function destroy($id)
    {
        $media = $this->model->find($id);

        if (!$media->delete()) {
            \Notify::error('Houve um erro ao processar seu pedido, tente novamente.');
            return \Admin::redirectBack();
        }

        \Notify::success('Media removida com sucesso.');
        return \Admin::redirect('colors.index');
    }

}
