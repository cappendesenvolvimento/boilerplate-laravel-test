<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Model\NewsCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Log;

class NewsCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Gate::denies('newsCategory-view')){
            abort(403, "Não autorizado");
        } 
        
        $search = $request['searchtxt'];

        $objs = NewsCategory::where(function($query)  use ($search){
                                if ($search != '') :
                                    return $query->where('name', 'like', '%'.$search.'%');
                                endif;
                            })->paginate(30);
        
        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => '', 'titulo' => 'Categoria de Notícias'],
        ];
        return view('admin.institucional.news_category.index', compact('objs', 'caminhos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('newsCategory-create')){
            abort(403, "Não autorizado");
        }

        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => route('newsCategory.index'), 'titulo' => 'Categoria de Notícias'],
            ['url' => '', 'titulo' => 'Adicionar'],
        ];
        return view('admin.institucional.news_category.adicionar', compact('caminhos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('newsCategory-create')){
            abort(403, "Não autorizado");
        }

        $request->validate([
            'name' => 'required|max:255',
        ]);


        $obj = new NewsCategory();
      
        $obj->name = $request['name'];
        $obj->image_id = $request['image_id'];

        $obj->save();

        $logs = new Log();
        $logs->insertLog('Categoria de Notícias', $obj->id);
        
        return redirect()->route('newsCategory.index')->with('status', 'Registro Salvo!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('newsCategory-edit')) {
            abort(403, "Não autorizado");
        }        

        $obj = NewsCategory::find($id);

        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => route('newsCategory.index'), 'titulo' => 'Categoria de Notícias'],
            ['url' => '', 'titulo' => 'Editar'],
        ];
        return view('admin.institucional.news_category.editar', compact('obj','caminhos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if (Gate::denies('newsCategory-edit')) {
            abort(403, "Não autorizado");
        }        

        $request->validate([
            'name' => 'required|max:255',
        ]);

        $obj = NewsCategory::find($id);

        $obj->name = $request['name'];
        $obj->image_id = $request['image_id'];

        $obj->save();
        
        $logs = new Log();
        $logs->updateLog('Categoria de Notícias', $id);

        return redirect()->route('newsCategory.index')->with('status', 'Registro Atualizado!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('newsCategory-delete')){
            abort(403, "Não autorizado");
        }  

        NewsCategory::find($id)->delete();

        $logs = new Log();
        $logs->deleteLog('Categoria de Notícias', $id);
        
        return redirect()->route('newsCategory.index')->with('status', 'Registro Excluído!');

    }

  
}