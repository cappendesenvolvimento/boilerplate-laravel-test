<?php


namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Banner;
use App\Model\Brand;
use App\Model\HistoriaItens;
use App\Model\HomeLancamento;
use App\Model\NewsPressFile;
use App\Model\PosvendaBanner;
use App\Model\PreAgendamentoService;
use App\Model\Product;
use App\Model\ProductGaleria;
use App\Model\ProductVersion;
use App\Model\ProductVersionColor;
use App\Model\ServiceBrand;
use App\Model\SobreItens;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class OrderController extends Controller
{

    public function order(Request $request){
        $arr = null;

        if ($request['model'] == 'homeBanner') {
            Banner::where('language', $request['language'])->update(['sequence' => NULL]);
        }
        
        foreach($request['ids'] as $key => $id) {
            $order = $key+1;
            if ($request['model'] == 'FilesNewsPress') { 
                NewsPressFile::where('id', $id)->update(['sequence' => $order]);
            } else if ($request['model'] == 'homeLancamento') { 
                HomeLancamento::where('id', $id)->where('language', '=', $request['language'])->update(['order' => $order]);
            } else if ($request['model'] == 'brand') { 
                Brand::where('id', $id)->update(['sequence' => $order]);
            } else if ($request['model'] == 'versionColor') { 
                ProductVersionColor::where('id', $id)->update(['sequence' => $order]);
            } else if ($request['model'] == 'modelo') { 
                Product::where('id', $id)->update(['sequence' => $order]);
            } else if ($request['model'] == 'modelo-version') { 
                ProductVersion::where('id', $id)->update(['sequence' => $order]);
            } else if ($request['model'] == 'modelo-galeria') { 
                ProductGaleria::where('id', $id)->update(['sequence' => $order]);
            } else if ($request['model'] == 'preAgendamentoServices') { 
                PreAgendamentoService::where('id', $id)->update(['sequence' => $order]);
            } else if ($request['model'] == 'posvendabanner') { 
                PosvendaBanner::where('id', $id)->update(['sequence' => $order]);
            } else if ($request['model'] == 'service-brand') { 
                ServiceBrand::where('id', $id)->update(['sequence' => $order]);
            } else if ($request['model'] == 'sobreItens') { 
                SobreItens::where('id', $id)->update(['sequence' => $order]);
            } else if ($request['model'] == 'historiaItens') { 
                HistoriaItens::where('id', $id)->update(['sequence' => $order]);
            } else if ($request['model'] == 'homeBanner') { 
                Banner::where('language', $request['language'])->where('id', $id)->update(['sequence' => $order]);
            }                
            
        }
        dd($arr);

    }
}
