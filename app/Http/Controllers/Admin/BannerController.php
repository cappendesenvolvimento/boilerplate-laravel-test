<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Model\Banner;
use App\Log;

class BannerController extends Controller
{

    
    protected $model;

    public function __construct(Banner $model)
    {
        $this->model = $model;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Gate::denies('banner-view')){
            abort(403, "Não autorizado");
        } 


       
        $objsPt = Banner::where('language', '=', 'pt_br' )
                                ->orderby('status', 'DESC')
                                ->orderby('sequence', 'ASC')
                                ->paginate(30); 
        $objsEn = Banner::where('language', '=', 'en_us' )
                                ->orderby('status', 'DESC')
                                ->orderby('sequence', 'ASC')
                                ->paginate(30); 
    

        
        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => '', 'titulo' => 'Banner'],
        ];
        return view('admin.home.banner.index', compact('objsPt', 'objsEn', 'caminhos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('banner-create')){
            abort(403, "Não autorizado");
        } 

        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => route('banner.index'), 'titulo' => 'Banner'],
            ['url' => '', 'titulo' => 'Adicionar'],
        ];
        return view('admin.home.banner.adicionar', compact('caminhos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('banner-create')){
            abort(403, "Não autorizado");
        } 

        $request->validate([
            'name' => 'required|max:255',
        ],
        [
            'name.required' => 'Nome precisa ser informado',
        ]);

        $obj = $this->model->store($request);

        if (!$obj->save()) {
            return redirect()->back()->withErrors(['Houve um erro ao processar seu pedido, tente novamente.']);
        }
        
        $logs = new Log();
        $logs->insertLog('Banner', $obj->id);
        
        return redirect()->route('banner.index')->with('status', 'Registro Salvo!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('banner-edit')){
            abort(403, "Não autorizado");
        } 

        $obj = Banner::with('file', 'file_mobile')->find($id);
        
        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => route('banner.index'), 'titulo' => 'Banner'],
            ['url' => '', 'titulo' => 'Editar'],
        ];
        return view('admin.home.banner.editar', compact('obj','caminhos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies('banner-edit')){
            abort(403, "Não autorizado");
        } 

        $request->validate([
            'name' => 'required|max:255',
        ],
        [
            'name.required' => 'Nome precisa ser informado',
        ]);

        $obj = $this->model->put($id, $request);

        if (!$obj->save()) {
            return redirect()->back()->withErrors(['Houve um erro ao processar seu pedido, tente novamente.']);
        }
       

        $logs = new Log();
        $logs->updateLog('Banner', $id);

        return redirect()->route('banner.index')->with('status', 'Registro Atualizado!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('banner-delete')){
            abort(403, "Não autorizado");
        }  

        Banner::find($id)->delete();

        $logs = new Log();
        $logs->deleteLog('Banner', $id);

        return redirect()->route('banner.index')->with('status', 'Registro Excluído!');
    }

    public function changeStatus(Request $request)
    {
        $obj = Banner::find($request['id']);
        if ($obj->status == 0) {
            $obj->status = '1';
            $obj->save();
            return 'true';
        } else {
            $obj->status = '0';
            $obj->save();
            return 'false';
        }
    }
}
