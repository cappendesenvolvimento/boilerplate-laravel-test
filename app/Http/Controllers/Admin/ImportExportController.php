<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Model\ImportExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Log;
use App\Model\SplitImportExport;

class ImportExportController extends Controller
{

    protected $model;

    public function __construct(ImportExport $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('import-export-view')){
            abort(403, "Não autorizado");
        } 

        $objs = ImportExport::orderby('id', 'DESC')->paginate(30);
       

        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => '', 'titulo' => 'Importação e Exportação'],
        ];
        return view('admin.outras-paginas.import-export.index', compact('objs',  'caminhos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('import-export-create')){
            abort(403, "Não autorizado");
        }

        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => route('import-export.index'), 'titulo' => 'Importação e Exportação'],
            ['url' => '', 'titulo' => 'Adicionar'],
        ];
        return view('admin.outras-paginas.import-export.adicionar', compact('caminhos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('import-export-create')){
            abort(403, "Não autorizado");
        }

        $request->validate([
            'title' => 'required|max:255',
        ],
        [
            'title.required' => 'Nome precisa ser informado',
        ]);

     
        $obj = $this->model->store($request);

        if (!$obj->save()) {
            return redirect()->back()->withErrors(['Houve um erro ao processar seu pedido, tente novamente.']);
        }

        $logs = new Log();
        $logs->insertLog('Importação e Exportação', $obj->id);
        
        
        return redirect()->route('import-export.index')->with('status', 'Registro Salvo!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('import-export-edit')) {
            abort(403, "Não autorizado");
        }        

        $obj = ImportExport::with('image')->find($id);
        $objSplit = SplitImportExport::where('import_export_id', $id)->get();
       
        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => route('import-export.index'), 'titulo' => 'Importação e Exportação'],
            ['url' => '', 'titulo' => 'Editar'],
        ];
        return view('admin.outras-paginas.import-export.editar', compact('obj','caminhos', 'objSplit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if (Gate::denies('import-export-edit')) {
            abort(403, "Não autorizado");
        }    

        $obj = $this->model->put($id, $request);

        if (!$obj->save()) {
            return redirect()->back()->withErrors(['Houve um erro ao processar seu pedido, tente novamente.']);
        }

        $logs = new Log();
        $logs->updateLog('Importação e Exportação', $id);

        return redirect()->route('import-export.edit', $id)->with('status', 'Registro Atualizado!');

    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('import-export-delete')){
            abort(403, "Não autorizado");
        }  

        ImportExport::find($id)->delete();

        $logs = new Log();
        $logs->deleteLog('Importação e Exportação', $id);
        
        return redirect()->route('import-export.index')->with('status', 'Registro Excluído!');

    }

  
}