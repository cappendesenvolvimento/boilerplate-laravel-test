<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class AdminController extends Controller
{
    /* public function __construct()
    {   
         $this->middleware('auth');
    } */

    public function index()
    {
        
        if (Auth::user()->role != 1) { 
            
            $logs = Log::with('users')->join('users', 'users.id', '=', 'logs.user_id')
                        ->where('users.role', '=', Auth::user()->role)
                        ->select('logs.*')
                        ->orderBy('id', 'DESC')
                        ->paginate(50);
        } else {
            $logs = Log::with('users')->orderBy('id', 'DESC')->paginate(50);
        }
        
      
        
        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Painel Administrativo']
        ];

        return view('admin.index', compact('caminhos', 'logs'));
    }

    

}
