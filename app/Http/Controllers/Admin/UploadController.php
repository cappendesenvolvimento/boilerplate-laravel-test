<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use App\Traits\UploadTrait;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;

//use Intervention\Image\Facades\Image as Image;

class UploadController extends Controller
{

    use UploadTrait;

   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('file');
        // set destination and save path ( relative and absolute )
        $destinationPath = 'uploads/content/';
        $savePath        = public_path($destinationPath);


        // create directory if isn't created
        if(!File::isDirectory($savePath)){
            File::makeDirectory($savePath, 0775, true);
        }

        // set the filename and type
        $originalName = $file->getClientOriginalName();

            // $filename     = \Str::slug(time().'-'.substr($originalName, 0, -3)).'.'.$file->getClientOriginalExtension();

        $namefile = strtotime(date('h:i:s'));
        
        $filename     = $namefile.Str::slug(substr($originalName, 0, -3)).'.'.$file->getClientOriginalExtension();
        $type         = $file->getMimeType();

        // transform in a manipulable object
        $file = Image::make($file->getRealPath());


        // try save file
        if(!$file->save($savePath.$filename)) {
            return false;
        }

        return ['location' => $destinationPath.$filename];


        // if ($request->has('file')) {
        //     $image = $request->file('file');
        //     $name = Str::slug($request->input('name')).'_'.time();
        //     $folder = 'uploads/';
        //     $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
        //     // Upload image
        //     $this->uploadOne($image, $folder, 'public', $name);
        // }
        
        // $path = $request->file('file')->store('uploads', 'public');
        // return ['location' => Storage::url($path)];
    }

 
}
