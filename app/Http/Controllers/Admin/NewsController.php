<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Model\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Log;
use App\Model\Autor;
use App\Model\NewsCategory;
use App\Model\NewsRelated;

class NewsController extends Controller
{

    protected $model;

    public function __construct(News $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request )
    {
        if (Gate::denies('newsSite-view')){
            abort(403, "Não autorizado");
        } 

        $search = $request['searchtxt'];

        $objs = News::with('category')
                    ->where(function($query)  use ($search){
                        if ($search != '') :
                            return $query->where('main_title', 'like', '%'.$search.'%')
                                        ->orwhere('content', 'like', '%'.$search.'%');
                        endif;
                    })
                    ->orderby('id', 'DESC')
                    ->paginate(30);
       

        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => '', 'titulo' => 'Notícias'],
        ];
        return view('admin.institucional.news.index', compact('objs',  'caminhos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('newsSite-create')){
            abort(403, "Não autorizado");
        }

        $categorias = NewsCategory::all();
        $autores = Autor::all();
        $news = News::all();

        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => route('news.index'), 'titulo' => 'Notícias'],
            ['url' => '', 'titulo' => 'Adicionar'],
        ];
        return view('admin.institucional.news.adicionar', compact('caminhos', 'news', 'categorias', 'autores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('newsSite-create')){
            abort(403, "Não autorizado");
        }

        $request->validate([
            'main_title' => 'required|max:255',
            'content' => 'required',
        ],
        [
            'main_title.required' => 'Título precisa ser informado',
            'content.required' => 'Conteúdo precisa ser informado',
        ]);

     
        $obj = $this->model->store($request);

        if (!$obj->save()) {
            return redirect()->back()->withErrors(['Houve um erro ao processar seu pedido, tente novamente.']);
        }

        if (is_array($request['relateds'])) { 
            if (count(@$request['relateds']) > 0) { 
                if(!NewsRelated::populateNewsRelated($request['relateds'], $obj)){
                    return redirect()->route('news.index')->withErrors(['Registro Salvo, mas com erros ao vincular as notícias relacionadas, verifique novamente.']);
                }
            }
        }

        $logs = new Log();
        $logs->insertLog('Notícias', $obj->id);
        
        
        return redirect()->route('news.index')->with('status', 'Registro Salvo!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('newsSite-edit')) {
            abort(403, "Não autorizado");
        }        

        $obj = News::with('category', 'featured_img', 'featured_img_mobile')->find($id);
        $categorias = NewsCategory::all();
        $autores = Autor::all();
        $news = News::all();

        $relacionados = null;

        $newsRel = NewsRelated::where('news_id', '=', $id )->get();
        if ($newsRel){
            foreach($newsRel as $rel){
                $posts = News::find($rel['related_id']);
                $relacionados[] = array('rel_id' => $rel['id'], 'post' => $posts);
            }
        }

        //dd($relacionados);

        $caminhos = [
            ['url' => '/admin', 'titulo' => 'Admin'],
            ['url' => route('news.index'), 'titulo' => 'Notícias'],
            ['url' => '', 'titulo' => 'Editar'],
        ];
        return view('admin.institucional.news.editar', compact('obj','caminhos', 'categorias', 'news', 'relacionados', 'autores'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if (Gate::denies('newsSite-edit')) {
            abort(403, "Não autorizado");
        }    

        $request->validate([
            'main_title' => 'required|max:255',
            'content' => 'required',
        ],
        [
            'main_title.required' => 'Título precisa ser informado',
            'content.required' => 'Conteúdo precisa ser informado',
        ]);

        $obj = $this->model->put($id, $request);

        if (!$obj->save()) {
            return redirect()->back()->withErrors(['Houve um erro ao processar seu pedido, tente novamente.']);
        }

        $logs = new Log();
        $logs->updateLog('Notícias', $id);

        return redirect()->route('news.edit', $id)->with('status', 'Registro Atualizado!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('newsSite-delete')){
            abort(403, "Não autorizado");
        }  

        News::find($id)->delete();

        $logs = new Log();
        $logs->deleteLog('Notícias', $id);
        
        return redirect()->route('news.index')->with('status', 'Registro Excluído!');

    }

    //Adicionar relacioautornto de news
    public function relacionadosStore(Request $request, $id)
    {
        $obj = News::find($id);

        if(!NewsRelated::populateNewsRelated($request['relateds'], $obj)){
            return redirect()->route('news.index')->withErrors(['Registro Atualizado, mas com erros ao vincular as notícias relacionadas, verifique novamente.']);
        }
        
        return redirect()->back()->with('status', 'News Relacionada!');;

    }


    public function relacionadosDestroy($id){

        NewsRelated::find($id)->delete();

        $logs = new Log();
        $logs->deleteLog('Notícias Relacionada', $id);

        return redirect()->back()->with('status', 'News relacionada excluída!');
    }


    public function changeStatus(Request $request)
    {
        $obj = News::find($request['id']);
        if ($obj->active == 0) {
            $obj->active = '1';
            $obj->save();
            return 'true';
        } else {
            $obj->active = '0';
            $obj->save();
            return 'false';
        }
    }
  
}