<?php

function parseLocale()
{
    // Busca a primeira rota da URL
    $locale = Request::segment(1);
    // Define os locales da aplicação
    $languages = ['en','pt', 'it', 'fr', 'ru', 'es'];
    // Verifica se o locale passado na URL é válido
    // Se sim, então muda o idioma da aplicação e retorna o locale
    // Se não, então deixa o idioma padrão
    if (in_array($locale, $languages)) {
        App::setLocale($locale);
        return $locale;
    }
    return '/';
}

// For add'active' class for activated route nav-item
function active_class($path, $page, $active = 'active') {
    
    if ( ($page == 'brand-banner' && (str_replace('admin/', '', $path[0]) == 'brand')) OR 
         ($page == 'posvenda-banner' && (str_replace('admin/', '', $path[0]) == 'brand'))  ) {
        return $active;
    }
    return ($page ==  str_replace('admin/', '', $path[0])) ? $active : '';
    //return call_user_func_array('Request::is', (array)$path) ? $active : '';
}

// For checking activated route
function is_active_route($path) {
    return call_user_func_array('Request::is', (array)$path) ? 'true' : 'false';
}

// For add 'show' class for activated route collapse
function show_class($path) {
    return call_user_func_array('Request::is', (array)$path) ? 'show' : '';
}

function dataBr($dataHora = ""){
    $separador	= explode(" ",$dataHora);
    $data	=  explode("-",$separador[0]);
    $ano		=  $data[0];
    $mes		=  $data[1];
    $dia		=  $data[2];
    $retorno	= $dia.'/'.$mes."/".$ano;
    return $retorno;
}

function dataIng($data = ""){
    $data	=  explode("/",$data);
    $ano		=  $data[2];
    $mes		=  $data[1];
    $dia		=  $data[0];
    $retorno	= $ano.'-'.$mes."-".$dia;
    return $retorno;
}

function formatDatePublished($dataHora = ""){
    $separador	= explode(" ",$dataHora);
    $data	=  explode("-",$separador[0]);
    $ano		=  $data[0];
    switch ($data[1]) {
        case '01': $mes = 'janeiro'; break;
        case '02': $mes = 'fevereiro'; break;
        case '03': $mes = 'março'; break;
        case '04': $mes = 'abril'; break;
        case '05': $mes = 'maio'; break;
        case '06': $mes = 'junho'; break;
        case '07': $mes = 'julho'; break;
        case '08': $mes = 'agosto'; break;
        case '09': $mes = 'setembro'; break;
        case '10': $mes = 'outubro'; break;
        case '11': $mes = 'novembro'; break;
        case '12': $mes = 'dezembro'; break;
    }
    
    $dia		=  $data[2];
    $retorno	= $dia.' de '.$mes.", ".$ano;
    return $retorno;
}

function formatDatPress($dateFormatFrom, $dateFormatTo, $date)
{

    $newFormatData = \DateTime::createFromFormat($dateFormatFrom, $date);

    return $newFormatData->format($dateFormatTo);
}

function getLanguage($string = ''){
    switch ($string) {
        case 'pt': $lang = 'pt_br'; break;
        case 'es': $lang = 'es_es'; break;
        case 'en': $lang = 'en_us'; break;
        default:  $lang = 'pt_br'; break;
    }
    return $lang;
}

function getLegendLanguage($string = ''){
    switch ($string) {
        case 'pt_br': $leg = 'Português'; break;
        case 'es_es': $leg = 'Espanhol'; break;
        case 'en_us': $leg = 'Inglês'; break;
    }
    return $leg;
}

function formatDate($dateFormatFrom, $dateFormatTo, $date){
    $newFormatData = \DateTime::createFromFormat($dateFormatFrom, $date);
    return $newFormatData->format($dateFormatTo);
}

function getPosicionTextForFull($posicao = '', $default = null){
    switch ($posicao) {
        case 'sl': $class = '--left-top'; break;
        case 'cl': $class = '--left-center'; break;
        case 'il': $class = '--left-bottom'; break;
        case 'sc': $class = '--center-top'; break;
        case 'cc': $class = '--center-center'; break;
        case 'ic': $class = '--center-bottom'; break;
        case 'sr': $class = '--right-top'; break;
        case 'cr': $class = '--right-center'; break;
        case 'ir': $class = '--right-bottom'; break;
        default: $class = $default; break;
    }




    return $class;
}

function getPosicionTextForSplit($posicao = '', $default = null){
    switch ($posicao) {
        case 't': $class = '--left-top'; break;
        case 'c': $class = '--left-center'; break;
        case 'i': $class = '--left-bottom'; break;
        default: $class = $default; break;
    }
    return $class;
}

function getTransmissao($string){
    switch ($string) {
        case 'MT': $texto = 'Manual'; break;
        case 'AT': $texto = 'Automático'; break;
        default : $texto = $string; break;
    }
    return $texto;
}

function getHostCAOA()
{
    $host = 'caoa.com.br';
    if ($_SERVER['HTTP_HOST'] === 'caoa.test') {
        $host = 'caoa.test';
    }
    return $host;
}