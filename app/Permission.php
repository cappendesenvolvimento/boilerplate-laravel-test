<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Permission extends Model
{
    protected $table = 'permissions';
    protected $fillabel = ['name', 'description', 'module', 'label'];

    
    //Muitos usuarios tem muitos papeis
    public function rules()
    {
      return $this->belongsToMany('App\Rule');
    }
}
