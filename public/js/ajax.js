$(function() {

    //render map
    var mark_cluster;
    var image = SERVER_URL + '/assets/img/icons/pin.png'
    var markImage = SERVER_URL + '/assets/img/icons/circle.png'
    var endereco = SERVER_URL + '/assets/img/icons/endereco.svg'
    var car = SERVER_URL + '/assets/img/icons/car.svg'
    var config = SERVER_URL + '/assets/img/icons/config.svg'
    var calendar = SERVER_URL + '/assets/img/icons/calendar2.svg'
    var iconWhatsapp = SERVER_URL + '/assets/img/icons/whatsapp.svg'
    var markers = []
    var markerCluster = ""
    var directionsService = new google.maps.DirectionsService()
    var directionsRenderer = new google.maps.DirectionsRenderer({
        suppressMarkers: true
    })
    var map = {
        zoom: 5,
        scrollwheel: !1,
        center: new google.maps.LatLng(-11.4770231, -50.5133745),
        panControl: !1
    }
    t = new google.maps.Map(document.getElementById('mapa'), map)
    directionsRenderer.setMap(t)

    var markerIcon = {
        url: image,
        labelOrigin: new google.maps.Point(10, -10)
    }

    function DeleteMarkers(t) {
        //Loop through all the markers and remove
        console.log(markerCluster);
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(t);
            markerCluster.removeMarker(markers[i]);
        }
        markers = [];
    };

    function addPinMap(itens) {

        DeleteMarkers(null);

        itens.forEach(myFunction);

        function myFunction(item) {
            var o = item.latitude,
                a = item.longitude,
                name = item.name,
                title = item.name,
                location = item.city,
                rua = item.address + ', ' + item.number + '. ' + item.city + ', ' + item.state + '. ' + item.zipcode,
                showroom = item.phone,
                posVenda = item.phone_service,
                agendamento = item.email_posvenda,
                revisao = item.phone_revision,
                whatsapp = item.whatsapp_revision,
                url = "http://maps.google.com/?saddr=&daddr=" + item.latitude + "," + item.longitude,
                s = new google.maps.LatLng(o, a),
                marker = new google.maps.Marker({
                    position: s,
                    map: t,
                    icon: markerIcon,
                    label: { color: '#000', fontWeight: 'bold', fontSize: '14px', text: name, textShadow: '3px 3px 0 #000,-1px -1px 0 #000, 1px -1px 0 #000,-1px 1px 0 #000,1px 1px 0 #000' },
                    animation: google.maps.Animation.DROP
                })

            var content =
                '<div id="location__map-title">' +
                '<h3 style="font-size: 16px; font-weight: bold; color: #000000; margin-bottom: 0;">' +
                title +
                '</h3>' +
                '<h5 style="font-size: 12px; font-weight: normal; color: #404040;margin-bottom: 30px;">' +
                location +
                '</h5>' +
                '<div class="location__list--content">' +
                '<h4><img src="' +
                endereco +
                '">' +
                rua +
                '</h4>' +
                '</div>';
            if (showroom) {
                content += '<div class="location__list--content">' +
                    '<h4><img src="' +
                    car +
                    '">' +
                    showroom +
                    '</h4>' +
                    '</div>';
            }
            if (posVenda) {
                content += '<div class="location__list--content">' +
                    '<h4><img src="' +
                    config +
                    '">' +
                    posVenda +
                    '</h4>' +
                    '</div>';
            }
            if (agendamento) {
                content += '<div class="location__list--content">' +
                    '<h4><img src="' +
                    calendar +
                    '">' +
                    agendamento +
                    '</h4>' +
                    '</div>';
            }
            if (revisao) {
                content += '<div class="location__list--content">' +
                    '<h4><img src="' +
                    calendar +
                    '">' +
                    revisao +
                    '</h4>' +
                    '</div>';
            }
            if (whatsapp) {
                content += '<div class="location__list--content">' +
                    '<h4><img src="' +
                    iconWhatsapp +
                    '">' +
                    whatsapp +
                    '</h4>' +
                    '</div>';
            }
            content += '<a style="margin-top: 30px;" class="c-link-line c-black" href="' +
                url +
                '" target="_blank" >VEJA COMO CHEGAR</a>' +
                '</div>'

            // A new Info Window is created and set content
            var infowindow = new google.maps.InfoWindow({
                content: content

                // Assign a maximum value for the width of the infowindow allows
                // greater control over the various content elements
            })

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(t, marker)
            })

            //Event that closes the Info Window with a click on the map
            google.maps.event.addListener(t, 'click', function() {
                infowindow.close()
            })

            markers.push(marker)

        }
        var style = [{
            url: markImage,
            anchor: [0, 0],
            textColor: "white",
            fontWeight: "bold",
            textSize: 14,
            lineHeight: "45px",
            textAlign: "center",
            paddingTop: 10,
            width: 45,
            height: 45
        }];
        markerCluster = new MarkerClusterer(t, markers, {
            maxZoom: 14,
            gridSize: 60,
            styles: style
        });
    }


    $('body').on('click', '.click-open-lc', function(event) {
        event.preventDefault()
        $(this).toggleClass('active')
        var contentLocation = $(this)
            .closest('.location__list-item')
            .find('.location__list--mobile')
        if ($(contentLocation).is(':hidden ')) {
            contentLocation.slideDown(500)
        } else {
            contentLocation.slideUp(500)
        }
    })

    var pos;
    //Aqui estou pegando a posição dele
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                };

            }
        )
    }

    var self = this;
    var bounds;
    var user_marker;
    var current_marker;
    var address_input = $('#address_input');


    bt_search = $('.c-form__btn-search');

    bt_search.click(function(event) {
        event.preventDefault();

        address_input.trigger('geocode');
    })

    address_input.geocomplete().on('geocode:result', function(event, result) {
        self.set_location_by_address(result);
    });

    this.set_location_by_address = function(location) {

        user_position = {
            latitude: location.geometry.location.lat(),
            longitude: location.geometry.location.lng(),
            error: false
        };

        current_focus = new google.maps.LatLng(user_position.latitude, user_position.longitude);

        if (!location.error) {

            already_filtered = true;

            if (current_marker) {
                current_marker.info.close();
            }

            if (user_marker) {
                user_marker.setMap(null);
            }

            filter_dealers();

            var userPos, focusPos;

            userPos = new google.maps.LatLng(user_position.latitude, user_position.longitude);

            if (bounds && bounds['extend']) {
                bounds.extend(user_marker.getPosition());
            }

            if (brand_id != "") {
                focusPos = new google.maps.LatLng(filtered_dealers[0].latitude, filtered_dealers[0].longitude);
                zoom = 15;
            } else
                focusPos = userPos;

            t.setCenter(focusPos);
            t.setZoom(13);


        } else {
            start_zoom = 4;
            self.load_dealers();
        }
    }

    // end render map


    var state = $('#state_filter');
    var city = $('#city_filter');
    var category_filter = $('#category_filter');
    var shop_filter = $('#shop_filter');
    var brand_filter = $('#brand_filter');
    let filtered_dealers;
    var category_id = '';
    var brand_id = '';
    var shop_id;
    var search_results;
    var result_offset = 0;
    var start_position = {
        latitude: 0,
        longitude: 0,
        start: true
    }

    var dealers_loaded = false;
    var loaded_dealers;

    var bt_more;

    var user_position;

    var geolocator = new Geolocator(start_position);

    search_results = $('.location__list-line');

    bt_more = $('.bt-more-dealers');

    brand_id = brand_id == '' ? $('#brand_filter').val() : brand_id;

    category_id = category_id == '' ? $('#category_filter').val() : category_id;

    $('#category_filter option').filter(function() {
        return $(this).val() == category_id;
    }).prop('selected', true);


    $('#brand_filter option').filter(function() {
        return $(this).val() == brand_id;
    }).prop('selected', true);

    $('#brand_filter').on("change", function(event) { // todo: brandFilterChange
        brand_id = event.target.value;
        filter_dealers(false);

        // if (brand_id && !user_position.start)
        //   new google.maps.event.trigger(markers[0], 'click');
        // ga('send', 'event', 'Portal CAOA', self.get_brand_name(), 'CEP (Marca - '+$("#brand_filter option:selected").text()+')');
    });

    $('#category_filter').on("change", function(event) {
        category_id = event.target.value;

        filter_dealers(false);
        // ga('send', 'event', 'Portal CAOA', self.get_brand_name(), 'CEP (Categoria - '+$("#category_filter option:selected").text()+')');
    });

    bt_more.click(function(event) {
        event.preventDefault();

        //ga('send', 'event', 'Portal CAOA', 'Lojas', 'Carregar mais lojas');

        add_dealers_to_results();

    });


    load_dealers();


    function getallStates() {

        var url = SERVER_URL + '/ajax/allstates/';

        $.ajax({
            method: 'GET',
            url: url,
            headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') },
            dataType: 'json',
            success: function(states) {
                for (var option in states) {
                    var option = states[option];

                    state.append('<option value="' + option.id + '">' + option.name + '</option>');
                }
                //console.log(response)

            }
        })
    }

    function getallcitiesbystate() {

        var url = SERVER_URL + '/ajax/allcitiesbystate/MG';

        $.ajax({
            method: 'GET',
            url: url,
            headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') },
            dataType: 'json',
            success: function(response) {

                //console.log(response)

            }
        })
    }

    function getallDealersonlyshops() {

        var url = SERVER_URL + '/ajax/getalldealers?onlyshops=true';

        $.ajax({
            method: 'GET',
            url: url,
            headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') },
            dataType: 'json',
            success: function(responseShops) {
                //console.log(responseShops)
                filtered_dealers = responseShops;

            }
        })
    }

    function getallDealerHours() {

        var shop_id = 44;
        var url = SERVER_URL + '/ajax/getalldealersHours?shop_id=' + shop_id;

        $.ajax({
            method: 'GET',
            url: url,
            headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') },
            dataType: 'json',
            success: function(response) {

                //console.log(response)

            }
        })
    }


    shop_filter.change(function(event) {
        shop_id = $('#shop_filter').val();
        load_dealers();
        // shop_filter
        filter_dealers(true);
        $('#shop_filter option').filter(function() {
            return $(this).val() == shop_id;
        }).prop('selected', true);
    });

    // brand_filter.change(function(event) {
    //     brand_id = $('#brand_filter').val();
    //     dealers_loaded = false;
    //     filter_dealers(true);
    // });

    // category_filter.change(function(event) {
    //     category_id = $('#category_filter').val();
    //     filter_dealers(true);
    // });

    state.change(function(event) {
        state_id = event.currentTarget.value;

        load_cities_by_state(state_id);

        geolocator.getLocationByAddress(state_id + ', Brasil', function(data) {

            var latLng = new google.maps.LatLng(data.latitude, data.longitude);

            user_position = {
                latitude: data.latitude,
                longitude: data.longitude,
                error: false
            }

            t.setCenter(latLng);
            t.setZoom(6);

            filter_dealers(true);
        });
    });


    city.change(function(event) {
        city_id = $(event.target).find('option:selected').text();


        geolocator.getLocationByAddress(city_id + ', Brasil', function(data) {
            var latLng = new google.maps.LatLng(data.latitude, data.longitude);

            user_position = {
                latitude: data.latitude,
                longitude: data.longitude,
                error: false
            }

            t.setCenter(latLng);
            t.setZoom(12);

            filter_dealers(true);
        });
    })

    load_cities_by_state = function(state) {
        var _this = this;
        $.ajax({
            url: SERVER_URL + "/ajax/allcitiesbystate/" + state,
            success: function(data) {
                city.find('option').not('.default-option').remove();

                data.forEach(function(item) {
                    var option = $("<option></option>");
                    option.attr("value", item.id);
                    option.text(item.name);
                    city.append(option);
                });
            }
        });
    }


    //getallBrands();

    //Carrega todos os estados
    getallStates();
    getallDealerHours();
    // getallcitiesbystate();
    //getallDealersonlyshops();

    function add_dealers(dealers, lock_map) {
        console.log(dealers); //LISTAGEM DE TODAS AS CONCESSIONÁRIAS

        dealers = dealers.filter(function(dealer) {
            return dealer.name.toLowerCase().indexOf("caoa") !== -1;
        });

        // lock_map = typeof lock_map !== 'undefined' ? lock_map : false;

        search_results.empty();
        result_offset = 0;
        add_dealers_to_results();
        addPinMap(dealers);


    }

    function add_dealers_to_results() {

        var result_list = search_results;
        var counter = 6;

        bt_more.show();

        if (result_offset + 6 > filtered_dealers.length) {
            counter = filtered_dealers.length - result_offset;
            bt_more.hide();
        }

        if (result_offset < filtered_dealers.length) {
            for (var i = 0; i < counter; i++) {
                var dealer = filtered_dealers[result_offset + i];

                result_list.append(set_content_dealer(dealer, result_offset + i));
            }

            result_offset += 6;
        }
    }

    function load_dealers() {

        if (!dealers_loaded) {
            var mapaPage = $('#mapa').data('page');
            var page = '';
            if (typeof mapaPage !== "undefined")
                page = mapaPage;

            $.ajax({
                url: SERVER_URL + '/ajax/getAllConcessionarias?onlyshops=true&page=' + page, // todo: 0 -> getalldealers
                success: function(data) {
                    dealers_loaded = true;
                    loaded_dealers = data.filter(function(item) {
                        return !(new RegExp('Hyundai REDE', 'i').test(item.name));
                    });

                    populate_dealers(loaded_dealers);

                    loaded_dealers = calculate_dealer_distance(loaded_dealers);
                    filter_dealers();

                }
            });
        } else {
            loaded_dealers = calculate_dealer_distance(loaded_dealers);

            populate_dealers(loaded_dealers);
            filter_dealers();
        }



    }

    function populate_dealers(loaded_dealers) {

        allshops = loaded_dealers;

        shop_filter.empty();
        shop_filter.append('<option value>Selecione uma Loja</option>');

        allshops.forEach(function(item) {
            shop_filter.append('<option value="' + item.id + '">' + item.name + '</option>');
        });

        shop_filter.trigger('render');
    }

    function calculate_dealer_distance(dealers) {

        // Calculate and add distance param for each dealer
        for (var dealer in dealers) {
            dealer = dealers[dealer];

            if (!user_position || user_position.error || user_position.latitude == "0") {
                dealer.distance = 'Distância indefinida.'
            } else {
                dealer.distance = geolocator.getDistance(user_position.latitude, user_position.longitude, dealer.latitude, dealer.longitude) + 'km';
            }
        }

        // Sort dealers by distance
        dealers.sort(function(a, b) {
            var distanceA = a.distance.split('km')[0];
            var distanceB = b.distance.split('km')[0];

            return distanceA - distanceB;
        });

        return dealers;
    }


    function filter_dealers(bool) {
        if (!dealers_loaded) {

            load_dealers();
            return;
        }

        filtered_dealers = loaded_dealers;

        if (typeof brand_id !== "undefined") {

            if (brand_id != "") {
                var hypnobox_id = getHypnoboxId(brand_id);
                filtered_dealers = filtered_dealers.filter(function(item) {
                    return item.brand.id == hypnobox_id;
                });
            }
            var hyundaiCaminhoesBrandId = 6;
            if (parseInt(brand_id) === hyundaiCaminhoesBrandId) {
                filtered_dealers = loaded_dealers.filter(function(item) {
                    return item.sell_trucks;
                });
            }
        }

        if (parseInt(shop_id) > 0) {
            filtered_dealers = filtered_dealers.filter(function(item) {
                if (item.id == shop_id) {
                    return item.id;
                }

            });
        }

        if (parseInt(category_id) > 0) {
            filtered_dealers = filtered_dealers.filter(function(item) {

                // if (item.observacao != null) {
                if (category_id == 1) {

                    return item.category == 1 || item.category == 4;
                } else if (category_id == 3) {

                    return item.category == 3 || item.category == 4;
                }
                // }


                return item.category == category_id;
            });
        }
        //console.log(filtered_dealers);

        filtered_dealers = calculate_dealer_distance(filtered_dealers);

        add_dealers(filtered_dealers);

    }


    function getHypnoboxId(selected_brand) {
        var option = $('#brand_filter').find('option:selected');

        var hypno_id = option.attr('data-hypnobox-id');

        if (hypno_id == '' || hypno_id == undefined) {
            hypno_id = $('#brand_filter').attr('data-hypnobox-id');
        }

        return hypno_id;
    }



    function set_content_dealer(data, marker_id) {

        if (data.name.toLowerCase().indexOf("caoa") !== -1) {
            var name = data.name;
            var distance = data.distance;
            var neighbor = data.neighborhood;
            var city = data.city;
            var phone = '<a href="tel:+55' + data.phone + '">' + data.phone + '</a>';
            var phone_revision = '<a href="tel:+55' + data.phone_revision + '">' + data.phone_revision + '</a>';
            var phone_service = '<a href="tel:+55' + data.phone_service + '">' + data.phone_service + '</a>';

            let email_revision = `<a href="mailto:${data.email_revision}">${data.email_revision}</a>`
            let email_posvenda = `<a href="mailto:${data.email_posvenda}">${data.email_posvenda}</a>`


            var wp_rev_number = data.whatsapp_revision;
            var wp_show_number = data.whatsapp_showroom;

            var whatsapp_revision = '<a href="https://api.whatsapp.com/send?l=pt_BR&phone=55' + wp_rev_number.slice(0, 2) + wp_rev_number.slice(3, 8) + wp_rev_number.slice(9, 13) + '">' + data.whatsapp_revision + '</a>';
            var whatsapp_showroom = '<a href="https://api.whatsapp.com/send?l=pt_BR&phone=55' + wp_show_number.slice(0, 2) + wp_show_number.slice(3, 8) + wp_show_number.slice(9, 13) + '">' + data.whatsapp_showroom + '</a>';

            var url = '';
            if (pos && !pos.error && pos.lat != "0") {
                var url = "http://maps.google.com/?saddr=" + pos.lat + "," + pos.lng + "&daddr=" + data.latitude + "," + data.longitude;
            } else {
                var url = "http://maps.google.com/?saddr=&daddr=" + data.latitude + "," + data.longitude;
            }

            var phone_vd = '';
            var phone_sr = '';
            var segunda_vd = '';
            var segunda_sr = '';
            var terca_vd = '';
            var terca_sr = '';
            var quarta_vd = '';
            var quarta_sr = '';
            var quinta_vd = '';
            var quinta_sr = '';
            var sexta_vd = '';
            var sexta_sr = '';
            var sabado_vd = '';
            var sabado_sr = '';
            var domingo_vd = '';
            var domingo_sr = '';


            if (data.hours) {
                phone_vd = (data.hours.phone_vd) ? data.hours.phone_vd : '';
                phone_sr = (data.hours.phone_sr) ? data.hours.phone_sr : '';
                segunda_vd = (data.hours.segunda_vd) ? data.hours.segunda_vd : '';
                segunda_sr = (data.hours.segunda_sr) ? data.hours.segunda_sr : '';
                terca_vd = (data.hours.terca_vd) ? data.hours.terca_vd : '';
                terca_sr = (data.hours.terca_sr) ? data.hours.terca_sr : '';
                quarta_vd = (data.hours.quarta_vd) ? data.hours.quarta_vd : '';
                quarta_sr = (data.hours.quarta_sr) ? data.hours.quarta_sr : '';
                quinta_vd = (data.hours.quinta_vd) ? data.hours.quinta_vd : '';
                quinta_sr = (data.hours.quinta_sr) ? data.hours.quinta_sr : '';
                sexta_vd = (data.hours.sexta_vd) ? data.hours.sexta_vd : '';
                sexta_sr = (data.hours.sexta_sr) ? data.hours.sexta_sr : '';
                sabado_vd = (data.hours.sabado_vd) ? data.hours.sabado_vd : '';
                sabado_sr = (data.hours.sabado_sr) ? data.hours.sabado_sr : '';
                domingo_vd = (data.hours.domingo_vd) ? data.hours.domingo_vd : '';
                domingo_sr = (data.hours.domingo_sr) ? data.hours.domingo_sr : '';
            }



            var html = '<div class="location__list-item" data-name="' + name + '" data-latitude="' + data.latitude + '" data-longitude="' + data.longitude + '" data-title="' + data.name + '"  ' +
                'data-location="' + data.city + '" data-endereco="' + data.address + ', ' + data.number + '. ' + city + ', ' + data.state + '. ' + data.zipcode + '" ' +
                'data-showroom="';
            if (data.phone) html += data.phone;
            html += '" data-pos-venda="';
            if (data.phone_service) html += data.phone_service;
            html += '" data-agendamento="';
            if (data.email_posvenda) html += data.email_posvenda;
            html += '" data-revisao="';
            if (data.phone_revision) html += data.phone_revision;
            html += '" data-whatsapp="';
            if (data.whatsapp_revision) html += data.whatsapp_revision;
            html += '" data-url="';
            if (url) html += url;
            html += '">' +
                '<div class="location__list-title">' +
                '<div>' +
                '<h3>' + name + '</h3>' +
                '<h5>' + data.city + '</h5>' +
                '</div>' +
                '<a class="click-open-lc" href="#">' +
                '<img class="open" src="' + SERVER_URL + '/assets/img/icons/open.svg" alt="">' +
                '<img src="' + SERVER_URL + '/assets/img/icons/close.svg" alt="">' +
                '</a>' +
                '</div>' +
                '<div class="location__list--mobile">' +
                '<div class="location__list--content">' +
                '    <h4><img src="' + SERVER_URL + '/assets/img/icons/endereco.svg"> Endereço</h4>' +
                '    <p>' + data.address + ', ' + data.number + '. ' + city + ', ' + data.state + '. ' + data.zipcode + '</p>' +
                '</div>';
            if (data.phone) {
                html += '<div class="location__list--content">' +
                    '    <h4><img src="' + SERVER_URL + '/assets/img/icons/car.svg"> Tel. Showroom:</h4>' +
                    '    <p>' + phone + '</p>' +
                    '</div>';
            }
            if (data.phone_service) {
                html += '<div class="location__list--content">' +
                    '    <h4><img src="' + SERVER_URL + '/assets/img/icons/config.svg"> Tel. Pós Venda:</h4>' +
                    '    <p>' + phone_service + '</p>' +
                    '</div>';
            }
            if (data.email_posvenda) {
                html += '<div class="location__list--content">' +
                    '    <h4><img src="' + SERVER_URL + '/assets/img/icons/calendar2.svg"> Agendamento de Serviço:</h4>' +
                    '    <p>' + email_posvenda + '</p>' +
                    '</div>';
            }
            if (data.phone_revision) {
                html += '<div class="location__list--content">' +
                    '    <h4><img src="' + SERVER_URL + '/assets/img/icons/calendar2.svg"> Tel. Revisão:</h4>' +
                    '    <p>' + phone_revision + '</p>' +
                    '</div>';
            }
            if (data.phone_revision) {
                html += '<div class="location__list--content">' +
                    '    <h4><img src="' + SERVER_URL + '/assets/img/icons/whatsapp.svg"> Agend. Revisão Whatsapp</h4>' +
                    '    <p>' + whatsapp_revision + '</p>' +
                    '</div>';
            }
            if (data.hours) {
                html += '<a class="c-link-line c-black" data-fancybox="locationId' + data.id + '" data-src="#locationId' + data.id + '" href="javascript:;" >CONFIRA OS HORÁRIOS</a>' +
                    '</div>' +
                    '</div>' +
                    '<div style="display: none;" id="locationId' + data.id + '">' +
                    '<div class="table-hours">' +
                    '<div class="row">' +
                    '  <div class="col-md-4"></div>' +
                    '  <div class="col-md-4">' +
                    '    <h3 class="table-hours__title">Vendas</h3>' +
                    '    <h4 class="table-hours__subtitle">' + phone_vd + '</h4>' +
                    '  </div>' +
                    '  <div class="col-md-4">' +
                    '    <h3 class="table-hours__title">Serviços</h3>' +
                    '    <h4 class="table-hours__subtitle">' + phone_sr + '</h4>' +
                    '  </div>' +
                    '</div>' +
                    '<div class="row line-days">' +
                    '  <div class="col-md-4">' +
                    '    <p><strong>Segunda</strong></p>' +
                    '  </div>' +
                    '  <div class="col-md-4">' +
                    '    <p>' + segunda_vd + '</p>' +
                    '  </div>' +
                    '  <div class="col-md-4">' +
                    '    <p>' + segunda_sr + '</p>' +
                    '  </div>' +
                    '</div>' +
                    '<div class="row line-days">' +
                    '  <div class="col-md-4">' +
                    '    <p><strong>Terça</strong></p>' +
                    '  </div>' +
                    '  <div class="col-md-4">' +
                    '    <p>' + terca_vd + '</p>' +
                    '  </div>' +
                    '  <div class="col-md-4">' +
                    '    <p>' + terca_sr + '</p>' +
                    '  </div>' +
                    '</div>' +
                    '<div class="row line-days">' +
                    '  <div class="col-md-4">' +
                    '    <p><strong>Quarta</strong></p>' +
                    '  </div>' +
                    '  <div class="col-md-4">' +
                    '    <p>' + quarta_vd + '</p>' +
                    '  </div>' +
                    '  <div class="col-md-4">' +
                    '    <p>' + quarta_sr + '</p>' +
                    '  </div>' +
                    '</div>' +
                    '<div class="row line-days">' +
                    '  <div class="col-md-4">' +
                    '    <p><strong>Quinta</strong></p>' +
                    '  </div>' +
                    '  <div class="col-md-4">' +
                    '    <p>' + quinta_vd + '</p>' +
                    '  </div>' +
                    '  <div class="col-md-4">' +
                    '    <p>' + quinta_sr + '</p>' +
                    '  </div>' +
                    '</div>' +
                    '<div class="row line-days">' +
                    '  <div class="col-md-4">' +
                    '    <p><strong>Sexta</strong></p>' +
                    '  </div>' +
                    '  <div class="col-md-4">' +
                    '    <p>' + sexta_vd + '</p>' +
                    '  </div>' +
                    '  <div class="col-md-4">' +
                    '    <p>' + sexta_sr + '</p>' +
                    '  </div>' +
                    '</div>' +
                    '<div class="row line-days">' +
                    '  <div class="col-md-4">' +
                    '    <p><strong>Sábado</strong></p>' +
                    '  </div>' +
                    '  <div class="col-md-4">' +
                    '    <p>' + sabado_vd + '</p>' +
                    '  </div>' +
                    '  <div class="col-md-4">' +
                    '    <p>' + sabado_sr + '</p>' +
                    '  </div>' +
                    '</div>' +
                    '<div class="row line-days remove-border">' +
                    '  <div class="col-md-4">' +
                    '    <p><strong>Domingo</strong></p>' +
                    '  </div>' +
                    '  <div class="col-md-4">' +
                    '    <p>' + domingo_vd + '</p>' +
                    '  </div>' +
                    '  <div class="col-md-4">' +
                    '    <p>' + domingo_sr + '</p>' +
                    '  </div>' +
                    '</div>' +
                    '<div class="row">' +
                    '  <div class="col-md-4">' +

                    '  </div>' +
                    // '  <div class="col-md-4">' +
                    // '    <a class="c-form__btn" href="#">Entre em contato</a>' +
                    // '  </div>' +
                    // '  <div class="col-md-4">' +
                    // '    <a class="c-form__btn" href="#">Agende o serviço</a>' +
                    // '  </div>' +
                    '</div>' +
                    '</div>' +

                    '<div class="table-hours table-hours-mobile">' +
                    ' <h3 class="table-hours__subtitle">Vendas - ' + phone_vd + '</h3>' +
                    ' <div class="row line-days">' +
                    '   <div class="col-6">' +
                    '     <p><strong>Segunda</strong></p>' +
                    '   </div>' +
                    '   <div class="col-6">' +
                    '     <p>' + segunda_vd + '</p>' +
                    '   </div>' +
                    ' </div>' +
                    ' <div class="row line-days">' +
                    '   <div class="col-6">' +
                    '     <p><strong>Terça</strong></p>' +
                    '   </div>' +
                    '   <div class="col-6">' +
                    '     <p>' + terca_vd + '</p>' +
                    '   </div>' +
                    ' </div>' +
                    ' <div class="row line-days">' +
                    '   <div class="col-6">' +
                    '     <p><strong>Quarta</strong></p>' +
                    '   </div>' +
                    '   <div class="col-6">' +
                    '     <p>' + quarta_vd + '</p>' +
                    '   </div>' +
                    ' </div>' +
                    ' <div class="row line-days">' +
                    '   <div class="col-6">' +
                    '     <p><strong>Quinta</strong></p>' +
                    '   </div>' +
                    '   <div class="col-6">' +
                    '     <p>' + quinta_vd + '</p>' +
                    '   </div>' +
                    ' </div>' +
                    ' <div class="row line-days">' +
                    '   <div class="col-6">' +
                    '     <p><strong>Sexta</strong></p>' +
                    '   </div>' +
                    '   <div class="col-6">' +
                    '     <p>' + sexta_vd + '</p>' +
                    '   </div>' +
                    ' </div>' +
                    ' <div class="row line-days">' +
                    '   <div class="col-6">' +
                    '     <p><strong>Sábado</strong></p>' +
                    '   </div>' +
                    '   <div class="col-6">' +
                    '     <p>' + sabado_vd + '</p>' +
                    '   </div>' +
                    ' </div>' +
                    ' <div class="row line-days">' +
                    '   <div class="col-6">' +
                    '     <p><strong>Domingo</strong></p>' +
                    '   </div>' +
                    '   <div class="col-6">' +
                    '     <p>' + domingo_vd + '</p>' +
                    '   </div>' +
                    ' </div>' +
                    // ' <div class="center">' +
                    // '   <a class="c-form__btn" href="#">Entre em contato</a>' +
                    // ' </div>' +
                    ' <h3 class="table-hours__subtitle">Serviços - ' + phone_sr + '</h3>' +
                    ' <div class="row line-days">' +
                    '   <div class="col-6">' +
                    '     <p><strong>Segunda</strong></p>' +
                    '   </div>' +
                    '   <div class="col-6">' +
                    '     <p>' + segunda_sr + '</p>' +
                    '   </div>' +
                    ' </div>' +
                    ' <div class="row line-days">' +
                    '   <div class="col-6">' +
                    '     <p><strong>Terça</strong></p>' +
                    '   </div>' +
                    '   <div class="col-6">' +
                    '     <p>' + terca_sr + '</p>' +
                    '   </div>' +
                    ' </div>' +
                    ' <div class="row line-days">' +
                    '   <div class="col-6">' +
                    '     <p><strong>Quarta</strong></p>' +
                    '   </div>' +
                    '   <div class="col-6">' +
                    '     <p>' + quarta_sr + '</p>' +
                    '   </div>' +
                    ' </div>' +
                    ' <div class="row line-days">' +
                    '   <div class="col-6">' +
                    '     <p><strong>Quinta</strong></p>' +
                    '   </div>' +
                    '   <div class="col-6">' +
                    '     <p>' + quinta_sr + '</p>' +
                    '   </div>' +
                    ' </div>' +
                    ' <div class="row line-days">' +
                    '   <div class="col-6">' +
                    '     <p><strong>Sexta</strong></p>' +
                    '   </div>' +
                    '   <div class="col-6">' +
                    '     <p>' + sexta_sr + '</p>' +
                    '   </div>' +
                    ' </div>' +
                    ' <div class="row line-days">' +
                    '   <div class="col-6">' +
                    '     <p><strong>Sábado</strong></p>' +
                    '   </div>' +
                    '   <div class="col-6">' +
                    '     <p>' + sabado_sr + '</p>' +
                    '   </div>' +
                    ' </div>' +
                    ' <div class="row line-days">' +
                    '   <div class="col-6">' +
                    '     <p><strong>Domingo</strong></p>' +
                    '   </div>' +
                    '   <div class="col-6">' +
                    '     <p>' + domingo_sr + '</p>' +
                    '   </div>' +
                    ' </div>' +
                    // ' <div class="center">' +
                    // '   <a class="c-form__btn" href="#">Agende o serviço</a>' +
                    // ' </div>' +
                    '</div>' +
                    '</div>';
            }
            return html;


        }
    }



});