$(function() {
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems, options);
    });

    // Or with jQuery

    // $(document).ready(function () {
    //     $('select').formSelect();
    // });

    $('body').on('click','.alteraStatus', function (ev){
        ev.preventDefault();
        var id = $(this).attr('data-id');
        var secao = '/admin/'+$(this).attr('data-secao')+'/change-status';
        var idlink = $(this).attr('id');
        console.log(secao);

        $.ajax({
            type:'POST',
            url:host+secao,
            headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
            data: {id:id},
            success:function(data){
                if (data == 'true') {
                    console.log(data);
                    $('#'+idlink).removeClass('btn-inverse-info');
                    $('#'+idlink).addClass('btn-inverse-success');
                    $('#'+idlink).children('.iconStatus').removeClass('mdi-eye-off');
                    $('#'+idlink).children('.iconStatus').addClass('mdi-eye');
                } else {
                     console.log(data);
                    $('#'+idlink).removeClass('btn-inverse-success');
                    $('#'+idlink).addClass('btn-inverse-info');
                    $('#'+idlink).children('.iconStatus').removeClass('mdi-eye');
                    $('#'+idlink).children('.iconStatus').addClass('mdi-eye-off');
                }

            }
         });

        //$.post(secao, {id:id}, function() {

    });

    $('body').on('click','.btCallModal', function(){
        console.log('clicou');
        var id = $(this).data("item");
        var pagina = $(this).data('pagina');

        var list = $(this).data('list');
        var obj = $(this).data('obj');

        $.ajax({
            type: "GET",
            url: host+'/admin/newsPress/files/' + id,

            dataType: "json",
            success: function( data ) {
                console.log(data);

                $('#modal_id_item').val(id);
                $('#modal_pagina').val(pagina);

                $('#modal_title').val(data.title);
                $(tinymce.get('modal_description').getBody()).html(data.description);

                $('#modal_type').val(data.type);
                $('#modal_link').val(data.link);
                $('#modal_sequence').val(data.sequence);
                $('#modal_media_id').val(data.media_id);
                if (data.media_id) {
                    $('.fl-donwload').removeClass('hide');
                    $('.fl-donwload').attr('href', host+'/'+data.media.directory+data.media.url)
                }

                $('#modal_list').val(list);
                $('#modal_obj').val(obj);

            }
        });

        modal.removeClass('hide')
    });




    $('.close').on('click', function(){
        modal.addClass('hide')
    });

    $('.close-modal').on('click', function(){
        modal.addClass('hide')
    });


    $('.editItem').on('click', function(){
        var item = $(this).data("item");
        var textarea = $('#item_'+item);
        var acoes_edita = $('.acoes_edit_'+item);
        var acoes_salvar = $('.acoes_salvar_'+item);

        textarea.removeAttr("disabled");
        textarea.css("background","#fff");

        acoes_edita.addClass('hide');
        acoes_salvar.removeClass('hide');

    });

    $('.cancelItem').on('click', function(){
        var item = $(this).data("item");
        var textarea = $('#item_'+item);

        var acoes_edita = $('.acoes_edit_'+item);
        var acoes_salvar = $('.acoes_salvar_'+item);

        textarea.attr("disabled","disabled");
        textarea.css("background","none");

        acoes_edita.removeClass('hide');
        acoes_salvar.addClass('hide');

    });

    $( ".formItemFiles" ).on( "submit", function( event ) {
        event.preventDefault();

        var id = $('#modal_id_item').val();
        var data = $( this ).serialize();
        var list = $('#modal_list').val();

        $.ajax({
            type: "PUT",
            url: host+'/admin/newsPress/files/'+id,
            data: data,
            dataType: "json",
            success: function( data ) {
                $('#'+list).html(data.output);

                modal.addClass('hide')
                $( ".sortable tbody" ).sortable({
                    update : function (){
                        var values = [];
                        $("input[name='ids_items[]']").each(function() {
                            values.push($(this).val());
                        });
                        var token =  token;
                        var model = $("input[name='model_items']").val();
                        var parent_id = $("input[name='parent_id_items']").val();
                        $.ajax({
                            type: "PUT",
                            url: host+'/admin/orders',
                            headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
                            data: {ids:values, model:model, parent_id:parent_id},
                            dataType: "json",
                            success: function( data ) {

                            }
                        });
                    }
                });
                $( ".sortable tbody" ).disableSelection();
                $( "#sortable tbody" ).sortable({
                    update : function (){
                        var values = [];
                        $("input[name='ids[]']").each(function() {
                            values.push($(this).val());
                        });
                        var model = $("input[name='model']").val();
                        var parent_id = $("input[name='parent_id']").val();
                        var token =  token;

                        $.ajax({
                            type: "PUT",
                            url: host+'/admin/orders',
                            headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
                            data: {ids:values, model:model, parent_id:parent_id},
                            dataType: "json",
                            success: function( data ) {

                            }
                        });
                    }
                });
                $( "#sortable tbody" ).disableSelection();
            }
        });
        return false;
    });


    var modal = $("#myModal");


    $("body").on("change", ".media", function (e) {
        var file = $(this)[0].files[0];
        var model = $(this).data('model');
        var inputId = $(this).data('inputid');
        var formData = new FormData();
        console.log(inputId);
        formData.append("media", file);
        formData.append("model", model);
        formData.append("_token", $('meta[name="_token"]').attr('content'));

        $.ajax({
            type: "POST",
            dataType: "json",
            url: host+'/admin/medias/upload-file',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                console.log(data);
                if (data.status) {
                    $('#'+inputId).val(data.id)
                    console.log($('#'+inputId));
                    if (data.type == 'video/mp4') {
                        $('.vd-donwload.'+ inputId).find('source').attr('src', host+'/'+data.file);
                        $('.vd-donwload.'+ inputId).removeClass('hide');
                        $('.vd-donwload.'+ inputId)[0].load();
                        $('.fl-donwload.'+ inputId).addClass('hide');
                    } else {
                        $('.fl-donwload.'+ inputId).attr('src', host+'/'+data.file);
                        $('.fl-donwload.'+ inputId).removeClass('hide');
                        $('.vd-donwload.'+ inputId).addClass('hide');
                    }
                } else {
                    $('.erro_'+ inputId).html(data.msg);
                }
            }
        });
        return false;
    });

    $("body").on("change", ".mediapress", function (e) {
        var file = $(this)[0].files[0];
        var model = $(this).data('model');
        var inputId = $(this).data('inputid');
        var formData = new FormData();
        console.log(inputId);
        formData.append("media", file);
        formData.append("model", model);
        formData.append("_token", $('meta[name="_token"]').attr('content'));

        $.ajax({
            type: "POST",
            dataType: "json",
            url: host+'/admin/mediaspress/upload-file',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                console.log(data);
                if (data.status) {
                    $('#'+inputId).val(data.id)
                    console.log($('#'+inputId));
                    if (data.type == 'video/mp4') {
                        $('.vd-donwload.'+ inputId).find('source').attr('src', host+'/'+data.file);
                        $('.vd-donwload.'+ inputId).removeClass('hide');
                        $('.vd-donwload.'+ inputId)[0].load();
                        $('.fl-donwload.'+ inputId).addClass('hide');
                    } else {
                        $('.fl-donwload.'+ inputId).attr('src', host+'/'+data.file);
                        $('.fl-donwload.'+ inputId).removeClass('hide');
                        $('.vd-donwload.'+ inputId).addClass('hide');
                    }
                } else {
                    $('.erro_'+ inputId).html(data.msg);
                }
            }
        });
        return false;
    });

    $("body").on("change", ".imagepress", function (e) {

        var file = $(this)[0].files[0];
        var model = $(this).data('model');
        var inputId = $(this).data('inputid');

        var formData = new FormData();

        console.log(inputId);

        formData.append("media", file);
        formData.append("model", model);
        formData.append("_token", $('meta[name="_token"]').attr('content'));

        $.ajax({
            type: "POST",
            dataType: "json",
            url: host+'/admin/mediaspress/upload',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                console.log(data);
                if (data.status) {
                    $('#'+inputId).val(data.id)
                    if (data.file) {
                        console.log(data.file);
                        $('.fl-donwload.'+ inputId).attr('src', host+'/'+data.file);
                        $('.fl-donwload.'+ inputId).removeClass('hide');
                    }
                } else {
                    $('.erro_'+ inputId).html('Ocorreu um erro ao fazer o upload da imagem.');
                }
            }
        });
        return false;
    });


    $("body").on("change", ".image", function (e) {

        var file = $(this)[0].files[0];
        var model = $(this).data('model');
        var inputId = $(this).data('inputid');

        var formData = new FormData();

        console.log(inputId);

        formData.append("media", file);
        formData.append("model", model);
        formData.append("_token", $('meta[name="_token"]').attr('content'));

        $.ajax({
            type: "POST",
            dataType: "json",
            url: host+'/admin/medias/upload',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                console.log(data);
                if (data.status) {
                    $('#'+inputId).val(data.id)
                    if (data.file) {
                        console.log(data.file);
                        $('.fl-donwload.'+ inputId).attr('src', host+'/'+data.file);
                        $('.fl-donwload.'+ inputId).removeClass('hide');
                    }
                } else {
                    $('.erro_'+ inputId).html('Ocorreu um erro ao fazer o upload da imagem.');
                }
            }
        });
        return false;
    });


    $('.add-btn').click(function (e) {
        e.preventDefault();

        $('.wrapper').append(`
        <div class="input-box">
            <div class="row clearfix">
                <div class="col-sm-4">
                    <div class="form-group">
                        <div class="input-group ">
                            <input type="text" name="in_item_value[]" placeholder="Link"  class="form-control">
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <div class="input-group ">
                            <input type="text" name="in_item_text[]" placeholder="Texto Link"  class="form-control">
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <div class="input-group">
                            <select name="in_item_target[]"  class="form-control" >
                                <option value="">Target</option>
                                <option value="0" >Abrir na mesma Aba</option>
                                <option value="1" >Abrir em nova Aba</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <button class="remove-lnk btn btn-icons btn-rounded btn-outline-primary add-btn"><i class="mdi mdi-delete"></i></button>
                </div>
            </div>
        </div>
        `); // add input field
      });


      $('.wrapper').on("click", ".remove-lnk", function (e) {
        e.preventDefault();
        $(this).parent().parent('div').remove();  // remove input field

      });

      $("body").on("click", ".btViewLead", function (e) {
        e.preventDefault();

        if ( $(this).data('tipomodal') == 'agendamento') {
            var url = host+'/admin'+$(this).data('url');
            var _this = $(this);

            $.get(url, function (data) {

                var result = `<div class="tab-content">
                            <div class="tab-pane active" id="tab_general">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <th>Nome:</th>
                                            <td>${data.name}</td>
                                        </tr>
                                        <tr>
                                            <th>Email:</th>
                                            <td>${data.email}</td>
                                        </tr>
                                        <tr>
                                            <th>CPF/CNPJ:</th>
                                            <td>${data.cpf_cnpj}</td>
                                        </tr>
                                        <tr>
                                            <th>Telefone:</th>
                                            <td>${data.phone}</td>
                                        </tr>
                                        <tr>
                                            <th>CEP:</th>
                                            <td>${data.cep}</td>
                                        </tr>
                                        <tr>
                                            <th>Data de visita:</th>
                                            <td>${data.date}</td>
                                        </tr>
                                        <tr>
                                            <th>Horario:</th>
                                            <td>${data.time}</td>
                                        </tr>
                                        <tr>
                                            <th>Servico:</th>
                                            <td>${data.service}</td>
                                        </tr>
                                        <tr>
                                            <th>Loja:</th>
                                            <td>${data.shop_id}</td>
                                        </tr>
                                        <tr>
                                            <th>Modelo:</th>
                                            <td>${data.model_id}</td>
                                        </tr>
                                        <tr>
                                            <th>Marca:</th>
                                            <td>${data.brand_id}</td>
                                        </tr>
                                        <tr>
                                            <th>Placa Carro:</th>
                                            <td>${data.placa_carro}</td>
                                        </tr>
                                        <tr>
                                            <th>Estou ciente:</th>
                                            <td>${data.estou_ciente}</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>`;

                $("#body").empty();
                $("#body").append(result);

            }, "json")

        } else if( $(this).data('tipomodal') == 'testdrive') {
            var url = host+'/admin'+$(this).data('url');
            var _this = $(this);

            $.get(url, function (data) {

                var result = `<div class="tab-content">
                            <div class="tab-pane active" id="tab_general">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <th>Nome:</th>
                                            <td>${data.name}</td>
                                        </tr>
                                        <tr>
                                            <th>Email:</th>
                                            <td>${data.email}</td>
                                        </tr>
                                        <tr>
                                            <th>CPF/CNPJ:</th>
                                            <td>${data.cpf}</td>
                                        </tr>
                                        <tr>
                                            <th>Telefone:</th>
                                            <td>${data.phone}</td>
                                        </tr>
                                        <tr>
                                            <th>CEP:</th>
                                            <td>${data.cep}</td>
                                        </tr>
                                        <tr>
                                            <th>Data:</th>
                                            <td>${data.date}</td>
                                        </tr>
                                        <tr>
                                            <th>Horario:</th>
                                            <td>${data.time}</td>
                                        </tr>

                                        <tr>
                                            <th>Loja:</th>
                                            <td>${data.LojaID}</td>
                                        </tr>
                                        <tr>
                                            <th>Modelo:</th>
                                            <td>${data.LojaCatalogoID}</td>
                                        </tr>
                                        <tr>
                                            <th>Marca:</th>
                                            <td>${data.BandeiraID}</td>
                                        </tr>
                                        <tr>
                                            <th>Whatsapp:</th>
                                            <td>${data.whatsapp}</td>
                                        </tr>
                                        <tr>
                                            <th>SMS:</th>
                                            <td>${data.sms}</td>
                                        </tr>
                                        <tr>
                                            <th>Contato Email:</th>
                                            <td>${data.contato_email}</td>
                                        </tr>
                                        <tr>
                                            <th>Estou ciente:</th>
                                            <td>${data.sou_cliente}</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>`;

                $("#body").empty();
                $("#body").append(result);

            }, "json");
        } else if( $(this).data('tipomodal') == 'proposta') {
            var url = host+'/admin'+$(this).data('url');
            var _this = $(this);

            $.get(url, function (data) {

                var result = `<div class="tab-content">
                            <div class="tab-pane active" id="tab_general">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <th>Nome:</th>
                                            <td>${data.name}</td>
                                        </tr>
                                        <tr>
                                            <th>Email:</th>
                                            <td>${data.email}</td>
                                        </tr>
                                        <tr>
                                            <th>CPF/CNPJ:</th>
                                            <td>${data.cpf}</td>
                                        </tr>
                                        <tr>
                                            <th>Telefone:</th>
                                            <td>${data.phone}</td>
                                        </tr>
                                        <tr>
                                            <th>CEP:</th>
                                            <td>${data.cep}</td>
                                        </tr>
                                        <tr>
                                            <th>Loja:</th>
                                            <td>${data.LojaID}</td>
                                        </tr>
                                        <tr>
                                            <th>Modelo:</th>
                                            <td>${data.LojaCatalogoID}</td>
                                        </tr>
                                        <tr>
                                            <th>Marca:</th>
                                            <td>${data.BandeiraID}</td>
                                        </tr>
                                        <tr>
                                            <th>Mensagem:</th>
                                            <td>${data.message}</td>
                                        </tr>
                                        <tr>
                                            <th>Whatsapp:</th>
                                            <td>${data.whatsapp}</td>
                                        </tr>
                                        <tr>
                                            <th>SMS:</th>
                                            <td>${data.sms}</td>
                                        </tr>
                                        <tr>
                                            <th>Contato Email:</th>
                                            <td>${data.contato_email}</td>
                                        </tr>
                                        <tr>
                                            <th>Estou ciente:</th>
                                            <td>${data.sou_cliente}</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>`;

                $("#body").empty();
                $("#body").append(result);

            }, "json");
        }

        modal.removeClass('hide');

    });

    $("body").on("click", ".btViewShop", function (e) {
        e.preventDefault();

        var url = host+'/admin'+$(this).data('url');
        var _this = $(this);

        $.get(url, function (data) {

            const contatos = data.Contatos;
            const addrs = JSON.parse(data.Enderecos);
            var dataItems = '';
            var dataItemsAddr = '';
            console.log(addrs);

            $.each(JSON.parse(contatos), function (index, itemData) {
                dataItems += "<b>Nome:</b> " + itemData.Nome + ' - <b>Email:</b> '+ itemData.Email + ' <br /> <br />' +
                             "<b>Email Pos Venda:</b> " +  itemData.EmailPosVenda + ' <br /> <br />' +
                             "<b>Email Revisão:</b> " +  itemData.EmailRevisao + ' <br /> <br />' +
                             "<b>Telefone:</b> " +  itemData.Telefone + " - <b>Celular:</b>" + itemData.Celular + ' <br /> <br />' +
                             "<b>Telefone Pós Venda:</b> " +  itemData.TelefonePosVenda + " - <b>Telefone Revisão:</b> " +  itemData.TelefoneRevisao + ' <br /> <br />' +
                             "<b>WhatsApp:</b> " +  itemData.WhatsApp + " - <b>WhatsApp Revisao:</b> " +  itemData.WhatsAppRevisao + ' <br /> <br />' +
                             "<b>Contato Cobrança:</b> " +  itemData.ContatoCobranca  + ' <br /> <br /><br />';

            });

                // dataItemsAddr += "<b>LojaEnderecoID:</b> " + addrs.LojaEnderecoID + ' - <b>LojaID:</b> '+ addrs.LojaID + ' <br /> <br />' +
                //              "<b>CEP:</b> " +  addrs.CEP + ' <br /> <br />' +
                //              "<b>Logradouro:</b> " +  addrs.Logradouro + ' <br /> <br />' +
                //              "<b>Numero:</b> " +  addrs.Numero + ' - <b>Complemento:</b>'+ addrs.Complemento +'  <br /> <br />' +
                //              "<b>Bairro:</b> " +  addrs.Bairro + " - <b>Estado:</b>" + addrs.Estado   + ' <br /> <br />' +
                //              "<b>Cidade:</b> " +  addrs.Cidade + ' <br /> <br />' +
                //              "<b>Latitude:</b> " +  addrs.Latitude + " - <b>Longitude:</b> " +  addrs.Longitude + ' <br /> <br />' +
                //              "<b>EnderecoCobranca:</b> " +  addrs.EnderecoCobranca + '<br /> <br /><br />';




            var result = `<div class="tab-content">
                        <div class="tab-pane active" id="tab_general">
                            <table class="table table-striped">
                                <tbody>
                                    <tr>
                                        <th>ID Loja:</th>
                                        <td>${data.LojaID}</td>
                                        <th>Nom Fantasia:</th>
                                        <td>${data.NomeFantasia}</td>
                                    </tr>
                                    <tr>
                                        <th>Observação:</th>
                                        <td colspan='4'>${data.Observacao}</td>
                                    </tr>
                                    <tr>
                                        <th>Cnpj:</th>
                                        <td>${data.CNPJ}</td>
                                        <th>Razão Social:</th>
                                        <td>${data.RazaoSocial}</td>
                                    </tr>
                                    <tr>
                                        <th>Bandeira:</th>
                                        <td>${data.BandeiraDescricao}</td>
                                        <th>Loja Endereço ID:</th>
                                        <td>${data.LojaEnderecoID}</td>
                                    </tr>
                                    <tr>
                                        <th>CEP:</th>
                                        <td>${data.CEP}</td>
                                        <th>Bairro:</th>
                                        <td>${data.Bairro}</td>
                                    </tr>
                                    <tr>
                                        <th>Logradouro:</th>
                                        <td colspan="4">${data.Logradouro}</td>
                                    </tr>
                                    <tr>
                                        <th>Número:</th>
                                        <td>${data.Numero}</td>
                                        <th>Complemento:</th>
                                        <td>${data.Complemento}</td>
                                    </tr>
                                    <tr>

                                        <th>Estado:</th>
                                        <td>${data.Estado}</td>
                                        <th>Cidade:</th>
                                        <td>${data.Cidade}</td>
                                    </tr>
                                    <tr>
                                        <th>Latitude:</th>
                                        <td>${data.Latitude}</td>
                                        <th>Longitude:</th>
                                        <td>${data.Longitude}</td>
                                    </tr>

                                    <tr>
                                        <th valign=top>Contatos:</th>
                                        <td colspan="4">${dataItems}</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>`

            $("#body").empty();
            $("#body").append(result);

        }, "json")

        modal.removeClass('hide')

    });


    $('body').on('click','.bt-excluir-img', function(e) {
        e.preventDefault();

        var pagina = $(this).data("pagina");
        var id = $(this).data("id");
        var campo = $(this).data("campo");
        var token = $(this).data("token");

        $('img.'+campo).attr('src', '');
        $('#'+campo).val('');

        /*$.ajax({
            type: "DELETE",
            url: host+'/admin/images-del',
            data: {id:id, pagina:pagina, campo:campo, "_method": 'DELETE', "_token": token},
            dataType: "json",
            success: function( data ) {
                $('img.'+campo).attr('src', '');
                console.log(campo);
                $('#'+campo).val('');
            }
        });*/
    });



    $("body").on("click", ".bt-sync", function (e) {
        e.preventDefault();

        $('.loading-sync').removeClass('hide');
        if (!$('.alert-danger').hasClass('hide')) {
            $('.alert-danger').addClass('hide');
        }
        $('.alert-danger').text('');

        var type = $('select[name=type] option').filter(':selected').val();
        console.log(type)
        $.ajax({
            type: "POST",
            dataType: "json",
            headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
            url: host+'/admin/sync',
            data: {tipo:type},
            cache: false,
            success: function (data) {
                console.log(data);
                $('.loading-sync').addClass('hide');

                if (data.status) {
                    $('.alert-success').removeClass('hide');
                    $('.alert-success').text(data.msg);

                } else {
                    $('.alert-danger').removeClass('hide');
                    $('.alert-danger').text(data.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('.loading-sync').addClass('hide');
                $('.alert-danger').removeClass('hide');
            }
        });
        return false;
    });



    $("body").on("click", ".bt-rollback", function (e) {
        e.preventDefault();

        var id = $(this).data('sync');

        $('.loading-sync').removeClass('hide');
        if (!$('.alert-danger').hasClass('hide')) {
            $('.alert-danger').addClass('hide');
        }
        $('.alert-danger').text('');

        var type = $('select[name=type] option').filter(':selected').val();
        console.log(type)
        $.ajax({
            type: "POST",
            dataType: "json",
            headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
            url: host+'/admin/sync/rollback',
            data: {id:id},
            cache: false,
            success: function (data) {
                console.log(data);
                $('.loading-sync').addClass('hide');

                if (data.status) {
                    $('.alert-success').removeClass('hide');
                    $('.alert-success').text(data.msg);

                } else {
                    $('.alert-danger').removeClass('hide');
                    $('.alert-danger').text(data.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('.loading-sync').addClass('hide');
                $('.alert-danger').removeClass('hide');
            }
        });
        return false;
    });



    // var $navbarNew = $('.js-navbar-new li')
    var $menuDropdownNew = $('.dropdown-menu-new')

    // $('.js-navbar-new li').on('click', function() {
	// 	$menuDropdownNew.addClass('show')
	// })

    $(document).on('mouseup', function(e) {
        if (!$menuDropdownNew.is(e.target) && $menuDropdownNew.has(e.target).length === 0) {
            $menuDropdownNew.removeClass('show')
        }
    })




});



