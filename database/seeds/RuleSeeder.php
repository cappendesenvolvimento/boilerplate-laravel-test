<?php

use Illuminate\Database\Seeder;
use \App\Rule;

class RuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $r1 = Rule::firstOrCreate([
            'name' => 'Admin',
            'description' => 'Acesso total ao sistema'
        ]);

        $r2 = Rule::firstOrCreate([
            'name' => 'Gerente',
            'description' => 'Gerenciamento do sistema'
        ]);

        $r3 = Rule::firstOrCreate([
            'name' => 'Usuario',
            'description' => 'Acesso ao site'
        ]);
    }
}
