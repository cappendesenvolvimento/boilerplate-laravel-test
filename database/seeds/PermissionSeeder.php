 <?php

use Illuminate\Database\Seeder;
use \App\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = Permission::firstOrCreate([
            'name' => 'usuario-view',
            'label' => 'Visualizar',
            'module' => 'Usuário',
            'description' => 'Acesso a lista de usuários'
        ]);
        
        $user2 = Permission::firstOrCreate([
            'name' => 'usuario-create',
            'label' => 'Criar',
            'module' => 'Usuário',
            'description' => 'Adicionar usuários'
        ]);

        $user3 = Permission::firstOrCreate([
            'name' => 'usuario-edit',
            'label' => 'Editar',
            'module' => 'Usuário',
            'description' => 'Editar usuários'
        ]);

        $user4 = Permission::firstOrCreate([
            'name' => 'usuario-delete',
            'label' => 'Deletar',
            'module' => 'Usuário',
            'description' => 'Deletar usuários'
        ]);

        $rule1 = Permission::firstOrCreate([
            'name' => 'rule-view',
            'label' => 'Visualizar',
            'module' => 'Perfil',
            'description' => 'Acesso a lista de perfil'
        ]);
        
        $rule2 = Permission::firstOrCreate([
            'name' => 'rule-create',
            'label' => 'Criar',
            'module' => 'Perfil',
            'description' => 'Adicionar perfil'
        ]);

        $rule3 = Permission::firstOrCreate([
            'name' => 'rule-edit',
            'label' => 'Editar',
            'module' => 'Perfil',
            'description' => 'Editar perfil'
        ]);

        $rule4 = Permission::firstOrCreate([
            'name' => 'rule-delete',
            'label' => 'Deletar',
            'module' => 'Perfil',
            'description' => 'Deletar perfil'
        ]); 

        $author1 = Permission::firstOrCreate([
            'name' => 'author-view',
            'label' => 'Visualizar',
            'module' => 'Autor',
            'description' => 'Acesso a lista de autores'
        ]);
        
        $author2 = Permission::firstOrCreate([
            'name' => 'author-create',
            'label' => 'Criar',
            'module' => 'Autor',
            'description' => 'Adicionar autor'
        ]);

        $author3 = Permission::firstOrCreate([
            'name' => 'author-edit',
            'label' => 'Editar',
            'module' => 'Autor',
            'description' => 'Editar autor'
        ]);

        $author4 = Permission::firstOrCreate([
            'name' => 'author-delete',
            'label' => 'Deletar',
            'module' => 'Autor',
            'description' => 'Deletar autor'
        ]);

        $educativos1 = Permission::firstOrCreate([
            'name' => 'matEducativos-view',
            'label' => 'Visualizar',
            'module' => 'Mat. Educativos',
            'description' => 'Acesso a lista de materiais educativos'
        ]);
        
        $educativos2 = Permission::firstOrCreate([
            'name' => 'matEducativos-create',
            'label' => 'Criar',
            'module' => 'Mat. Educativos',
            'description' => 'Adicionar materiais educativos'
        ]);

        $educativos3 = Permission::firstOrCreate([
            'name' => 'matEducativos-edit',
            'label' => 'Editar',
            'module' => 'Mat. Educativos',
            'description' => 'Editar materiais educativos'
        ]);

        $educativos4 = Permission::firstOrCreate([
            'name' => 'matEducativos-delete',
            'label' => 'Deletar',
            'module' => 'Mat. Educativos',
            'description' => 'Deletar materiais educativos'
        ]);


        $blogCases1 = Permission::firstOrCreate([
            'name' => 'blogCases-view',
            'label' => 'Visualizar',
            'module' => 'Blog/Cases',
            'description' => 'Acesso a lista de Blog/Cases'
        ]);
        
        $blogCases2 = Permission::firstOrCreate([
            'name' => 'blogCases-create',
            'label' => 'Criar',
            'module' => 'Blog/Cases',
            'description' => 'Adicionar Blog/Cases'
        ]);

        $blogCases3 = Permission::firstOrCreate([
            'name' => 'blogCases-edit',
            'label' => 'Editar',
            'module' => 'Blog/Cases',
            'description' => 'Editar Blog/Cases'
        ]);

        $blogCases4 = Permission::firstOrCreate([
            'name' => 'blogCases-delete',
            'label' => 'Deletar',
            'module' => 'Blog/Cases',
            'description' => 'Deletar Blog/Cases'
        ]);    
        

        $depoimentos1 = Permission::firstOrCreate([
            'name' => 'depoimentos-view',
            'label' => 'Visualizar',
            'module' => 'Depoimentos',
            'description' => 'Acesso a lista de depoimentos'
        ]);
        
        $depoimentos2 = Permission::firstOrCreate([
            'name' => 'depoimentos-create',
            'label' => 'Criar',
            'module' => 'Depoimentos',
            'description' => 'Adicionar depoimentos'
        ]);

        $depoimentos3 = Permission::firstOrCreate([
            'name' => 'depoimentos-edit',
            'label' => 'Editar',
            'module' => 'Depoimentos',
            'description' => 'Editar depoimentos'
        ]);

        $depoimentos4 = Permission::firstOrCreate([
            'name' => 'depoimentos-delete',
            'label' => 'Deletar',
            'module' => 'Depoimentos',
            'description' => 'Deletar depoimentos'
        ]);   
        

        $produtos1 = Permission::firstOrCreate([
            'name' => 'produtos-view',
            'label' => 'Visualizar',
            'module' => 'Produtos',
            'description' => 'Acesso a lista de produtos'
        ]);
        
        $produtos2 = Permission::firstOrCreate([
            'name' => 'produtos-create',
            'label' => 'Criar',
            'module' => 'Produtos',
            'description' => 'Adicionar produtos'
        ]);

        $produtos3 = Permission::firstOrCreate([
            'name' => 'produtos-edit',
            'label' => 'Editar',
            'module' => 'Produtos',
            'description' => 'Editar produtos'
        ]);

        $produtos4 = Permission::firstOrCreate([
            'name' => 'produtos-delete',
            'label' => 'Deletar',
            'module' => 'Produtos',
            'description' => 'Deletar produtos'
        ]);               

        $solucoes1 = Permission::firstOrCreate([
            'name' => 'solucoes-view',
            'label' => 'Visualizar',
            'module' => 'Soluções',
            'description' => 'Acesso a lista de solucoes'
        ]);
        
        $solucoes2 = Permission::firstOrCreate([
            'name' => 'solucoes-create',
            'label' => 'Criar',
            'module' => 'Soluções',
            'description' => 'Adicionar solucoes'
        ]);

        $solucoes3 = Permission::firstOrCreate([
            'name' => 'solucoes-edit',
            'label' => 'Editar',
            'module' => 'Soluções',
            'description' => 'Editar solucoes'
        ]);

        $solucoes4 = Permission::firstOrCreate([
            'name' => 'solucoes-delete',
            'label' => 'Deletar',
            'module' => 'Soluções',
            'description' => 'Deletar solucoes'
        ]);               

        $segmentos1 = Permission::firstOrCreate([
            'name' => 'segmentos-view',
            'label' => 'Visualizar',
            'module' => 'Segmentos',
            'description' => 'Acesso a lista de segmentos'
        ]);
        
        $segmentos2 = Permission::firstOrCreate([
            'name' => 'segmentos-create',
            'label' => 'Criar',
            'module' => 'Segmentos',
            'description' => 'Adicionar segmentos'
        ]);

        $segmentos3 = Permission::firstOrCreate([
            'name' => 'segmentos-edit',
            'label' => 'Editar',
            'module' => 'Segmentos',
            'description' => 'Editar segmentos'
        ]);

        $segmentos4 = Permission::firstOrCreate([
            'name' => 'segmentos-delete',
            'label' => 'Deletar',
            'module' => 'Segmentos',
            'description' => 'Deletar segmentos'
        ]);               

        $clientes1 = Permission::firstOrCreate([
            'name' => 'clientes-view',
            'label' => 'Visualizar',
            'module' => 'Clientes',
            'description' => 'Acesso a lista de clientes'
        ]);
        
        $clientes2 = Permission::firstOrCreate([
            'name' => 'clientes-create',
            'label' => 'Criar',
            'module' => 'Clientes',
            'description' => 'Adicionar clientes'
        ]);

        $clientes3 = Permission::firstOrCreate([
            'name' => 'clientes-edit',
            'label' => 'Editar',
            'module' => 'Clientes',
            'description' => 'Editar clientes'
        ]);

        $clientes4 = Permission::firstOrCreate([
            'name' => 'clientes-delete',
            'label' => 'Deletar',
            'module' => 'Clientes',
            'description' => 'Deletar clientes'
        ]);               

        $banner1 = Permission::firstOrCreate([
            'name' => 'banner-view',
            'label' => 'Visualizar',
            'module' => 'Banner',
            'description' => 'Acesso a lista de banner'
        ]);
        
        $banner2 = Permission::firstOrCreate([
            'name' => 'banner-create',
            'label' => 'Criar',
            'module' => 'Banner',
            'description' => 'Adicionar banner'
        ]);

        $banner3 = Permission::firstOrCreate([
            'name' => 'banner-edit',
            'label' => 'Editar',
            'module' => 'Banner',
            'description' => 'Editar banner'
        ]);

        $banner4 = Permission::firstOrCreate([
            'name' => 'banner-delete',
            'label' => 'Deletar',
            'module' => 'Banner',
            'description' => 'Deletar banner'
        ]);               

        $paginas1 = Permission::firstOrCreate([
            'name' => 'paginas-view',
            'label' => 'Visualizar',
            'module' => 'Peginas',
            'description' => 'Acesso a lista de paginas'
        ]);
        
        $paginas2 = Permission::firstOrCreate([
            'name' => 'paginas-create',
            'label' => 'Criar',
            'module' => 'Peginas',
            'description' => 'Adicionar paginas'
        ]);

        $paginas3 = Permission::firstOrCreate([
            'name' => 'paginas-edit',
            'label' => 'Editar',
            'module' => 'Peginas',
            'description' => 'Editar paginas'
        ]);

        $paginas4 = Permission::firstOrCreate([
            'name' => 'paginas-delete',
            'label' => 'Deletar',
            'module' => 'Peginas',
            'description' => 'Deletar paginas'
        ]);    

        $timeline1 = Permission::firstOrCreate([
            'name' => 'timeline-view',
            'label' => 'Visualizar',
            'module' => 'Peginas',
            'description' => 'Acesso a lista de timeline'
        ]);
        
        $timeline2 = Permission::firstOrCreate([
            'name' => 'timeline-create',
            'label' => 'Criar',
            'module' => 'Peginas',
            'description' => 'Adicionar timeline'
        ]);

        $timeline3 = Permission::firstOrCreate([
            'name' => 'timeline-edit',
            'label' => 'Editar',
            'module' => 'Peginas',
            'description' => 'Editar timeline'
        ]);

        $timeline4 = Permission::firstOrCreate([
            'name' => 'timeline-delete',
            'label' => 'Deletar',
            'module' => 'Peginas',
            'description' => 'Deletar timeline'
        ]);               

        $faq1 = Permission::firstOrCreate([
            'name' => 'faq-view',
            'label' => 'Visualizar',
            'module' => 'Peginas',
            'description' => 'Acesso a lista de faq'
        ]);
        
        $faq2 = Permission::firstOrCreate([
            'name' => 'faq-create',
            'label' => 'Criar',
            'module' => 'Peginas',
            'description' => 'Adicionar faq'
        ]);

        $faq3 = Permission::firstOrCreate([
            'name' => 'faq-edit',
            'label' => 'Editar',
            'module' => 'Peginas',
            'description' => 'Editar faq'
        ]);

        $faq4 = Permission::firstOrCreate([
            'name' => 'faq-delete',
            'label' => 'Deletar',
            'module' => 'Peginas',
            'description' => 'Deletar faq'
        ]);               

        $tags1 = Permission::firstOrCreate([
            'name' => 'tag-view',
            'label' => 'Visualizar',
            'module' => 'Tags',
            'description' => 'Acesso a lista de tags'
        ]);
        
        $tags2 = Permission::firstOrCreate([
            'name' => 'tag-create',
            'label' => 'Criar',
            'module' => 'Tags',
            'description' => 'Adicionar tag'
        ]);

        $tags3 = Permission::firstOrCreate([
            'name' => 'tag-edit',
            'label' => 'Editar',
            'module' => 'Tags',
            'description' => 'Editar tag'
        ]);

        $tags4 = Permission::firstOrCreate([
            'name' => 'tag-delete',
            'label' => 'Deletar',
            'module' => 'Tags',
            'description' => 'Deletar tag'
        ]);               

        $category1 = Permission::firstOrCreate([
            'name' => 'categoria-view',
            'label' => 'Visualizar',
            'module' => 'Categorias',
            'description' => 'Acesso a lista de categorias'
        ]);
        
        $category2 = Permission::firstOrCreate([
            'name' => 'categoria-create',
            'label' => 'Criar',
            'module' => 'Categorias',
            'description' => 'Adicionar categoria'
        ]);

        $category3 = Permission::firstOrCreate([
            'name' => 'categoria-edit',
            'label' => 'Editar',
            'module' => 'Categorias',
            'description' => 'Editar categoria'
        ]);

        $category4 = Permission::firstOrCreate([
            'name' => 'categoria-delete',
            'label' => 'Deletar',
            'module' => 'Categorias',
            'description' => 'Deletar categoria'
        ]);               

        
        $permission1 = Permission::firstOrCreate([
            'name' => 'permission-create',
            'label' => 'Criar',
            'module' => 'Peginas',
            'description' => 'Adicionar permission'
        ]);

        
        $permission2 = Permission::firstOrCreate([
            'name' => 'permission-delete',
            'label' => 'Deletar',
            'module' => 'Peginas',
            'description' => 'Deletar permission'
        ]);               
    }
}
