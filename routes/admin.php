<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
*/


use Illuminate\Support\Facades\Auth;


  
// Route::group(['middleware' => ['auth', 'revalidate'], 'prefix' => 'admin'], function (){
Route::middleware(['auth', 'revalidate'])->group( function (){
  
    Route::get('logout', 'Auth\LoginController@logout')->name('admin.logout');

    // Registration Routes...
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register');

    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');  

    Route::get('/dashboard', 'Admin\AdminController@index')->name('home');
    
    Route::resource('usuarios', 'Admin\UsuarioController');
    
    Route::get('usuarios/rule/{id}', ['as' => 'usuarios.rule', 'uses' => 'Admin\UsuarioController@rule']);
    Route::post('usuarios/rule/{rule}', ['as' => 'usuarios.rule.store', 'uses' => 'Admin\UsuarioController@ruleStore']);
    Route::delete('usuarios/rule/{user}/{rule}', ['as' => 'usuarios.rule.destroy', 'uses' => 'Admin\UsuarioController@ruleDestroy']);
    Route::put('usuarios/changepassword/{id}',  ['as' => 'usuario.password.update', 'uses' => 'Admin\UsuarioController@changePassword']);
    Route::get('usuarios/perfil/{id}',  ['as' => 'usuario.perfil.edit', 'uses' => 'Admin\UsuarioController@editPerfil']);

    Route::resource('rules', 'Admin\RuleController');

    Route::get('rules/permission/{id}', ['as' => 'rules.permission', 'uses' => 'Admin\RuleController@permission']);
    Route::post('rules/permission/{permission}', ['as' => 'rules.permission.store', 'uses' => 'Admin\RuleController@permissionStore']);
    Route::delete('rules/permission/{rule}/{permission}', ['as' => 'rules.permission.destroy', 'uses' => 'Admin\RuleController@permissionDestroy']);


    
    Route::resource('pagina', 'Admin\PaginaController');
    
    
    Route::put('orders', ['as' => 'orders', 'uses' => 'Admin\OrderController@order']);


});

Route::get('login', 'Auth\LoginController@showLoginForm')->name('admin.login');
Route::post('login', 'Auth\LoginController@login')->name('admin.login');

Route::get('password/reset', function (){
    return view('auth.reset');
})->name('admin.password-reset');

Route::post('/password', array(
    'uses' => 'Auth\ResetPasswordController@postRemind',
    'as' => 'admin.password-request'
  ));

Route::get('/password/reset/{token}', array(
'uses' => 'Auth\ResetPasswordController@getReset',
'as' => 'admin.new-password-reset'
));

Route::post('/password/reset', array(
    'uses' => 'Auth\ResetPasswordController@postReset',
    'as' => 'admin.password-update'
  ));

Route::get('send-email-press', array(
    'uses' => 'Admin\JobSendEmailPressController@sendEmailPress',
    'as' => 'sendEmailPress'
  ));

