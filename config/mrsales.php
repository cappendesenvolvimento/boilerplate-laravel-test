<?php

return [

	'env' => 'homolog',

	'cache_minutes' => 60 * 24,

    'authorization_token' => 'eEW+2rTc2oYWG9PP6lre+A==',

	'credentials' => [
        'Login' => 'zmais',
        'Senha' => 'zmais1212',
        'CNPJ' => '40.049.609/0001-70',
    ],

    'cache_keys' => [
        'token' => 'MrSales-Token',
        'flags' => 'MrSales-Bandeiras',
        'shops' => 'MrSales-Lojas',
        'catalog' => 'MrSales-Catalogo',
        'catalog_schedules' => 'MrSales-HoráriosCatálogo',
    ],

    'url' => [
        'local' => 'https://api.mrsales.com.br',
        'staging' => 'https://api.mrsales.com.br',
        'production' => 'https://api.mrsales.com.br',
        'homolog' => 'http://api-homolog.mrsales.com.br',
    ],

    'api' => [
        'auth' => '/parceiro/autenticar',
        'flags' => '/parceiro/bandeiras',
        'shops' => '/parceiro/lojas',
        'catalog' => '/parceiro/catalogo/%s',
        'catalog_schedules' => '/parceiro/catalogo-horario',
        'store_lead' => '/parceiro/lead',
        'store_lead_test_drive' => '/parceiro/testdrive',
    ]
];
